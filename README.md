
# nemus #

1. [Overview](#markdown-header-overview)
    1. [Basics](#markdown-header-basics)
2. [Sounds](#markdown-header-sounds)
3. [Channels](#markdown-header-channels)
4. [Envelopes](#markdown-header-envelopes)
5. [Graph editing](#markdown-header-graph-editing)
6. [Export](#markdown-header-export)
    1. [WAV](#markdown-header-wav)
    2. [NSF and assembly](#markdown-header-nsf-and-assembly)
        1. [Reducing size](#markdown-header-reducing-size)
7. [License](#markdown-header-license)


## Overview

nemus is a music editor for NES/Famicom APU (Ricoh 2A03). It is capable of exporting NSF that works on actual hardware or emulators.

nemus outputs audio at 48 kHz. For best quality, make sure the OS audio mixer, audio device and speakers are configured to use 48 kHz.


### Basics

A map of the song is shown at top right. It has markers for start, end, loop and displays content at a beat resolution.

Next to it are various settings for the currently selected item - Song, Sound, ST or CH channel.

On the left side are ID lists of available items. Click to select or create, and edit in the corresponding right-hand view. One can also Mouse Drag to move an item, or Shift+Drag to make a copy. Right-Click deletes items.

General controls:

* Scroll - Space and Mouse Drag or Mouse Scroll
* Undo - Ctrl+Z
* Redo - Shift+Ctrl+Z
* Play view - Space (Ctrl+Space for solo channel)
* Play song - F5 (Ctrl+F5 to solo play song)
* Save - Ctrl+S
* Save as - Shift+Ctrl+S
* Open - Ctrl+O
* Export NSF - Ctrl+E
* Export WAV - Shift+Ctrl+E
* Export assembly - Alt+Shift+Ctrl+E
* Play note - Keyboard, using the current sound(s)
* Also play note - Alt+Click
* Fullscreen - Alt+Enter

**Note: Ctrl key is mirrored to Command key on macOS.**

Drag numbers to adjust them. Some values can be clicked to cycle through different settings. Right-Click to reset a number where applicable.

Save a file named `startup.nms` in the same folder as the executable (or inside the `.app` bundle for macOS) to define a personalized layout.


### Settings

There are some global program settings accessible in the settings menu.

Low power mode reduces CPU usage, which may be useful when running on a laptop battery.

Alt mouse buttons enables using Alt key as the left mouse button or Shift+Alt as the right.

One can also adjust the audio buffer size or the audio driver. Smaller buffer size reduces latency to keyboard/MIDI input, but may cause crackling sound if set too low.

DirectSound is the default audio driver on Windows. Its timing adjustment allows fine-tuning the buffer size. It's also possible to use ASIO on Windows to achieve lower latency than with DirectSound. To use ASIO one needs to install the "asio4all" package (http://www.asio4all.org/).


## Sounds

There are 60 slots available for defining basic sounds. Each sound uses one hardware waveform generator (square1, square2, triangle, noise or DPCM) and has a given behavior when triggered and released.

Click an ID number to select that sound. It will be created if necessary, using sound 00 as a default template. New sounds are currently named automatically with the first available number. Drag to move them, Shift+Drag to copy. Right-Click to remove.

Sounds can be played individually with the keyboard or by channels as explained in the next section.

Each waveform can only provide one output at any given moment, but multiple sounds targeting the same waveform at once will temporarily override each other favoring the most recently triggered, so short sounds can cut through an older note and then return back to the original sound when finished.


## Channels

There are two types of channels, ST (sound triggers) and CH (music channels). Both are internally equivalent but organized for different purposes in the interface.

A channel can simultaneously trigger any number of the 60 available sound slots. Click "select" and then pick the desired sounds from their IDs.

The ST section must be expanded to edit its contents. Click the white plus on blue background next to "ST". There are 18 available ST channels, intended for drums and sound effects. Click an ID to select or create it.

Depending on screen height, there are up to 99 CH channels. Similar management applies, but these are shown in the bottom area with individual note frequency for each trigger.

CH channels can be visually selected by Ctrl+Clicking on any notes inside. This also applies without holding Ctrl when no CH channel is currently selected.


## Envelopes

Each sound has two envelopes, for volume and pitch. These control the respective parameters while the sound plays, and reset each time the sound is triggered.

Channels also have volume envelopes that span the entire song duration, and can be edited to control associated sound volume. By default these are empty and not shown. Click the plus button to the left between ST and CH areas to expand the envelope editor.


## Graph editing

All envelopes and channels are represented using the same kind of "graph" information. A graph is a list of points along the time axis. There are two types of point - a trigger and a curve point. Triggers set the value instantly, and curves connect multiple values to make a gradual sweep.

There can only be one single point of each type at the same location in time, so creating a new one above or below will overwrite the previous.

By default, channel areas create triggers when clicked and curve points with Shift+Click. Envelope editors are identical but have this convention reversed.

Action summary:

* Create point - (Shift+)Click and release left button
* Move point - Click and drag an existing point
* Erase points - Click and drag with right button
* Make selection - Click and drag from empty space, envelope selections are one-dimensional and channel selection is a 2D box
* When selected:
    * Move selected points - Drag from open area inside selection
    * Move all points on a row - Drag one of them (transpose)
    * Erase all selected - Press Delete
    * Copy, Cut - Ctrl+C, Ctrl+X
    * Paste - Ctrl+V
    * ...once - Click
    * ...repeat - Click and drag
* Repeat latest action - Ctrl+R (applies to creating a regular sequence of points)
* Toggle timeline zoom - F2

In the CH editor, one can also edit scale notes throughout the song. Each beat can have its own scale, and they're extended to the right. Scales are a visual guide, and also control automatic arpeggios if enabled in the channel settings. Create a note on any scale position to cycle through it by the set number of steps.

* Draw/erase scale key - Alt+right mouse (Click or drag)

Also in this editor, notes by default are of a specified length, but can be set manually by adding a curve point (Shift+Click).
ST channels also have a default note length, but no curve points are allowed, and it's not currently indicated visually.

The song area allows similar graph editing but on a larger scale. One can work on all channels (in song mode) or just the currently selected one.


## Export

Supported export formats are: WAV, NSF and assembly (ASM).

**Important: export is a one-way process. Remember to save the original song!**


### WAV

Exports a .wav file in 16-bit mono format. Contains a single loop over the song (no loop markers).


### NSF and assembly

Both NSF and assembly exports target playback on actual NES/Famicom hardware (Ricoh 2A03) and emulators.

**Important: make sure native mode is enabled in song options when creating a song for NSF or assembly export. Native mode changes nemus playback to match exported result and respects hardware limitations. It will currently disable unconstrained DPCM note pitch, DPCM volume and arpeggio.**


#### Reducing size

This section is for game developers and musicians who use nmn driver and wish to reduce the size of the exported song data.

The NSF and assembly export features are designed to produce small song data automatically. The export removes redundant and unused data and uses simple forms of RLE and dictionary compression. However, if it is desirable, it is generally possible to reduce the size even further by manual optimization (hand tuning) for the export.

As every song is different, there are no exact steps on how and what to optimize. However, here are some "rules of thumb" that may result in smaller size:

* Avoid using channel volume envelope:
    * Use only if channel notes require different volume but are otherwise the same.
    * Also use for a long fade that can't be achieved with a sound volume envelope.
    * In general, try to use sound volume envelope for volume effects.
    * If possible, use "stair-stepped" flat sections of volume instead of ramps.
* Use as few channels and sounds as possible.
* Tweak sound envelope graphs:
    * Prefer single-value envelopes, i.e. completely flat envelopes.
    * Prefer using exact same envelope in multiple sounds.
* Reduce number of notes:
    * May be difficult, but ex. section with rapid note sections may be replaced by sound envelopes.
* Use DPCM samples sparingly.


## License

Source code of nemus and its playback libraries are released under MIT license. See accompanied `license.txt`.

```
nemus - NES music editor and playback libraries
-----------------------------------------------

Copyright (c) 2015 Tomas Pettersson, 2016 Valtteri Heikkila

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```

#include "bitstream.h"
#include "nmutil.h"
#include "nmsong.h"
#include "nmaudio.h"

static void ReadFile(const char *filename, void** data, size_t* size)
{
	FILE* f=fopen(filename, "rb");
	if(!f)
	{
		*data=nullptr;
		*size=0;
		return;
	}
	fseek(f, 0, SEEK_END);
	*size=ftell(f);
	rewind(f);
	*data=new char[*size];
	fread(*data, *size, 1, f);
	fclose(f);
}

static void Usage()
{
	puts(
		"nemusc - nemus song converter\n"
		"usage: nemusc wav|nsf|asm <in_nms> <out_file>"
	);
}

static int ParseFormat(const char* s)
{
	if(!strncmp("wav", s, 4)) return 1;
	if(!strncmp("nsf", s, 4)) return 2;
	if(!strncmp("asm", s, 4)) return 3;
	return 0;
}

int main(int argc, const char** argv)
{
	if(argc!=4)
	{
		Usage();
		return 1;
	}

	int format=ParseFormat(argv[1]);
	if(!format)
	{
		printf("error: unrecognized format: %s\n", argv[1]);
		Usage();
		return 2;
	}

	NMSong song={};
	NMASoundStack stack;
	NMASongPlayer player(stack);
	void* in_data;
	size_t in_size;

	ReadFile(argv[2], &in_data, &in_size);
	song.Read((unsigned char*)in_data, in_size);

	switch(format)
	{
		case 1: player.ExportWAV(&song, argv[3]); break;
		case 2: player.ExportNSF(&song, argv[3]); break;
		case 3: player.ExportASM(&song, argv[3]); break;
	}

	return 0;
}

#define glrAppTimeMs() 0
#include "bitstream.h"
#include "blitter.h"
#include "graphics.h"
#include <vector>

static int _mode;
static int _width;
static int _height;
static const char* _out_file;
static std::vector<unsigned int> _in_buf;

static void Src(const char* prefix)
{
	if(!prefix)
		prefix="data";

	unsigned char* out_buf=0;
	int out_size=0;
	GPK_Encode(_in_buf.data(), _width, _height, &out_buf, &out_size);
	if(!out_size)
	{
		puts("error: encoding failed");
		return;
	}

	FILE* f=fopen(_out_file, "w");
	fprintf(f,
		"const int %s_size=%d;\n"
		"const unsigned char %s_gpk[]={",
		prefix, out_size, prefix
	);
	for(int i=0;i<out_size;i++)
	{
		if(!(i&15))
			fprintf(f, "\n");
		fprintf(f, "0x%02X,", out_buf[i]);
	}
	fprintf(f, "\n};\n");
	fclose(f);
}

static void Dec()
{
	//GPK_Decode();
}

static void Usage()
{
	puts(
		"gpkimtool - encode/decode raw rgb images\n"
		"usage: gpkimtool src|dec <width> <height> <in_file> <out_file> [prefix]"
	);
}

static int ParseMode(const char* s)
{
	if(!strncmp("src", s, 4)) return 1;
	if(!strncmp("dec", s, 4)) return 2;
	return 0;
}

int main(int argc, const char** argv)
{
	if(argc<6 || argc>7)
	{
		Usage();
		return 1;
	}

	_mode=ParseMode(argv[1]);
	if(!_mode)
	{
		printf("error: unrecognized mode:%s\n", argv[1]);
		Usage();
		return 2;
	}

	_width=atoi(argv[2]);
	_height=atoi(argv[3]);
	if(!_width || !_height)
	{
		printf("error: option width:%s or height:%s is invalid\n", argv[2], argv[3]);
		Usage();
		return 3;
	}

	FILE *f=fopen(argv[4], "rb");
	if(!f)
	{
		printf("error: can't open in_file:%s\n", argv[4]);
		Usage();
		return 4;
	}
	fseek(f, 0, SEEK_END);
	auto size=ftell(f);
	if(size%3)
		printf("warning: in_file:%s size is not multiple of 3 bytes (rgb raw)\n", argv[4]);
	std::vector<uint8_t> buf(size);
	if((int)size/3!=_width*_height)
	{
		printf("error: in_file:%s size doesn't match width:%d and height:%d\n", argv[4], _width, _height);
		return 5;
	}
	rewind(f);
	fread(buf.data(), 1, size, f);
	_in_buf.resize(size/3);
	for(int i=0;i<size;i+=3)
		_in_buf[i/3]=buf[i+2]+(buf[i+1]<<8)+(buf[i]<<16);
	fclose(f);

	_out_file=argv[5];

	switch(_mode)
	{
		case 1: Src(argc>6?argv[6]:nullptr); break;
		case 2: Dec(); break;
	}

	return 0;
}

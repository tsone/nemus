# Copyright (c) 2017 Valtteri Heikkila
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# Allowed instruction opcodes for address replacement (everynes.htm)
OPCODES = set([
  'AD', # LDA nnnn    MOV A,[nnnn]        ;A=[nnnn]
  'BD', # LDA nnnn,X  MOV A,[nnnn+X]      ;A=[nnnn+X]
  'B9', # LDA nnnn,Y  MOV A,[nnnn+Y]      ;A=[nnnn+Y]
  'AE', # LDX nnnn    MOV X,[nnnn]        ;X=[nnnn]
  'BE', # LDX nnnn,Y  MOV X,[nnnn+Y]      ;X=[nnnn+Y]
  'AC', # LDY nnnn    MOV Y,[nnnn]        ;Y=[nnnn]
  'BC', # LDY nnnn,X  MOV Y,[nnnn+X]      ;Y=[nnnn+X]
  '6D', # ADC nnnn    ADC A,[nnnn]        ;A=A+C+[nnnn]
  '7D', # ADC nnnn,X  ADC A,[nnnn+X]      ;A=A+C+[nnnn+X]
  '79', # ADC nnnn,Y  ADC A,[nnnn+Y]      ;A=A+C+[nnnn+Y]
  'ED', # SBC nnnn    SBC A,[nnnn]        ;A=A+C-1-[nnnn]
  'FD', # SBC nnnn,X  SBC A,[nnnn+X]      ;A=A+C-1-[nnnn+X]
  'F9', # SBC nnnn,Y  SBC A,[nnnn+Y]      ;A=A+C-1-[nnnn+Y]
  '2D', # AND nnnn    AND A,[nnnn]        ;A=A AND [nnnn]
  '3D', # AND nnnn,X  AND A,[nnnn+X]      ;A=A AND [nnnn+X]
  '39', # AND nnnn,Y  AND A,[nnnn+Y]      ;A=A AND [nnnn+Y]
  '4D', # EOR nnnn    XOR A,[nnnn]        ;A=A XOR [nnnn]
  '5D', # EOR nnnn,X  XOR A,[nnnn+X]      ;A=A XOR [nnnn+X]
  '59', # EOR nnnn,Y  XOR A,[nnnn+Y]      ;A=A XOR [nnnn+Y]
  '0D', # ORA nnnn    OR  A,[nnnn]        ;A=A OR [nnnn]
  '1D', # ORA nnnn,X  OR  A,[nnnn+X]      ;A=A OR [nnnn+X]
  '19', # ORA nnnn,Y  OR  A,[nnnn+Y]      ;A=A OR [nnnn+Y]
  'CD', # CMP nnnn    CMP A,[nnnn]        ;A-[nnnn]
  'DD', # CMP nnnn,X  CMP A,[nnnn+X]      ;A-[nnnn+X]
  'D9', # CMP nnnn,Y  CMP A,[nnnn+Y]      ;A-[nnnn+Y]
  'EC', # CPX nnnn    CMP X,[nnnn]        ;X-[nnnn]
  'CC', # CPY nnnn    CMP Y,[nnnn]        ;Y-[nnnn]
  '2C'  # BIT nnnn    TEST A,[nnnn]       ;test and set flags
])

first_tag = 'c_song_inst_mod_vol'
labels = ['nmn_play', 'nmn_update']

def read_nl(fn):
  with open(fn) as f:
    for line in f:
      s = line.split('#')
      yield s[1], int(s[0][1:], 16)

drv_addr = dict(read_nl('nmn.bin.0.nl'))
drv_base = 0x8000
drv_size = drv_addr['c_song_dict'] - drv_base

tag_addr = dict(read_nl('nmn.bin.1.nl'))
tag_list = sorted(tag_addr.iteritems(), lambda x,y: x[1]-y[1])

def read_lst(fn, tag_addr):
  result = [ [] for _ in range(len(tag_list)) ]
  with open(fn) as f:
    for line in f:
      try:
        s = line.split(';')[1].split()

        # Record address replacements, criteria:
        # 1. Instruction length is 3 bytes
        if len([ x for x in s[1:-2] if len(x) == 2 ]) != 3:
          continue
        # 2. Instruction opcode is one in OPCODES
        if s[1] not in OPCODES:
          continue
        # 3. Instruction and tag address match
        inst_addr = int(s[3] + s[2], base=16)
        for tag_idx, tag in enumerate(tag_list):
          if tag[0] in line and inst_addr == tag[1]:
            result[tag_idx].append(int(s[0], base=16)-drv_base+1)
            break
      except (IndexError, ValueError):
        pass
  return result

repl_offs = read_lst('nmn.lst', tag_addr)

missing = [ tag_list[tag_idx][0] for tag_idx, offs in enumerate(repl_offs) if not offs ]
if missing:
  print('WARNING: Following song data labels not referenced in code: ' + ','.join(missing))

drv = []
with open('nmn.bin', 'rb') as f:
  drv = f.read(drv_size)

with open('nmn.h', 'w') as f:

  f.write('//\n// This is a generated file. Do not edit by hand!\n//\n')

  f.write('#define NMN_DRIVER_BASE 0x%04X\n' % (drv_base))
  f.write('#define NMN_DRIVER_SIZE %d\n' % (drv_size))

  for label in labels:
    f.write('#define NMN_DRIVER_%s 0x%04X\n' % (label[4:].upper(), drv_addr[label]))

  tags_list = sorted(tag_addr.iteritems(), lambda x,y: x[1]-y[1])
  f.write('enum NMNTag\n{\n')
  for k, v in tag_list:
    f.write('\tNMN_DATA_%s,\n' % (k[7:].upper()))
  f.write('\tNMN_DATA_COUNT\n};\n')

  f.write('\nstatic const unsigned char nmn_driver[NMN_DRIVER_SIZE]=\n{\n')
  for row in [ drv[i:i+16] for i in range(0, len(drv), 16) ]:
    f.write('\t' + ''.join(map(lambda c: '%3d,' % (ord(c)), row)) + '\n')
  f.write('};\n')

  offs_len = max(len(x) for x in repl_offs)+1
  f.write('\nstatic const unsigned short nmn_addr_repl_offs[NMN_DATA_COUNT][%d]=\n{\n' % (offs_len))
  for offs in repl_offs:
    f.write('\t{' + ''.join(map(lambda c: '%d,' % (c), offs)) + '0},\n')
  f.write('};\n')


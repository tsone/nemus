;--
; Copyright (c) 2017 Valtteri Heikkila
;
; Permission is hereby granted, free of charge, to any person
; obtaining a copy of this software and associated documentation
; files (the "Software"), to deal in the Software without
; restriction, including without limitation the rights to use,
; copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following
; conditions:
;
; The above copyright notice and this permission notice shall be
; included in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
; OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
; OTHER DEALINGS IN THE SOFTWARE.
;--

; Include nk audio module (we don't need full nk)
.INCLUDE    "neskit/nk/modules/nk_audio.s"

.SEGMENT    "ZEROPAGE"
    _NK_AUDIO_ZEROPAGE                  ; Insert nk audio zeropage

.SEGMENT    "BSS"
    _NK_AUDIO_BSS                       ; Insert nk audio bss

.SEGMENT    "CODE"

.BANK       0

.BYTE       $6E, $6D, $6E, $05          ; Version signature

    _NK_AUDIO_CODE                      ; Insert nk audio code

nmn_play:                               ; Implements NSF INIT routine
    PHA
    JSR   nk_audio_init
    PLA
    TAX                                 ; Select song part in A
    LDY   #NK_AUDIO_PLAYER_SFX          ; Select SFX player
    JMP   nk_audio_play                 ; Start playback

nmn_update:                             ; Implements NSF PLAY routine
    JMP   _nk_audio_update

; Preserve frame dictionary (fixed size)
; NOTE: Must be first label after nmn_update!
c_song_dict:             .RES NK_AUDIO_DICT_SIZE

; Temp address "tags" to song data. These are replaced with the real
; addresses by looking the instruction in driver binary.
; Must be unique enough to avoid clashes w/ registers and opcodes.
; NOTE: Do not rename!

.BANK       1
.ORG        $FF00

c_song_inst_mod_vol:     .BYTE 0 ; NOTE: Must be first!
c_song_inst_vib:         .BYTE 0
c_song_inst_vib_dly:     .BYTE 0
c_song_inst_det:         .BYTE 0
c_song_inst_arp_rate:    .BYTE 0
c_song_inst_mod_pos:     .BYTE 0
c_song_inst_penv_scl:    .BYTE 0
c_song_inst_venv_spd0:   .BYTE 0
c_song_inst_venv_spd1:   .BYTE 0
c_song_inst_venv_spd2:   .BYTE 0
c_song_inst_penv_spd0:   .BYTE 0
c_song_inst_penv_spd1:   .BYTE 0
c_song_inst_penv_spd2:   .BYTE 0
c_song_inst_venv_ptr_lo: .BYTE 0
c_song_inst_venv_ptr_hi: .BYTE 0
c_song_inst_penv_ptr_lo: .BYTE 0
c_song_inst_penv_ptr_hi: .BYTE 0
c_song_arp_data:         .BYTE 0
c_song_part_ptr_lo:      .BYTE 0
c_song_part_ptr_hi:      .BYTE 0
c_song_dmc_addr:         .BYTE 0
c_song_dmc_len:          .BYTE 0



# nmn - nemus native

nemus native (nmn) is the native audio driver for nemus.


## Fundamentals

nmn plays songs exported from nemus music editor. It's possible to export to NSF or
to assembly. The assembly export is intended for expert users -- for games or homebrew.

Playback update is called at NMI at the NTSC NES/Famicom (2A03) rate of ~60 Hz.


## Songs

Songs in nmn are composed of multiple data: _parts_, _channel streams_ ,
_volume streams_, _dictionary_, _instruments_, and _samples_.


### Parts

Song can have one or multiple parts. The concept of part is same as NSF song or track.
Each part contains 5 channel and volume streams -- one for each APU channel.


### Channel streams

Channel streams are composed of _frames_ containing _commands_ to control
APU channel playback. The decoding of frames is timed in frames (~60 Hz, NMI),
and a frame can contain multiple commands which all trigger when the frame is timed.

A frame has a header byte containing command bits mapped as follows:

    DOESTINL

If a bit is set, corresponding command is included in the frame.

The header is followed by zero or more data bytes. The total number of data bytes
depends on the number of commands in the frame. Command data is in same order
as the same as the command bit order; from L to D (LSB to MSB).

A brief description of command and data follows.


#### L - Dictionary command

Lookup a frame from dictionary (simple decompression).

Data: None, but the remaining 7 MSB of the header are remapped as follows:

    DOESTIN -> IIIIIII

    I - Index to dictionary for lookup.

The index is used to calculate a pointer in the dictionary, and this pointer is then used
in place of the current stream pointer for the current frame.


#### N - Note or loop command

Dual command: note or loop. The note command sets current note (pitch), and
the loop command changes current stream position to an earlier one (loops back).

Data: 1 byte.

If bit 7 of the data byte is 0, this is a note command. 7 LSB are used to set current note.

Otherwise, if bit 7 is 1, this is a loop command. An offset is formed from the
7 LSB of the data byte and the remaining 6 MSB of the header byte.

    Data byte 7 LSB: lllllll
    Header byte 6 MSB: DOESTI -> hhhhhl

    l - Offset low byte
    h - Offset high 5 bits

The offset is then subtracted from the current stream pointer position, and the
frame at the new stream position is executed immediately.


#### I - Instrument command

Set current instrument.

Data: 1 byte, containing set instrument index.


#### T - Trigger command

Trigger set note on set current instrument.

Data: None.


#### S - Slope command

Set slope (pitch slide) for the note that is played at the frame.

Data: 2 bytes.

The bytes are loaded to nm_v_chn_slope_lo/hi.


#### E - Length command

Set length of played note at the frame.

Data: 1 or 2 bytes.

The data bytes set instrument sustain length override (in frames).
If bit 0 of first data byte is 1, data size is 2 bytes. Otherwise it is 1 byte.


#### O - Offset command

Offset note played at the frame.

Data: 3 bytes.

The data bytes override instrument envelope and vibrato positions of current
decoded frame. Description of the data bytes:

    Byte 0: Volume envelope position as bits llllhhhh. These are
    transformed to a 16-bit value 0000hhhh llll0000.

    Byte 1: Pitch envelope position. Same encoding as above.

    Byte 2: Release flag and vibrato position as bits rvvvvvvv as follows:
    r - Release bit: if 1, note is released.
    v - Vibrato position/delay: multiplied x2 and set in nm_v_chn_vib_pos_hi.


#### D - Delay command

Set current delay. This is the delay until next stream frame decode
(in frames, 1-256).

Data: 1 byte.


### Volume streams

Channel streams contain channel volume envelopes encoded in a variable-length
RLE command stream.

There are four command variants in the stream:

    | Var. | Byte 1   | Byte 2   | Byte 3   | Type |
    | ---- | -------- | -------- | -------- | ---- |
    | #1   | LLLLVVVV |          |          | Run  |
    | #2   | 1110VVVV | LLLLLLLL |          | Run  |
    | #3   | 1111VVVV | 0HHHHHHH | LLLLLLLL | Run  |
    | #4   | 1111VVVV | 1DDDDDDD | CCCCCCCC | Loop |

Explanation of bits:

    V: Volume (#1-4: 0..15)
    L/H: Delay lo/hi (#1: 1..14, #2: 1..256, #3 1..32768)
    C/D: Loop lo/hi (ADC w/ ptr hi for reverse subtract)

Variants #1-3 are run commands (RLE). They keep V volume for H*256+L frames.

Variant #4 is a loop command. It changes the stream pointer by 16-bit ADC'ing
the byte 2 and 3 from current stream pointer.


### Dictionary

A 256-byte dictionary containing channel stream frames. The dictionary is constructed
from all frames of the song (all parts). The frames are stored as-is but aligned to 2 bytes.


### Instruments

TBD


### Samples

Samples are 2A03 DPCM samples as raw data.


## Usage

TBD


#!/bin/bash
set -Eeuo pipefail
SCRIPTDIR=`dirname "$0"`
TEMPDIR=$SCRIPTDIR/temp
SRCDIR=$SCRIPTDIR/../src

NEMUSCDIR="$SCRIPTDIR/../tools/nemusc"
NEMUSC="$NEMUSCDIR/nemusc"

NSFPLAYDIR="$SCRIPTDIR/nsfplay"
NSF2WAVDIR="$NSFPLAYDIR/contrib"
NSF2WAV="$NSF2WAVDIR/nsf2wav"

COMPARE="python3 $SCRIPTDIR/compare.py specgram"

if [ ! -d $NSFPLAYDIR ]; then
  echo "Cloning nsfplay"
  git clone --quiet https://github.com/bbbradsmith/nsfplay.git $NSFPLAYDIR
  # Checkout at commit that'll work.
  git -C $NSFPLAYDIR checkout --quiet 01aef87341d30046c0b1a8370d6d375bb85be82c
fi
echo "Building nsf2wav"
make --quiet -C $NSF2WAVDIR nsf2wav

echo "Building nemusc"
make --quiet -C $NEMUSCDIR

echo "Creating temp dir $TEMPDIR"
mkdir -p $TEMPDIR

function compare () {
  local FN=$1
  local BASEFN=${FN##*/}
  local FLACWAVFN=$SCRIPTDIR/flac_wav/${BASEFN%.nms}.flac
  local FLACFN=$SCRIPTDIR/flac_nsf/${BASEFN%.nms}.flac
  local WAVFN=$TEMPDIR/${BASEFN%.nms}_nsf.wav
  local REFFN=$TEMPDIR/${BASEFN%.nms}_nsf_ref.wav
  local DIFFFN=$TEMPDIR/${BASEFN%.nms}_nsf_diff.wav
  local NSFFN=$TEMPDIR/${BASEFN%.nms}.nsf
  local MS=`afinfo $FLACWAVFN | awk '/estimated duration/ { printf "%.0f\n", $3 * 1000 + 0.5 }'`
  # NOTE: tsone: following can create the .flac reference files
  # $NEMUSC nsf $FN $NSFFN && $NSF2WAV --fade_ms=0 --length_ms=$MS $NSFFN $WAVFN >/dev/null 2>&1 && flac -f -s --best $WAVFN -o $FLACFN
  $NEMUSC nsf $FN $NSFFN && $NSF2WAV --fade_ms=0 --length_ms=$MS $NSFFN $WAVFN >/dev/null 2>&1 && flac -d -f -s $FLACFN -o $REFFN && $COMPARE $REFFN $WAVFN $DIFFFN && echo -n "OK" || echo -n "FAIL" ; echo " $BASEFN"
}

echo "Running NSF export tests"
for FN in $SCRIPTDIR/nms/*.nms ; do
  { compare $FN ; } &
done
wait

echo "Done"

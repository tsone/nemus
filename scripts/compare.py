import sys
import scipy.stats as stats
import numpy as np
from scipy.io import wavfile

from scipy import signal
import matplotlib.pyplot as plt

class Error(BaseException): pass

def write_diff(x1, x2, fs, diff_fn):
	if diff_fn:
		x3 = x1 - x2
		wavfile.write(diff_fn, fs, x3)

def test_pearson(x1, x2, fs, diff_fn):
	write_diff(x1, x2, fs, diff_fn)
	r, p = stats.pearsonr(x1, x2)
	# x1,x2 are extremely correlated -> test extreme significance using an ad-hoc threshold
	# Based on comment: https://stats.stackexchange.com/a/267295
	#thr = 1 - 256*(len(x1) ** -2.0)
	thr = 0.99999999996 # Above was unreliable, use fixed threshold instead.
	if r < thr:
		raise Error("Pearson r=%.15g < thr=%.15g" % (r, thr))

def test_exact(x1, x2, fs, diff_fn):
	write_diff(x1, x2, fs, diff_fn)
	if len(x1) != len(x2):
		raise Error("Read sample count differs (%d vs %d)" % (len(x1), len(x2)))
	for i in range(len(x1)):
		if x1[i] != x2[i]:
			raise Error("Sample %d differs (%g vs %g)" % (i, x1[i], x2[i]))

def test_specgram(x1, x2, fs, diff_fn):
	# Resize if lengths differ
	if len(x1) != len(x2):
		size = min(len(x1), len(x2))
		x1 = x1.flatten()
		x1.resize(size)
		x2 = x2.flatten()
		x2.resize(size)

	# Scale range to [-1,1] and decimate by 8: 48 kHz -> 6 kHz
	x1 = x1 / (1<<15)
	x2 = x2 / (1<<15)
	x1 = signal.decimate(x1, 8)
	x2 = signal.decimate(x2, 8)
	fs = fs // 8

	write_diff(x1, x2, fs, diff_fn)

	# Compute spectrograms
	fig, (ax1, ax2) = plt.subplots(nrows=2)
	kwargs = { 'Fs':fs, 'NFFT':2048, 'noverlap':1024, 'scale':'linear', 'scale_by_freq':True }
	Pxx1, _, _, _ = ax1.specgram(x1, **kwargs)
	ax1.axis('tight')
	Pxx2, _, _, _ = ax2.specgram(x2, **kwargs)
	ax2.axis('tight')

	# Compute max difference
	r = np.max(np.abs(Pxx1 - Pxx2))

	# Test, produces false positives, but catches small enough differences
	thr = 1e-6 # Ad-hoc
	if r > thr:
		raise Error("Specgram r=%.15g < thr=%.15g" % (r, thr))

def main():
	if not (len(sys.argv) >= 4 and len(sys.argv) <= 5):
		print("Usage: %s exact|pearson|specgram <wav_file1> <wav_file2> [<wav_debug>]" % (sys.argv[0]))
		return -1
	try:
		fs1, x1 = wavfile.read(sys.argv[2])
		fs2, x2 = wavfile.read(sys.argv[3])
		if fs1 != fs2:
			raise Error("Sampling rates differ (%g vs %g)" % (fs1, fs2))

		diff_fn = sys.argv[4] if len(sys.argv) > 4 else None
		if sys.argv[1] == 'exact':
			test_exact(x1, x2, fs1, diff_fn)
		elif sys.argv[1] == 'pearson':
			test_pearson(x1, x2, fs1, diff_fn)
		elif sys.argv[1] == 'specgram':
			test_specgram(x1, x2, fs1, diff_fn)
		else:
			raise Error("Invalid comparison method:%s" % (sys.argv[1]))
	except Error as e:
		print("Difference found: %s (%s vs %s)" % (str(e), sys.argv[2], sys.argv[3]))
		return -2

if __name__ == '__main__':
	exit(main())


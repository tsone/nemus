
# Scripts

Collection of scripts for testing etc. Written on macOS, bash.


**Note: Help is wanted to add support for Linux and Windows!**


## Test Scripts

Test if nemus WAV and NSF export remains similar to reference files.
Use to catch programmer errors when modifying audio or NSF driver.

**Note: Failed result indicates mismatch against reference. It doesn't indicate
an error, and it could be a false positive. One needs to manually inspect the
generated files in `temp/` to verify the error, ex. `temp/*_diff.wav` files.**

The scripts take nemus song files in `nms/` and generate WAV files to compare
with FLAC reference files in `flac_nsf/` or `flac_wav/`.

Requirements:

* clang or gcc (via Xcode, Command Line Tools etc.)
* scipy (for python3)
* Wine, if `test_nsf.sh` is run (via Homebrew etc.)


### `test_wav.sh`

Test nemus WAV export, i.e. audio output.


### `test_nsf.sh`

Test nemus NSF export. The script is complex and may hang upon calling `nsfplay`
program via Wine. If it hangs, run commands `killall wine-preloader` and
`killall wine64-preloader` and retry.

**Note: 2A03 generates audio at ~1,7MHz. Hence, only a few cycles change in
NSF code timing will cause large changes in generated WAV. This means there
are much more false positives than with `test_wav.sh`.**


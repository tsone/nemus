import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt

clockrate=1789773.0
samplerate=48000.0

def normclean(a):
  a = map(lambda x: x if abs(x) >= 1e-6 else 0., a)
  sa = 1. / sum(a)
  return [ x * sa for x in a ]

def print_cs(cs):
  print('{ ' + ', '.join(['%.12g' % (c) for c in cs]) + ' }')

def print_sos(sos):
  for s in sos:
    print('{ ' + ', '.join(["%.12g" % (s[i]) for i in range(6) if i != 3]) + ' }')

def lanczos(n, a):
  w = np.ones(n)
  m = n // 2
  for i in range(1, m + 1):
    x = a * i / (m + 1)
    v = np.sin(np.pi * x) * np.sin(np.pi * x / a) / (np.pi * np.pi * x * x / a)
    w[m - i] = w[m + i] = v
  w = normclean(w)
  return w

def fir_design():
  b1 = signal.remez(11, [0, .5/3, .5-.5/3, .5], [1, 0])
  w1, h1 = signal.freqz(b1)
  plt.title('Frequency response')
  plt.plot(w1/np.pi, 20*np.log10(np.abs(h1)), 'b')
  plt.ylabel('Amplitude Response (dB)')
  plt.xlabel('Frequency')
  plt.grid()
  plt.show()

def sos_design():
  fc = 0.98 * 0.5*samplerate/(clockrate/64)
  sos = signal.iirdesign(0.75*fc, fc, .9, 60., output='sos', analog=False)
  print_sos(sos)
  w, h = signal.sosfreqz(sos)
  db = 20*np.log10(np.abs(h))
  plt.plot(w/np.pi, db)
  plt.grid()
  plt.show()

def iir_design():
  sos = signal.iirfilter(2, [0.25], rp=6.0, rs=120., btype='lowpass', analog=False, output='sos')
  print_sos(sos)
  w, h = signal.sosfreqz(sos, 10000)
  db = 20*np.log10(np.abs(h))
  plt.plot(w*0.5*samplerate/np.pi, db)
  plt.grid()
  plt.show()

def remez_opt0(m, lim, i):
  pc = 1e5
  cf = .5 * lim
  step = .25 * lim
  while abs(step) >= 1e-12:
    rem = signal.remez(m, [0, cf, lim, .5], [1, 0], maxiter=50000)
    rem = normclean(rem)
    if abs(rem[i]) >= pc:
      cf -= step
      if step < 0:
        step = .5 * abs(step)
      else:
        step = -step
    else:
      pc = abs(rem[i])
      #print("pc:", pc, "cf:", cf)
      if pc < 1e-12:
        break
      step = abs(step)
    cf = max(0., min(lim, cf + step))
  return rem

def plot_fir():
  rem12 = remez_opt0(7, .923744 / 2, 1)
  print("Remez 1/2:")
  print_cs(rem12)

  rem13 = remez_opt0(11, .808/3, 2)
  print("Remez 1/3:")
  print_cs(rem13)

  kaiser13 = [ -0.055130, -0.068919, .0, 0.137829, 0.275668, 0.333333, 0.275668, 0.137829, .0, -0.068919, -0.055130 ]
  kaiser13 = normclean(kaiser13)
  print("Kaiser 1/3:")
  print_cs(kaiser13)

  lz7 = lanczos(7, 2.)
  lz11 = lanczos(11, 2.)
  print("Lanczos2 1/2:")
  print_cs(lz7)
  print("Lanczos2 1/3:")
  print_cs(lz11)

  ks = [
    ( "Box 1/2",        ( .5, .5 ) ),
    ( "Tent 1/2",       ( 1/4., 2/4., 1/4. ) ),
    ( "Tent 1/2 (1/2)", ( 1/8., 3/8., 3/8., 1/8. ) ),
    ( "Lanczos2 1/2 A", ( -.032, .0, .284, .496, .284, .0, -.032 ) ),
    ( "Lanczos2 1/2 B", lz7 ),
    ( "Turkowski 1/2",  ( -1/32., 0., 9/32., 16/32., 9/32., 0., -1/32.) ),
    ( "Remez 1/2",      rem12 ),
    ( "Nemus 1/2 A",    ( -5/128., 0., 37/128., 64/128., 37/128., 0., -5/128. ) ),
    ( "Nemus 1/2 B",    ( -9/256., 0., 73/256., 128/256., 73/256., 0., -9/256. ) ),
    ( "Gabriel 1/2",    ( -1/16., 0., 5/16., 8/16., 5/16., 0., -1/16. ) ),
    #( "Tent 1/3 (1/2)", ( 1/18., 3/18., 5/18., 5/18., 3/18., 1/18. ) ),
    #( "Lanczos2 1/3 A",  ( -.010, -.028, .0, .113, .261, .330, .261, .113, .0, -.028, -.010 ) ),
    #( "Lanczos2 1/3 B",  lz11 ),
    #( "Remez 1/3",       rem13 ),
    #( "Kaiser-Bessel 1/3", kaiser13 ),
    #( "Lanczos2 1/4",  ( -.004, -.016, -.021, .0, .058, .142, .217, .248, .217, .142, .058, .0, -.021, -.016, -.004 ) ),
  ]
  whs = [ ( signal.freqz(k[1], worN=4096), k[0]) for k in ks ]
  plt.title('Frequency response')
  plt.gca().set_ylim(-90, 3)
  for wh in whs:
    plt.plot(wh[0][0]/np.pi, 20*np.log10(np.abs(wh[0][1])), label=wh[1])
  plt.legend()
  plt.ylabel('Amplitude Response (dB)')
  plt.xlabel('Frequency')
  plt.grid()
  plt.show()


#fir_design()
#iir_design()
#sos_design()
plot_fir()


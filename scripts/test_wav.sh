#!/bin/bash
set -Eeuo pipefail
SCRIPTDIR=`dirname "$0"`
TEMPDIR=$SCRIPTDIR/temp
SRCDIR=$SCRIPTDIR/../src

NEMUSCDIR="$SCRIPTDIR/../tools/nemusc"
NEMUSC="$NEMUSCDIR/nemusc"

COMPARE="python3 $SCRIPTDIR/compare.py pearson"

echo "Building nemusc"
make -C $NEMUSCDIR

echo "Creating temp dir $TEMPDIR"
mkdir -p $TEMPDIR

function compare () {
  local FN=$1
  local BASEFN=${FN##*/}
  local WAVFN=$TEMPDIR/${BASEFN%.nms}.wav
  local REFFN=$TEMPDIR/${BASEFN%.nms}_ref.wav
  local DIFFFN=$TEMPDIR/${BASEFN%.nms}_diff.wav
  local FLACFN=$SCRIPTDIR/flac_wav/${BASEFN%.nms}.flac
  # NOTE: tsone: following can create the .flac reference files
  #$NEMUSC wav $FN $WAVFN && flac -f -s --best $WAVFN -o $FLACFN
  $NEMUSC wav $FN $WAVFN && flac -d -f -s $FLACFN -o $REFFN && $COMPARE $REFFN $WAVFN $DIFFFN && echo -n "OK" || echo -n "FAIL" ; echo " $BASEFN"
}

echo "Running audio/WAV export tests"
for FN in $SCRIPTDIR/nms/*.nms ; do
  { compare $FN ; } &
done
wait

echo "Done"

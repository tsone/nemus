# Changelog

## [0.9.0] - 2019-08-03
### Added
- Song parts (multisong).
- Song part swing option.
- Song title and author options.
- Sound alternative wave mode.
- Sound pulse wave 3:4 mode.
- Native mode, export: arpeggion support.
- Gamma setting.
### Removed
- Moved nmn code to neskit (renamed as nk audio, used as NSF driver).
- Export: triangle and DMC channel volume envelope.
### Changed
- Native mode: handling of very short notes, zero volume condition.
- Export: 256-byte compression dictionary (was 128).
### Fixed
- Selection move.
- Subrate calculation (note slopes).
- Playback cursor tracking.
- Detune rounding error.
- Volume envelope update in case of error.
- Channel note source and time shift issues.
- Base pitch not saved.
- Undo/redo bugs.
- Random macOS CoreAudio crash.
- Export:
    - Random export failures.
    - Error if song was single note or frame.
    - Invalid slope combine, handling slope w/o note.
    - Invalid instrument length check.
    - Delay off by one.
    - Missing two lowest noise periods.

## [0.8.3] - 2018-09-15
### Changed
- macOS version to mirror Ctrl to Command key.
### Fixed
- Many stability and crash fixes.

## [0.8.2] - 2018-09-02 [YANKED]
### Added
- WAV export support.
- Setting to change output buffer size on macOS.
- MIDI support on macOS.
### Changed
- Better audio output quality.
- Output at 48 kHz rate (was 44100 Hz).
- Audio visualization responsiveness.

## [0.8.0] - 2018-05-02
### Added
- Support for channel volume envelopes in exported NSF.
### Fixed
- Wrong default volume in channel volume envelopes.
- Crash when opening a song while hovering a channel.

## [0.7.3] - 2018-04-29
### Added
- Song/settings menu and song save/open/export in GUI.
### Removed
- Extra triangle.
### Changed
- Reorganized GUI.
### Fixed
- Channel corruption bug and a memory leak.

## [0.7.2] - 2018-04-22
### Added
- TIME-SHIFT and NOTE SOURCE channel options.
- Escape to deselect channels/sounds.
### Changed
- Both Backspace and Delete to erase selected content.
- Bumped version: nms 11, nmn 1.
### Fixed
- nmn envelope stepping bug.

## [0.7.1] - 2018-04-19
### Changed
- macOS version to use Command key instead of Ctrl.
- Right mouse button to cycle options backwards.
- Enabled native mode by default (for new songs).

## [0.7.0] - 2018-04-12
### Added
- nemus native driver (nmn) that plays exported songs on NES/Famicom.
- NSF and assembly export (Ctrl+E).
- "Native mode" song setting that emulates the exported playback.

## [0.5.2] - 2018-04-12
### Fixed
- Issue with time signatures.
- Alt key not working on macOS.

## [0.5.1] - 2018-03-26
### Added
- Initial public release.

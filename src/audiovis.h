void DrawAudioVisPart(unsigned chi, int xoffs, int yoffs, const unsigned* colors)
{
	constexpr int width=140;

	auto& ra=recent_audio[chi];
	const auto ReadBuf=[&ra, width](int i)
	{
		return ra.buffer[(recent_audio_i-8*width+i+8*RecentAudio::SIZE)%RecentAudio::SIZE];
	};

	const float xscale=(chi!=(unsigned)NMSound::Type::DMC?1:1.0/13.0);
	const float ci=(ra.level_max-ra.level_min)*4*xscale;
	unsigned color=colors[(int)std::max(0.0f, std::min(3.0f, ci))];

	const int x0=gui_main_x3/2+xoffs;
	int py=0;
	const float offset=0.5f*(ra.level_max+ra.level_min);

	int shift=0;
	if(chi!=4)
	{
		// track waveforms
		float v0=ReadBuf(0);
		int first_crossing=0;
		for(int i=-16;i>-(int)(RecentAudio::SIZE-8*width);i-=16)
		{
			const float v1=ReadBuf(i);
			if((v1<offset) != (v0<offset))
			{
				if(!first_crossing)
					first_crossing=i;
				if(v1<offset)
				{
					shift=i;
					if(first_crossing-shift>=8*width)
						break;
				}
			}
			v0=v1;
		}
	}

	for(unsigned x=0;x<width;x++)
	{
		int y1=py;
		int y0=25-(int)((ReadBuf(x*8+shift)-offset)*50*xscale);
		if(x==0)
			y1=y0;
		py=y0;
		if(y0>y1)
			std::swap(y0, y1);
		if(y1>50) y1=50;
		if(y0<0) y0=0;
		blitter.DrawBar(x0+x, yoffs+y0, 1, y1+1-y0, color);
	}
}
void DrawAudioVis()
{
	const unsigned border=guibits.GetPixel(80, 48);
	blitter.DrawBox(gui_main_x3/2-1, 14, 402, 113, border);
	blitter.DrawBox(gui_main_x3/2-300-11, 14, 142, 53, border);
	blitter.DrawBox(gui_main_x3/2-300-11, 74, 142, 53, border);
	blitter.DrawBox(gui_main_x3/2-450-11, 14, 142, 53, border);
	blitter.DrawBox(gui_main_x3/2-450-11, 74, 142, 53, border);
	blitter.DrawBox(gui_main_x3/2-150-11, 14, 142, 53, border);

	static constexpr unsigned colors[4] =
	{
		0x05426C,
		0x135E8F,
		0x1A83C8,
		0xFFFFFF
	};

	int x0=gui_main_x3/2;
	int py=0;
	const unsigned i1=recent_mix_i;
	const unsigned i0=(i1-8*400+2*recent_mix.size())%recent_mix.size();
	const int ci=(int)(std::sqrt(recent_mix_energy.Get())*5*4.125f);
	const unsigned color=colors[std::max(0, std::min(3, ci))];
	for(unsigned i=i0,x=0;x<400;x++)
	{
		int y1=py;
		int y0=(int)(recent_mix[i]*2*55);
		if(x==0)
			y1=y0;
		py=y0;
		if(y0>y1)
			std::swap(y0, y1);
		if(y1>55) y1=55;
		if(y0<-55) y0=-55;
		blitter.DrawBar(x0+x, 15+55+y0, 1, y1+1-y0, color);
		i=(i+8)%recent_mix.size();
	}

	DrawAudioVisPart(0, -450-10, 15, colors);
	DrawAudioVisPart(1, -300-10, 15, colors);
	DrawAudioVisPart(2, -150-10, 15, colors);
	DrawAudioVisPart(3, -450-10, 75, colors);
	DrawAudioVisPart(4, -300-10, 75, colors);
}

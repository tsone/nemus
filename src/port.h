#ifndef PORT_H
#define PORT_H

#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <memory>
#include <cassert>

#ifndef _WIN32

#include <unistd.h>

#if !HEADLESS
#if __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#else
#error "platform not supported (yet?)"
#endif
#endif // !HEADLESS

#define __int64 int64_t

using namespace std;

#endif

static inline const char *get_basename(const char* path)
{
	if(!path)
		return nullptr;
	auto n=strlen(path);
	while(n)
	{
		--n;
		if(path[n]=='/' || path[n]=='\\')
			return path+n+1;
	}
	return nullptr;
}

#endif //PORT_H

void DrawCHEnvelopeGraph(int mode, MapGraph* graph)
{
	int gridstep_x=ch_editor.gridstep_x;
	int gsm=1;
	if(!timeline_zoomed)
		gsm=timeline_substeps;
	int gridstep_y=5;

	int gx0=scroll_x/gridstep_x*gsm-1;
	if(gx0<0) gx0=0;
	int gx1=gx0+(window.width-gui_main_x1)/gridstep_x*gsm+3;
	int final_i=graph->FindNext(gx1-1);
	if(final_i!=NONE_POS)
	{
		GraphPoint fp=graph->Read(final_i);
		if(fp.curve!=0xFF)
			gx1=final_i+1; // need to include this for accurate snake drawing
	}

	int spc=2;
	if(mode==3) // no points shown for compact chenv graph
		spc=1;
	for(int sp=0;sp<spc;sp++)
	{
		int prev_i=NONE_POS;
		int next_i=NONE_POS;
		int prev_v=-1;

		prev_i=graph->FindPrevious(gx0+1); // include gx0
		next_i=graph->FindNext(gx0); // exclude gx0
		if(prev_i==NONE_POS && next_i==NONE_POS)
			return;

		if(prev_i!=NONE_POS)
		{
			GraphPoint point=graph->Read(prev_i);
			if(point.curve!=0xFF)
				prev_v=point.curve;
			if(point.trigger!=0xFF)
				prev_v=point.trigger;
		}

		for(int x=gx0;x<gx1;x++)
		{
			GraphPoint point=graph->Read(x);

			if(sp==0 && x==gx1-1 && point.Empty()) // extrapolate to end
			{
				point.trigger=0;
			}

			if(!point.Empty())
			{
				int v=prev_v;
				if(point.curve!=0xFF)
					v=point.curve;
				else if(v==-1)
					v=point.trigger;
				if(prev_i==NONE_POS)
				{
					prev_i=0;
					prev_v=v;
				}
				if(prev_i!=NONE_POS && sp==0) // finish current segment
				{
					if(mode==2)
					{
						int vx0=gui_main_x1+12+prev_i*gridstep_x/gsm;
						int vy0=gui_main_y4-9-prev_v*gridstep_y+gridstep_y/2;
						int vx1=gui_main_x1+12+x*gridstep_x/gsm;
						int vy1=gui_main_y4-9-v*gridstep_y+gridstep_y/2;
						for(int vx=vx0;vx<vx1;vx++)
						{
							int vy=vy0+(vy1-vy0)*(vx-vx0)/(vx1-vx0);
							blitter.DrawBar(vx, vy, 1, gui_main_y4-4-vy, guibits.GetPixel(49, 50));
							DrawGUIb(48, 44, 1, 6, vx, vy-3);
						}
					}
					if(mode==3)
					{
						int vcy=(gui_main_y3+gui_main_y4)/2;
						int vry=8*16;
						int vx0=gui_main_x1+12+prev_i*gridstep_x/gsm;
						int vy0=prev_v*vry/15;
						int vx1=gui_main_x1+12+x*gridstep_x/gsm;
						int vy1=v*vry/15;
						unsigned int c0=guibits.GetPixel(48, 64);
						unsigned int c1=guibits.GetPixel(48, 47);
						UColor ct=UColor(c1);
						for(int vx=vx0;vx<vx1;vx++)
						{
							int vy=vy0+(vy1-vy0)*(vx-vx0)/(vx1-vx0)+15;
							UColor c=UColor(c0);
							int a=vy%16;
							c.r+=(ct.r-c.r)*a/16;
							c.g+=(ct.g-c.g)*a/16;
							c.b+=(ct.b-c.b)*a/16;
							vy/=16;
							blitter.DrawBar(vx, vcy-vy, 1, vy*2+1, c1);
							blitter.DrawPixel(vx, vcy-vy, c.ToUInt());
							blitter.DrawPixel(vx, vcy+vy, c.ToUInt());
						}
					}
				}

				if(sp==1)
				{
					if(point.curve!=0xFF)
					{
						int nvy=point.curve;

						int vx=gui_main_x1+12+x*gridstep_x/gsm;
						int vy=gui_main_y4-9-nvy*gridstep_y+gridstep_y/2;

						DrawGUIb(31,56,9,9, vx-4, vy-4);
					}
					if(point.trigger!=0xFF)
					{
						int nvy=point.trigger;

						int vx=gui_main_x1+12+x*gridstep_x/gsm;
						int vy=gui_main_y4-9-nvy*gridstep_y+gridstep_y/2;

						DrawGUIb(31,44,9,9, vx-4, vy-4);
					}
				}

				if(point.trigger!=0xFF)
					v=point.trigger;
				prev_i=x;
				prev_v=v;
			}
		}
	}
}

void EditorPart_CHEnv()
{
	SelectFont(0);
	if(cv_expand==1 || cv_expand==2)
		DrawString(gui_main_x1-4, (gui_main_y3+gui_main_y4)/2-4, "-");
	else
		DrawString(gui_main_x1-4, (gui_main_y3+gui_main_y4)/2-4, "+");
	static int cv_expanding_t=0;
	Box cvx_b;
	if(cv_expand==0)
		cvx_b.SetXYWH(gui_main_x1-7, (gui_main_y3+gui_main_y4)/2-10, 13, 22);
	else
		cvx_b.SetXYWH(gui_main_x1-7, (gui_main_y3+gui_main_y4)/2-45, 13, 84);
	bool toggle_expand=false;
	if(cvx_b.Contains(mousepoint) && mouse.lclick && (cv_expand==0 || cv_expand==2))
		song->gui_chenv_expanded=!song->gui_chenv_expanded;
	if(cv_expand==0 && song->gui_chenv_expanded)
		toggle_expand=true;
	if(cv_expand==2 && !song->gui_chenv_expanded)
		toggle_expand=true;
	if(toggle_expand)
	{
		cv_expand=(cv_expand+1)%4;
		cv_expanding_t=glrAppTimeMs();
	}
	if(cv_expand==1 || cv_expand==3)
	{
		AnimateEditor();

		int t=(glrAppTimeMs()-cv_expanding_t)/2;
		if(t>70)
			t=70;
		if(cv_expand==1)
			gui_main_y4=gui_main_y3+17+t;
		else
			gui_main_y4=gui_main_y3+17+70-t;
		if(t==70)
			cv_expand=(cv_expand+1)%4;
	}
	else
	{
		if(cv_expand==0)
			gui_main_y4=gui_main_y3+17;
		else
			gui_main_y4=gui_main_y3+17+70;
	}

	if(song->selected_ch==-1 && song->selected_st==-1)
		return;


	if(chenv_editor.graphs.num==0)
	{
		if(song->selected_ch!=-1)
		{
			chenv_editor.graphs.num=1;
			chenv_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->envelope;
		}
		if(song->selected_st!=-1)
		{
			chenv_editor.graphs.num=1;
			chenv_editor.graphs.graph_source[0]=song->GetST(song->selected_st)->envelope;
		}
	}


	blitter.SetScroll(scroll_x, 0);
	blitter.SetClipping(cv_viewbox);
	if(cv_expand==2)
		DrawCHEnvelopeGraph(2, chenv_editor.graphs.graph_source[0]);
	if(cv_expand==0)
		DrawCHEnvelopeGraph(3, chenv_editor.graphs.graph_source[0]);


	if(cv_expand==2)
		GraphEditor(EPM_CHENV);


	blitter.DisableClipping();
	blitter.SetScroll(0, 0);
}

// 
// NMAS - nemus audio stream library
//
#ifndef NMAS_H
#define NMAS_H
#ifdef __cplusplus
extern "C" {
#endif

#if defined(_WIN32)
// TODO: tsone: fix asio support
//#define NMAS_ENABLE_ASIO
#define NMAS_ENABLE_DIRECTSOUND
#elif defined(__APPLE__)
#define NMAS_ENABLE_CORE_AUDIO
#else
#define NMAS_ENABLE_SDL
#endif

// Names of available drivers on current platform (null-terminated)
extern const char* nmas_drivers[];
extern const unsigned nmas_drivers_count;

int NMAS_Callback(float* buffer, int size); // implemented by user

int NMAS_Open(int samplerate, int buffersize, int timingbias, int prefer_asio);

void NMAS_Close(int exit);

#ifdef __cplusplus
}
#endif
#endif

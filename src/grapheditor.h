
bool test_hover_touch=false;

int song_setscrollpos=false;
bool song_edit_scalemap=false;

void GraphEditor(int mode)
{
	if(current_editor==NULL)
		return;

	const auto* part=song->part;
	int timesig2=part->timesig2;

	if(!mouse.left && !mouse.right)
	{
		if(editorprofiles[mode]->region.Contains(mousepoint) && current_editor->mode!=mode) // hovering over this editor and it's not selected, so select it!
			SelectEditor(*editorprofiles[mode]);
	}

	if(current_editor->mode!=mode)
	{
		if(selection_active && selection_editorid==mode) // temp hack for selection visibility
		{
			EditorProfile* ep=editorprofiles[mode];
			blitter.SetScroll(ep->scroll_x, ep->scroll_y);
			blitter.SetClipping(ep->view);
			int slx0=selection_x0;
			slx0=std::min(std::max(slx0, 0), ep->max_x);
			int slx1=selection_x1-1;
			slx1=std::min(std::max(slx1, 0), ep->max_x);

			slx0/=ep->quantize_x;
			slx1/=ep->quantize_x;

			blitter.DrawBarAlpha(ep->grid_x0+slx0*ep->gridstep_x-ep->gridstep_x*0, ep->view.y0+ep->scroll_y,
								(slx1-slx0)*ep->gridstep_x+ep->gridstep_x, ep->view.y1-ep->view.y0, 0x25FF8822);
			blitter.SetScroll(0, 0);
			blitter.DisableClipping();
		}
		return;
	}
	bool prevent_click=false;
	if(selection_deactivated)
		prevent_click=true;

	if(mode==EPM_SONG && selection_active)
	{
		if(song->selected_sound==-1 && song->selected_st==-1 && song->selected_ch==-1) // all selected
			all_song_cache_dirty=true;
	}
	if(song->selected_st!=-1)
	{
		NMChannel* st=song->GetST(song->selected_st);
		if(st!=NULL)
			st->song_cache_dirty=true;
	}
	if(song->selected_ch!=-1)
	{
		NMChannel* ch=song->GetCH(song->selected_ch);
		if(ch!=NULL)
			ch->song_cache_dirty=true;
	}

	blitter.SetScroll(current_editor->scroll_x, current_editor->scroll_y);
	blitter.SetClipping(current_editor->view);

	static int scalemap_draw=0;
	if(!mouse.right)
		scalemap_draw=0;

	MapGraph* editgraph=editgraphs->graph_source[0]; // assume there's one (but NULL is ok)
	static int st_editrow=-1;
	static int st_grabrow=-1;
	static int st_touchrow=-1;

	if(mode==EPM_ST)
	{
		editgraph=NULL;
		if(st_editrow!=-1)
		{
			editgraph=NULL;
			if(song->GetST(st_editrow)!=NULL)
				editgraph=song->GetST(st_editrow)->content;
		}
	}
	if(mode==EPM_SONG)
		editgraph=NULL;

	bool modified_graph=false;

	// store these so it can be drawn at the previous position (to match lagged content)
	bool vis_selection_active=selection_active;
	int vis_selection_x0=selection_x0;
	int vis_selection_x1=selection_x1;
	static int selection_y0o=-1; // reference for moving selection vertically
	static int selection_y1o=-1; // reference for moving selection vertically

	static GraphPoint movepoint_target;
	static GraphPoint movepoint_prev;
	static int movepoint_px;
	static int c_nvx=0;
	static int c_nvy=0;
	static bool clicked=false;
	static bool creating=false;
	static bool selecting=false;
	static bool moving_wavemode_pos=false;
	static bool moving=false;
	static int move_subpos=0;
	if(!mouse.left && !mouse.right)
	{
		if(mode==EPM_ST && creating && st_touchrow!=st_grabrow)
			creating=false;

		if(selection_active)
		{
			if(creating)
				selection_active=false;
			creating=false;

			// clip selection to edges when not moving it around
			if(selection_x0<0) selection_x0=0;
			if(selection_x1>current_editor->max_x+1) selection_x1=current_editor->max_x+1;
		}
		else
		{
			if(creating && mode==EPM_SONG)
			{
				song_setscrollpos=true;
				creating=false;
			}
			if(creating && !ctrl_down && editgraph!=NULL)
			{
				GraphPoint point=editgraph->Read(c_nvx);
				int point_type=0; // curve
				if(mode==EPM_VENV || mode==EPM_PENV || mode==EPM_CHENV) // reversed default point type in envelopes
				{
					if(shift_down)
					{
						point_type=1; // trigger
						point.trigger=c_nvy;
					}
					else
						point.curve=c_nvy;
				}
				else
				{
					if(mode!=EPM_ST && shift_down)
						point.curve=c_nvy;
					else
					{
						point_type=1; // trigger
						point.trigger=c_nvy;
					}
				}
				editgraph->SetPoint(c_nvx, point);

				song_modified=true;

				modified_graph=true;
				creating=false;

				if(point_type!=graph_recent_type || graph_recent_graph!=editgraph)
					GraphRecentForget();
				graph_recent_graph=editgraph;
				graph_recent_x0=graph_recent_x1;
				graph_recent_x1=c_nvx;
				graph_recent_value0=graph_recent_value1;
				if(point_type==0)
					graph_recent_value1=point.curve;
				else
					graph_recent_value1=point.trigger;
				graph_recent_type=point_type;
			}
		}
		creating=false;
		clicked=false;
		selecting=false;
		if(moving || selection_moving)
			modified_graph=true;
		moving=false;
		selection_moving=false;
		moving_wavemode_pos=false;
		st_grabrow=-1;
	}

	if(ctrl_down && glrKeyHit(GLR_KEY_A) && current_editor->region.Contains(mousepoint))
	{
		selection_active=true;
		selection_x0=0;
		selection_x1=current_editor->max_x+1;
		selection_y0=-1;
		selection_y1=-1;
		selection_editorid=mode;
	}
	if(selection_active && (glrKeyHit(GLR_KEY_DELETE) || glrKeyHit(GLR_KEY_BACKSPACE)))
	{
		if(mode==EPM_CH && selection_y0!=-1)
			editgraphs->ClearRange_Values(selection_x0, selection_x1, selection_y0, selection_y1);
		else if(mode==EPM_ST && selection_y0!=-1)
			editgraphs->ClearRange_Graphs(selection_x0, selection_x1, selection_y0, selection_y1);
		else
			editgraphs->ClearRange(selection_x0, selection_x1);
//		editgraphs->ClearRange(selection_x0, selection_x1);
		selection_active=false;
		modified_graph=true;

		song_modified=true;
	}

	static bool pasting=false;
	static bool paste_finish=false;
	if(selection_active && selection_editorid==mode && current_editor->region.Contains(mousepoint) && ctrl_down && (glrKeyHit(GLR_KEY_C) || glrKeyHit(GLR_KEY_X)))
	{
		bool clear=false;
		if(glrKeyHit(GLR_KEY_X))
			clear=true;
		if(mode==EPM_CH && selection_y0!=-1)
		{
			clipboard_buffer.ReadRange_Values(selection_x0, selection_x1, selection_y0, selection_y1);
			if(clear)
				editgraphs->ClearRange_Values(selection_x0, selection_x1, selection_y0, selection_y1);
		}
		else if(mode==EPM_ST && selection_y0!=-1)
		{
			clipboard_buffer.ReadRange_Graphs(selection_x0, selection_x1, selection_y0, selection_y1);
			if(clear)
				editgraphs->ClearRange_Graphs(selection_x0, selection_x1, selection_y0, selection_y1);
		}
		else
		{
			clipboard_buffer.ReadRange(selection_x0, selection_x1);
			if(clear)
				editgraphs->ClearRange(selection_x0, selection_x1);
		}
		clipboard_size=selection_x1-selection_x0;
		clipboard_y0=selection_y0;
		clipboard_y1=selection_y1;
		if(clear)
		{
//			editgraphs->ClearRange(selection_x0, selection_x1);
			modified_graph=true;
			song_modified=true;
		}
		selection_active=false;
		clipboard_exists=true;
		clipboard_editorid=mode;
	}
	if(clipboard_exists && current_editor->region.Contains(mousepoint) && clipboard_editorid==mode && ctrl_down && glrKeyHit(GLR_KEY_V))
	{
		pasting=true;
		selection_active=true;
		paste_finish=false;
		selection_target.BufferSource(&clipboard_buffer);
		selection_target.ReadRange(0, clipboard_size);
		selection_target.SetSource(editgraphs);
		selection_px0=-1;
		selection_px1=-1;
		selection_y0=clipboard_y0;
		selection_y1=clipboard_y1;
		selection_editorid=mode;
	}
	if(paste_finish && !mouse.left)
	{
		pasting=false;
		paste_finish=false;
		modified_graph=true;

		song_modified=true;

		GraphRecentForget();
	}
	if(pasting)
	{
		if(selection_px0!=-1)
			selection_prev.WriteRange(selection_px0, selection_px1);
		selection_px0=-1;
		selection_px1=-1;
		if(!selection_active) // cancel paste
		{
			pasting=false;
			paste_finish=false;
		}
	}

	if((current_editor->region.Contains(mousepoint) || moving || moving_wavemode_pos || selection_moving) && !prevent_edit)
	{
		int graphcursor_x=(mousepoint.x+current_editor->scroll_x)-current_editor->grid_x0;
		if(graphcursor_x<0)
			graphcursor_x-=current_editor->gridstep_x;
		graphcursor_x/=current_editor->gridstep_x;

		int graphcursor_y=current_editor->grid_y0-(mousepoint.y+current_editor->scroll_y);
		if(graphcursor_y<0)
			graphcursor_y-=current_editor->gridstep_y;
		graphcursor_y/=current_editor->gridstep_y;


		int nvx=graphcursor_x*current_editor->quantize_x;
		int nvy=graphcursor_y;
		nvx=std::min(std::max(nvx, 0), current_editor->max_x);
		nvy=std::min(std::max(nvy, 0), current_editor->max_y);

		if(mode==EPM_ST)
		{
			st_touchrow=17-graphcursor_y;
//			if(st_touchrow<0) st_touchrow=-1;
			if(st_touchrow<0) st_touchrow=0;
//			if(st_touchrow>=18) st_touchrow=-1;
			if(st_touchrow>17) st_touchrow=17;
			if(st_grabrow==-1)
				st_editrow=st_touchrow;
			nvy=1;
		}

		if(!ctrl_down)
		{
			if(current_editor->mode==EPM_CH)
			{
				GUIbAlpha(50);
				DrawGUIb(1,101,13,13, current_editor->grid_x0+nvx/current_editor->quantize_x*current_editor->gridstep_x+current_editor->gridstep_x/2-7, current_editor->grid_y0-nvy*current_editor->gridstep_y-(current_editor->gridstep_y+1)/2-7);
				GUIbAlpha(255);
			}
			if(current_editor->mode==EPM_ST && editgraph!=NULL && song->GetST(st_editrow)!=NULL)
			{
				GUIbAlpha(50);
				DrawGUIb(58-20,150+77,9,9, current_editor->grid_x0+nvx/current_editor->quantize_x*current_editor->gridstep_x+current_editor->gridstep_x/2-4, current_editor->grid_y0-18*12+2+st_editrow*current_editor->gridstep_y);
				GUIbAlpha(255);
			}
			if((current_editor->mode==EPM_VENV || current_editor->mode==EPM_PENV) && editgraph!=NULL)
			{
				GUIbAlpha(50);
				DrawGUIb(31,44,9,9, gui_main_x2+33+nvx*envwidth-4, gui_main_y0+25+(current_editor->max_y-nvy)*envheight-4);
				GUIbAlpha(255);
			}
			if(current_editor->mode==EPM_CHENV && editgraph!=NULL)
			{
				GUIbAlpha(50);
				int vx=gui_main_x1+12+nvx/current_editor->quantize_x*current_editor->gridstep_x;
				int vy=gui_main_y4-9-nvy*current_editor->gridstep_y+current_editor->gridstep_y/2;
				DrawGUIb(31,44,9,9, vx-4, vy-4);
				GUIbAlpha(255);
			}
		}

		if(pasting)
		{
			int dx=nvx;
			if(paste_finish)
				dx=c_nvx;
			int repeatcount=1;
			int repeatdir=1;
			int quantize=1;
			if(!(mode==EPM_VENV || mode==EPM_PENV))	// quantize repeat length in editors with song timeline since it's likely that the user picks endpoints inclusively
			{
				if(!timeline_zoomed)
					quantize=2*timeline_substeps;		// can't reliably do the same in envelopes though, since there's less likelihood that an even repeat length is desired...(?)
				else
					quantize=2;
			}
			int repeat_size=clipboard_size/quantize*quantize;
			if(paste_finish && repeat_size>0)
			{
				int rx=nvx-(c_nvx-clipboard_size/2);
				if(rx>=0)
				{
					repeatcount=1+rx/repeat_size;
					repeatdir=1;
				}
				else
				{
					repeatcount=2-rx/repeat_size;
					repeatdir=-1;
				}
			}
			selection_x0=dx-clipboard_size/2;
			if((mode==EPM_CH || mode==EPM_ST || mode==EPM_CHENV) && selection_x1-selection_x0>=current_editor->max_x/2) // don't allow moving selection when pasting from select-all (center will be very far off)
			{
				selection_x0=0;
				paste_finish=true; // complete paste immediately since there's no moving
			}
			else if(selection_x1-selection_x0==current_editor->max_x) // ...for env and penv
				paste_finish=true; // complete paste immediately since there's no moving
			if(quantize!=1)
				selection_x0=selection_x0/(quantize/2)*(quantize/2);
			if(mode==EPM_SONG)
			{
				int sbq=part->timesig2*2*timeline_substeps;
				selection_x0=(selection_x0+part->timesig2)/sbq*sbq; // quantize paste position with beat resolution
			}
			if(repeatdir==-1)
				selection_x0-=(repeatcount-1)*repeat_size;
			if(mode==EPM_VENV || mode==EPM_PENV) // limit to edges in the short envelope editors
			{
				if(selection_x0<0) selection_x0=0;
			}
			selection_x1=selection_x0+repeatcount*repeat_size;
			if(repeat_size!=clipboard_size)
				selection_x1+=1;
			if(mode==EPM_VENV || mode==EPM_PENV) // limit to edges in the short envelope editors
			{
				if(selection_x1>current_editor->max_x+1)
				{
					selection_x1=current_editor->max_x+1;
					selection_x0=selection_x1-repeatcount*repeat_size;
					if(repeat_size!=clipboard_size)
						selection_x0-=1;
				}
			}
			selection_px0=selection_x0;
			selection_px1=selection_x1;
			// always clip prev range for restore
			int selw=selection_x1-selection_x0;
			if(selection_px0<0)
				selection_px0=0;
			if(selection_px1>current_editor->max_x+1)
			{
				selection_px1=current_editor->max_x+1;
				selection_px0=selection_px1-selw;
			}

			selection_prev.ReadRange(selection_px0, selection_px1);
			for(int ri=0;ri<repeatcount;ri++)
				selection_target.MergeRange(selection_x0+ri*repeat_size, selection_x0+ri*repeat_size+clipboard_size);
			if(mouse.lclick)
			{
				c_nvx=nvx;
				c_nvy=nvy;
				paste_finish=true;
			}
		}

		if(alt_down)
		{
			if(mode==EPM_CH)
			{
				if(mouse.rclick)
				{
					int sci=song->ScalemapIndex(nvx);
					bool prev=song->ScalemapRead(sci, nvy%12);
					if(prev==true)
						scalemap_draw=2;
					else
						scalemap_draw=1;

					GraphRecentForget();
				}
				if(scalemap_draw==1)
				{
					int sci=song->ScalemapIndex(nvx);
					song->ScalemapWrite(sci, nvy%12, true);

					song_modified=true;
				}
				if(scalemap_draw==2)
				{
					int sci=song->ScalemapIndex(nvx);
					song->ScalemapWrite(sci, nvy%12, false);

					song_modified=true;
				}
			}
			if(mouse.lclick)
			{
				if(mode==EPM_ST)
					nemus->keyjazz.ExternalTrigger(st_touchrow);
				else
					nemus->keyjazz.ExternalTrigger(nvy);
			}
		}
		else
		{
			if(mouse.lclick && !pasting && !prevent_click)
			{
				c_nvx=nvx;
				c_nvy=nvy;
				creating=true;
				clicked=true;
				if(mode==EPM_ST)
					st_grabrow=st_editrow;
			}
			if(mouse.rclick && selection_active)
			{
				c_nvx=nvx;
				c_nvy=nvy;
				clicked=true;
				creating=true;
				if(mode==EPM_ST)
					st_grabrow=st_editrow;
			}
			if(creating && (nvx!=c_nvx || nvy!=c_nvy))
			{
				creating=false;
				if(selection_active && c_nvx>=selection_x0 && c_nvx<selection_x1) // try to move selection
				{
					selection_moving=true;
					selection_grabx=c_nvx-selection_x0;
					if(editgraph!=NULL)
					{
						GraphPoint gp=editgraph->Read(c_nvx);
						if(gp.trigger==c_nvy || gp.curve==c_nvy)
							selection_transpose_y=c_nvy;
						else
							selection_transpose_y=-1;
					}

					if(mode==EPM_CH && selection_y0!=-1)
					{
						selection_target.ReadRange_Values(selection_x0, selection_x1, selection_y0, selection_y1);
						editgraphs->ClearRange_Values(selection_x0, selection_x1, selection_y0, selection_y1);
					}
					else if(mode==EPM_ST && selection_y0!=-1)
					{
						selection_target.ReadRange_Graphs(selection_x0, selection_x1, selection_y0, selection_y1);
						editgraphs->ClearRange_Graphs(selection_x0, selection_x1, selection_y0, selection_y1);
					}
					else
					{
						selection_target.ReadRange(selection_x0, selection_x1);
						editgraphs->ClearRange(selection_x0, selection_x1);
					}

//					selection_target.ReadRange(selection_x0, selection_x1);
//					editgraphs->ClearRange(selection_x0, selection_x1);
					selection_px0=-1;
					selection_px1=-1;
					selection_axis=0;
					if(abs(c_nvx-nvx)>abs(c_nvy-nvy) && selection_transpose_y==-1)
						selection_axis=0;
					else if(editgraph!=NULL) // currently only support moving single graph vertically
						selection_axis=1;
					if(mode==EPM_ST)
						selection_axis=0;

					song_modified=true;

					selection_y0o=selection_y0;
					selection_y1o=selection_y1;

					GraphRecentForget();
				}
				else if(editgraph!=NULL) // try to move point
				{
					movepoint_target.Clear();
					int dx=0;
					GraphPoint point=editgraph->Read(c_nvx);
					if(!timeline_zoomed && current_editor->mode!=EPM_VENV && current_editor->mode!=EPM_PENV)
					{
						while(point.Empty() && abs(dx)<timeline_substeps)
						{
							if(dx==0)
								dx=1;
							else if(dx>0)
								dx=-dx;
							else
								dx=-dx+1;
							if(abs(dx)>=timeline_substeps)
								break;
							point=editgraph->Read(c_nvx+dx);
						}
					}
					int tolerance=0;
					if(mode==EPM_CHENV)
						tolerance=1;
					if(point.trigger!=0xFF && abs(c_nvy-point.trigger)<=tolerance)
					{
						movepoint_target.trigger=point.trigger;
						point.trigger=0xFF;
						moving=true;
					}
					else if(point.curve!=0xFF && abs(c_nvy-point.curve)<=tolerance)
					{
						movepoint_target.curve=point.curve;
						point.curve=0xFF;
						moving=true;
					}
					if(moving)
					{
						editgraph->SetPoint(c_nvx+dx, point);
						movepoint_px=-1;
						move_subpos=dx;

						selection_active=false;

						song_modified=true;

						GraphRecentForget();
					}
				}
			}
			auto* sound=song->GetSound(song->selected_sound);
			if(mouse.left && clicked && nvx!=c_nvx && !moving && !moving_wavemode_pos && !selection_moving)
			{
				if(mode==EPM_PENV && sound && sound->wavemode_pos==c_nvx)
					moving_wavemode_pos=true;
				else
					selecting=true;
			}
			if(selecting)
			{
				int q_c_nvx=c_nvx;
				if(shift_down)
				{
					int qsize=timesig2*2;
					q_c_nvx=(c_nvx+qsize/2)/qsize*qsize;
					int dx0=nvx-q_c_nvx;
					int dx=dx0/qsize;
					dx*=qsize;
					if(dx>0)
						dx-=1;
					if(dx<0)
						dx+=1;
					nvx=q_c_nvx+dx;
				}
				selection_x0=std::min(q_c_nvx, nvx);
				selection_x1=std::max(q_c_nvx, nvx)+1*current_editor->quantize_x;
				selection_y0=std::min(c_nvy, nvy);
				selection_y1=std::max(c_nvy, nvy);
				if(mode==EPM_ST)
				{
					selection_y0=std::min(st_grabrow, st_touchrow);
					selection_y1=std::max(st_grabrow, st_touchrow);
				}
				if(ctrl_down || !(mode==EPM_CH || mode==EPM_ST))
				{
					selection_y0=-1;
					selection_y1=-1;
				}
				selection_active=true;
				selection_editorid=mode;
			}
			if(selection_moving)
			{
				int selw=selection_x1-selection_x0;
				if(selection_px0!=-1)
					selection_prev.WriteRange(selection_px0, selection_px1);
				if(selection_axis==0)
				{
					selection_x0=nvx-selection_grabx;
					if(mode==EPM_VENV || mode==EPM_PENV)
					{
						if(selection_x0<0)
							selection_x0=0;
					}
					selection_x1=selection_x0+selw;
					if(mode==EPM_VENV || mode==EPM_PENV)
					{
						if(selection_x1>current_editor->max_x+1)
						{
							selection_x1=current_editor->max_x+1;
							selection_x0=selection_x1-selw;
						}
					}
					selection_px0=selection_x0;
					selection_px1=selection_x1;
					// always clip prev range for restore
					if(selection_px0<0)
						selection_px0=0;
					if(selection_px1>current_editor->max_x+1)
					{
						selection_px1=current_editor->max_x+1;
						selection_px0=selection_px1-selw;
					}
				}
				selection_prev.ReadRange(selection_px0, selection_px1);
				selection_target.MergeRange(selection_x0, selection_x0+selw);
				if(selection_axis==1 && editgraph!=NULL)
				{
					for(int i=selection_x0;i<selection_x1;i++)
					{
						i=editgraph->FindNext(i-1);
						if(i==NONE_POS || i>=selection_x1)
							break;
						GraphPoint gp=editgraph->Read(i);
						if(gp.trigger!=0xFF)
						{
							if(selection_y0==-1 || (gp.trigger>=selection_y0o && gp.trigger<selection_y1o))
							{
								if(selection_transpose_y==-1 || gp.trigger==selection_transpose_y)
								{
									int ny=gp.trigger+(nvy-c_nvy);
									gp.trigger=std::min(std::max(ny, 0), current_editor->max_y);
//									if(ny<0) ny=0;
//									if(ny>current_editor->max_y) ny=current_editor->max_y;
//									gp.trigger+=nvy-c_nvy;
//									gp.trigger=y1;
//									gp.trigger=std::min(std::max(gp.trigger, 0), current_editor->max_y);
								}
							}
						}
						if(gp.curve!=0xFF)
						{
							if(selection_y0==-1 || (gp.curve>=selection_y0o && gp.curve<selection_y1o))
							{
								if(selection_transpose_y==-1 || gp.curve==selection_transpose_y)
								{
									int ny=gp.curve+(nvy-c_nvy);
									gp.curve=std::min(std::max(ny, 0), current_editor->max_y);
//									gp.curve+=nvy-c_nvy;
//									gp.curve=std::min(std::max(gp.curve, 0), current_editor->max_y);
								}
							}
						}
						editgraph->SetPoint(i, gp);

						song_modified=true;
					}
					if(selection_y0!=-1)
					{
						int selection_height=selection_y1-selection_y0;
						selection_y0=selection_y0o+nvy-c_nvy;
						selection_y1=selection_y0+selection_height;
					}
				}
			}
			if(moving && editgraph!=NULL)
			{
				if(movepoint_px!=-1)
					editgraph->SetPoint(movepoint_px, movepoint_prev);
				movepoint_px=nvx+move_subpos;
				movepoint_prev=editgraph->Read(movepoint_px);
				GraphPoint gp=movepoint_prev;
				if(movepoint_target.trigger!=0xFF)
					gp.trigger=nvy;
				else if(movepoint_target.curve!=0xFF)
					gp.curve=nvy;
				editgraph->SetPoint(movepoint_px, gp);

				song_modified=true;
			}
			if(mode==EPM_PENV && moving_wavemode_pos && sound)
			{
				sound->wavemode_pos=std::max(0, std::min(47, nvx));
				song_modified=true;
			}

			if(mouse.right && !selection_active && editgraph!=NULL)
			{
				GraphPoint point=editgraph->Read(nvx);
				int tolerance=0;
				if(mode==EPM_CHENV)
					tolerance=1;

				bool removed_something=false;
				if(point.trigger!=0xFF)
				{
					if(abs(nvy-point.trigger)<=tolerance)
					{
						point.trigger=0xFF;
						removed_something=true;
					}
				}
				if(point.curve!=0xFF)
				{
					if(abs(nvy-point.curve)<=tolerance)
					{
						point.curve=0xFF;
						removed_something=true;
					}
				}
				editgraph->SetPoint(nvx, point);
				if(removed_something)
				{
					modified_graph=true;

					song_modified=true;

					GraphRecentForget();
				}
			}
		}
	}

	if(modified_graph && !(mode==EPM_VENV || mode==EPM_PENV || mode==EPM_CHENV))
		editgraphs->CleanPoints();

	if(!selection_moving && !pasting) // update immediately when there's no risk of content lag
	{
		vis_selection_active=selection_active;
		vis_selection_x0=selection_x0;
		vis_selection_x1=selection_x1;
	}
	if(vis_selection_active && selection_editorid==mode && !(pasting && prevent_edit))
	{
		int slx0=vis_selection_x0;
		slx0=std::min(std::max(slx0, 0), current_editor->max_x);
		int slx1=vis_selection_x1-1;
		slx1=std::min(std::max(slx1, 0), current_editor->max_x);

		slx0/=current_editor->quantize_x;
		slx1/=current_editor->quantize_x;

/*		int sly0=selection_y0;
		sly0=std::min(std::max(sly0, 0), current_editor->max_y);
		int sly1=vis_selection_y1-1;
		sly1=std::min(std::max(sly1, 0), current_editor->max_y);
*/
//		int sly0=std::min(selection_y0*current_editor->gridstep_y, selection_y1*current_editor->gridstep_y);
//		int sly1=std::max(selection_y0*current_editor->gridstep_y, selection_y1*current_editor->gridstep_y);

		if(selection_y0==-1 || !(mode==EPM_CH || mode==EPM_ST))
		{
			blitter.DrawBarAlpha(current_editor->grid_x0+slx0*current_editor->gridstep_x-current_editor->gridstep_x*0, current_editor->view.y0+current_editor->scroll_y,
								(slx1-slx0)*current_editor->gridstep_x+current_editor->gridstep_x, current_editor->view.y1-current_editor->view.y0, 0x25FF8822);
		}
		else
		{
			if(mode==EPM_CH)
			{
				int y0=current_editor->grid_y0-(selection_y1+1)*current_editor->gridstep_y;
				int y1=current_editor->grid_y0-selection_y0*current_editor->gridstep_y;
				blitter.DrawBarAlpha(current_editor->grid_x0+slx0*current_editor->gridstep_x, y0,
									(slx1-slx0)*current_editor->gridstep_x+current_editor->gridstep_x, y1-y0, 0x25FF8822);
			}
			if(mode==EPM_ST)
			{
				int y0=current_editor->grid_y0-(18-selection_y0)*current_editor->gridstep_y;
				int y1=current_editor->grid_y0-(17-selection_y1)*current_editor->gridstep_y;
				blitter.DrawBarAlpha(current_editor->grid_x0+slx0*current_editor->gridstep_x, y0,
									(slx1-slx0)*current_editor->gridstep_x+current_editor->gridstep_x, y1-y0, 0x25FF8822);
			}
		}
	}

	blitter.SetScroll(0, 0);
	blitter.DisableClipping();
}

bool mouse_touched_graph=false;

// y0,y1 are 10-bit fixed-point (Q10)
static inline bool SnakeHitQ10(const int x0, int y0, const int x1, int y1)
{
	constexpr int sh=7;

	const int mx=(mouse.x+scroll_x);
	const int my=(mouse.y+scroll_y)*(1<<10);

	const int dyx=(y1-y0)/(x1-x0);
	const int py=(mx-x0)*dyx+y0+sh*((1<<10)/2);

	if(y0>y1)
		std::swap(y0, y1);

	if(mx<x0-(sh>>1) || mx>x1+(sh>>1) || my<y0 || my>y1+sh*(1<<10))
		return false;

	return (abs(py-my)<=sh*std::max((1<<10),abs(dyx)));
}

// y0,y1 are 10-bit fixed-point (Q10)
void SnakeDrawQ10(const int x0, const int y0, const int x1, const int y1, const int sx0, const int sx1, const int sx2, const int sy)
{
	constexpr int sh=7;

	if(x1<=x0)
		return;

	const int dyx=(y1-y0)/(x1-x0);

	if(SnakeHitQ10(x0, y0, x1, y1))
		mouse_touched_graph=true;

	int yx=y0-sh*((1<<10)/2);
	int xs0=x0+(sx1-sx0);
	int xs1=x1-(sx2-sx1);
	int sx=sx2-std::min(sx2-sx0,x1-x0);
	for(int x=x0;x<x1;x++)
	{
		DrawGUIb(sx, sy, 1, sh, x, yx/(1<<10));
		if(x<xs0 || x>=xs1)
			sx++;
		yx+=dyx;
	}
}

void DrawChannelGraph(GraphReader* graph, bool shadowed, int color, int default_length)
{
	constexpr int colormap[]={0,1,3,4,2};

	const int gridstep_x=ch_editor.gridstep_x;
	const int gridstep_y=ch_editor.gridstep_y;
	const int gsm=(!timeline_zoomed)?timeline_substeps:1;

	const int gx0=scroll_x/gridstep_x*gsm-4;
	const int gx1=(scroll_x+window.width-gui_main_x1)/gridstep_x*gsm+4;

	color=colormap[color];

	// styling
	const int clx=color*18;
	const int ssy=shadowed?2*8:0;

	const auto start_it=graph->GetIterator(gx0);
	if(!start_it.IsValid(graph))
		return;

	// draw snakes and stumps
	auto it=start_it;
	auto gp2=it.Read(graph);
	auto p2=it.Pos(graph);
	auto gp1=gp2;
	auto p1=p2;
	do
	{
		const auto gp0=gp1;
		const auto p0=p1;
		gp1=gp2;
		p1=p2;
		it.Next(graph);
		gp2=it.Read(graph);
		p2=it.Pos(graph);

		int n0=-1, x0;
		int n1, x1;

		if(gp1.curve!=0xFF)
		{
			// possibly curve
			if(gp0.trigger!=0xFF)
				n0=gp0.trigger;
			else if(gp0.curve!=0xFF)
				n0=gp0.curve;
			x0=p0;

			n1=gp1.curve;
			x1=p1;
		}
		else
		{
			// possibly trigger
			if(gp0.trigger!=0xFF)
				n0=gp0.trigger;
			x0=p0;

			n1=n0;
			x1=x0+default_length;
			if(p1!=NONE_POS)
				x1=std::min(x1, p1);
		}

		if(n0!=-1 && x1>x0)
		{
			// let's draw a snake!

			n0*=gridstep_y*(1<<10);
			n1*=gridstep_y*(1<<10);

			// clipping
			if(x1>gx1)
			{
				n1=n1+(n0-n1)*(gx1-x1)/(x0-x1);
				x1=gx1;
			}
			if(x0<gx0)
			{
				n0=n0+(n1-n0)*(gx0-x0)/(x1-x0);
				x0=gx0;
			}

			constexpr int sx1=34;
			const int sx0=gp0.trigger==0xFF?34:21;
			const int sx2=(gp1.curve!=0xFF && gp2.curve!=0xFF)?34:38;

			SnakeDrawQ10(gui_main_x1+12+(x0*gridstep_x)/gsm, (1<<10)*(gui_main_y4+4+gridstep_y/2)-n0,
					   gui_main_x1+12+(x1*gridstep_x)/gsm, (1<<10)*(gui_main_y4+4+gridstep_y/2)-n1,
					   sx0+clx-20, sx1+clx-20, sx2+clx-20, 101+ssy+77);
		}
	}
	while(p1<gx1 && p1!=NONE_POS);

	// draw handles
	it=start_it;
	do
	{
		const auto gp0=it.Read(graph);
		const auto p0=it.Pos(graph);

		const int nvx=p0;
		if(gp0.curve!=0xFF && !shadowed)
		{
			const int nvy=gp0.curve;
			int sy=gui_main_y4-nvy*gridstep_y+gridstep_y/2;
			sy-=scroll_y;
			sy=std::min(std::max(sy, gui_main_y4-1), gui_main_y5-6);
			sy+=scroll_y;
			DrawGUIb(21+clx-20,139+77,9,9, gui_main_x1+12+(nvx*gridstep_x)/gsm-4, sy);
		}
		if(gp0.trigger!=0xFF)
		{
			const int nvy=gp0.trigger;
			int sy=gui_main_y4-nvy*gridstep_y+gridstep_y/2-2;
			sy-=scroll_y;
			sy=std::min(std::max(sy, gui_main_y4-3), gui_main_y5-8);
			sy+=scroll_y;
			if(shadowed)
				DrawGUIb(21+clx-20,125+77,13,13, gui_main_x1+12+(nvx*gridstep_x)/gsm-6, sy);
			else
				DrawGUIb(1,101,13,13, gui_main_x1+12+(nvx*gridstep_x)/gsm-6, sy);
		}

		it.Next(graph);

		if(it.Pos(graph)>=gx1 || !it.IsValid(graph))
			break;
	}
	while(true);
}

void EditorPart_CH()
{
	static int clicked_ch=-1;
	static int dragged_ch=-1;
	static bool dragged_copy=false;
	static bool clicked_right=false;
	int hovered_ch=-1;


	const auto* part=song->part;
	int timesig1=part->timesig1;
	int timesig2=part->timesig2;

	blitter.SetScroll(0, 0);
//	blitter.SetClipping(ch_viewbox);
	SelectFont(1);
	for(int ci=0;ci<99;ci++)
	{
		int i=ci+song->gui_scroll_channels;
		if(i<0) continue;
		if(i>98) break;
		int rx=gui_main_x0+3;
		int ry=gui_main_y4+7+ci*12;
//		ry-=song->gui_scroll_channels;
		if(ry>window.height)
			break;

		bool active=false;

		SelectFont(1);
		if(song->GetCH(i)!=NULL)
		{
			active=true;
			SelectFont((1+song->GetCH(i)->color)%5);
		}
		if(!active)
			FontAlpha(60);

		DrawString(rx, ry, "CH");
		SelectFont(1);
		DrawString(rx+9*3, ry, "%02i", i+1);

		bool hoverlight=false;
		if(active && preview_hover_sound!=-1 && song->GetCH(i)->sounds[preview_hover_sound]==true)
			hoverlight=true;

		Box bb;
		bb.SetXYWH(rx-5, ry, 52, 12);
		if(bb.Contains(mousepoint) || hoverlight)
		{
			FontAlpha(90);
			if(active)
			{
				SelectFont(0);
				FontAlpha(170);
			}
			else
				SelectFont(1);
			DrawString(rx, ry, "CH %02i", i+1);

			if(!hoverlight)
				hovered_ch=i;
		}

		FontAlpha(255);
	}


	if(hovered_ch!=-1)
		preview_hover_channel=song->GetCH(hovered_ch);


	if(mouse.lclick && hovered_ch!=-1)
		clicked_ch=hovered_ch;
	if(mouse.rclick && hovered_ch!=-1)
	{
		clicked_ch=hovered_ch;
		clicked_right=true;
	}
	if(mouse.left && clicked_ch!=-1 && hovered_ch!=clicked_ch)
	{
		if(song->GetCH(clicked_ch)!=NULL)
		{
			dragged_ch=clicked_ch;
			if(shift_down)
				dragged_copy=true;
			else
				dragged_copy=false;
			clicked_ch=-1;
		}
	}
	if(!mouse.left && !mouse.right)
	{
		if(dragged_ch!=-1 && hovered_ch!=dragged_ch && hovered_ch!=-1)
		{
			if(dragged_copy || shift_down)
			{
				song->SelectCH(hovered_ch);
				song->GetCH(hovered_ch)->CopyFrom(song->GetCH(dragged_ch));
			}
			else
			{
				song->SwapCHs(dragged_ch, hovered_ch);
				song->SelectCH(hovered_ch);
			}
			ch_editor.graphs.num=1;
			ch_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->content;
			SelectEditor(ch_editor);

			chenv_editor.graphs.num=1;
			chenv_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->envelope;

			song_modified=true;
		}
		if(clicked_ch!=-1 && clicked_ch==hovered_ch)
		{
			if(clicked_right)
			{
				if(song->GetCH(clicked_ch)!=NULL)
					song->RemoveCH(clicked_ch);

				song_modified=true;
			}
			else if(clicked_ch!=-1)
			{
				bool deselect=(song->selected_ch==clicked_ch);
				if(deselect)
				{
					song->selected_ch=-1;
				}
				else
				{
					bool created=(song->GetCH(clicked_ch)==NULL);
					song->SelectCH(clicked_ch);
					if(created)
					{
						if(recent_sound!=-1) // shortcut to assign recently selected instrument when creating a new channel
						{
							NMChannel* ch=song->GetCH(song->selected_ch);
							bool any_sounds=false;
							for(int i=0;i<SOUNDS_MAX;i++)
								if(ch->sounds[i])
								{
									any_sounds=true;
									break;
								}
							if(!any_sounds)
								ch->sounds[recent_sound]=true;
							recent_sound=-1;
						}
						else if(recent_chst!=-1)
						{
							NMChannel* ch=song->GetCH(song->selected_ch);
							bool any_sounds=false;
							for(int i=0;i<SOUNDS_MAX;i++)
								if(ch->sounds[i])
								{
									any_sounds=true;
									break;
								}
							if(!any_sounds)
							{
								const auto recent_channel=song->GetCHST(recent_chst);
								if(recent_channel)
								{
									for(int i=0;i<SOUNDS_MAX;i++)
										ch->sounds[i]=recent_channel->sounds[i];
								}
							}
						}
					}
					recent_sound=-1;

					ch_editor.graphs.num=1;
					ch_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->content;
					SelectEditor(ch_editor);

					chenv_editor.graphs.num=1;
					chenv_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->envelope;
				}
				song_modified=true;
			}
		}
		clicked_ch=-1;
		clicked_right=false;
		dragged_ch=-1;
	}


	if(dragged_ch!=-1)
	{
		SelectFont(0);
		DrawString(mouse.x-18, mouse.y-4, "CH %02i", dragged_ch+1);
	}


	int gridstep_x=ch_editor.gridstep_x;
	int gridstep_y=ch_editor.gridstep_y;
	int doublestep_x=gridstep_x*2;
	if(timeline_zoomed)
		doublestep_x*=timeline_substeps;

	blitter.SetScroll(scroll_x, scroll_y);
	blitter.SetClipping(ch_viewbox);

	blitter.DrawBar(-50000, -50000, gui_main_x1+10+50000, 100000, 0x000000);
	blitter.DrawBar(gui_main_x1+10+std::max(scroll_x, 0), -50000, window.width, gui_main_y4+4-119*gridstep_y+50000, 0x000000);
	blitter.DrawBar(gui_main_x1+10+std::max(scroll_x, 0), gui_main_y4+4, window.width, 50000, 0x000000);

	for(int y=0;y<120;y++)
	{
		int ni=y%12;

		int y0=gui_main_y4+4;
		int ry=y0-y*gridstep_y;
		if(ry-scroll_y<gui_main_y4-gridstep_y) continue;
		if(ry-scroll_y>gui_main_y5) continue;
		int gx0=scroll_x/doublestep_x-1;
		if(gx0<0) gx0=0;
		int gx1=gx0+(window.width-gui_main_x1)/doublestep_x+2+timesig2;

		int cur_scalemap_i=0;
		int sci0=song->ScalemapIndex(gx0*2*timeline_substeps);
		while(sci0>=0)
		{
			unsigned short s=part->scalemap[sci0];
			if(s!=0x0000)
			{
				cur_scalemap_i=sci0;
				break;
			}
			sci0--;
		}

		for(int x=gx0;x<gx1;x++)
		{
			int x0=gui_main_x1+10;
			int rx=x0+x*doublestep_x;

			int t=x;
			int qx0=t/(timesig1*timesig2)%2;
			int qx1=t%(timesig1*timesig2);
			int qx2=t%timesig2;

			int ntype=1-notegrid_type[ni];
			//int srco0=15-15*qx0;

			int smx=x*2*timeline_substeps;
			int sci=song->ScalemapIndex(smx);
			unsigned short sc=part->scalemap[sci];
			if(sc!=0x0000)
				cur_scalemap_i=sci;

			bool scalemark=song->ScalemapRead(cur_scalemap_i, ni);

			int gci=0;
			if(qx1==0)
			{
				if(ntype==0)
				{
					gci=0;
				}
				else
					gci=1;
			}
			else if(qx2==0)
				gci=2;
			else
				gci=3;
			int gcl=1-qx0;
			if(vis_noteplaying[y])
				gcl++;
			gci+=gcl*4;
			Sprite s;
			s.dx=rx;
			s.dy=ry;
			if(timeline_zoomed)
				s.source=gridcache_zoomed[gci];
			else
				s.source=gridcache[gci];
			s.region=s.source.Bounds();
			s.colorkey=0x77000000;
			blitter.DrawSpriteFast(s);

			if(scalemark && qx2==3)
			{
				if(sci==cur_scalemap_i)
				{
					for(int j=0;j<doublestep_x*4/12;j++)
						DrawGUIb(125,28,7,4, rx-doublestep_x*3+5+j*12, ry+6);
				}
				else
				{
					for(int j=0;j<doublestep_x*4/12;j++)
					{
						DrawGUIb(125,29,7,1, rx-doublestep_x*3+5+j*12, ry+7);
						DrawGUIb(125,29,7,1, rx-doublestep_x*3+5+j*12+3, ry+7);
					}
				}
			}

			if(qx1==3 && ntype==1)
			{
				if(ni==0)
				{
					DrawGUIb(76+notegrid_key[ni]*17,102-14*qx0,16,13, rx-doublestep_x*3+1, ry+1);
					DrawGUIb(76+y/12*11,131-14*qx0,11,15, rx-doublestep_x*3+13, ry);
				}
				else
					DrawGUIb(76+notegrid_key[ni]*17,102-14*qx0,16,13, rx-doublestep_x*3+5, ry+1);
			}
		}
	}

	bool inside_editor=(ch_editor.region.Contains(mousepoint));
	int hover_id=-1;
	static int clicked_graph=-1;
	static int hover_touch=-1;
	bool allow_clickselect=IsCtrlDown();
	if(song->selected_ch==-1)
		allow_clickselect=true;
	if(prevent_edit)
		allow_clickselect=false;

	if(hovered_ch!=-1)
		hover_touch=hovered_ch;

	// draw bg graphs
	for(int i=0;i<100;i++)
	{
		NMChannel* ch=song->GetCH(i);
		if(ch==NULL)
			continue;
		if(i==song->selected_ch)
			continue;

		mouse_touched_graph=false;
		auto reader=ch->MakeGraphReader(song.get(), false);
		const bool is_sourced=reader.GetSource()!=ch->content;

		if(is_sourced)
			GUIbAlpha(160);
		DrawChannelGraph(&reader, true, ch->color, ch->notelength*timeline_substeps+ch->notelength_adjust);
		if(is_sourced)
			GUIbAlpha(255);

		if(hover_touch==i)
		{
			GUIbAlpha(50);
			DrawChannelGraph(&reader, false, ch->color, ch->notelength*timeline_substeps+ch->notelength_adjust);
			GUIbAlpha(255);
		}
		if(mouse_touched_graph)
			hover_id=i;
	}

	if(song->selected_ch!=-1 && song->GetCH(song->selected_ch)!=NULL)
	{
		NMChannel* ch=song->GetCH(song->selected_ch);
		auto reader=ch->MakeGraphReader(song.get(), false);
		const bool is_sourced=reader.GetSource()!=ch->content;

		mouse_touched_graph=false;
		if(is_sourced)
			GUIbAlpha(160);
		DrawChannelGraph(&reader, false, ch->color, ch->notelength*timeline_substeps+ch->notelength_adjust);
		if(is_sourced)
			GUIbAlpha(255);
		if(mouse_touched_graph)
			hover_id=song->selected_ch;
	}
	if(inside_editor && allow_clickselect)
	{
		hover_touch=hover_id;

		if(hover_id!=-1 && hover_id!=song->selected_ch)
		{
			if(mouse.lclick)
				clicked_graph=hover_id;
		}
		if(!mouse.left && clicked_graph!=-1 && hover_id==clicked_graph)
		{
			song->SelectCH(clicked_graph);
			ch_editor.graphs.num=1;
			ch_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->content;
			SelectEditor(ch_editor);

			chenv_editor.graphs.num=1;
			chenv_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->envelope;

			song_modified=true;
		}
	}
	else
		hover_touch=-1;
	if(!mouse.left)
		clicked_graph=-1;



	if(song->selected_ch==-1)
	{
		if(picking_sounds_target==1)
			picking_sounds=false;

		blitter.SetScroll(0, 0);
		blitter.DisableClipping();

		return;
	}



	if(ch_editor.graphs.num==0) // TODO: if it's in any way not equal to what it should be
	{
		ch_editor.graphs.num=1;
		ch_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->content;
		SelectEditor(ch_editor);

		chenv_editor.graphs.num=1;
		chenv_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->envelope;
	}


	selected_panel=2;



	GraphEditor(EPM_CH);

	blitter.SetScroll(0, 0);
	blitter.DisableClipping();
//	Box clip;
//	clip.SetXYWH(0, 
//	blitter.SetClipping(clip);

	int seli=song->selected_ch-song->gui_scroll_channels;
	if(seli>=0)
		DrawGUIb(156,1,51,12, gui_main_x0+3-4, gui_main_y4+7+seli*12-2);

//	blitter.DisableClipping();

	NMChannel* cur_ch=song->GetCH(song->selected_ch);


	ButtonResult br;

	PrintMenu(0, 0, 0, "MUSIC CHANNEL");
	PrintMenu(1, 14, 0, "%02i", song->selected_ch+1);

	if(picking_sounds && picking_sounds_target==1)
	{
		PrintMenu(2, 0,5, ".........");
//		PrintMenu(0, 0,5, ".");
		PrintMenu(0, 10,5, "SELECT SOUNDS");
		PrintMenu(0, 20,8, "-  -");
		br=MenuButton(1000, 21, 8, "OK");
		if(br.clicked)
			picking_sounds=false;
	}
	else
	{
		ButtonColor(2);

		int numsounds=0;
		for(int i=0;i<SOUNDS_MAX;i++)
			if(cur_ch->sounds[i])
				numsounds++;
		PrintMenu(0, 12, 2, "%2i %s (      )", numsounds, numsounds==1?"SOUND ":"SOUNDS");
		br=MenuButton(1000, 12+11, 2, "SELECT");
		if(br.clicked)
		{
			picking_sounds=true;
			picking_sounds_target=1;
		}

		ButtonColor(1);
		br=MenuButton(1020, 22, 0, "SAVE");
		if(br.clicked)
		{
			char filename[512];
			if(FileSelectorSave(filename, FST_CHANNEL, "Save Channel"))
				cur_ch->SaveFile(filename);
			ResetPath();
		}
		br=MenuButton(1021, 27, 0, "OPEN");
		if(br.clicked)
		{
			char filename[512];
			if(FileSelectorLoad(filename, FST_CHANNEL, "Open Channel"))
				cur_ch->OpenFile(filename);
			ResetPath();
		}

		PrintMenu(0, 1, 2, "COLOR");
		ButtonColor((1+cur_ch->color)%5);
		static const char* colornames[]={"\"A\"", "\"B\"", "\"C\"", "\"D\"", "\"E\"", "?"};
		br=MenuButton(1001, 1+6, 2, colornames[cur_ch->color]);
		br.ClickTick(cur_ch->color, 5);
		ButtonColor(1);

		PrintMenu(0, 20, 4, "VOLUME");
		br=MenuButton(1002, 20+7, 4, "%i", cur_ch->volume);
		cur_ch->volume+=br.delta; cur_ch->volume=std::min(std::max(cur_ch->volume, 0), 15);
		if(br.rclicked)
		{
			if(cur_ch->volume==0)
				cur_ch->volume=15;
			else
				cur_ch->volume=0;
		}

		PrintMenu(0, 1, 4, "NOTE LENGTH");
		br=MenuButton(1003, 1+12, 4, "%i", cur_ch->notelength);
		cur_ch->notelength+=br.delta; cur_ch->notelength=std::min(std::max(cur_ch->notelength, 1), 128);
		if(br.rclicked) cur_ch->notelength=1;
		br=MenuButton(1007, 1+15, 4, "%s%i", cur_ch->notelength_adjust<0?"":"+", cur_ch->notelength_adjust);
		cur_ch->notelength_adjust+=br.delta; cur_ch->notelength_adjust=std::min(std::max(cur_ch->notelength_adjust, -4), 4);
		if(br.rclicked) cur_ch->notelength_adjust=0;

		PrintMenu(0, 1, 6, "TIME-SHIFT    :");
		br=MenuButton(1008, 1+11, 6, "%+3.0f", std::trunc(-cur_ch->time_shift/(float)timeline_substeps));
		cur_ch->time_shift-=br.delta*timeline_substeps;
		if(br.rclicked) cur_ch->time_shift=0;
		br=MenuButton(1010, 1+14, 6, " %i", std::abs(cur_ch->time_shift%timeline_substeps));
		if(br.rclicked) cur_ch->time_shift=0;
		cur_ch->time_shift-=br.delta;
		cur_ch->time_shift=std::min(std::max(cur_ch->time_shift, -128), 127);

		PrintMenu(0, 1, 7, "NOTE SOURCE");
		if(cur_ch->note_source==-1)
			br=MenuButton(1009, 1+12, 7, "SELF");
		else
			br=MenuButton(1009, 1+12, 7, "CH %02i", cur_ch->note_source+1);
		cur_ch->note_source+=br.delta;
		cur_ch->note_source=std::min(std::max(cur_ch->note_source, -1), 99);
		if(br.rclicked)
			cur_ch->note_source=-1;

		PrintMenu(0, 1, 9, "ARPEGGIO COUNT");
		br=MenuButton(1004, 1+15, 9, "%i", cur_ch->arp_size);
		cur_ch->arp_size+=br.delta; cur_ch->arp_size=std::min(std::max(cur_ch->arp_size, 1), 8);
		if(br.rclicked) cur_ch->arp_size=1;

		PrintMenu(0, 1, 10, "ARPEGGIO SPEED");
		br=MenuButton(1005, 1+15, 10, "%i", cur_ch->arp_speed);
		cur_ch->arp_speed+=br.delta; cur_ch->arp_speed=std::min(std::max(cur_ch->arp_speed, 1), 16);
		br=MenuButton(1006, 1+18, 10, "%s", cur_ch->arp_dir==0?"UP":"DOWN");
		br.ClickTick(cur_ch->arp_dir, 2);
	}
}

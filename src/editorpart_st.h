void EditorPart_ST()
{
	SelectFont(0);
	if(st_expand==0 || st_expand==3)
		DrawString(gui_main_x1-4, (gui_main_y2+gui_main_y3)/2-4, "+");
	if(st_expand==0)
		DrawString(gui_main_x0+17, (gui_main_y2+gui_main_y3)/2-4, "ST");
	static int st_expanding_t=0;
	Box cvx_b;
	if(st_expand==0)
		cvx_b.SetXYWH(gui_main_x1-7, (gui_main_y2+gui_main_y3)/2-10, 13, 22);
	else
		cvx_b.SetXYWH(gui_main_x1-7, gui_main_y2+5, 13, gui_main_y3-gui_main_y2-10);
	bool toggle_expand=false;
	if(cvx_b.Contains(mousepoint) && mouse.lclick && (st_expand==0 || st_expand==2))
		song->gui_st_expanded=!song->gui_st_expanded;
	if(st_expand==0 && song->gui_st_expanded)
		toggle_expand=true;
	if(st_expand==2 && !song->gui_st_expanded)
		toggle_expand=true;
	if(toggle_expand)
	{
		st_expand=(st_expand+1)%4;
		st_expanding_t=glrAppTimeMs();
	}
	if(st_expand==1 || st_expand==3)
	{
		AnimateEditor();

		int d=225-35;
		int t=(glrAppTimeMs()-st_expanding_t);
		if(t>d)
			t=d;
		if(st_expand==1)
			gui_main_y3=gui_main_y2+35+t;
		else
			gui_main_y3=gui_main_y2+35+d-t;
		if(t==d)
			st_expand=(st_expand+1)%4;
	}
	else
	{
		if(st_expand==0)
			gui_main_y3=gui_main_y2+35;
		if(st_expand==2)
			gui_main_y3=gui_main_y2+225;
	}

	if(st_expand!=2)
	{
		// TODO: draw compact view
		return;
	}


	const int num_st_available=18;
	int st_spacing=12;

	static int clicked_st=-1;
	static int dragged_st=-1;
	static bool dragged_copy=false;
	static bool clicked_right=false;
	int hovered_st=-1;


	const auto* part=song->part;
	int timesig1=part->timesig1;
	int timesig2=part->timesig2;

	for(int i=0;i<num_st_available;i++)
	{
		int rx=gui_main_x0+3;
		int ry=gui_main_y2+7+i*st_spacing;

		bool active=false;
		if(song->GetST(i)!=NULL)
			active=true;
		SelectFont(1);
		if(song->GetST(i)!=NULL)
		{
			active=true;
			SelectFont((1+song->GetST(i)->color)%5);
		}
		if(!active)
			FontAlpha(60);

		DrawString(rx, ry, "ST");
		SelectFont(1);
		DrawString(rx+9*3, ry, "%02i", i+1);

		bool hoverlight=false;
		if(active && preview_hover_sound!=-1 && song->GetST(i)->sounds[preview_hover_sound]==true)
			hoverlight=true;

		Box bb;
		bb.SetXYWH(rx-5, ry, 52, st_spacing);
		if(bb.Contains(mousepoint) || hoverlight)
		{
			FontAlpha(90);
			if(active)
			{
				SelectFont(0);
				FontAlpha(170);
			}
			else
				SelectFont(1);
			DrawString(rx, ry, "ST %02i", i+1);

			if(!hoverlight)
				hovered_st=i;
		}

		FontAlpha(255);
	}


	if(hovered_st!=-1)
		preview_hover_channel=song->GetST(hovered_st);


	if(mouse.lclick && hovered_st!=-1)
		clicked_st=hovered_st;
	if(mouse.rclick && hovered_st!=-1)
	{
		clicked_st=hovered_st;
		clicked_right=true;
	}
	if(mouse.left && clicked_st!=-1 && hovered_st!=clicked_st)
	{
		if(song->GetST(clicked_st)!=NULL)
		{
			dragged_st=clicked_st;
			if(shift_down)
				dragged_copy=true;
			else
				dragged_copy=false;
			clicked_st=-1;
		}
	}
	if(!mouse.left && !mouse.right)
	{
		if(dragged_st!=-1 && hovered_st!=dragged_st && hovered_st!=-1)
		{
			if(dragged_copy || shift_down)
			{
				song->SelectST(hovered_st);
				song->GetST(hovered_st)->CopyFrom(song->GetST(dragged_st));
			}
			else
			{
				song->SwapSTs(dragged_st, hovered_st);
				song->SelectST(hovered_st);
			}
/*			st_editor.graphs.num=0;
			for(int i=0;i<num_st_available;i++)
				if(song->GetST(i)!=NULL)
					st_editor.graphs.graph_source[st_editor.graphs.num++]=song->GetST(i)->content;*/
			st_editor.graphs.num=num_st_available;
			for(int i=0;i<num_st_available;i++)
			{
				if(song->GetST(i)!=NULL)
					st_editor.graphs.graph_source[i]=song->GetST(i)->content;
				else
					st_editor.graphs.graph_source[i]=NULL;
			}
			SelectEditor(st_editor);
			chenv_editor.graphs.num=1;
			chenv_editor.graphs.graph_source[0]=song->GetST(song->selected_st)->envelope;

			song_modified=true;
		}
		if(clicked_st!=-1 && clicked_st==hovered_st)
		{
			if(clicked_right)
			{
				if(song->GetST(clicked_st)!=NULL)
				{
					song->RemoveST(clicked_st);
					song_modified=true;
				}
			}
			else if(clicked_st!=-1)
			{
				bool deselect=(song->selected_st==clicked_st);
				if(deselect)
				{
					song->selected_st=-1;
				}
				else
				{
					bool created=(song->GetST(clicked_st)==NULL);
					song->SelectST(clicked_st);
					if(created)
					{
						if(recent_sound!=-1)
						{
							NMChannel* st=song->GetST(song->selected_st);
							bool any_sounds=false;
							for(int i=0;i<SOUNDS_MAX;i++)
								if(st->sounds[i])
								{
									any_sounds=true;
									break;
								}
							if(!any_sounds)
								st->sounds[recent_sound]=true;
							recent_sound=-1;
						}
						else if(recent_chst!=-1)
						{
							NMChannel* st=song->GetST(song->selected_st);
							bool any_sounds=false;
							for(int i=0;i<SOUNDS_MAX;i++)
								if(st->sounds[i])
								{
									any_sounds=true;
									break;
								}
							if(!any_sounds)
							{
								auto recent_channel=song->GetCHST(recent_chst);
								assert(recent_channel);
								for(int i=0;i<SOUNDS_MAX;i++)
									st->sounds[i]=recent_channel->sounds[i];
							}
						}
					}
/*					st_editor.graphs.num=0;
					for(int i=0;i<num_st_available;i++)
						if(song->GetST(i)!=NULL)
							st_editor.graphs.graph_source[st_editor.graphs.num++]=song->GetST(i)->content;*/
					st_editor.graphs.num=num_st_available;
					for(int i=0;i<num_st_available;i++)
					{
						if(song->GetST(i)!=NULL)
							st_editor.graphs.graph_source[i]=song->GetST(i)->content;
						else
							st_editor.graphs.graph_source[i]=NULL;
					}
					SelectEditor(st_editor);
					chenv_editor.graphs.num=1;
					chenv_editor.graphs.graph_source[0]=song->GetST(song->selected_st)->envelope;
				}
				song_modified=true;
			}
		}
		clicked_st=-1;
		clicked_right=false;
		dragged_st=-1;
	}

	if(dragged_st!=-1)
	{
		SelectFont(0);
		DrawString(mouse.x-18, mouse.y-4, "ST %02i", dragged_st+1);
	}



	const int gridstep_x=st_editor.gridstep_x;
	int doublestep_x=gridstep_x*2;
	if(timeline_zoomed)
		doublestep_x*=timeline_substeps;

	blitter.SetScroll(scroll_x, 0);
	blitter.SetClipping(st_viewbox);

	for(int y=0;y<num_st_available;y++)
	{
		const int x0=gui_main_x1+11;
		int y0=gui_main_y2+8;
		int ry=y0+y*st_spacing;

		if(song->GetST(y)==NULL)
			continue;

		int gx0=scroll_x/doublestep_x-1;
		if(gx0<0) gx0=0;
		int gx1=gx0+(window.width-gui_main_x1)/doublestep_x+2;
		for(int x=gx0;x<gx1;x++)
		{
			int rx=x0+x*doublestep_x;

			int t=x;
			int qx0=t/(timesig1*timesig2)%2;
			int qx2=t%timesig2;

			int tt=1;
			if(qx2==0)
				tt=0;
			if(qx2==timesig2-1)
				tt=2;
			rx+=2;

			int lm=(y%2+qx0*2);
			if(vis_stplaying[y] || hovered_st==y)
			{
				if(lm==0) lm=-1;
				else if(lm==1) lm=2;
				else if(lm==2) lm=0;
				else if(lm==3) lm=1;
			}

			int gci=16+tt*5+lm+1;
			Sprite s;
			s.dx=rx;
			s.dy=ry-1;
			if(timeline_zoomed)
				s.source=gridcache_zoomed[gci];
			else
				s.source=gridcache[gci];
			s.region=s.source.Bounds();
			s.colorkey=0x77000000;
			blitter.DrawSpriteFast(s);
//			blitter.DrawBar(s.dx, s.dy, s.region.Width(), s.region.Height(), 0x00FFFF);
//			blitter.DrawBar(s.dx, s.dy, doublestep_x, 7, 0x00FFFF);

/*
			DrawGUIb(124+tt*13,47+lm*8,6,7, rx, ry-1);
			DrawGUIr(124+6+tt*13,47+lm*8,1,7, rx+6, ry-1, doublestep_x-6-5, 0);
			DrawGUIb(124+12-5+tt*13,47+lm*8,6,7, rx+doublestep_x-6, ry-1);*/
		}

		int gsm=1;
		if(!timeline_zoomed)
			gsm=timeline_substeps;

		gx0=scroll_x/gridstep_x*gsm-1;
		if(gx0<0) gx0=0;
		gx1=gx0+(window.width-gui_main_x1)/gridstep_x*gsm+2;
		for(int x=gx0;x<gx1;x++)
		{
			GraphPoint gp=song->GetST(y)->content->Read(x);
			if(gp.Empty())
				continue;

			const int rx=x0+1+x*gridstep_x/gsm;

//			int colormap[]={2,0,1};
			int colormap[]={0,1,3,4,2};
			int color=colormap[song->GetST(y)->color];
			DrawGUIb(22-20+color*18,150+77+12,9,9, rx-4, ry-2);
		}
	}


	GraphEditor(EPM_ST);


	blitter.SetScroll(0, 0);
	blitter.DisableClipping();

	if(song->selected_st==-1)
	{
		if(picking_sounds_target==0)
			picking_sounds=false;
		return;
	}



	if(st_editor.graphs.num==0) // TODO: if it's in any way not equal to what it should be
	{
		st_editor.graphs.num=1;
		st_editor.graphs.graph_source[0]=song->GetST(song->selected_st)->content;
		SelectEditor(st_editor);

		chenv_editor.graphs.num=1;
		chenv_editor.graphs.graph_source[0]=song->GetST(song->selected_st)->envelope;
	}




	selected_panel=1;



	DrawGUIb(156,1,51,12, gui_main_x0+3-4, gui_main_y2+7+song->selected_st*st_spacing-2);




	NMChannel* cur_st=song->GetST(song->selected_st);


	ButtonResult br;

	PrintMenu(0, 0, 0, "SOUND TRIGGER");
	PrintMenu(1, 14, 0, "%02i", song->selected_st+1);

	if(picking_sounds)
	{
		PrintMenu(2, 0,5, ".........");
		PrintMenu(0, 0,5, ".");
		PrintMenu(0, 10,5, "SELECT SOUNDS");
		PrintMenu(0, 20,8, "-  -");
		br=MenuButton(1000, 21, 8, "OK");
		if(br.clicked)
			picking_sounds=false;
	}
	else
	{
		ButtonColor(2);

		int numsounds=0;
		for(int i=0;i<SOUNDS_MAX;i++)
			if(cur_st->sounds[i])
				numsounds++;
		PrintMenu(0, 12, 2, "%2i %s (      )", numsounds, numsounds==1?"SOUND ":"SOUNDS");
		br=MenuButton(1000, 12+11, 2, "SELECT");
		if(br.clicked)
		{
			picking_sounds=true;
			picking_sounds_target=0;
		}

		ButtonColor(1);
		br=MenuButton(1020, 22, 0, "SAVE");
		if(br.clicked)
		{
			char filename[512];
			if(FileSelectorSave(filename, FST_CHANNEL, "Save Sound Trigger"))
				cur_st->SaveFile(filename);
			ResetPath();
		}
		br=MenuButton(1021, 27, 0, "OPEN");
		if(br.clicked)
		{
			char filename[512];
			if(FileSelectorLoad(filename, FST_CHANNEL, "Open Sound Trigger"))
				cur_st->OpenFile(filename);
			ResetPath();
		}

		PrintMenu(0, 1, 2, "COLOR");
		ButtonColor((1+cur_st->color)%5);
		static const char* colornames[]={"\"A\"", "\"B\"", "\"C\"", "\"D\"", "\"E\"", "?"};
		br=MenuButton(1001, 1+6, 2, colornames[cur_st->color]);
		br.ClickTick(cur_st->color, 5);
		ButtonColor(1);

		PrintMenu(0, 20, 4, "VOLUME");
		br=MenuButton(1002, 20+7, 4, "%i", cur_st->volume);
		cur_st->volume+=br.delta; cur_st->volume=std::min(std::max(cur_st->volume, 0), 15);
		if(br.rclicked)
		{
			if(cur_st->volume==0)
				cur_st->volume=15;
			else
				cur_st->volume=0;
		}

		int ni=cur_st->basepitch;
		ButtonColor(2);
		PrintMenu(0, 1, 4, "BASE PITCH");
		br=MenuButton(1004, 1+11, 4, "%c%c%i", 'A'+notegrid_key[ni%12], notegrid_type[ni%12]==1?'#':'-', ni/12);
		cur_st->basepitch+=br.delta; cur_st->basepitch=std::min(std::max(cur_st->basepitch, 0), 119);
	}
}


Image gc_input;
Image gc_output;

unsigned char* gcx_data=NULL;
int gcx_size=0;

void GCompress(Image& input, unsigned char** output_data, int* output_size)
{
	Bitstream bitstream;

	unsigned int palette[256];
	int numcolors=0;
	unsigned char* pimage=new unsigned char[(input.width+16)*(input.height+16)];
	int pistride=(input.width+15)/8*8;
	for(int y=0;y<input.height;y++)
		for(int x=0;x<input.width;x++)
		{
			unsigned int p=input.data[y*input.width+x];
			int pi=0;
			bool addcolor=true;
			for(int j=0;j<numcolors;j++)
				if(p==palette[j])
				{
					addcolor=false;
					pi=j;
					break;
				}
			if(addcolor)
			{
				pi=numcolors;
				palette[numcolors++]=p;
				if(numcolors==256)
					break;
			}
			pimage[y*pistride+x]=pi;
		}
	int bitdepth=0;
	int nct=numcolors-1;
	for(int i=0;i<8;i++)
	{
		bitdepth++;
		nct>>=1;
		if(nct==0)
			break;
	}

	bitstream.AppendValue(input.width, 24);
	bitstream.AppendValue(input.height, 24);
	int widthbits=0;
	int wbt=input.width-1;
	for(int i=0;i<24;i++)
	{
		widthbits++;
		wbt>>=1;
		if(wbt==0)
			break;
	}
	bitstream.AppendValue(widthbits, 5);
	int heightbits=0;
	int hbt=input.height-1;
	for(int i=0;i<24;i++)
	{
		heightbits++;
		hbt>>=1;
		if(hbt==0)
			break;
	}
	bitstream.AppendValue(heightbits, 5);
	bitstream.AppendValue(numcolors&0xFF, 8); // 0 means 256
	for(int i=0;i<numcolors;i++)
		bitstream.AppendValue(palette[i], 32);

	unsigned char tile[64];
	unsigned char tpalette[64];

	int stats_rawbits=0;
	int stats_xrubits=0;

	int tw=(input.width+7)/8;
	int th=(input.height+7)/8;
	int* idqueue=new int[tw+th];
	unsigned int* reuse_map=new unsigned int[tw*th];
	for(int i=0;i<tw*th;i++)
		reuse_map[i]=0xFFFFFFFF;
	unsigned int* hash_map=new unsigned int[input.width*input.height];
	for(int y0=0;y0<input.height;y0++)
		for(int x0=0;x0<input.width;x0++)
		{
			unsigned int hash=0;
			for(int y=0;y<8;y+=2)
				for(int x=(y/2)&1;x<8;x+=2)
				{
					unsigned int h0=pimage[(y0+y)*pistride+(x0+x)];
					unsigned int h=x+y+19;
					h^=(h0<<(x*5+y*3));
					h^=(h0<<(x*4+y));
					hash^=h;
					hash=(hash<<3)|((hash>>29)&7);
				}
			hash_map[y0*input.width+x0]=hash;
		}
	int mru_x=0;
	int mru_y=0;
	int wc=std::max(tw, th);
	for(int wi=0;wi<wc;wi++)
	{
		int tqi=0;
		if(wi<th)
		{
			int w=std::min(wi, tw);
			for(int tx=0;tx<w;tx++)
				idqueue[tqi++]=wi*tw+tx;
		}
		if(wi<tw)
		{
			int h=std::min(wi+1, th);
			for(int ty=0;ty<h;ty++)
				idqueue[tqi++]=ty*tw+wi;
		}
		int dbt=wi*8;
		int curdimbits=0;
		for(int j=0;j<16;j++)
		{
			curdimbits++;
			dbt>>=1;
			if(dbt==0)
				break;
		}
		mru_x=std::min(wi-1, tw-1);
		mru_y=std::min(wi-1, th-1);
		for(int qi=0;qi<tqi;qi++)
		{
			int ti=idqueue[qi];
			int tx=ti%tw;
			int ty=ti/tw;

			int x0=tx*8;
			int y0=ty*8;

			for(int y=0;y<8;y++)
				for(int x=0;x<8;x++)
					tile[y*8+x]=pimage[(y0+y)*pistride+(x0+x)];

			// search for reuse
			{
				bool match;
				int prx, pry;
				if(tx>0)
				{
					prx=reuse_map[ty*tw+tx-1]&0xFFFF;
					pry=(reuse_map[ty*tw+tx-1]>>16)&0xFFFF;
					if(prx!=0xFFFF) // continue from left?
					{
						prx+=8;
						match=true;
						if(prx>mru_x*8)
						{
							if(prx>tx*8 || pry>ty*8-8)
								match=false;
						}
						if(match)
						{
							for(int y=0;y<8;y++)
								for(int x=0;x<8;x++)
									if(tile[y*8+x]!=pimage[(pry+y)*pistride+(prx+x)])
									{
										match=false;
										y=99;
										break;
									}
						}
						if(match)
						{
							bitstream.AppendValue(1, 1); // reuse: continue from left
							reuse_map[ty*tw+tx]=(pry<<16)|prx;
							continue;
						}
					}
				}
				if(ty>0)
				{
					prx=reuse_map[(ty-1)*tw+tx]&0xFFFF;
					pry=(reuse_map[(ty-1)*tw+tx]>>16)&0xFFFF;
					if(prx!=0xFFFF) // continue from above?
					{
						pry+=8;
						match=true;
						if(pry>mru_y*8)
						{
							if(pry>ty*8 || prx>tx*8-8)
								match=false;
						}
						if(match)
						{
							for(int y=0;y<8;y++)
								for(int x=0;x<8;x++)
									if(tile[y*8+x]!=pimage[(pry+y)*pistride+(prx+x)])
									{
										match=false;
										y=99;
										break;
									}
						}
						if(match)
						{
							bitstream.AppendValue(0, 1); // reuse: not from left
							bitstream.AppendValue(1, 1); // reuse: continue from above
							reuse_map[ty*tw+tx]=(pry<<16)|prx;
							continue;
						}
					}
				}

				int rxmax=std::max(tx, mru_x);
				int rymax=std::max(ty, mru_y);

				const int checksize=8;
				int best_area=0;
				int max_area=checksize*checksize+1;
				int best_rx=0;
				int best_ry=0;

				int rxend=rxmax*8;
				int ryend=rymax*8;
				int rxd=0;
				int ryd=-1;
				int rx=0;
				int ry=1;
				int rsize=0;
				int rsizeend=std::max(rxend, ryend);

				while(rsize<=rsizeend)
				{
					rx+=rxd;
					ry+=ryd;
					if(ryd==1 && ry==ryend+1)
					{
						rsize++;
						rx++;
						ry=ryend;
						ryd=-1;
					}
					if(rxd==1 && rx==rxend+1)
					{
						rsize++;
						ry++;
						rx=rxend;
						rxd=-1;
					}
					if(ryd==-1 && ry==-1)
					{
						rsize++;
						rx++;
						ry=0;
						ryd=1;
					}
					if(ryd==1 && ry==rsize)
					{
						ryd=0;
						rxd=-1;
					}
					if(rxd==-1 && rx==-1)
					{
						rsize++;
						ry++;
						rx=0;
						rxd=1;
					}
					if(rxd==1 && rx==rsize)
					{
						rxd=0;
						ryd=-1;
					}

					if(rx+8>=x0 && ry+8>=y0)
						continue;

					for(int mh=0;mh<checksize;mh++)
					{
						int rry=ry+mh*8;
						if((rry+7)/8>=th)
							break;
						int ry0=y0+mh*8;
						if((ry0+7)/8>=th)
							break;
						for(int mw=0;mw<checksize;mw++)
						{
							int rrx=rx+mw*8;
							if((rrx+7)/8>=tw)
								break;
							int rx0=x0+mw*8;
							if((rx0+7)/8>=tw)
								break;
							match=true;
							if(hash_map[rry*input.width+rrx]!=hash_map[ry0*input.width+rx0])
								match=false;
							else
							{
								for(int y=0;y<8;y++)
									for(int x=0;x<8;x++)
										if(pimage[(ry0+y)*pistride+(rx0+x)]!=pimage[(rry+y)*pistride+(rrx+x)])
										{
											match=false;
											y=99;
											break;
										}
							}
							if(match)
							{
								int area=(mw+1)*(mh+1);
								if(rrx/8<mru_x-checksize && rry/8<mru_y-checksize) // bonus score for safe sources
									area+=1;
								if(area>best_area)
								{
									best_area=area;
									best_rx=rx;
									best_ry=ry;
									if(best_area==max_area)
									{
										mh=99999;
										break;
									}
								}
							}
							else
							{
								if(mw==0) // first vertical column ends here, can't get any taller
									mh=99999;
								break;
							}
						}
					}

					if(best_area==max_area)
					{
						ry=-99999;
						break;
					}
				}
				if(best_area>0) // TODO: keep evaluating reuse points and find the one with largest matching rectangle (for optimum continue later) - use similar rect growth as with tilemap
				{
					bitstream.AppendValue(0, 1); // reuse: not from left
					bitstream.AppendValue(0, 1); // reuse: not from above
					bitstream.AppendValue(1, 1); // reuse: explicit coordinates
					bitstream.AppendValue(best_rx, std::min(curdimbits, widthbits)); // rx
					bitstream.AppendValue(best_ry, std::min(curdimbits, heightbits)); // ry
					reuse_map[ty*tw+tx]=(best_ry<<16)|best_rx;

					stats_xrubits+=3+std::min(curdimbits, widthbits)+std::min(curdimbits, heightbits);

					continue;
				}
			}

			reuse_map[ty*tw+tx]=0xFFFFFFFF;
			bitstream.AppendValue(0, 1); // reuse: not from left
			bitstream.AppendValue(0, 1); // reuse: not from above
			bitstream.AppendValue(0, 1); // reuse: not at all

			stats_rawbits+=3;

			int tpsize=0;
			for(int i=0;i<64;i++)
			{
				unsigned char p=tile[i];
				int pi=0;
				bool addc=true;
				for(int j=0;j<tpsize;j++)
					if(tpalette[j]==p)
					{
						addc=false;
						pi=j;
						break;
					}
				if(addc)
				{
					pi=tpsize;
					tpalette[tpsize++]=p;
				}
				tile[i]=pi;
			}
			int nbits=0;
			int psv=tpsize-1;
			for(int b=0;b<6;b++)
			{
				nbits++;
				psv>>=1;
				if(psv==0)
					break;
			}
			bitstream.AppendValue(tpsize, bitdepth);
			stats_rawbits+=bitdepth;
			if(tpsize==1) // solid color
			{
				bitstream.AppendValue(tpalette[0], bitdepth);
				stats_rawbits+=bitdepth;
			}
			else
			{
				if(tpsize<numcolors/2 && tpsize<32)
				{
					for(int j=0;j<tpsize;j++)
					{
						bitstream.AppendValue(tpalette[j], bitdepth);
						stats_rawbits+=bitdepth;
					}
				}
				else
				{
					nbits=bitdepth;
					for(int y=0;y<8;y++)
						for(int x=0;x<8;x++)
							tile[y*8+x]=pimage[(y0+y)*pistride+(x0+x)];
				}

				int sparse_size=0;
				int raw_size=64*nbits;
				unsigned char fill_values[8];
				unsigned char fill_map[8*16];
				int predict_size[8];
				for(int bi=0;bi<nbits;bi++)
				{
					predict_size[bi]=0;
					int mask[16];
					for(int i=0;i<16;i++)
						mask[i]=0;
					for(int i=0;i<64;i++)
					{
						int fmi=(i/8/2)*4+i%8/2;
						int bit=(tile[i]>>bi)&1;
						mask[fmi]|=bit+1;
					}
					int bcount[2];
					bcount[0]=0;
					bcount[1]=0;
					for(int i=0;i<16;i++)
					{
						if(mask[i]==1)
							bcount[0]++;
						if(mask[i]==2)
							bcount[1]++;
					}
					int nf=std::max(bcount[0], bcount[1]);
					int nr=16-nf;
					int size=2+16+nr*4;
					if(size<1+64)
					{
						fill_values[bi]=0;
						if(bcount[1]>bcount[0])
							fill_values[bi]=1;
						for(int i=0;i<16;i++)
							fill_map[bi*16+i]=((mask[i]==fill_values[bi]+1)?1:0);
						sparse_size+=size;
						predict_size[bi]=size;
					}
					else
					{
						fill_values[bi]=2;
						sparse_size+=1+64;
					}
				}
				if(sparse_size<raw_size)
				{
					bitstream.AppendValue(1, 1); // sparse fill
					stats_rawbits+=1;
					for(int bi=0;bi<nbits;bi++)
					{
						if(fill_values[bi]!=2) // sparse bitplane
						{
							int testsize=0;

							bitstream.AppendValue(1, 1); // fill flag (sparse)
							stats_rawbits+=1;
							bitstream.AppendValue(fill_values[bi], 1); // fill value
							stats_rawbits+=1;

							testsize+=2;

							for(int i=0;i<16;i++)
							{
								bitstream.AppendValue(fill_map[bi*16+i], 1);
								stats_rawbits+=1;
							}

							testsize+=16;

							for(int i=0;i<64;i++)
							{
								int fmi=(i/8/2)*4+i%8/2;
								if(fill_map[bi*16+fmi]==0)
								{
									bitstream.AppendValue((tile[i]>>bi)&1, 1);
									stats_rawbits+=1;

									testsize++;
								}
							}
						}
						else // raw bitplane
						{
							bitstream.AppendValue(0, 1); // fill flag (raw)
							stats_rawbits+=1;
							for(int i=0;i<64;i++)
							{
								bitstream.AppendValue((tile[i]>>bi)&1, 1);
								stats_rawbits+=1;
							}
						}
					}
				}
				else
				{
					bitstream.AppendValue(0, 1); // no sparse fill
					for(int i=0;i<64;i++)
					{
						bitstream.AppendValue(tile[i], nbits);
						stats_rawbits+=nbits;
					}
				}
			}
		}
	}
	delete[] idqueue;
	delete[] pimage;
	delete[] reuse_map;

	*output_size=(int)bitstream.ByteLength();
	*output_data=new unsigned char[*output_size];
	bitstream.WriteToBuffer(*output_data);

	//int totalbits=*output_size*8;
}

void GExtract(unsigned char* input_data, int input_size, Image& output)
{
	Bitstream bitstream;
	bitstream.FromBuffer(input_data, input_size);

	output.width=bitstream.ReadValue(24);
	output.height=bitstream.ReadValue(24);
	int widthbits=bitstream.ReadValue(5);
	int heightbits=bitstream.ReadValue(5);
	output.data=new unsigned int[output.width*output.height];
	int numcolors=bitstream.ReadValue(8);
	if(numcolors==0)
		numcolors=256;
	unsigned int palette[256];
	for(int i=0;i<numcolors;i++)
		palette[i]=bitstream.ReadValue(32);
	int bitdepth=0;
	int nct=numcolors-1;
	for(int i=0;i<8;i++)
	{
		bitdepth++;
		nct>>=1;
		if(nct==0)
			break;
	}

	unsigned char* pimage=new unsigned char[(output.width+15)*(output.height+15)];
	int pistride=(output.width+15)/8*8;

	unsigned char tile[64];
	unsigned char tpalette[64];

	int stats_palettesize=0;
	int stats_nbits=0;
	int stats_nbitsmax=0;
	int stats_nbitsmin=999;
	int stats_count=0;

	int tw=(output.width+7)/8;
	int th=(output.height+7)/8;

	int* debugmap=new int[tw*th];
	for(int i=0;i<tw*th;i++)
		debugmap[i]=0;

	int* idqueue=new int[tw+th];
	unsigned int* reuse_map=new unsigned int[tw*th];
	for(int i=0;i<tw*th;i++)
		reuse_map[i]=0xFFFFFFFF;
	int wc=std::max(tw, th);
	for(int wi=0;wi<wc;wi++)
	{
		int tqi=0;
		if(wi<th)
		{
			int w=std::min(wi, tw);
			for(int tx=0;tx<w;tx++)
				idqueue[tqi++]=wi*tw+tx;
		}
		if(wi<tw)
		{
			int h=std::min(wi+1, th);
			for(int ty=0;ty<h;ty++)
				idqueue[tqi++]=ty*tw+wi;
		}
		int dbt=wi*8;
		int curdimbits=0;
		for(int j=0;j<16;j++)
		{
			curdimbits++;
			dbt>>=1;
			if(dbt==0)
				break;
		}
		for(int qi=0;qi<tqi;qi++)
		{
			int ti=idqueue[qi];
			int tx=ti%tw;
			int ty=ti/tw;

			int y0=ty*8;
			int x0=tx*8;

			int rx=-1, ry=-1;
			bool any_reuse=false;
			if(bitstream.ReadValue(1)) // reuse from left
			{
				rx=(reuse_map[ty*tw+tx-1]&0xFFFF)+8;
				ry=(reuse_map[ty*tw+tx-1]>>16)&0xFFFF;

//						debugmap[ty*tw+tx]=0;
				any_reuse=true;
			}
			else if(bitstream.ReadValue(1)) // reuse from above
			{
				rx=(reuse_map[(ty-1)*tw+tx]&0xFFFF);
				ry=((reuse_map[(ty-1)*tw+tx]>>16)&0xFFFF)+8;

//						debugmap[ty*tw+tx]=1;
				any_reuse=true;
			}
			else if(bitstream.ReadValue(1)) // explicit coordinates
			{
				rx=bitstream.ReadValue(std::min(curdimbits, widthbits));
				ry=bitstream.ReadValue(std::min(curdimbits, heightbits));

//				debugmap[ty*tw+tx]=3;

				any_reuse=true;
			}
			if(any_reuse)
			{
				for(int y=0;y<8;y++)
					for(int x=0;x<8;x++)
						pimage[(y0+y)*pistride+(x0+x)]=pimage[(ry+y)*pistride+(rx+x)];
				reuse_map[ty*tw+tx]=(ry<<16)|rx;
				continue;
			}
			reuse_map[ty*tw+tx]=0xFFFFFFFF;

			int nbits=0;
			int tpsize=bitstream.ReadValue(bitdepth);
			if(tpsize==0)
				tpsize=numcolors;
			if(tpsize==1)
			{
				tpalette[0]=bitstream.ReadValue(bitdepth);
				for(int i=0;i<64;i++)
					tile[i]=0;

//				debugmap[ty*tw+tx]=2;
			}
			else
			{
				int psv=tpsize-1;
				for(int b=0;b<6;b++)
				{
					nbits++;
					psv>>=1;
					if(psv==0)
						break;
				}
				if(tpsize<numcolors/2 && tpsize<32)
				{
					for(int j=0;j<tpsize;j++)
						tpalette[j]=bitstream.ReadValue(bitdepth);
				}
				else
				{
					nbits=bitdepth;
					for(int j=0;j<numcolors;j++)
						tpalette[j]=j;
				}

				if(bitstream.ReadValue(1)) // sparse fill
				{
					for(int i=0;i<64;i++)
						tile[i]=0;
					for(int bi=0;bi<nbits;bi++)
					{
						if(bitstream.ReadValue(1)) // sparse bitplane
						{
							unsigned char mask[16];
							int fill_value=bitstream.ReadValue(1);
							for(int i=0;i<16;i++)
							{
								int f=bitstream.ReadValue(1);
								mask[i]=f;
								if(f==1)
								{
									const int ty0=i/4*2;
									const int tx0=i%4*2;
									for(int j=0;j<4;j++)
									{
										int y=j/2;
										int x=j%2;
										tile[(ty0+y)*8+(tx0+x)]|=fill_value<<bi;
									}
								}
							}
							for(int i=0;i<64;i++)
							{
								int fmi=(i/8/2)*4+i%8/2;
								if(mask[fmi]==0)
								{
									int b=bitstream.ReadValue(1);
									tile[i]|=b<<bi;
								}
							}
						}
						else // raw bitplane
						{
							for(int i=0;i<64;i++)
							{
								int b=bitstream.ReadValue(1);
								tile[i]|=b<<bi;
							}
						}
					}
//					debugmap[ty*tw+tx]=5;
				}
				else // raw
				{
					for(int i=0;i<64;i++)
						tile[i]=bitstream.ReadValue(nbits);
//					debugmap[ty*tw+tx]=4;
				}
			}

			for(int y=0;y<8;y++)
				for(int x=0;x<8;x++)
					pimage[(y0+y)*pistride+(x0+x)]=tpalette[tile[y*8+x]];

			stats_palettesize+=tpsize;
			stats_nbits+=nbits;
			stats_nbitsmax=std::max(nbits, stats_nbitsmax);
			stats_nbitsmin=std::min(nbits, stats_nbitsmin);
			stats_count++;
		}
	}

	for(int y=0;y<output.height;y++)
		for(int x=0;x<output.width;x++)
			output.data[y*output.width+x]=palette[pimage[y*pistride+x]];

	delete[] idqueue;
	delete[] pimage;
	delete[] reuse_map;

//	delete[] debugmap;
}

void Graphics()
{
}

void GPK_Encode(unsigned int* pixels, int width, int height, unsigned char** output_buffer, int* output_size)
{
	gc_input.data=pixels;
	gc_input.width=width;
	gc_input.height=height;
	GCompress(gc_input, output_buffer, output_size);
}

void GPK_Decode(unsigned int** pixels, int* width, int* height, unsigned char* input_buffer, int input_size)
{
	GExtract(input_buffer, input_size, gc_output);
	*pixels=gc_output.data;
	*width=gc_output.width;
	*height=gc_output.height;
}


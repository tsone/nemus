
#include "version.h"
#include "port.h"
#include <ctime>

#define rnd(n) (rand()%n)

#include "glr/glr.h"


#define BYTE unsigned char
#define DWORD unsigned int
struct PNGImage
{
	int width;
	int height;
	DWORD* data;
};


#include "bitstream.h"
#include "blitter.h"

Blitter blitter;

Image window;
Image guibits;

Image guibits_orig;
Image font_orig;

#include "font.h"

const int window_width=1280;
const int window_height=720;
bool redraw_window=false;

#include "editor.h"

#include "graphics.h"
void GPK_Encode(unsigned int* pixels, int width, int height, unsigned char** output_buffer, int* output_size);
void GPK_Decode(unsigned int** pixels, int* width, int* height, unsigned char* input_buffer, int input_size);

bool debugmode=false;

#include "generated/guibits_gen.h"
#include "generated/font_gen.h"

char programdir[1024];
void ResetPath()
{
	glrSetCurrentDirectory(programdir);
}

glrBool glrAppInit()
{
	srand((unsigned int)time(0));

	glrGetCurrentDirectory(1024, programdir);

	FILE* file=fopen("debugbuild.txt", "rb");
	if(file!=NULL)
	{
		debugmode=true;
		glrEnableLog();
		fclose(file);
	}


	glrOpenWindow("nemus", window_width, window_height, GLR_MODE_BITMAP);

	GPK_Decode(&guibits.data, &guibits.width, &guibits.height, (unsigned char*)guibits_gpk, guibits_size);
	GPK_Decode(&font.data, &font.width, &font.height, (unsigned char*)font_gpk, font_size);

	guibits_orig.CopyFrom(guibits);
	font_orig.CopyFrom(font);

	InitFont();

	InitEditor();

	return true;
}

void glrAppExit()
{
	CloseEditor();
}


glrBool glrAppUpdate()
{
	window.data=glr_buffer.data;
	window.width=glr_buffer.width;
	window.height=glr_buffer.height;
//	for(int i=0;i<buffer.width*buffer.height;i++)
//		buffer.data[i]=0x000000;
	redraw_window=false;

	Editor();
//	Sleep(1);

	blitter.SetTarget(window); // to allow proper resize detection at start of next frame (only used for editor right now)

	if(redraw_window)
		glrDisplayPixelBuffer();

	if(glrKeyPressed(GLR_KEY_ESCAPE) && debugmode)
		return false;
/*
	if(frt0==0)
		frt0=glrAppTimeMs();
	fc++;
	unsigned int t=glrAppTimeMs();
	if(t>frt0+1000)
	{
		glrLog("%i fps\n", fc*1000/(glrAppTimeMs()-frt0));
		frt0=t;
		fc=0;
	}
*/
	return true;
}

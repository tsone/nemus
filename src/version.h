#ifndef VERSION_H
#define VERSION_H

#define STR2(s) #s
#define STR(s) STR2(s)

#define VER_MAJOR               0
#define VER_MINOR               9
#define VER_REVISION            0
#define VER_BUILD               0

#define VER_DISPLAY             STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_REVISION)

#define VER_FILE_DESCRIPTION_STR    "nemus - NES music editor"
#define VER_FILE_VERSION            VER_MAJOR, VER_MINOR, VER_REVISION, VER_BUILD
#define VER_FILE_VERSION_STR        STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_REVISION) "." STR(VER_BUILD)

#define VER_PRODUCTNAME_STR         "nemus"
#define VER_PRODUCT_VERSION         VER_FILE_VERSION
#define VER_PRODUCT_VERSION_STR     VER_FILE_VERSION_STR
#define VER_ORIGINAL_FILENAME_STR   VER_PRODUCTNAME_STR "_" STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_REVISION) ".exe"

#define VER_INTERNAL_NAME_STR       VER_ORIGINAL_FILENAME_STR
#define VER_COPYRIGHT_STR           "Copyright (C) 2015 Tomas Pettersson, 2016 Valtteri Heikkila"
#define VER_COMPANY_NAME_STR        "Tomas Pettersson, Valtteri Heikkila"

#endif // VERSION_H

#include "fileselector.h"
#import <Cocoa/Cocoa.h>

// Internal shared
extern char chosen_filename[FST_COUNT][512];
extern char chosen_file[FST_COUNT][512];
const char* FileSelectorGetFilter(int type);

static int FileSelectorCocoa(char* filename, int type, const char* title, int is_save)
{
	NSWindow* keyWindow=[NSApp keyWindow];

	NSMutableArray* allowed_exts=@[].mutableCopy;
	const char* desc=FileSelectorGetFilter(type);
	const char* ext=desc+strlen(desc)+1;
	while(*desc && *ext)
	{
		[allowed_exts addObject:@(ext+2)];
		desc=ext+strlen(ext)+1;
		ext=desc+strlen(desc)+1;
	}

	NSSavePanel* p;
	if(is_save)
	{
		p=[NSSavePanel savePanel];
		[p setPrompt:@"Save"];
		[p setNameFieldLabel:@"Save As:"];
		if(chosen_file[type][0])
			[p setNameFieldStringValue:@(chosen_file[type])];
		[p setExtensionHidden:NO];
	}
	else
	{
		NSOpenPanel* o=[NSOpenPanel openPanel];
		[o setCanChooseFiles:YES];
		[o setCanChooseDirectories:NO];
		[o setAllowsMultipleSelection:NO];
		[o setPrompt:@"Open"];
		p = o;
	}

	[p setTitle:@(title)];
	[p setAllowedFileTypes:allowed_exts];

	if ([p runModal]==NSFileHandlingPanelOKButton)
	{
		strcpy(chosen_filename[type], [[[p URL] path] UTF8String]);
		strcpy(chosen_file[type], [[[p URL] lastPathComponent] UTF8String]);
		strcpy(filename, chosen_filename[type]);
		[keyWindow makeKeyWindow];
		return 1;
	}
	else
	{
		[keyWindow makeKeyWindow];
		return 0;
	}
}

int FileSelectorSave(char* filename, int type, const char* title)
{
	return FileSelectorCocoa(filename, type, title, 1);
}

int FileSelectorLoad(char* filename, int type, const char* title)
{
	return FileSelectorCocoa(filename, type, title, 0);
}

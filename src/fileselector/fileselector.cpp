#include "fileselector.h"
#ifdef __APPLE__
extern "C" {
#endif

// Internal shared
char chosen_filename[FST_COUNT][512]={{0}};
char chosen_file[FST_COUNT][512]={{0}};
const char* FileSelectorGetFilter(FileSelectorType type)
{
	switch(type)
	{
	case FST_SONG_OPEN:
		return "All song files\0*.nms;*.nmc\0NMS files\0*.nms\0NMC files\0*.nmc\0\0All Files (*.*)\0*.*\0";
	case FST_SONG_SAVE:
		return "NMS files\0*.nms\0NMC files\0*.nmc\0\0All Files (*.*)\0*.*\0";
	case FST_SOUND:
		return "NMA files\0*.nma\0\0All Files (*.*)\0*.*\0";
	case FST_CHANNEL:
		return "NMM files\0*.nmm\0\0All Files (*.*)\0*.*\0";
	case FST_SAMPLE:
		return "DMC files\0*.dmc\0\0All Files (*.*)\0*.*\0";
	case FST_NSF:
		return "NSF files\0*.nsf\0\0All Files (*.*)\0*.*\0";
	case FST_ASSEMBLY:
		return "S files\0*.s\0ASM files\0*.asm\0\0All Files (*.*)\0*.*\0";
	case FST_WAV:
		return "WAV files\0*.wav\0\0All Files (*.*)\0*.*\0";
	default:
		return 0;
	}
}

#ifdef __APPLE__
}
#endif

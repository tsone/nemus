#include "fileselector.h"
#include <windows.h>
#include <commdlg.h>
#include <stdlib.h>
#include "../glr/glr.h"

// Internal shared
extern char chosen_filename[FST_COUNT][512];
extern char chosen_file[FST_COUNT][512];
const char* FileSelectorGetFilter(FileSelectorType type);

static char fsel_defext[8];

int FileSelectorSave(char* filename, int type, const char* title)
{
	HWND hwnd=(HWND)glrWindowNativeHandle();
	char workingdir[512];
	GetCurrentDirectory(512, workingdir);

	const char* filter=FileSelectorGetFilter((FileSelectorType)type);
	static OPENFILENAME dia;
	dia.lStructSize=sizeof(OPENFILENAME);
	dia.hwndOwner=hwnd;
	dia.lpstrFile=chosen_filename[type];
	dia.nMaxFile=_MAX_DIR;
	dia.lpstrFileTitle=chosen_file[type];
	dia.nMaxFileTitle=_MAX_FNAME;
	dia.lpstrInitialDir=NULL;
	dia.lpstrFilter=filter;
	dia.lpstrDefExt=fsel_defext;
	if(title!=NULL)
		dia.lpstrTitle=title;
	else
		dia.lpstrTitle="Save As";
	dia.Flags=OFN_EXPLORER | OFN_OVERWRITEPROMPT;
	if(!GetSaveFileName(&dia))
	{
		SetCurrentDirectory(workingdir);
		return 0;
	}
	strcpy(filename, chosen_filename[type]);

	SetCurrentDirectory(workingdir);

	return 1;
}

int FileSelectorLoad(char* filename, int type, const char* title)
{
	HWND hwnd=(HWND)glrWindowNativeHandle();
	char workingdir[512];
	GetCurrentDirectory(512, workingdir);

	const char* filter=FileSelectorGetFilter((FileSelectorType)type);
	static OPENFILENAME dia;
	dia.lStructSize=sizeof(OPENFILENAME);
	dia.hwndOwner=hwnd;
	dia.lpstrFile=chosen_filename[type];
	dia.nMaxFile=_MAX_DIR;
	dia.lpstrFileTitle=chosen_file[type];
	dia.nMaxFileTitle=_MAX_FNAME;
	dia.lpstrInitialDir=NULL;
	dia.lpstrFilter=filter;
	dia.lpstrDefExt=fsel_defext;
	if(title!=NULL)
		dia.lpstrTitle=title;
	else
		dia.lpstrTitle="Open";
	dia.Flags=OFN_EXPLORER;
	if(!GetOpenFileName(&dia))
	{
		SetCurrentDirectory(workingdir);
		return 0;
	}
	strcpy(filename, chosen_filename[type]);

	SetCurrentDirectory(workingdir);

	return 1;
}

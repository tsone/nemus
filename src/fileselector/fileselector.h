#ifndef FILESELECTOR_H
#define FILESELECTOR_H
#if defined(__APPLE__) && defined(__cplusplus)
extern "C" {
#endif

typedef enum FileSelectorType
{
	FST_NONE,
	FST_SONG_OPEN,
	FST_SONG_SAVE,
	FST_SOUND,
	FST_CHANNEL,
	FST_SAMPLE,
	FST_NSF,
	FST_ASSEMBLY,
	FST_WAV,
	//
	FST_COUNT
} FileSelectorType;

int FileSelectorSave(char* filename, int type, const char* title);

int FileSelectorLoad(char* filename, int type, const char* title);

#if defined(__APPLE__) && defined(__cplusplus)
}
#endif
#endif


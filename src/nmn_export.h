#ifndef NMN_EXPORT_H
#define NMN_EXPORT_H

#include <array>
#include <string>
#include <functional>
#include <map>

#include "version.h"

// TODO: tsone: add in-editor messaging/log
#define MSG(...) printf(__VA_ARGS__)
#define WRN(...) MSG("WARNING: " __VA_ARGS__)
#define ERR(...) MSG("ERROR: " __VA_ARGS__)

// Options: (if changed, also update nk audio and rebuild nmn.h)
#define NM_DICT_SIZE      256 // 256 or 128

#define DISABLE_DICT 0

#define DEBUG_CAPTURE     0
#define DEBUG_FILTERED    0
#define DEBUG_STREAM      0
#define DEBUG_DICT        0
#define DEBUG_CHN_VOL     0
#define DEBUG_NSF_DRIVER  0
#define DEBUG_SUMMARY     0

template <int N>
inline int BA(int x_)
{
	assert((x_)>=0 && (x_)<(1<<(N)) && "num bits mismatch");
	return x_;
}

inline int align(int x, int b)
{
	assert(x>=0 && b>=1);
	return x+b-1-(x-1)%b;
}

struct NMNByteBuffer : protected std::vector<unsigned char>
{
	void Add(int v)
	{
		push_back(BA<8>(v));
	}

	void Add(const NMNByteBuffer& b)
	{
		insert(end(), b.begin(), b.end());
	}

	void Clear() { clear(); }
	size_t Size() const { return size(); }

	bool IsEqual(const NMNByteBuffer &o) const
	{
		return *this==o;
	}

	bool IsEqual(const NMNByteBuffer &o, unsigned i0, unsigned i1) const
	{
		return equal(o.begin()+i0, o.begin()+i1, begin()+i0);
	}

	static std::string FormatHex(int v)
	{
		char buf[16];
		sprintf(buf, "$%02X", v);
		return buf;
	}

	void Print(FILE *f, unsigned row_bytes=UINT_MAX, std::function<std::string(int v)> formatter=nullptr) const
	{
		if(empty())
			return;

		if(!formatter)
			formatter=FormatHex;

		auto rows=(Size()+row_bytes-1)/row_bytes;
		for(size_t i=0;i<rows;i++)
		{
			fprintf(f, "\n    .BYTE   ");
			auto end=std::min(Size(),(i+1)*row_bytes)-1;
			for(size_t j=i*row_bytes;j<=end;j++)
			{
				auto c=j!=end?", ":"";
				fprintf(f, "%s%s", formatter(at(j)).c_str(), c);
			}
		}
	}

	size_t Size()
	{
		return size();
	}

	const void* Data() const
	{
		return data();
	}
};

struct NMNFrame
{
	struct Value
	{
		int value;
		bool set;

		void Set(int v)
		{
			value=v;
			set=true;
		}
	};
	Value delay;
	Value len;
	Value note;
	Value slope;
	Value instrument;
	Value offset;
	Value arp_offset;
	int venv_offset;
	int penv_offset;

	bool meta_trg;
	bool meta_rel;
	bool loop_start;

	void Encode(NMNByteBuffer &buf) const
	{
		assert(!(meta_trg && offset.set));

		// filters out zero slopes
		const bool has_slope=(slope.set && slope.value);

		int v;

		// command byte
		v=(offset.set<<7)
			+(meta_trg<<6)
			+(delay.set<<5)
			+(len.set<<4)
			+(arp_offset.set<<3)
			+((note.set || has_slope)<<2)
			+(instrument.set<<1)
		;
		buf.Add(v);

		if(instrument.set)
		{
			v=instrument.value;
			buf.Add(v);
		}

		if(note.set || has_slope)
		{
			if(note.set)
			{
				assert(note.value>=0 && note.value<=127);
				v=(note.value<<1)+has_slope;
				assert(v>=0 && v<0xFF);
			}
			else
			{
				v=0xFF; // no note, sadly the byte is wasted
				assert(has_slope);
			}
			buf.Add(v);

			if(has_slope)
			{
				v=slope.value;
				buf.Add(v&0xFF);
				buf.Add((v>>8)&0xFF);
			}
		}

		if(arp_offset.set)
		{
			v=arp_offset.value;
			buf.Add(v);
		}

		if(len.set)
		{
			v=len.value-1;
			if(v<0)
			{
				WRN("note len %d clamped to 0\n", v);
				v=0;
			}
			if(v>32767)
			{
				WRN("note len %d clamped to 32767\n", v);
				v=32767;
			}
			if(v<=0x7F)
			{
				buf.Add(v<<1);
			}
			else
			{
				buf.Add(((v>>8)<<1)+1);
				buf.Add(v&0xFF);
			}
		}

		if(delay.set)
		{
			buf.Add(delay.value-1);
		}

		if(offset.set)
		{
			buf.Add(offset.value&0xFF);

			v=venv_offset>>6;
			v=(v>>2)+((v<<6)&0xFF);
			buf.Add(v);

			v=penv_offset>>6;
			v=(v>>2)+((v<<6)&0xFF);
			buf.Add(v);
		}
	}

	bool operator == (const NMNFrame &o) const
	{
		NMNByteBuffer b1, b2;
		Encode(b1);
		o.Encode(b2);
		return b1.IsEqual(b2);
	}
};

struct NMNCapState
{
	const NMChannel *chn=nullptr;
	const NMSound *snd=nullptr;
	int frame=-1;
	int pos=-1;
	int ptabi=-1;
	int slope=0;
	int vib_pos=-1;
	int venv_pos=NONE_POS;
	int penv_pos=NONE_POS;
	int chn_env_vol=15;

	NMAArpState arp;

	struct
	{
		unsigned act : 1; // active
		unsigned trg : 1; // triggered
		unsigned slp : 1; // slope/custom length
		unsigned rel : 1; // released
		unsigned cch : 1; // channel changed
	};

	NMNCapState() : act(0), trg(0), slp(0), rel(0), cch(0) {}

	void Capture(NMSong* song, int frame_, int pos_, const NMASoundPlayer& sp)
	{
		frame=frame_;
		pos=pos_;
		chn=song->GetCHST(sp.chst);
		snd=song->GetSound(sp.snd_idx);
		ptabi=sp.period_tab_idx;
		act=true;
		trg=sp.triggered;
		rel=sp.GetReleased();
		slp=false;

		if(sp.vib_delay>0)
			vib_pos=sp.vib_delay/-timeline_substeps;
		else
			vib_pos=sp.vib_pos/256;

		venv_pos=sp.v_envelope.pos;
		penv_pos=sp.p_envelope.pos;

		chn_env_vol=sp.GetChannelEnvelopeVolume();

		arp=sp.arp;
	}
};

using NMNCapRecording = std::vector<NMNCapState>;

#define INSTRUMENTS \
I(MOD_VOL,     mod_vol) /* 0..3=volume, 4..5=ones, 6..7=modifier */ \
I(VIB,         vib)     /* 0..3=strength, 4..7=speed */ \
I(VIB_DLY,     vib_dly) /* 0..7=delay */ \
I(DET,         det)     /* 0..3=detune */ \
I(ARP_RATE,    arp_rate) /* 0..7=arpeggio rate */ \
I(MOD_POS,     mod_pos) /* 0..5=modifier position */ \
I(PENV_SCL,    penv_scl) /* see NMNInstrument::Encode() */ \
I(VENV_SPD0,   venv_spd0) \
I(VENV_SPD1,   venv_spd1) \
I(VENV_SPD2,   venv_spd2) \
I(PENV_SPD0,   penv_spd0) \
I(PENV_SPD1,   penv_spd1) \
I(PENV_SPD2,   penv_spd2) \
/* NOTE: tsone: DO NOT REORDER THE FOLLOWING! */ \
I(VENV_PTR_LO, venv_ptr_lo) \
I(VENV_PTR_HI, venv_ptr_hi) \
I(PENV_PTR_LO, penv_ptr_lo) \
I(PENV_PTR_HI, penv_ptr_hi) \

enum NMNInstrumentStreamID
{
#define I(id_, name_) IS_ ## id_ ,
INSTRUMENTS
#undef I
	IS_COUNT
};

static inline const char *GetInstrumentStreamName(int is_idx)
{
	static const char *names[] =
	{
#define I(id_, name_) # name_ ,
INSTRUMENTS
#undef I
	};
	if(is_idx>=0 || is_idx<IS_COUNT)
		return names[is_idx];
	return "unknown";
}

using NMNInstrumentStreams = std::array<NMNByteBuffer, IS_COUNT>;

// NMNEnvelope
struct NMNEnvelope
{
	NMNEnvelope(NMAEnvelopePlayer::Type type, const NMSound *snd) : _type(type)
	{
		NMAEnvelopePlayer env={};
		env.Init(type, snd);
		for(int i=0;i<48;i++)
		{
			env.pos=i*256;
			_data[i]=env.Read()/16;
		}
	}

	void Encode(NMNByteBuffer &buf)
	{
		for(int i=0;i<48/2;i++)
			buf.Add(_data[2*i]+(_data[2*i+1]<<4));
	}

	bool IsNearEqual(const NMNEnvelope &o) const
	{
		int sum=0;
		for(int i=0;i<48;i++)
			sum+=abs(_data[i]-o._data[i]);
		return (sum<=48/24); // MAE, 4.167%
	}

	int GetConstant() const
	{
		int constant=_data[0];
		for(int i=1;i<48;i++)
			if(_data[i]!=constant)
				return -1;
		return constant;
	}

	void DebugPrint() const
	{
		printf("-- type:%s\n",
			_type==NMAEnvelopePlayer::VOLUME?"VOLUME":"PITCH");
		for(int y=15;y>=0;y--)
		{
			for(int x=0;x<48;x++)
			{
				char c=(_data[x]>=y?'+':' ');
				putchar(c);
			}
			putchar('\n');
		}
	}

private:

	int _data[48]={};
	NMAEnvelopePlayer::Type _type;
};

// unique envelopes in an array
struct NMNEnvelopeArray
{
	std::vector<NMNEnvelope> envelopes;

	int GetIdx(NMAEnvelopePlayer::Type type, const NMSound *snd) const
	{
		if(!snd)
			return -1;

		const NMNEnvelope tmp(type, snd);

		int constant=tmp.GetConstant();
		if(constant!=-1)
			return 0x80+constant;

		for(int i=0;i<(int)envelopes.size();i++)
		{
			if(envelopes[i].IsNearEqual(tmp))
				return i;
		}
		return -1;
	}

	void AddIfNeeded(NMAEnvelopePlayer::Type type, const NMSound *snd)
	{
		if(snd && GetIdx(type, snd)<0)
			envelopes.emplace_back(type, snd);
	}
};

struct NMNSample
{
	NMNSample(const NMSound *snd)
	{
		assert(snd && snd->waveform==NMSound::Type::DMC);
		assert(!snd->dpcm_sample.data.empty());

		_data=snd->dpcm_sample.data;
		_data.resize(align((int)_data.size()-1, 16)+1);
	}

	int GetAPULength() const
	{
		return ((int)_data.size()-1)/16;
	}

	void Encode(NMNByteBuffer &buf)
	{
		for(auto &d : _data)
			buf.Add(d);
	}

	bool IsEqual(const NMNSample &o) const
	{
		return (o._data==_data);
	}

private:
	// Size is multiple of 16+1, ex. 33, 161...
	std::vector<unsigned char> _data;
};

// unique samples (in an array)
struct NMNSampleArray
{
	std::vector<NMNSample> samples;

	int GetIdx(const NMSound *snd) const
	{
		if(!snd)
			return -1;

		const NMNSample tmp(snd);
		for(int i=0;i<(int)samples.size();i++)
		{
			if(samples[i].IsEqual(tmp))
				return i;
		}
		return -1;
	}

	void AddIfNeeded(const NMSound *snd)
	{
		if(snd && snd->waveform==NMSound::Type::DMC && !snd->dpcm_sample.data.empty() && GetIdx(snd)<0)
			samples.emplace_back(snd);
	}

	size_t Print(FILE *f)
	{
		size_t size=0;

		// write samples
		for(int i=0;i<(int)samples.size();i++)
		{
			auto &sample=samples[i];
			fprintf(f, ".ALIGN 64\nc_song_sample%d:", i);
			NMNByteBuffer b;
			sample.Encode(b);
			b.Print(f, 16);
			size+=b.Size();
			fputs("\n", f);
		}

		// write sample addresses
		fputs("c_song_dmc_addr:\n", f);
		for(int i=0;i<(int)samples.size();i++)
		{
			fprintf(f, "    .BYTE   (c_song_sample%d - $C000) / 64\n", i);
			size++;
		}
		// write sample len
		fputs("c_song_dmc_len:\n", f);
		for(int i=0;i<(int)samples.size();i++)
		{
			fprintf(f, "    .BYTE   %d\n", samples[i].GetAPULength());
			size++;
		}

		return size;
	}
};

// NMNInstrument = NMChannel + NMSound
struct NMNInstrument
{
	const NMChannel *chn;
	const NMSound *snd;

	NMNInstrument(const NMChannel *chn_, const NMSound *snd_) : chn(chn_), snd(snd_) {}

	bool IsEqual(const NMNInstrument &o, const NMNEnvelopeArray &envelopes, const NMNSampleArray &samples) const
	{
		if(chn->GetNoteLength()!=o.chn->GetNoteLength())
			return false;
		NMNInstrumentStreams a, b;
		o.Encode(a, envelopes, samples);
		Encode(b, envelopes, samples);
		for(int i=0;i<IS_COUNT;i++)
		{
			if(!a[i].IsEqual(b[i]))
				return false;
		}
		return true;
	}

	static void EncodeEnvPtr(NMNByteBuffer &lo, NMNByteBuffer &hi, int v)
	{
		if(v<128)
		{
			v*=24;
			lo.Add(v&0xFF);
			hi.Add(v>>8);
		}
		else
		{
			// if ptr_hi bit7=0, bits 0-3 is constant env value
			lo.Add(0);
			hi.Add(v);
		}
	}

	int GetLengthFrames(const NMSongPart &part) const
	{
		return chn->GetLengthFrames(&part, snd);
	}

	void Encode(NMNInstrumentStreams &s, const NMNEnvelopeArray &envelopes, const NMNSampleArray &samples) const
	{
		int v;

		// volume and modifier
		if(snd->waveform==NMSound::Type::TRIANGLE)
		{
			v=0;
		}
		else if(snd->waveform==NMSound::Type::DMC)
		{
			v=samples.GetIdx(snd)<<1;
			v+=(snd->wavemode^1);
		}
		else
		{
			// sq1,sq2,noise
			v=(BA<4>(VolumeMul(snd->Volume(), chn->volume)));
			v+=(BA<2>(snd->wavemode)<<6);
			v+=(BA<2>(snd->wavemode2)<<4);
		}
		s[IS_MOD_VOL].Add(v);

		// vibrato
		v=BA<4>(snd->vibrato_speed-1);
		v+=(BA<4>(snd->vibrato_strength)<<4);
		s[IS_VIB].Add(v);

		// vibrato delay
		v=CalcVibratoDelay(snd->vibrato_delay);
		v=(int)(-std::round((float)v/timeline_substeps))&0xFF;
		assert(!snd->vibrato_delay || (0x80&v)); // sign bit must be set for non-zero
		v=BA<8>(v);
		s[IS_VIB_DLY].Add(v);

		// detune
		// note is already transformed so only semitone offset is needed
		v=BA<4>((CalcDetune(snd->detune)+FULL_STEPS)%SEMITONE_STEPS);
		s[IS_DET].Add(v);

		// arpeggio rate
		v=chn->GetArpRate();
		assert(!(v%4));
		v=BA<7>((v+2)/4-1);
		s[IS_ARP_RATE].Add(v);

		// modifier position
		v=BA<6>(snd->wavemode_pos);
		s[IS_MOD_POS].Add(v);

		// pitch range
		v=BA<4>(snd->pitchrange-1);
		s[IS_PENV_SCL].Add(v);

		const auto PackEnvRate=[](int rate)
		{
			rate=BA<7>(GetEnvRate(rate));
			rate<<=1;
			rate=(rate>>6)+((rate<<2)&255);
			return rate;
		};

		// volume envelope speeds
		s[IS_VENV_SPD0].Add(PackEnvRate(snd->v_env_speed[0]));
		s[IS_VENV_SPD1].Add(PackEnvRate(snd->v_env_speed[1]));
		s[IS_VENV_SPD2].Add(PackEnvRate(snd->v_env_speed[2]));

		// pitch envelope speeds
		s[IS_PENV_SPD0].Add(PackEnvRate(snd->p_env_speed[0]));
		s[IS_PENV_SPD1].Add(PackEnvRate(snd->p_env_speed[1]));
		s[IS_PENV_SPD2].Add(PackEnvRate(snd->p_env_speed[2]));

		// envelope ptrs
		v=envelopes.GetIdx(NMAEnvelopePlayer::VOLUME, snd);
		s[IS_VENV_PTR_LO].Add(v);
		s[IS_VENV_PTR_HI].Add(v);
		v=envelopes.GetIdx(NMAEnvelopePlayer::PITCH, snd);
		s[IS_PENV_PTR_LO].Add(v);
		s[IS_PENV_PTR_HI].Add(v);
	}
};

struct NMNInstrumentArray
{
	std::vector<NMNInstrument> instruments;

	int GetIdx(const NMChannel *chn, const NMSound *snd, const NMNEnvelopeArray &envelopes, const NMNSampleArray &samples) const
	{
		if(!chn || !snd)
			return -1;

		const NMNInstrument tmpi(chn, snd);
		for(int i=0;i<(int)instruments.size();i++)
		{
			if(instruments[i].IsEqual(tmpi, envelopes, samples))
				return i;
		}
		return -1;
	}

	void AddIfNeeded(const NMChannel *chn, const NMSound *snd, const NMNEnvelopeArray &envelopes, const NMNSampleArray &samples)
	{
		if(chn && snd && GetIdx(chn, snd, envelopes, samples)<0)
			instruments.emplace_back(chn, snd);
	}
};

// One instrument lengths array for each unique song part tempo setting
using NMNInstrumentLengths = std::vector<uint16_t>;
struct NMNInstrumentLengthsArray
{
	std::vector<NMNInstrumentLengths> lengths;

	static NMNInstrumentLengths Create(const NMSongPart &part, const NMNInstrumentArray &instruments)
	{
		assert(!instruments.instruments.empty());

		NMNInstrumentLengths result(instruments.instruments.size());
		for(int i=0;i<instruments.instruments.size();i++)
			result[i]=instruments.instruments[i].GetLengthFrames(part);
		return result;
	}

	int GetIdx(const NMSongPart &part, const NMNInstrumentArray &instruments) const
	{
		const auto length=Create(part, instruments);
		for(int i=0;i<(int)lengths.size();i++)
		{
			if(lengths[i]==length)
				return i;
		}
		return -1;
	}

	void AddIfNeeded(const NMSongPart &part, const NMNInstrumentArray &instruments)
	{
		if(GetIdx(part, instruments)<0)
			lengths.emplace_back(Create(part, instruments));
	}

	void Encode(NMNByteBuffer &buf) const
	{
		for(const auto &length:lengths)
		{
			for(const auto v:length)
			{
				buf.Add(v&255);
				buf.Add(v>>8);
			}
		}
	}

	int GetOffset(const NMSongPart &part, const NMNInstrumentArray &instruments) const
	{
		const auto i=GetIdx(part, instruments);
		assert(i>=0 && i<lengths.size());
		return 2*i*(int)instruments.instruments.size();
	}
};

struct NMNArpArray
{
	// set first arp as 0 (=empty arp)
	std::vector<NMAArpScale> arps={{}};

	int GetOffs(const NMAArpScale &arp) const
	{
		int offs=0;
		for(const auto& a:arps)
		{
			if(arp==a)
				return offs;
			int len=1;
			while(a[len])
				len++;
			offs+=len;
		}
		return -1;
	}

	void AddIfNeeded(const NMAArpScale &arp)
	{
		if(GetOffs(arp)<0)
			arps.emplace_back(arp);
	}

	void Encode(NMNByteBuffer &buf) const
	{
		for(int arp_i=0;arp_i<arps.size();arp_i++)
		{
			const NMAArpScale& a=arps[arp_i];
			int i=0;
			do
			{
				buf.Add(a[i]&255);
				i++;
			}
			while(a[i]);
		}
		buf.Add(0); // terminator
	}
};

struct NMNSongPartData;
struct NMNStream
{
	std::vector<NMNFrame> v;

	void EncodeDelays(int remaining)
	{
		bool insert=v.empty();
		while(remaining>0)
		{
			auto current=std::min(255+1,remaining);
			remaining-=current;
			if(insert)
				v.emplace_back(NMNFrame{});
			assert(current>0);
			v.back().delay.Set(current);
			insert=true;
		}
	}

	NMNStream(const std::vector<NMNFrame> &v_) : v(v_) {}
	NMNStream(const NMNCapRecording &nw, const NMSongPart &part, const NMNInstrumentArray &instruments, const NMNArpArray &arps, const NMNInstrumentLengthsArray &lengths, const NMNEnvelopeArray &envelopes, const NMNSampleArray &samples, int loop_frame, int end_frame)
	{
		if(nw.empty())
			return;

		int prev_frame=0;
		for(int j=0;j<(int)nw.size();j++)
		{
			auto &c=nw[j];

			if(c.frame<prev_frame) continue;
			if(!c.trg && !c.slp && !c.cch) continue;

			if(c.frame>prev_frame)
			{
				// set delay to previous frame
				EncodeDelays(c.frame-prev_frame);
			}
			prev_frame=c.frame;

			NMNFrame f={};

			if(j<(int)nw.size()-1)
			{
				auto &cn=nw[j+1];
				if(cn.frame!=-1 && cn.frame>c.frame && (cn.rel || c.trg || c.slp || c.cch))
				{
					if(!c.rel)
					{
						f.len.Set(cn.frame-c.frame);
					}
					if(cn.rel)
					{
						f.meta_rel=true;
					}
				}
			}

			if(c.trg)
			{
				f.meta_trg=true;
			}

			if(c.ptabi!=-1)
			{
				int note=c.snd->CalcFinalPeriodTabIdx(c.ptabi)/SEMITONE_STEPS;
				f.note.Set(note);

				if(c.slp)
				{
					// also sets zero slopes to get correct note lengths (removed in frame encode)
					f.slope.Set(c.slope);
				}
			}

			int inst=instruments.GetIdx(c.chn, c.snd, envelopes, samples);
			if(inst>=0)
			{
				f.instrument.Set(inst);
			}

			int arp_offset=arps.GetOffs(c.arp.scale);
			if(arp_offset>0)
			{
				f.arp_offset.Set(arp_offset+c.arp.pos);
			}

			if(c.cch && !c.trg && c.venv_pos!=NONE_POS && c.penv_pos!=NONE_POS)
			{
				f.offset.Set(((c.vib_pos>>1)&0x7F)+(c.rel<<7));
				f.venv_offset=c.venv_pos;
				f.penv_offset=c.penv_pos;
			}

			v.push_back(f);
		}

		for(int i=1;i<v.size();)
		{
			NMNFrame& pf=v[i-1];
			const NMNFrame& f=v[i];

			if(!f.meta_trg && f.slope.set && pf.slope.set && f.slope.value == pf.slope.value)
			{
				if(f.len.set && pf.len.set)
				{
					pf.len.value+=f.len.value;
					pf.delay.value+=f.delay.value;
					v.erase(v.begin()+i);
					continue;
				}
			}
			i++;
		}

		FixLength(lengths.lengths[lengths.GetIdx(part, instruments)]);
		InsertLoop(loop_frame, end_frame);
	}

	void FixLength(const NMNInstrumentLengths &length);

	void InsertLoop(int loop_frame, int end_frame)
	{
		if(!v.size())
			return;

		int frame=0;
		bool loop_set=false;
		int prev_delay=-1;

		for(int i=0;i<(int)v.size();i++)
		{
			auto &f=v[i];
			if(f.delay.set)
				prev_delay=f.delay.value;
			else if(v.size()==1)
				prev_delay=0; // No delay is set if there's only one frame

			assert(prev_delay!=-1);
			int next_frame=frame+prev_delay;

			if(!loop_set && loop_frame==frame)
			{
				f.loop_start=true;
				loop_set=true;
			}
			else if(!loop_set && loop_frame>frame && loop_frame<next_frame)
			{
				// set loop between current and next (split)

				assert(loop_frame-frame>0);
				f.delay.Set(loop_frame-frame);

				NMNFrame nf={};
				nf.loop_start=true;
				assert(next_frame-loop_frame>0);
				nf.delay.Set(next_frame-loop_frame);
				v.insert(v.begin()+i+1, nf);

				next_frame=frame+f.delay.value;
				loop_set=true;
			}

			if(next_frame>end_frame)
			{
				// stream is too long, need to cut
				assert(end_frame-frame>0);
				f.delay.Set(end_frame-frame);
				v.erase(v.begin()+i+1, v.end());
				break;
			}

			if(i==v.size()-1)
			{
				// last frame in stream
				EncodeDelays(end_frame-frame);
				break; // must break, above might have added frames
			}

			frame=next_frame;
		}
	}

	void Optimize()
	{
		int prev_delay=-1;
		int prev_inst=-1;
		int prev_note=-1;
		bool prev_slope=false;

		for(auto &f : v)
		{
			if(f.loop_start)
			{
				// reset at loop, can't assume continuity
				prev_delay=-1;
				prev_inst=-1;
				prev_note=-1;
				prev_slope=false;
			}

			if(f.delay.set)
			{
				if(f.delay.value==prev_delay)
					f.delay.set=false;
				else
					prev_delay=f.delay.value;
			}

			if(f.instrument.set)
			{
				if(f.instrument.value==prev_inst)
					f.instrument.set=false;
				else
					prev_inst=f.instrument.value;
			}

			if(f.note.set)
			{
				if(!prev_slope && f.note.value==prev_note)
					f.note.set=false;
				else
					prev_note=f.note.value;

				prev_slope=f.slope.set;
			}
		}
	}
};

static void EncodeLoopCmd(NMNByteBuffer &buf, size_t src_offs, size_t dst_offs)
{
	assert(dst_offs>=0);
	assert(dst_offs<=src_offs);
	assert(src_offs-dst_offs+2<(1<<(8+5)));

	// loop is indicated in frame header as:
	// - offset (bit7) = 1
	// - trigger (bit6) = 1
	// - dict (bit0) = 0
	int offs=int(src_offs-dst_offs+2);
	buf.Add(0xC0+(BA<5>(offs>>8)<<1));
	buf.Add(offs&0xFF);
}

static void EncodeDictCmd(NMNByteBuffer &buf, int dict_offs)
{
	assert(dict_offs>=0 && dict_offs<NM_DICT_SIZE);
#if NM_DICT_SIZE == 256
	assert(!(dict_offs&0x01));
	buf.Add(dict_offs+0x01);
#else
	buf.Add((dict_offs<<1)+0x01);
#endif
}

struct NMNSongCapture
{
	NMNSongCapture(const NMSongPart &part) : _loop_frame(part.GetLoopFrame()), _end_frame(part.GetEndFrame())
	{
	}

	std::array<NMNCapRecording, 5> caps={};

	const int _loop_frame;
	const int _end_frame;

	std::array<NMNCapState, 5> curr={};
	std::array<NMNCapState, 5> prev={};

	int prev_frame=-1;

	void BeginFrame()
	{
		curr={};
	}

	void Capture(NMSong* song, int frame, int pos, const NMASoundPlayer& sp)
	{
		auto sound=song->GetSound(sp.snd_idx);
		if(!sound || !sp.HasOutput(song)) return;

		NMSound::Type waveform=sound->waveform;
		if(waveform==NMSound::Type::TRIANGLE)
			waveform=NMSound::Type::NOISE;
		else if(waveform==NMSound::Type::NOISE)
			waveform=NMSound::Type::TRIANGLE;

		curr[(int)waveform].Capture(song, frame, pos, sp);
	}

	int DetectSlope(int pos, MapGraph* content, const NMSongPart &part) const
	{
		const int endpos=content->FindNext(pos);
		if(endpos==NONE_POS)
			return INT_MAX;
		const auto endgp=content->Read(endpos);
		if(endgp.curve==0xFF)
			return INT_MAX;

		const int startpos=content->FindPrevious(endpos);
		if(startpos==NONE_POS)
			return INT_MAX;
		const auto startgp=content->Read(startpos);

		const int startnote=startgp.trigger!=0xFF?startgp.trigger:startgp.curve;
		const int endnote=endgp.curve;
		const int dp=(1<<4)*SEMITONE_STEPS*(endnote-startnote);
		const int dt=part.Pos2Frames(endpos-startpos)+1;
		if(dt<1)
		{
			// TODO: tsone: is this necessary?
			ERR("negative slope length! should not happen\n");
			return INT_MAX;
		}
		else
		{
			// also sets zero slopes to get correct note lengths (removed in frame encode)
			return (int)std::round((float)dp/dt);
		}
	}

	bool EndFrame(int frame, int startpos, int endpos, const NMSongPart &part)
	{
		if(frame>=prev_frame)
			prev_frame=frame;
		else
			frame=prev_frame+1;

#if DEBUG_CAPTURE
		printf("%4d | ", frame);
#endif

		for(int i=0;i<5;i++)
		{
			auto &c=curr[i];
			auto &p=prev[i];

			c.cch=(c.chn!=p.chn);

			if(c.chn && c.chn->content)
			{
				auto content=c.chn->content;
				int framepos=content->FindPrevious(endpos);
				if(framepos>=startpos || c.cch)
				{
					int slope=DetectSlope(endpos-1, content, part);
					if(slope!=INT_MAX)
					{
						c.slope=slope;
						c.slp=true;
					}
				}
			}

			char marker='-';
			if(!c.act && p.act && !p.rel && !c.rel)
			{
				// forced clear
				c={};
				c.frame=frame;
				c.act=true;
				c.rel=true;
				marker=':';
			}

			if(c.act)
			{
				NMNCapState cc=c;
				cc.rel=((!p.rel || c.cch || c.trg) && c.rel);
				caps[i].push_back(cc);
			}

#if DEBUG_CAPTURE
			printf("%c%c%c%c%c %4d | ",
				c.act ? 'A':marker,
				c.trg ? 'T':marker,
				c.rel ? 'R':marker,
				c.slp ? 'S':marker,
				c.cch ? 'H':marker,
				c.arp.pos
			);
#endif

			prev[i]=c;
		}
#if DEBUG_CAPTURE
		puts("");
#endif
		return true;
	}
};

#include "../nmn/nmn.h"

struct NMNDict
{
	struct NMNDictItem
	{
		NMNByteBuffer buf;
		int count;

		inline int EncodedSize() const
		{
#if NM_DICT_SIZE == 256
			return align((int)buf.Size(), 2);
#else
			return (int)buf.Size();
#endif
		}
	};

	using NMNDictArray=std::vector<NMNDictItem>;

	NMNDictArray _dict;

	int GetIndex(const NMNByteBuffer &buf)
	{
#if !DISABLE_DICT
		for(size_t i=0;i<_dict.size();i++)
			if(buf.IsEqual(_dict[i].buf))
				return (int)i;
#endif
		return -1;
	}

	int GetOffset(const NMNByteBuffer &buf) const
	{
		int offs=0;
		for(const auto &item:_dict)
		{
			if(buf.IsEqual(item.buf))
				return offs;
			offs+=item.EncodedSize();
		}
		return -1;
	}

	void Insert(const NMNByteBuffer &buf)
	{
		if(buf.Size()<2)
			return;
		int i=GetIndex(buf);
		if(i==-1)
			_dict.push_back({buf, 1});
		else
			_dict[i].count++;
	}

	void Optimize()
	{
		std::vector<std::pair<int, int>> score;
		score.reserve(_dict.size());

		for(size_t i=0;i<_dict.size();i++)
		{
			const auto &item=_dict[i];
			int size=(int)item.buf.Size();
			int before=size*item.count;
			int after=item.EncodedSize()+item.count;
			if(after<before)
			{
				assert(size<16);
				score.emplace_back((after-before)*16-size, i);
			}
		}

		std::sort(score.begin(), score.end());

		NMNDictArray new_dict;

		size_t i=0;
		int remaining=NM_DICT_SIZE;
		while(i<score.size() && remaining>0)
		{
			const auto &item=_dict[score[i].second];
			if(item.EncodedSize()<=remaining)
			{
				remaining-=item.EncodedSize();
				new_dict.emplace_back(item);
			}
			i++;
		}

		_dict=new_dict;
	}

	void Encode(NMNByteBuffer &buf)
	{
		for(const auto &item:_dict)
		{
			int size=(int)item.buf.Size();
			auto buf_data=(const unsigned char *)item.buf.Data();
			buf.Add(buf_data[0]>>1); // special first byte
			int i=1;
			for(;i<size;i++)
				buf.Add(buf_data[i]);
			for(;i<item.EncodedSize();i++)
				buf.Add(0); // padding for alignment
		}
	}

	void DebugPrint()
	{
		int tot_before=0;
		int tot_after=0;
		int tot_dict=0;

		printf("\n\n---- DICTIONARY ----\n");

		for(const auto &p:_dict)
		{
			p.buf.Print(stdout);
			int before=(int)p.buf.Size()*p.count;
			int after=p.EncodedSize()+p.count;
			printf("\t\t%6.2f%% (%d/%d)", 100.0f*after/before, after, before);
			tot_before+=before;
			tot_after+=after;
			tot_dict+=p.EncodedSize();
		}

		printf("\ndict: saving:%d size:%d items:%d\n", tot_after-tot_before, tot_dict, (int)_dict.size());
	}
};

// Channel volume stream run command (RLE)
struct NMNRun
{
	static unsigned _debug_buckets[3];

	unsigned short num_frames;
	unsigned char volume;

	NMNRun()=delete;
	NMNRun(int num_frames_, int volume_) : num_frames(num_frames_), volume(volume_)
	{
		assert(volume_>=0&&volume_<=15);
		assert(num_frames_>=1&&num_frames_<=32768);
	}

	void Encode(NMNByteBuffer& buf) const
	{
		assert(num_frames>=1);
		if(num_frames<=14)
		{
			buf.Add(((num_frames-1)<<4)+volume);
			_debug_buckets[0]++;
		}
		else if(num_frames<=256)
		{
			buf.Add((14<<4)+volume);
			buf.Add(num_frames-1);
			_debug_buckets[1]++;
		}
		else
		{
			buf.Add((15<<4)+volume);
			buf.Add((num_frames-1)>>8);
			buf.Add((num_frames-1)&255);
			_debug_buckets[2]++;
		}
	}
};

unsigned NMNRun::_debug_buckets[3]={};

struct NMNChannelVolume
{
	std::vector<NMNRun> _runs;

	NMNChannelVolume()=default;
	NMNChannelVolume(const NMNCapRecording& cap, int end_frame)
	{
		// TODO: tsone: for some reason needs -1
		end_frame--;

		const int ANY_VOLUME=-1;
		std::vector<char> frame_volume(end_frame, ANY_VOLUME);

		int volume=ANY_VOLUME;
		for(const auto& c:cap)
		{
			if(!c.chn)
				continue;

			if(volume==ANY_VOLUME)
				volume=c.chn_env_vol;

			assert(c.frame>=0);

			// TODO: tsone: happens sometimes for some reason
			if(c.frame>=end_frame)
				break;

			frame_volume[c.frame]=c.chn_env_vol;
		}

		if(volume==ANY_VOLUME)
			// no caps -> empty stream -> no runs
			return;

		for(int j=0;j<end_frame;j++)
		{
			if(frame_volume[j]==ANY_VOLUME)
				frame_volume[j]=volume;
			else
				volume=frame_volume[j];
		}

		int prev_idx=0;
		int prev_vol=frame_volume[0];
		for(int j=1;j<end_frame;j++)
		{
			if(frame_volume[j]!=prev_vol)
			{
				_runs.emplace_back(j-prev_idx, prev_vol);
				prev_idx=j;
				prev_vol=frame_volume[j];
			}
		}
		_runs.emplace_back(end_frame-prev_idx, prev_vol);
	}

	int GetConstant() const
	{
		if(_runs.size()<=1)
			return _runs.empty()?15:_runs[0].volume;
		else
			return -1;
	}

	void Encode(NMNByteBuffer& buf, int loop_frame) const
	{
		assert(GetConstant()==-1);

		int loop_offs=-1;
		int frame_start=0;
		for(size_t i=0;i<_runs.size();i++)
		{
			auto run(_runs[i]);
			assert(run.num_frames>0);

			int frame_end=frame_start+run.num_frames;
			if(i==_runs.size()-1)
			{
				// last run, decrement frames by 1 due loop
				frame_end--;
				if(frame_end==frame_start)
					// 0 frames run length -> don't encode
					continue;
			}

			if(frame_start>=loop_frame || frame_end<=loop_frame)
			{
				// no loop_frame during run -> regular encode
				if(frame_start==loop_frame && loop_offs==-1)
					loop_offs=(int)buf.Size();

				run.Encode(buf);

				if(frame_end==loop_frame && loop_offs==-1)
					loop_offs=(int)buf.Size();
			}
			else
			{
				assert(loop_offs==-1);

				// loop_frame during run -> split in two runs
				run.num_frames=loop_frame-frame_start;
				run.Encode(buf);

				loop_offs=(int)buf.Size();

				run.num_frames=frame_end-loop_frame;
				run.Encode(buf);
			}

			frame_start=frame_end;
		}

		// encode loop
		{
			// if there was no runs
			if(loop_offs==-1)
				loop_offs=0;

			int delta=loop_offs-((int)buf.Size()+2);
			assert(delta>=-32768&&delta<0);
			buf.Add(15<<4);
			buf.Add((delta>>8)&255);
			buf.Add(delta&255);
		}
	}
};

struct NMNSongPartData
{
	std::array<NMNCapRecording, 5> _filtered_caps;
	std::array<NMNChannelVolume, 3> _chn_vols;
	std::vector<NMNStream> _streams;

	std::array<NMNByteBuffer, 5> _chn_streams;

	const int _loop_frame;
	const int _end_frame;

	const NMSongPart& _part;

	NMNSongPartData(const NMSongPart& part, const NMNSongCapture& cap) : _loop_frame(cap._loop_frame), _end_frame(cap._end_frame), _part(part)
	{
		BuildChannelVolumes(cap);
		BuildFilteredCaps(cap);
	}

	void EncodeStreams(const NMNDict& dict)
	{
		int debug_delay_sum_test=-1;
		for(int i=0;i<5;i++)
		{
			const auto &stream=_streams[i];

#if DEBUG_STREAM
			printf("\n\n---- STREAM %d ----\n", i);
#endif

			size_t loop_offs=0;
			int debug_delay_curr=-1;
			int debug_delay_sum=0;
			for(int j=0;j<(int)stream.v.size();j++)
			{
				auto &fr=stream.v[j];

				if(fr.delay.set)
					debug_delay_curr=fr.delay.value;
				assert(debug_delay_curr!=-1);
				debug_delay_sum+=debug_delay_curr;

				if(fr.loop_start)
					loop_offs=_chn_streams[i].Size();

				NMNByteBuffer buf;
				fr.Encode(buf);
				int dict_offs=dict.GetOffset(buf);
				if(dict_offs==-1)
					_chn_streams[i].Add(buf);
				else
					EncodeDictCmd(_chn_streams[i], dict_offs);

#if DEBUG_STREAM
				{
					buf.Print(stdout); // frame per line
					for(auto k=buf.Size()*5;k<4*16;k++)
						printf(" ");
					if(dict_offs==-1)
					{
						const bool has_slope=(fr.slope.set && fr.slope.value);
						char marker='-';
						printf("; %c%c%c%c%c%c%c%c",
							fr.instrument.set ? 'I':marker,
							fr.note.set ? 'N':marker,
							has_slope ? 'S':marker,
							fr.arp_offset.set ? 'A':marker,
							fr.len.set ? 'L':marker,
							fr.delay.set ? 'D':marker,
							fr.meta_trg ? 'T':marker,
							fr.offset.set ? 'O':marker
						);
					}
					else
					{
						printf("; DICT_OFFS=$%02X", dict_offs);
					}
				}
#endif
			}

			if(!stream.v.empty())
			{
				if(debug_delay_sum_test==-1)
				{
					debug_delay_sum_test=debug_delay_sum;
				}
				else
				{
					// Part channels must have equal delay sum
					assert(debug_delay_sum_test==debug_delay_sum);
				}
			}

			// Encode last loop.
			EncodeLoopCmd(_chn_streams[i], _chn_streams[i].Size(), loop_offs);
		}
	}

	void AppendEnvelopes(NMNEnvelopeArray& envelopes) const
	{
		for(const auto &fcs:_filtered_caps)
		{
			for(auto &c:fcs)
			{
				envelopes.AddIfNeeded(NMAEnvelopePlayer::VOLUME, c.snd);
				envelopes.AddIfNeeded(NMAEnvelopePlayer::PITCH, c.snd);
			}
		}
	}

	void AppendSamples(NMNSampleArray& samples) const
	{
		for(auto &fcs:_filtered_caps)
		{
			for(auto &c:fcs)
			{
				samples.AddIfNeeded(c.snd);
			}
		}
	}

	void AppendInstruments(NMNInstrumentArray& instruments, const NMNEnvelopeArray& envelopes, const NMNSampleArray& samples) const
	{
		for(auto &fcs:_filtered_caps)
		{
			for(auto &c:fcs)
			{
				instruments.AddIfNeeded(c.chn, c.snd, envelopes, samples);
			}
		}
	}

	void AppendArps(NMNArpArray& arps) const
	{
		for(auto &fcs:_filtered_caps)
		{
			for(auto &c:fcs)
			{
				if(c.trg || c.cch)
					arps.AddIfNeeded(c.arp.scale);
			}
		}
	}

	void AppendInstrumentLengths(NMNInstrumentLengthsArray& lengths, const NMNInstrumentArray& instruments) const
	{
		lengths.AddIfNeeded(_part, instruments);
	}

private:

	void BuildChannelVolumes(const NMNSongCapture& capi)
	{
		for(int i=0;i<_chn_vols.size();i++)
		{
			const auto &cap=capi.caps[i];
			if(!cap.empty())
				_chn_vols[i]=NMNChannelVolume(cap, _end_frame);

#if DEBUG_CHN_VOL
			printf("chn%d vol runs:%zu\n", i, chn_vols[i]._runs.size());
#endif
		}
	}

	void BuildFilteredCaps(const NMNSongCapture& capi)
	{
		for(int i=0;i<5;i++)
		{
			auto &cap=capi.caps[i];
			if(cap.empty()) continue;

			auto &nw=_filtered_caps[i];
			int pframe=cap[0].frame;
			NMNCapState cs={};
			for(auto &c:cap)
			{
				if(!(c.trg || c.rel || c.slp || c.cch)) continue;

				if(c.frame!=pframe)
				{
					nw.push_back(cs);
					pframe=c.frame;
					cs={};
				}
				if(c.cch)
				{
					// if more than 1 cch per frame -> reset rel to current cap
					cs.cch=true;
					cs.rel=c.rel;
				}
				if(c.trg && !c.slp && cs.slp)
				{
					// pure trigger after slope at same pos -> cancel slope
					cs.slp=false;
				}
				cs.act|=c.act;
				cs.trg|=c.trg;
				cs.rel|=c.rel;
				cs.slp|=c.slp;
				cs.chn=c.chn;
				cs.snd=c.snd;
				cs.frame=c.frame;
				cs.ptabi=c.ptabi;
				cs.slope=c.slope;
				cs.vib_pos=c.vib_pos;
				cs.venv_pos=c.venv_pos;
				cs.penv_pos=c.penv_pos;
				cs.arp=c.arp;
			}
			if(cs.trg || cs.rel || cs.slp || cs.cch)
				nw.push_back(cs);

#if DEBUG_FILTERED
			{
				printf("\n---- CHANNEL %d ----\n\n", i);
				char marker='.';
				for(auto &c:nw)
				{
					printf("%4d %c%c%c%c%c %4d %d\n",
						c.frame,
						c.act ? 'A':marker,
						c.trg ? 'T':marker,
						c.rel ? 'R':marker,
						c.slp ? 'S':marker,
						c.cch ? 'H':marker,
						c.ptabi, c.arp.pos
					);
				}
			}
#endif
		}
	}
};

struct NMNSongData
{
	NMNEnvelopeArray _envelopes;
	NMNSampleArray _samples;
	NMNInstrumentArray _instruments;
	NMNInstrumentLengthsArray _lengths;
	NMNArpArray _arps;

	NMNInstrumentStreams _instrument_streams;
	NMNDict _dict;

	const NMSong& _song;

	std::vector<NMNSongPartData> _parts;

	NMNSongData(const std::vector<NMNSongCapture>& caps, const NMSong &song) : _song(song)
	{
		for(int i=0;i<PARTS_MAX;i++)
		{
			if(song.song_parts[i])
			{
				_parts.emplace_back(*song.song_parts[i], caps[i]);
				_parts.back().AppendEnvelopes(_envelopes);
				_parts.back().AppendSamples(_samples);
				_parts.back().AppendInstruments(_instruments, _envelopes, _samples);
				_parts.back().AppendArps(_arps);
			}
		}

		EncodeInstruments();

		// Build optimized streams
		for(auto& part:_parts)
		{
			part.AppendInstrumentLengths(_lengths, _instruments);

			for(int i=0;i<5;i++)
			{
				part._streams.emplace_back(part._filtered_caps[i], part._part, _instruments, _arps, _lengths, _envelopes, _samples, part._loop_frame, part._end_frame);
				part._streams.back().Optimize();
			}
		}

		// Push to dictionary
		for(auto& part:_parts)
		{
			for(const auto &s:part._streams)
			{
				for(const auto &fr:s.v)
				{
					NMNByteBuffer buf;
					fr.Encode(buf);
					_dict.Insert(buf);
				}
			}
		}

		_dict.Optimize();

		for(auto& part:_parts)
			part.EncodeStreams(_dict);

#if DEBUG_DICT
		_dict.DebugPrint();
#endif
	}

	void EncodeInstruments()
	{
		for(int i=0;i<(int)_instruments.instruments.size();i++)
		{
			_instruments.instruments[i].Encode(_instrument_streams, _envelopes, _samples);
		}
	}

	std::vector<unsigned char> GetDrv(const int *data_addr)
	{
		std::vector<unsigned char> drv(nmn_driver, nmn_driver+NMN_DRIVER_SIZE);

		// Relocate song data by replacing addresses in the binary
		for(int tag_idx=0;tag_idx<NMN_DATA_COUNT;tag_idx++)
		{
			const auto *offs=nmn_addr_repl_offs[tag_idx];
			const int addr=data_addr[tag_idx];
			assert(addr);
			while(*offs)
			{
				drv[*offs]=addr&0xFF;
				drv[*offs+1]=addr>>8;
				offs++;
			}
		}

		return drv;
	}

	bool WriteNSF(const char* filename)
	{
		assert(native_mode);
		int addr[NMN_DATA_COUNT]={};

		// create dict_buf
		NMNByteBuffer dict_buf;
		_dict.Encode(dict_buf);
		assert(dict_buf.Size()<=NM_DICT_SIZE);
		while(dict_buf.Size()!=NM_DICT_SIZE)
			dict_buf.Add(0);

		// create song_buf
		NMNByteBuffer song_buf;
		auto GetAddr=[&song_buf]()
		{
			return NMN_DRIVER_BASE+NMN_DRIVER_SIZE+NM_DICT_SIZE+(int)song_buf.Size();
		};

		int envelopes_base=GetAddr();
		for(auto &env:_envelopes.envelopes)
			env.Encode(song_buf);

		for(int i=0;i<IS_COUNT;i++)
		{
			addr[i]=GetAddr();
			const auto &is=_instrument_streams[i];
			if(i>=IS_VENV_PTR_LO && i<=IS_PENV_PTR_HI)
			{
				bool is_hi=(i==IS_VENV_PTR_HI || i==IS_PENV_PTR_HI);
				auto data=(const unsigned char *)is.Data();
				for(int j=0;j<(int)is.Size();j++)
				{
					if(data[j]<0x80)
					{
						auto ptr=envelopes_base+48/2*data[j];
						if(is_hi)
							song_buf.Add(ptr>>8);
						else
							song_buf.Add(ptr&0xFF);
					}
					else
					{
						song_buf.Add(data[j]&0x7f);
					}
				}
			}
			else
			{
				song_buf.Add(is);
			}
		}

		std::vector<int> chn_ptrs;
		std::vector<int> vol_ptrs;
		for(const auto& part:_parts)
		{
			for(int i=0;i<part._chn_streams.size();i++)
			{
				if(part._chn_streams[i].Size()>2)
				{
					chn_ptrs.push_back(GetAddr());
					song_buf.Add(part._chn_streams[i]);
				}
				else
				{
					chn_ptrs.push_back(0);
				}
			}

			for(int i=0;i<part._chn_vols.size();i++)
			{
				const int constvol=part._chn_vols[i].GetConstant();
				if(constvol==-1)
				{
					vol_ptrs.push_back(GetAddr());
					part._chn_vols[i].Encode(song_buf, part._loop_frame);
				}
				else
				{
					vol_ptrs.push_back(constvol<<8);
				}
			}
		}

		addr[NMN_DATA_ARP_DATA]=GetAddr();
		_arps.Encode(song_buf);

		int lengths_base=GetAddr();
		_lengths.Encode(song_buf);

		int parts_base=GetAddr();
		for(int pi=0;pi<_parts.size();pi++)
		{
			const auto& part=_parts[pi];

			{
				int k=(int)part._chn_streams.size();
				for(int i=0;i<k;i++)
					song_buf.Add(chn_ptrs[k*pi+i]&0xFF);
				for(int i=0;i<k;i++)
					song_buf.Add(chn_ptrs[k*pi+i]>>8);
			}

			{
				int k=(int)part._chn_vols.size();
				for(int i=0;i<k;i++)
					song_buf.Add(vol_ptrs[k*pi+i]&0xFF);
				for(int i=0;i<k;i++)
					song_buf.Add(vol_ptrs[k*pi+i]>>8);
			}

			const auto lengths_ptr=lengths_base+_lengths.GetOffset(_parts[pi]._part, _instruments);
			song_buf.Add(lengths_ptr&255);
			song_buf.Add(lengths_ptr>>8);
		}

		constexpr int part_data_size=18;
		assert(GetAddr()==parts_base+part_data_size*_parts.size());
		addr[NMN_DATA_PART_PTR_LO]=GetAddr();
		for(int pi=0;pi<_parts.size();pi++)
			song_buf.Add((parts_base+part_data_size*pi)&255);
		addr[NMN_DATA_PART_PTR_HI]=GetAddr();
		for(int pi=0;pi<_parts.size();pi++)
			song_buf.Add((parts_base+part_data_size*pi)>>8);

		addr[NMN_DATA_DMC_ADDR]=GetAddr();

		int num_dmc=(int)_samples.samples.size();
		addr[NMN_DATA_DMC_LEN]=GetAddr()+num_dmc;

		// create dmc_buf

		int dmc_base=std::max(addr[NMN_DATA_DMC_LEN]+num_dmc,0xC000);
		dmc_base=align(dmc_base, 64);

		NMNByteBuffer dmc_buf;

		std::vector<int> dmc_addr(num_dmc);
		for(int i=0;i<num_dmc;i++)
		{
			dmc_addr[i]=dmc_base+align((int)dmc_buf.Size(), 64);
			while(dmc_base+(int)dmc_buf.Size()<dmc_addr[i])
				dmc_buf.Add(0);

			_samples.samples[i].Encode(dmc_buf);
		}

		//

		for(int i=0;i<num_dmc;i++)
			song_buf.Add((int)(dmc_addr[i]-0xC000)/64);
		for(int i=0;i<num_dmc;i++)
			song_buf.Add(_samples.samples[i].GetAPULength());

		auto drv_bin=GetDrv(addr);
		if(drv_bin.empty())
			return false;

		auto f=fopen(filename, "wb");
		if(!f)
			return false;

		WriteNSFHeader(f);

		fwrite(drv_bin.data(), 1, drv_bin.size(), f);
		fwrite(dict_buf.Data(), 1, dict_buf.Size(), f);
		fwrite(song_buf.Data(), 1, song_buf.Size(), f);
		if(dmc_buf.Size()>0)
		{
			fseek(f, dmc_base-NMN_DRIVER_BASE+0x80, SEEK_SET);
			fwrite(dmc_buf.Data(), 1, dmc_buf.Size(), f);
		}

		fclose(f);
		return true;
	}

	void WriteAsmEnvPtrInstrumentStream(FILE *f, int stream_id) const
	{
		const auto &is=_instrument_streams[stream_id];
		bool is_hi=(stream_id==IS_VENV_PTR_HI || stream_id==IS_PENV_PTR_HI);
		auto formatter=[is_hi](int v)
		{
			if(v<0x80)
			{
				if(is_hi)
					return ">c_song_env" + std::to_string(v);
				else
					return "<c_song_env" + std::to_string(v);
			}
			else
			{
				return std::to_string(v&0x7f);
			}
		};
		is.Print(f, (unsigned) _instruments.instruments.size(), formatter);
	}

	bool WriteASM(const char* filename)
	{
		assert(native_mode);
		auto f=fopen(filename, "wb");
		if(!f)
			return false;

		size_t total_chns=0;
		size_t total_instruments=0;
		size_t total_envelopes=0;
		size_t total_samples=0;
		size_t total_dict=0;
		size_t total_vols=0;

		fputs("; Generated by nemus " VER_DISPLAY "\n", f);
		fprintf(f, "; Song: %s - %s (parts:%d)\n\n", _song.author, _song.title, (int)_parts.size());

		total_samples+=_samples.Print(f);

		// write envelopes
		for(int i=0;i<(int)_envelopes.envelopes.size();i++)
		{
			auto &env=_envelopes.envelopes[i];
			fprintf(f, "\nc_song_env%d:", i);

			NMNByteBuffer b;
			env.Encode(b);
			b.Print(f);

			total_envelopes+=b.Size();
		}

		// write instruments
		for(int i=0;i<IS_COUNT;i++)
		{
			auto &is=_instrument_streams[i];

			fprintf(f, "\nc_song_inst_%s:", GetInstrumentStreamName(i));
			if(i>=IS_VENV_PTR_LO && i<=IS_PENV_PTR_HI)
			{
				WriteAsmEnvPtrInstrumentStream(f, i);
			}
			else
			{
				is.Print(f, (unsigned)_instruments.instruments.size());
				fputs("", f);
			}

			total_instruments+=is.Size();
		}

		// write arps
		{
			NMNByteBuffer buf;
			_arps.Encode(buf);
			fprintf(f, "\nc_song_arp_data:");
			buf.Print(f, 16);
			fputs("\n", f);
		}

		// write instrument lengths
		{
			NMNByteBuffer buf;
			_lengths.Encode(buf);
			fprintf(f, "c_song_inst_lens:");
			buf.Print(f, 16);
			fputs("\n", f);
		}

		// write dict
		{
			NMNByteBuffer buf;
			_dict.Encode(buf);
			fprintf(f, "c_song_dict:");
			buf.Print(f, 16);
			fputs("\n", f);

			total_dict+=buf.Size();
		}

		for(int pi=0;pi<_parts.size();pi++)
		{
			const auto& part=_parts[pi];
			for(int i=0;i<part._chn_streams.size();i++)
			{
				if(part._chn_streams[i].Size()>2)
				{
					fprintf(f, "c_song_chn%02X%d:", pi, i);
					part._chn_streams[i].Print(f, 16);
					fputs("\n", f);

					total_chns+=part._chn_streams[i].Size();
				}
				else
				{
					fprintf(f, "c_song_chn%02X%d = 0\n", pi, i);
				}
			}
		}

		for(int pi=0;pi<_parts.size();pi++)
		{
			const auto& part=_parts[pi];
			for(int i=0;i<part._chn_vols.size();i++)
			{
				const int constvol=part._chn_vols[i].GetConstant();
				if(constvol==-1)
				{
					fprintf(f, "c_song_vol%02X%d:", pi, i);
					NMNByteBuffer b;
					part._chn_vols[i].Encode(b, part._loop_frame);
					b.Print(f, 16);
					fputs("\n", f);

					total_vols+=b.Size();
				}
				else
				{
					fprintf(f, "c_song_vol%02X%d = $%04X\n", pi, i, constvol<<8);
				}
			}
		}

		for(int pi=0;pi<_parts.size();pi++)
		{
			const auto& part=_parts[pi];

			fprintf(f, "c_song_part%02X:\n", pi);

			for(int lohi=0;lohi<2;lohi++)
			{
				fputs("    .BYTE  ", f);
				for(int pci=0;pci<part._chn_streams.size();pci++)
					fprintf(f, " %cc_song_chn%02X%d%c", lohi?'>':'<', pi, pci, (pci==part._chn_streams.size()-1?'\n':','));
			}

			for(int lohi=0;lohi<2;lohi++)
			{
				fputs("    .BYTE  ", f);
				for(int pci=0;pci<part._chn_vols.size();pci++)
					fprintf(f, " %cc_song_vol%02X%d%c", lohi?'>':'<', pi, pci, (pci==part._chn_vols.size()-1?'\n':','));
			}

			const auto offs=_lengths.GetOffset(part._part, _instruments);
			fprintf(f, "    .BYTE   <(c_song_inst_lens+%d), >(c_song_inst_lens+%d)\n", offs, offs);
		}

		for(int lohi=0;lohi<2;lohi++)
		{
			fprintf(f, "c_song_part_ptr_%s:\n", lohi?"hi":"lo");
			fputs("    .BYTE  ", f);
			for(int pi=0;pi<_parts.size();pi++)
				fprintf(f, " %cc_song_part%02X%c", lohi?'>':'<', pi, pi==_parts.size()-1?'\n':',');
		}

		fprintf(f, "SONG_PARTS_NUM = %lu\n", _parts.size());

#if DEBUG_CHN_VOL
		printf("chn vol bucketss:%u,%u,%u\n", NMNRun::_debug_buckets[0], NMNRun::_debug_buckets[1], NMNRun::_debug_buckets[2]);
#endif

#if DEBUG_SUMMARY
		printf("chns:%zu\n", total_chns);
		printf("vols:%zu\n", total_vols);
		printf("instruments:%zu (%zu items)\n",
			   total_instruments, _instruments.instruments.size());
		printf("envelopes:%zu (%zu items)\n",
			   total_envelopes, _envelopes.envelopes.size());
		printf("dict:%zu\n", total_dict);
		printf("samples:%zu (%zu items)\n", total_samples, _samples.samples.size());
		printf("total:%zu\n",
				   total_chns+total_instruments+total_envelopes+total_samples+total_dict+total_vols);
#endif

		fclose(f);
		return true;
	}

private:

	void WriteNSFHeader(FILE* f)
	{
		unsigned char nhb[32];
		for(int i=0;i<32;i++)
			nhb[i]=0x00;
		strcpy((char*)nhb, "NESM"); nhb[4]=0x1A; fwrite(nhb, 5, 1, f); // NESM
		nhb[0]=0x01; fwrite(nhb, 1, 1, f); // version
		nhb[0]=_parts.size(); fwrite(nhb, 1, 1, f); // numsongs
		nhb[0]=0x01; fwrite(nhb, 1, 1, f); // startsong
		nhb[0]=NMN_DRIVER_BASE&0xFF; nhb[1]=NMN_DRIVER_BASE>>8; fwrite(nhb, 2, 1, f); // load address
		nhb[0]=NMN_DRIVER_PLAY&0xFF; nhb[1]=NMN_DRIVER_PLAY>>8; fwrite(nhb, 2, 1, f); // init address
		nhb[0]=NMN_DRIVER_UPDATE&0xFF; nhb[1]=NMN_DRIVER_UPDATE>>8; fwrite(nhb, 2, 1, f); // play address
		strcpy((char*)nhb, _song.title); fwrite(nhb, 32, 1, f); // title
		strcpy((char*)nhb, _song.author); fwrite(nhb, 32, 1, f); // artist
		strcpy((char*)nhb, _song.author); fwrite(nhb, 32, 1, f); // copyright
		nhb[0]=0xFF; nhb[1]=0x40; fwrite(nhb, 2, 1, f); // NTSC speed (0x40FF=actual NTSC frame rate)
		for(int i=0;i<32;i++)
			nhb[i]=0x00;
		nhb[0]=0x00; fwrite(nhb, 8, 1, f); // bankswitch init
		nhb[0]=0x1D; nhb[1]=0x4E; fwrite(nhb, 2, 1, f); // PAL speed (=actual PAL frame rate)
		nhb[0]=0x00; fwrite(nhb, 1, 1, f); // PAL/NTSC bits (0=NTSC only)
		for(int i=0;i<5;i++)
			nhb[i]=0x00;
		nhb[0]=0x00; fwrite(nhb, 5, 1, f); // extra sound chip and expansion bits
	}
};

void NMNStream::FixLength(const NMNInstrumentLengths &length)
{
	for(int i=0;i<(int)v.size();i++)
	{
		auto &f=v[i];

		if(f.len.set && f.instrument.set)
		{
			const int inst_len=length[f.instrument.value]+1;

			if(inst_len==f.len.value)
			{
				f.len.set=false;
			}
			else if(f.delay.set)
			{
				if(inst_len>=f.delay.value && f.len.value>=f.delay.value)
				{
					f.len.set=false;
				}
			}
		}
	}
}

#endif // NHM_H


int debug_file_stats_graphcode=0;
int debug_file_stats_graphgaplong=0;
int debug_file_stats_graphgapshort=0;
int debug_file_stats_graphpoint=0;
int debug_file_stats_predictwaste=0;

void BitCrunch(Bitstream& bsin, Bitstream& bsout)
{
	// use a temp buffer of byte bits
	bsin.PadToBytes();
	auto numbits=bsin.ByteLength()*8;
	char* bitbuffer=new char[numbits];
	bsin.Restart();
	for(int i=0;i<numbits;i++)
		bitbuffer[i]=bsin.ReadValue(1);

	// start with 16-bit raw blocks implied
	int command=0;
	int blocksize=32;
	int readpos=0; // current source position where matching data is expected

	int copy_command=0;
	int copy_offset=0;
	int copy_blocksize=0;
	int copy_score=0;

	int writepos=0; // predictive write head
	int peekpos=0;

	//int bitpos0=bsout.BitPos();

	while(writepos<numbits)
	{
		// if command is copy, check whether another block of the current size would match
		//  -> yes: append 1 and update readpos,writepos, continue
		if(command==1)
		{
			bool match=true;
			for(int i=0;i<blocksize;i++)
			{
				if(writepos+i>=numbits)
					break;
				if(bitbuffer[readpos+i]!=bitbuffer[writepos+i])
				{
					match=false;
					break;
				}
			}
			if(match)
			{
				bsout.AppendValue(1, 1); // continue copying
				readpos+=blocksize;
				writepos+=blocksize;
				peekpos=writepos;
				continue;
			}
			else
				blocksize=-1;
		}

		// find best copy source for this position, scored by encodesize minus overhead (and minus current savings for repeating raw?)
		copy_command=0;
		copy_offset=0;
		copy_blocksize=0;
		copy_score=0;
		int debug_length=0;
		for(int cso=1;cso<(1<<12)-1;cso++)
		{
			if(peekpos-cso<0)
				break;
			int length=0;
			for(length=0;length<500;length++) // maximum copy length to search for
			{
				if(peekpos+length>=numbits)
					break;
				if(bitbuffer[peekpos-cso+length]!=bitbuffer[peekpos+length])
					break;
			}
			if(length>0)
			{
				int ofs=cso-1;
				int score_short=length/8*8-(1+2+3+length/8);
				int score_medium=length/16*16-(1+2+8+length/16);
				int score_long=length/32*32-(1+2+12+length/32);
				int minofs=1;
				if(ofs>7)
				{
					score_short=0;
					ofs-=8;
				}
				else
					minofs+=8;
				if(ofs<minofs)
					score_medium=0;
				if(ofs>0xFE)
				{
					score_medium=0;
					ofs-=0xFF;
				}
				else
					minofs+=0xFF;
				if(ofs<minofs)
					score_long=0;
				if(ofs>0xFFF)
					score_long=0;

				int cmd=1;
				int score=score_short;
				if(score_medium>score_short) { cmd=2; score=score_medium; }
				if(score_long>score_medium) { cmd=3; score=score_long; }
				if(score>copy_score)
				{
					copy_command=cmd;
					copy_score=score;
					if(cmd==1) copy_blocksize=8;
					if(cmd==2) copy_blocksize=16;
					if(cmd==3) copy_blocksize=32;
					copy_offset=ofs;
					debug_length=length;

					int rofs=ofs+1;
					if(cmd==2) rofs+=8+1;
					if(cmd==3) rofs+=0xFF+8+1;
				}
				if(length==500) // best possible (that we care to search for)
					break;
			}
		}

		bool output_raw=false;
		if(copy_score>0 || peekpos>=numbits)
			output_raw=true;

		if(output_raw && peekpos>writepos)
		{
			while(peekpos>writepos) // output raw blocks until we've caught up
			{
				int rawsize=peekpos-writepos;
				int newblocksize=blocksize;
				if(rawsize>=8) newblocksize=8;
				if(rawsize>=32) newblocksize=32;
				if(newblocksize!=blocksize)
				{
					bsout.AppendValue(0, 1); // don't repeat command
					bsout.AppendValue(0, 2); // command: raw
					if(newblocksize==8)
						bsout.AppendValue(0, 1);
					else
						bsout.AppendValue(1, 1);
					blocksize=newblocksize;
				}
				else
					bsout.AppendValue(1, 1); // repeat command
				for(int j=0;j<blocksize;j++)
				{
					int b=bitbuffer[writepos++];
					bsout.AppendValue(b, 1);
				}
				if(writepos>peekpos)
				{
					peekpos=writepos;
					copy_score=0;
				}
			}
		}

		if(copy_score>0)
		{
			bsout.AppendValue(0, 1); // don't repeat command
			bsout.AppendValue(copy_command, 2);
			if(copy_command==1)
				bsout.AppendValue(copy_offset, 3);
			if(copy_command==2)
				bsout.AppendValue(copy_offset, 8);
			if(copy_command==3)
				bsout.AppendValue(copy_offset, 12);
			blocksize=copy_blocksize;
			readpos=-999;
			if(copy_command==1)
				readpos=writepos-(copy_offset+1);
			if(copy_command==2)
				readpos=writepos-(copy_offset+8+1);
			if(copy_command==3)
				readpos=writepos-(copy_offset+0xFF+8+1);
			command=1;

			// move ahead one block immediately
			readpos+=blocksize;
			writepos+=blocksize;
			peekpos=writepos;

			continue;
		}
		else
		{
			command=0;
			peekpos+=8;
			continue;
		}
	}

	bsout.AppendValue(0, 1); // don't repeat
	bsout.AppendValue(2, 2); // medium pointer
	bsout.AppendValue(0xFF, 8); // end code

	delete[] bitbuffer;
}

void BitDecrunch(Bitstream& bsin, Bitstream& bsout)
{
	int bballoc=1024;
	char* bitbuffer=new char[bballoc];

	//int bitpos0=bsout.BitPos();

	int command=0;
	int blocksize=32;

	int readpos=0;
	int writepos=0;
	for(;;)
	{
		if(writepos+128>bballoc)
		{
			bballoc*=2;
			char* newbits=new char[bballoc];
			for(int i=0;i<writepos;i++)
				newbits[i]=bitbuffer[i];
			delete[] bitbuffer;
			bitbuffer=newbits;
		}
		int repeatflag=bsin.ReadValue(1);
		if(repeatflag)
		{
			if(command==1) // copy
			{
				for(int j=0;j<blocksize;j++)
					bitbuffer[writepos++]=bitbuffer[readpos++];
			}
			if(command==0) // raw
			{
				for(int j=0;j<blocksize;j++)
				{
					int b=bsin.ReadValue(1);
					bitbuffer[writepos++]=b;
				}
			}
			continue;
		}
		int bcmd=bsin.ReadValue(2);
		if(bcmd==0) // raw
		{
			blocksize=bsin.ReadValue(1)==0?8:32;

			for(int j=0;j<blocksize;j++)
			{
				int b=bsin.ReadValue(1);
				bitbuffer[writepos++]=b;
			}
			command=0;
			continue;
		}
		else // copy pointer
		{
			int offset=0;
			if(bcmd==1) // short
			{
				offset=bsin.ReadValue(3)+1;
				blocksize=8;
			}
			if(bcmd==2) // medium
			{
				offset=bsin.ReadValue(8);
				if(offset==0xFF) // end of stream
					break;
				offset+=8+1;
				blocksize=16;
			}
			if(bcmd==3) // long
			{
				offset=bsin.ReadValue(12)+0xFF+8+1;
				blocksize=32;
			}
			readpos=writepos-offset;
			for(int j=0;j<blocksize;j++)
				bitbuffer[writepos++]=bitbuffer[readpos++];
			command=1;
			continue;
		}
	}

	for(int i=0;i<writepos;i++)
		bsout.AppendValue(bitbuffer[i], 1);

	bsout.Restart();

	delete[] bitbuffer;
}

int song_file_version=0;

struct NMByteBuffer
{
	unsigned char* bytebuffer;
	long bytebuffer_alloc;
	long bytebuffer_i;
	NMByteBuffer() : bytebuffer(NULL), bytebuffer_alloc(0), bytebuffer_i(0) {}
	NMByteBuffer(unsigned char *buf, long size) : bytebuffer(buf), bytebuffer_alloc(size), bytebuffer_i(0) {}
	~NMByteBuffer()
	{
		delete[] bytebuffer;
	}
	long Size()
	{
		return bytebuffer_i;
	}
	unsigned char* Export()
	{
		unsigned char* b=bytebuffer;
		bytebuffer=NULL;
		bytebuffer_alloc=0;
		bytebuffer_i=0;
		return b;
	}
	void ReadBytes(void* dst, long size)
	{
		memcpy(dst, &bytebuffer[bytebuffer_i], size);
		bytebuffer_i+=size;
	}
	void WriteBytes(const void* src, long size)
	{
		if(size==0)
			return;
		long needed=(bytebuffer_i+size+4096)&(~4095);
		if(needed>bytebuffer_alloc)
		{
			auto bb2=new unsigned char[needed];
			memcpy(bb2, bytebuffer, bytebuffer_alloc);
			delete[] bytebuffer;
			bytebuffer=bb2;
			bytebuffer_alloc=needed;
		}
		memcpy(&bytebuffer[bytebuffer_i], src, size);
		bytebuffer_i+=size;
	}
};

struct GraphPoint
{
	unsigned char trigger;
	unsigned char curve;
	void Clear()
	{
		trigger=0xFF;
		curve=0xFF;
	}
	bool Empty() const
	{
		return trigger==0xFF && curve==0xFF;
	}
};

static constexpr int NONE_POS=-0x7FFFFFFE;
static constexpr int INVALID_POS=-0x7FFFFFFF;

#include <map>

struct MapGraph
{
	using Points=std::map<int, GraphPoint>;
private:
	Points points;

	static void FixSelection(int& pos0, int& pos1)
	{
		assert(pos0<=pos1);
		assert(pos0>=-1);

		pos0=std::max(0, std::min(0xFFFF, pos0));
		pos1=std::max(0, std::min(0xFFFF, pos1));
	}

	static void ClearPointValueRange(GraphPoint& gp, int v0, int v1, bool inverted)
	{
		assert(v0>=-1);
		assert(v1>=-1);

		if(v0==-1 || v1==-1)
		{
			assert(v0==v1);

			if(!inverted)
				gp.trigger=gp.curve=0xFF;
		}
		else
		{
			assert(v0<=v1);

			if(!inverted)
			{
				// inside range
				if(gp.trigger!=0xFF && gp.trigger>=v0 && gp.trigger<=v1)
					gp.trigger=0xFF;
				if(gp.curve!=0xFF && gp.curve>=v0 && gp.curve<=v1)
					gp.curve=0xFF;
			}
			else
			{
				// outside range
				if(gp.trigger!=0xFF && (gp.trigger<v0 || gp.trigger>v1))
					gp.trigger=0xFF;
				if(gp.curve!=0xFF && (gp.curve<v0 || gp.curve>v1))
					gp.curve=0xFF;
			}
		}
	}

	std::pair<Points::iterator, Points::iterator> IteratorRange(int pos0, int pos1)
	{
		return { points.lower_bound(pos0), points.upper_bound(pos1-1) };
	}

public:
	bool implicitly_triggered=true;
	int content_type=0;
	MapGraph()
	{
		implicitly_triggered=true;
		content_type=0;
	}
	MapGraph* MakeCopy()
	{
		return new MapGraph(*this);
	}
	void Clear()
	{
		points.clear();
	}
	bool Empty()
	{
		return points.empty();
	}
	void ClearRange(int pos0, int pos1, int v0, int v1)
	{
		FixSelection(pos0, pos1);
		auto range=IteratorRange(pos0, pos1);
		for(auto it=range.first;it!=points.end();) {
			// NOTE: tsone: it's unclear if range.second is valid after erase(), hence this break condition
			if(it->first>=pos1)
				break;

			auto& gp=it->second;
			ClearPointValueRange(gp, v0, v1, false);
			if(gp.Empty())
				it=points.erase(it);
			else
				it++;
		}
	}
	void ClearRange(int pos0, int pos1)
	{
		ClearRange(pos0, pos1, -1, -1);
	}
	void MergeRangeFrom(MapGraph* src, int pos0, int pos1, int dst, int v0, int v1)
	{
		FixSelection(pos0, pos1);
		auto range=src->IteratorRange(pos0, pos1);
		for(auto it=range.first;it!=range.second;it++) {
			int dp=dst+it->first-pos0;
			if(dp>0xFFFF)
				break;
			// NOTE: tsone: could calculate exact range first, but nah
			if(dp<0)
				continue;

			auto gp=it->second;
			ClearPointValueRange(gp, v0, v1, true);
			if(gp.Empty())
				continue;

			auto itd=points.find(dp);
			if(itd==points.end())
			{
				points.emplace(dp, gp);
			}
			else
			{
				auto& pp=itd->second;
				if(gp.trigger!=0xFF)
					pp.trigger=gp.trigger;
				if(gp.curve!=0xFF)
					pp.curve=gp.curve;
			}
		}
	}
	void MergeRangeFrom(MapGraph* src, int pos0, int pos1, int dst)
	{
		MergeRangeFrom(src, pos0, pos1, dst, -1, -1);
	}
	void CopyRangeFrom(MapGraph* src, int pos0, int pos1, int dst, int v0, int v1)
	{
		ClearRange(dst, dst+pos1-pos0, -1, -1);
		MergeRangeFrom(src, pos0, pos1, dst, v0, v1);
	}
	void CopyRangeFrom(MapGraph* src, int pos0, int pos1, int dst)
	{
		CopyRangeFrom(src, pos0, pos1, dst, -1, -1);
	}
	void SetPoint(int pos, GraphPoint point)
	{
		if(!point.Empty())
		{
			points[pos]=point;
		}
		else
		{
			// TODO: tsone: remove option to delete point by setting it!
			auto it=points.find(pos);
			if(it!=points.end())
				points.erase(it);
		}
	}
	GraphPoint Read(int pos)
	{
		auto it=points.find(pos);
		return it==points.end()?GraphPoint{0xFF, 0xFF}:it->second;
	}
	bool IsInvalid(const Points::iterator& it) const
	{
		return it==points.end();
	}
	std::pair<Points::iterator, Points::iterator> FindPrevNext(int pos)
	{
		if(points.empty())
			return { points.end(), points.end() };
		auto next=points.upper_bound(pos);
		if (next!=points.begin())
		{
			auto prev=next;
			--prev;
			return { prev, next };
		}
		else
		{
			return { points.end(), next };
		}
	}
	int FindPrevious(int pos)
	{
		const auto pn=FindPrevNext(pos-1);
		return pn.first!=points.end()?pn.first->first:NONE_POS;
	}
	int FindNext(int pos)
	{
		const auto pn=FindPrevNext(pos);
		return pn.second!=points.end()?pn.second->first:NONE_POS;
	}
	void CleanPoints()
	{
		if(implicitly_triggered)
			return;

		auto it=points.begin();
		while(it!=points.end())
		{
			auto& gp=it->second;
			if(gp.curve==0xFF)
				break;

			gp.curve=0xFF;
			if(!gp.Empty())
				break;

			it=points.erase(it);
		}
	}

	void Write(NMByteBuffer& bb)
	{
		int range=0;
		int i0=FindNext(-1);
		int i1=FindPrevious(0x10000);
		if(i0!=NONE_POS && i1!=NONE_POS)
			range=i1+1-i0;
		if(range>0xFFFF)
		{
			range=0xFFFF;
			i0=0;
		}
		unsigned short wordval=(unsigned short)range;
		bb.WriteBytes(&wordval, 2);
		if(range==0)
			return;

		wordval=(unsigned short)i0;
		bb.WriteBytes(&wordval, 2);
		int prev_i=i0;
		unsigned char byteval;

		for(const auto& p:points)
		{
			assert(!p.second.Empty());

			int di=p.first-prev_i-1;
			assert(di>=-1);

			while(di>0)
			{
				byteval=std::min(255,di); bb.WriteBytes(&byteval, 1);
				di-=byteval;
			}
			prev_i=p.first;

			byteval=0; bb.WriteBytes(&byteval, 1);
			bb.WriteBytes(&p.second.trigger, 1);
			bb.WriteBytes(&p.second.curve, 1);
		}
	}
	void Read(NMByteBuffer& bb)
	{
		Clear();

		unsigned short wordval=0;
		bb.ReadBytes(&wordval, 2);
		int range=(int)wordval;
		if(range==0)
			return;

		bb.ReadBytes(&wordval, 2);
		int i=(int)wordval;
		int i1=i+range;
		while(i<i1)
		{
			unsigned char gaplength=0;
			bb.ReadBytes(&gaplength, 1);
			i+=gaplength;
			if(gaplength==0)
			{
				GraphPoint gp;
				bb.ReadBytes(&gp.trigger, 1);
				bb.ReadBytes(&gp.curve, 1);
				if(song_file_version<2 && content_type!=0)
					SetPoint(i*timeline_substeps, gp);
				else
					SetPoint(i, gp);
				i++;
			}
		}
	}

	void WriteCompact(Bitstream& bs);
	void ReadCompact(Bitstream& bs);
};

struct GraphReader
{
	struct Iterator
	{
		Iterator(const MapGraph::Points::iterator& iter_)
		{
			iter=iter_;
		}
		bool IsValid(const GraphReader* reader) const
		{
			return !reader->GetSource()->IsInvalid(iter);
		}
		void Next(const GraphReader* reader)
		{
			if(IsValid(reader))
			   iter++;
		}
		int Pos(const GraphReader* reader) const
		{
			return IsValid(reader)?iter->first-reader->pos_offset:NONE_POS;
		}
		GraphPoint Read(const GraphReader* reader) const
		{
			return IsValid(reader)?iter->second:GraphPoint{0xFF, 0xFF};
		}
	private:
		MapGraph::Points::iterator iter;
	};

	Iterator GetIterator(int pos) const
	{
		const auto pn=source->FindPrevNext(pos+pos_offset);
		return Iterator((!source->IsInvalid(pn.first))?pn.first:pn.second);
	}

	int pos_offset;
	GraphReader()
	{
		source=NULL;
		defvalue=0;
		pos_offset=0;
	}
	void SetSource(MapGraph *source_, int defvalue_)
	{
		source=source_;
		defvalue=defvalue_;
	}
	const MapGraph* GetSource() const
	{
		return source;
	}
	GraphPoint Read(int pos) const
	{
		if(pos==INVALID_POS || pos==NONE_POS || !source)
			return { 0xFF, 0xFF };
		pos+=pos_offset;
		return source->Read(pos);
	}
	int FindPrevious(int pos) const
	{
		if(pos==INVALID_POS || pos==NONE_POS)
			return pos;
		pos+=pos_offset;
		pos=source->FindPrevious(pos);
		return pos!=NONE_POS?pos-pos_offset:NONE_POS;
	}
	int FindNext(int pos) const
	{
		if(pos==INVALID_POS || pos==NONE_POS)
			return pos;
		pos+=pos_offset;
		if(!source)
			return NONE_POS;
		pos=source->FindNext(pos);
		return pos!=NONE_POS?pos-pos_offset:NONE_POS;
	}
	int Read(int pos, int substep, int scale) const
	{
		assert(source);
		assert(substep>=1 && scale>=1);

		if(pos==INVALID_POS || pos==NONE_POS || source->Empty())
			return defvalue*scale;

		const int i=std::max(0, std::min(0xFFFF, pos/substep+pos_offset));

		const auto pn=source->FindPrevNext(i);
		if(source->IsInvalid(pn.first))
		{
			// before first point
			const auto& gp=pn.second->second;
			assert(!gp.Empty());
			return (gp.curve!=0xFF?gp.curve:gp.trigger)*scale;
		}
		else if(source->IsInvalid(pn.second))
		{
			// after last point
			const auto& gp=pn.first->second;
			assert(!gp.Empty());
			return (gp.trigger!=0xFF?gp.trigger:gp.curve)*scale;
		}
		else
		{
			// between points
			const auto& p0=pn.first->second;
			const auto& p1=pn.second->second;
			assert(!p0.Empty() && !p1.Empty());

			const int v0=p0.trigger!=0xFF?p0.trigger:p0.curve;

			if(p1.curve!=0xFF)
			{
				// interpolate
				const int v1=p1.curve;
				const int si0=pn.first->first*substep;
				const int si1=pn.second->first*substep;
				const int sv0=v0*scale;
				const int sv1=v1*scale;
				return sv0+(sv1-sv0)*(pos-si0)/(si1-si0);
			}
			else
			{
				return v0*scale;
			}
		}
	}

private:
	MapGraph* source;
	int defvalue;
};

struct MultiGraph
{
	MapGraph* graph_buffer[128];
	MapGraph* graph_source[128];
	int num;

	MultiGraph()
	{
		for(int i=0;i<128;i++)
			graph_buffer[i]=NULL;
		for(int i=0;i<128;i++)
			graph_source[i]=NULL;
		graph_buffer[0]=new MapGraph();
		num=0;
	}
	~MultiGraph()
	{
		for(int i=0;i<128;i++)
			delete graph_buffer[i];
	}

	void Reset()
	{
		num=0;
	}

	void SetSource(MultiGraph* src)
	{
		num=src->num;
		for(int i=0;i<num;i++)
		{
			graph_source[i]=src->graph_source[i];
			if(graph_source[i]!=NULL && graph_buffer[i]==NULL)
				graph_buffer[i]=new MapGraph();
		}
	}

	void BufferSource(MultiGraph* src) // set source to buffers in given multigraph (for copying)
	{
		for(int i=0;i<num;i++)
			graph_source[i]=src->graph_buffer[i];
	}

	void AddGraph(MapGraph* graph)
	{
		graph_source[num]=graph;
		if(graph_buffer[num]==NULL)
			graph_buffer[num]=new MapGraph();
		num++;
	}

	void ClearRange(int pos0, int pos1)
	{
		for(int i=0;i<num;i++)
			if(graph_source[i]!=NULL)
				graph_source[i]->ClearRange(pos0, pos1);
	}

	void ClearRange_Graphs(int pos0, int pos1, int g0, int g1)
	{
		for(int i=0;i<num;i++)
			if(graph_source[i]!=NULL && i>=g0 && i<=g1)
				graph_source[i]->ClearRange(pos0, pos1);
	}

	void ClearRange_Values(int pos0, int pos1, int v0, int v1)
	{
		for(int i=0;i<num;i++)
			if(graph_source[i]!=NULL)
				graph_source[i]->ClearRange(pos0, pos1, v0, v1);
	}

	void ReadRange(int pos0, int pos1)
	{
		for(int i=0;i<num;i++)
			if(graph_buffer[i]!=NULL && graph_source[i]!=NULL)
				graph_buffer[i]->CopyRangeFrom(graph_source[i], pos0, pos1, 0);
	}

	void ReadRange_Graphs(int pos0, int pos1, int g0, int g1)
	{
		for(int i=0;i<num;i++)
			if(graph_buffer[i]!=NULL && graph_source[i]!=NULL)
			{
				if(i>=g0 && i<=g1)
					graph_buffer[i]->CopyRangeFrom(graph_source[i], pos0, pos1, 0);
				else
					graph_buffer[i]->Clear();
			}
	}

	void ReadRange_Values(int pos0, int pos1, int v0, int v1)
	{
		for(int i=0;i<num;i++)
			if(graph_buffer[i]!=NULL && graph_source[i]!=NULL)
				graph_buffer[i]->CopyRangeFrom(graph_source[i], pos0, pos1, 0, v0, v1);
	}

	void WriteRange(int pos0, int pos1)
	{
		for(int i=0;i<num;i++)
			if(graph_buffer[i]!=NULL && graph_source[i]!=NULL)
				graph_source[i]->CopyRangeFrom(graph_buffer[i], 0, pos1-pos0, pos0);
	}

	void MergeRange(int pos0, int pos1)
	{
		for(int i=0;i<num;i++)
			if(graph_buffer[i]!=NULL && graph_source[i]!=NULL)
				graph_source[i]->MergeRangeFrom(graph_buffer[i], 0, pos1-pos0, pos0);
	}

	void CleanPoints()
	{
		for(int i=0;i<num;i++)
			if(graph_source[i]!=NULL)
				graph_source[i]->CleanPoints();
	}
};

#include <vector>
#include <array>

std::vector<unsigned char> ReadFile(const char *fn)
{
	auto f=fopen(fn, "rb");
	if(!f)
		return {};

	fseek(f, 0, SEEK_END);
	std::vector<unsigned char> result(ftell(f));
	rewind(f);
	fread(result.data(), 1, result.size(), f);
	fclose(f);
	return result;
}

struct DPCMSample
{
	char name[14];
	std::vector<unsigned char> data;

	DPCMSample() { name[0]='\0'; }
	~DPCMSample() { Clear(); }
	DPCMSample(const DPCMSample &c) { *this=c; }
	void operator=(const DPCMSample &c)
	{
		strcpy(name, c.name);
		data=c.data;
	}
	void Clear()
	{
		data.clear();
		name[0]='\0';
	}
	void Write(NMByteBuffer &bb) const
	{
		int len=(int)data.size();
		bb.WriteBytes(&len, 4);
		if(len>0)
		{
			bb.WriteBytes(name, sizeof(name));
			bb.WriteBytes(data.data(), len);
		}
	}
	void Read(NMByteBuffer &bb)
	{
		Clear();
		int len=0;
		if(song_file_version==8)
		{
			unsigned char byteval;
			bb.ReadBytes(&byteval, 1); len=byteval;
			bb.ReadBytes(&byteval, 1); len|=byteval<<8;
		}
		else
			bb.ReadBytes(&len, 4);
		if(len>0)
		{
			bb.ReadBytes(name, sizeof(name));
			data.resize(len);
			bb.ReadBytes(data.data(), len);
		}
	}
	void WriteCompact(Bitstream &bs)
	{
		if(data.empty())
		{
			bs.AppendValue(0, 1);
			return;
		}
		bs.AppendValue(1, 1);
		bs.AppendVLQ((int)data.size()-1);
		for(int i=0;i<(int)data.size();i++)
			bs.AppendValue(data[i], 8);
	}
	void ReadCompact(Bitstream &bs)
	{
		Clear();
		if(!bs.ReadValue(1))
			return;
		data.resize(bs.ReadVLQ()+1);
		for(int i=0;i<(int)data.size();i++)
			data[i]=bs.ReadValue(8);
		strcpy(name, "(COMPACTED)");
	}
	bool LoadDMC(const char *fn)
	{
		auto new_data=ReadFile(fn);
		if(new_data.empty())
			return false;
		data=new_data;
		name[0]='\0';
		strncat(name, get_basename(fn), sizeof(name)-1);
		return true;
	}
};

struct NMSound
{
	// NOTE: tsone: serialized, don't change!
	enum class Type : char
	{
		SQUARE1,
		SQUARE2,
		TRIANGLE,
		NOISE,
		DMC,
		//
		COUNT
	};

	static constexpr int wavemode_pos_default=8;

	bool active;
	int audio_light;
	int audio_v_envpos;
	int audio_p_envpos;

	int color;
	int name;
	Type waveform;
	int wavemode; // also controls dpcm looping
	int wavemode2;
	int wavemode_pos;
	int vibrato_delay;
	int vibrato_speed;
	int vibrato_strength;
	int detune;
	int pitchrange;
	bool fixedpitch;
	int basepitch;
	int recent_wavemode[5];
	int recent_wavemode2[5];

	int v_env_speed[3];
	MapGraph* v_env_graph;
	int p_env_speed[3];
	MapGraph* p_env_graph;

	DPCMSample dpcm_sample;

	NMSound()
	{
		v_env_graph=new MapGraph();
		v_env_graph->content_type=0;
		p_env_graph=new MapGraph();
		p_env_graph->content_type=0;
		Clear();
	}
	~NMSound()
	{
		delete v_env_graph;
		delete p_env_graph;
	}
	bool HasVolumeControl() const
	{
		return !(waveform==Type::TRIANGLE || (native_mode && waveform==Type::DMC));
	}
	void Volume(int volume_)
	{
		volume=volume_;
	}
	int Volume() const
	{
		return HasVolumeControl()?volume:15;
	}
	void Clear()
	{
		active=true;
		audio_light=0;
		audio_v_envpos=NONE_POS;
		audio_p_envpos=NONE_POS;

		color=1;
		name=0;
		waveform=Type::SQUARE1;
		wavemode=wavemode2=1;
		wavemode_pos=wavemode_pos_default;
		volume=15;
		vibrato_delay=7;
		vibrato_speed=9;
		vibrato_strength=0;
		detune=128;
		pitchrange=10;
		fixedpitch=false;
		basepitch=5*12;

		GraphPoint gp;
		gp.Clear();

		for(int i=0;i<5;i++)
			recent_wavemode[i]=recent_wavemode2[i]=1;

		v_env_speed[0]=10;
		v_env_speed[1]=0;
		v_env_speed[2]=0;
		v_env_graph->Clear();
		gp.curve=15; v_env_graph->SetPoint(0, gp);

		p_env_speed[0]=10;
		p_env_speed[1]=0;
		p_env_speed[2]=0;
		p_env_graph->Clear();
		gp.curve=7; p_env_graph->SetPoint(0, gp);

		dpcm_sample.Clear();
	}
	void CopyFrom(NMSound* src)
	{
		active=src->active;
		audio_light=src->audio_light;
		audio_v_envpos=src->audio_v_envpos;
		audio_p_envpos=src->audio_p_envpos;

		color=src->color;
		name=src->name;
		waveform=src->waveform;
		wavemode=src->wavemode; // also controls dpcm looping
		wavemode2=src->wavemode2;
		wavemode_pos=src->wavemode_pos;
		vibrato_delay=src->vibrato_delay;
		vibrato_speed=src->vibrato_speed;
		vibrato_strength=src->vibrato_strength;
		detune=src->detune;
		pitchrange=src->pitchrange;
		fixedpitch=src->fixedpitch;
		basepitch=src->basepitch;

		for(int i=0;i<5;i++)
		{
			recent_wavemode[i]=src->recent_wavemode[i];
			recent_wavemode2[i]=src->recent_wavemode2[i];
		}

		for(int i=0;i<3;i++)
		{
			v_env_speed[i]=src->v_env_speed[i];
			p_env_speed[i]=src->p_env_speed[i];
		}

		v_env_graph->CopyRangeFrom(src->v_env_graph, 0, 48, 0);
		p_env_graph->CopyRangeFrom(src->p_env_graph, 0, 48, 0);

		dpcm_sample=src->dpcm_sample;
	}
	NMSound* MakeCopy()
	{
		auto c=new NMSound();
		c->CopyFrom(this);
		return c;
	}

	int CalcFinalPeriodTabIdx(int pi) const
	{
		pi-=OCTAVE_STEPS;
		if(waveform==Type::TRIANGLE)
			pi+=OCTAVE_STEPS;
		else if(waveform==Type::DMC)
			pi+=3*OCTAVE_STEPS;
		return std::max(0, std::min(FULL_STEPS-1, pi));
	}

	int GetInstrumentVolume(int chn_vol) const
	{
		int result=VolumeMul(chn_vol, Volume());
		if(HasVolumeControl())
			return result;
		else
			return result>0?15:0;
	}

	int GetVolZeroPos(int chn_vol) const
	{
		int inst_vol=GetInstrumentVolume(chn_vol);
		if(inst_vol>0)
		{
			GraphReader graph;
			graph.SetSource(v_env_graph, 15);
			for(int i=47*256;i>=0;i-=256)
			{
				int v=graph.Read(i, 256, 16)/16;
				v=VolumeMul(v, inst_vol);
				if(v)
				{
					const int result=std::min(i+256, 47*256);
					assert(result>=0);
					return result;
				}
			}
		}
		return 0;
	}

	void SaveFile(const char* filename)
	{
		NMByteBuffer bb;
		Write(bb);
		const unsigned char current_version=2;
		FILE* file=fopen(filename, "wb");
		fwrite(&current_version, 1, 1, file);
		fwrite(bb.bytebuffer, 1, bb.Size(), file);
		fclose(file);
	}
	void OpenFile(const char* filename)
	{
		FILE* file=fopen(filename, "rb");
		fseek(file, 0, SEEK_END);
		auto fl=ftell(file)-1;
		fseek(file, 0, SEEK_SET);
		unsigned char* fbuffer=new unsigned char[fl];

		unsigned char version=0;
		fread(&version, 1, 1, file);
		fread(fbuffer, fl, 1, file);
		fclose(file);

		if(version<=1)
		{
			Bitstream bs;
			bs.FromBuffer(fbuffer, fl);
			ReadCompact(bs);
			delete[] fbuffer;
		}
		else
		{
			NMByteBuffer bb(fbuffer, fl);
			Read(bb);
		}
	}

	void Write(NMByteBuffer& bb) const
	{
		char byteval;
		if(song_file_version>=1)
		{
			byteval=color; bb.WriteBytes(&byteval, 1);
			byteval=name; bb.WriteBytes(&byteval, 1);
		}
		byteval=(char)waveform; bb.WriteBytes(&byteval, 1);
		byteval=wavemode; bb.WriteBytes(&byteval, 1);
		byteval=wavemode2; bb.WriteBytes(&byteval, 1);
		byteval=wavemode_pos; bb.WriteBytes(&byteval, 1);
		byteval=volume; bb.WriteBytes(&byteval, 1);
		byteval=vibrato_delay; bb.WriteBytes(&byteval, 1);
		byteval=vibrato_speed; bb.WriteBytes(&byteval, 1);
		byteval=vibrato_strength; bb.WriteBytes(&byteval, 1);
		byteval=detune; bb.WriteBytes(&byteval, 1);
		byteval=pitchrange; bb.WriteBytes(&byteval, 1);
		bb.WriteBytes(&fixedpitch, sizeof(bool));
		unsigned short wordval=0;
		wordval=basepitch; bb.WriteBytes(&wordval, 2);

		byteval=v_env_speed[0]; bb.WriteBytes(&byteval, 1);
		byteval=v_env_speed[1]; bb.WriteBytes(&byteval, 1);
		byteval=v_env_speed[2]; bb.WriteBytes(&byteval, 1);
		v_env_graph->Write(bb);
		byteval=p_env_speed[0]; bb.WriteBytes(&byteval, 1);
		byteval=p_env_speed[1]; bb.WriteBytes(&byteval, 1);
		byteval=p_env_speed[2]; bb.WriteBytes(&byteval, 1);
		p_env_graph->Write(bb);

		if(song_file_version>=8)
			dpcm_sample.Write(bb);
	}
	void Read(NMByteBuffer& bb)
	{
		Clear();

		char byteval;
		if(song_file_version>=1)
		{
			bb.ReadBytes(&byteval, 1); color=byteval;
			bb.ReadBytes(&byteval, 1); name=byteval;
		}
		else
		{
			color=1;
			name=0;
		}
		bb.ReadBytes(&byteval, 1); waveform=(Type)byteval;
		bb.ReadBytes(&byteval, 1); wavemode=byteval;
		if(waveform==Type::TRIANGLE)
			wavemode=1; // disable extra triangle
		if(song_file_version>=13)
		{
			bb.ReadBytes(&byteval, 1); wavemode2=byteval;
			bb.ReadBytes(&byteval, 1); wavemode_pos=byteval;
		}
		else
		{
			if(waveform==Type::SQUARE1 || waveform==Type::SQUARE2)
				wavemode=2-wavemode;
			wavemode2=wavemode;
			wavemode_pos=wavemode_pos_default;
		}
		bb.ReadBytes(&byteval, 1); volume=byteval;
		bb.ReadBytes(&byteval, 1); vibrato_delay=byteval;
		bb.ReadBytes(&byteval, 1); vibrato_speed=byteval;
		bb.ReadBytes(&byteval, 1); vibrato_strength=byteval;
		bb.ReadBytes(&byteval, 1); detune=(int)(unsigned char)byteval;
		bb.ReadBytes(&byteval, 1); pitchrange=byteval;
		bb.ReadBytes(&fixedpitch, sizeof(bool));
		unsigned short wordval=0;
		bb.ReadBytes(&wordval, 2); basepitch=wordval;

		bb.ReadBytes(&byteval, 1); v_env_speed[0]=byteval;
		bb.ReadBytes(&byteval, 1); v_env_speed[1]=byteval;
		bb.ReadBytes(&byteval, 1); v_env_speed[2]=byteval;
		v_env_graph->Read(bb);
		bb.ReadBytes(&byteval, 1); p_env_speed[0]=byteval;
		bb.ReadBytes(&byteval, 1); p_env_speed[1]=byteval;
		bb.ReadBytes(&byteval, 1); p_env_speed[2]=byteval;
		p_env_graph->Read(bb);

		if(song_file_version>=8)
			dpcm_sample.Read(bb);

		active=true;
	}

	void WriteCompact(Bitstream& bs);
	void ReadCompact(Bitstream& bs);

private:
	int volume;
};

static constexpr int SOUNDS_MAX=100;
static constexpr int PARTS_MAX=100;

struct NMSong;
struct NMSongPart;
struct NMChannel
{
	// NOTE: tsone: serialized, don't change!
	enum class Type : char
	{
		ST,
		CH
	};

	bool active;
	bool audio_light;
	bool mute;

	Type type;
	int color;
	bool sounds[SOUNDS_MAX];
	int volume;
	int notelength;
	int notelength_adjust;
	int arp_size;
	int arp_speed;
	int arp_dir;
	int basepitch;
	int time_shift;
	int note_source;

	MapGraph* content;
	MapGraph* envelope;
	unsigned char* song_cache;
	bool song_cache_dirty;

	NMChannel(Type type_) : type(type_)
	{
		content=new MapGraph();
		content->implicitly_triggered=false;
		content->content_type=(type==Type::CH?2:1);
		envelope=new MapGraph();
		envelope->content_type=3;
		song_cache=new unsigned char[0x10000/8];
		Clear();
	}
	~NMChannel()
	{
		delete content;
		delete envelope;
		delete[] song_cache;
	}
	void ClearSongCache()
	{
//		for(int i=0;i<0x10000/8;i++)
//			song_cache[i]=0xFF;
		memset(song_cache, 0xFF, 0x10000/8);
		song_cache_dirty=true;
	}
	void Clear()
	{
		active=true;
		audio_light=false;
		mute=false;

		color=0;
		for(int i=0;i<SOUNDS_MAX;i++)
			sounds[i]=false;
		volume=15;
		notelength=1;
		notelength_adjust=0;
		arp_size=1;
		arp_speed=14;
		arp_dir=0;
		basepitch=3*12;
		time_shift=0;
		note_source=-1;

		content->Clear();
		envelope->Clear();
		ClearSongCache();
	}
	void CopyFrom(NMChannel* src)
	{
		MapGraph* cg=content;
		MapGraph* eg=envelope;
		unsigned char* sc=song_cache;
		memcpy(this, src, sizeof(NMChannel));
		content=cg;
		envelope=eg;
		song_cache=sc;
		content->CopyRangeFrom(src->content, 0, 0x10000, 0);
		envelope->CopyRangeFrom(src->envelope, 0, 0x10000, 0);
		ClearSongCache();
	}
	int GetNoteLength() const
	{
		const auto result=notelength*timeline_substeps+notelength_adjust;
		return (result>=0?result:0);
	}
	inline int GetArpRate() const
	{
		assert(arp_speed>=1 && arp_speed<=16);
		assert(native_mode>=0 && native_mode<=1);
		static constexpr uint8_t rates[2][16]=
		{
			{ 31,28,25,22,18,16,14,12,10,8,7,6,5,4,3,2 },
			{ 32,28,24,24,20,16,16,12,12,8,8,8,4,4,4,4 }
		};
		return rates[native_mode][arp_speed-1];
	}

	NMChannel* GetSourceChannel(NMSong* song);

	int GetLengthFrames(const NMSongPart* part, const NMSound* snd) const;

	GraphReader MakeGraphReader(NMSong* song, bool is_envelope)
	{
		GraphReader reader;
		auto* source_ch=GetSourceChannel(song);
		if(!is_envelope)
			reader.SetSource(source_ch->content, 0);
		else
			reader.SetSource(source_ch->envelope, 7);
		reader.pos_offset=time_shift;
		return reader;
	}

	void SaveFile(const char* filename)
	{
		unsigned char current_version=1;
		Bitstream bs;
		WriteCompact(bs);
		FILE* file=fopen(filename, "wb");
		fwrite(&current_version, 1, 1, file);
		bs.WriteToFile(file);
		fclose(file);
	}
	void OpenFile(const char* filename)
	{
		//unsigned char current_version=1;

		FILE* file=fopen(filename, "rb");

		fseek(file, 0, SEEK_END);
		auto fl=ftell(file)-1;
		fseek(file, 0, SEEK_SET);
		unsigned char* fbuffer=new unsigned char[fl];

		unsigned char version=0;
		fread(&version, 1, 1, file);
		fread(fbuffer, fl, 1, file);
		fclose(file);

		bool soundmap[SOUNDS_MAX];
		for(int i=0;i<SOUNDS_MAX;i++)
			soundmap[i]=sounds[i];

		Bitstream bs;
		bs.FromBuffer(fbuffer, fl);
		ReadCompact(bs);

		for(int i=0;i<SOUNDS_MAX;i++)
			sounds[i]=soundmap[i];

		delete[] fbuffer;
	}

	void Write(NMByteBuffer& bb) const
	{
		char byteval;
		unsigned char byteval_uns;
		byteval=color; bb.WriteBytes(&byteval, 1);
		byteval=volume; bb.WriteBytes(&byteval, 1);
		if(type==Type::CH)
		{
			byteval=notelength; bb.WriteBytes(&byteval, 1);
			byteval=notelength_adjust; bb.WriteBytes(&byteval, 1);
			byteval=arp_size; bb.WriteBytes(&byteval, 1);
			byteval=arp_speed; bb.WriteBytes(&byteval, 1);
			byteval=arp_dir; bb.WriteBytes(&byteval, 1);

			byteval=time_shift; bb.WriteBytes(&byteval, 1);
			byteval_uns=note_source; bb.WriteBytes(&byteval_uns, 1);
		}
		if(type==Type::ST)
		{
			byteval=basepitch; bb.WriteBytes(&byteval, 1);
		}

		char num_sounds=0;
		for(int i=0;i<SOUNDS_MAX;i++)
			if(sounds[i])
				num_sounds++;
		bb.WriteBytes(&num_sounds, 1);
		for(char i=0;i<SOUNDS_MAX;i++)
			if(sounds[i])
				bb.WriteBytes(&i, 1);

		content->Write(bb);
		envelope->Write(bb);
	}
	void Read(NMByteBuffer& bb)
	{
		Clear();

		unsigned char byteval=0;
		char byteval_signed=0;
		if(song_file_version<4)
		{
			bb.ReadBytes(&byteval, 1);// type=byteval;
		}
		bb.ReadBytes(&byteval, 1); color=byteval;
		if(song_file_version<7)
		{
			int colormap[]={4,0,1};
			color=colormap[color];
		}
		bb.ReadBytes(&byteval, 1); volume=byteval;
		if(type==Type::CH || song_file_version<4)
		{
			bb.ReadBytes(&byteval, 1); notelength=byteval;
			if(song_file_version>=3)
				bb.ReadBytes(&byteval_signed, 1); notelength_adjust=byteval_signed;
			bb.ReadBytes(&byteval, 1); arp_size=byteval;
			bb.ReadBytes(&byteval, 1); arp_speed=byteval;
			bb.ReadBytes(&byteval, 1); arp_dir=byteval;

			if(song_file_version>=11)
			{
				bb.ReadBytes(&byteval_signed, 1); time_shift=byteval_signed;
				bb.ReadBytes(&byteval, 1); note_source=byteval==255?-1:byteval;
			}
		}
		if(type==Type::ST && song_file_version>=4)
		{
			bb.ReadBytes(&byteval, 1); basepitch=byteval;
		}

		if(song_file_version==2)
			notelength/=timeline_substeps;

		unsigned char num_sounds=0;
		bb.ReadBytes(&num_sounds, 1);
		for(int ii=0;ii<num_sounds;ii++)
		{
			unsigned char i=0;
			bb.ReadBytes(&i, 1);
			sounds[i]=true;
		}

		content->Read(bb);
		envelope->Read(bb);

		active=true;
	}

	void WriteCompact(Bitstream& bs) const;
	void ReadCompact(Bitstream& bs);
};

#include "nmas.h"

constexpr bool IsChstCh(int chst)
{
	return chst>=0;
}

constexpr bool IsChstSt(int chst)
{
	return chst<=-2;
}

constexpr int ChChst(int ch_idx)
{
	assert(ch_idx>=0);
	return ch_idx;
}

constexpr int StChst(int st_idx)
{
	assert(st_idx>=0);
	return -st_idx-2;
}

constexpr int ChstCh(int chst)
{
	assert(IsChstCh(chst));
	return chst;
}

constexpr int ChstSt(int chst)
{
	assert(IsChstSt(chst));
	return -chst-2;
}

struct NMSongPart
{
	int tempo;
	int timesig1;
	int timesig2;

	int swing_amount;
	int swing_time;
	int _old_swing_amount;

	int volume;

	int mark_start;
	int mark_end;
	int mark_loop;

	std::array<std::unique_ptr<NMChannel>, 20> st_store;
	std::array<std::unique_ptr<NMChannel>, 100> ch_store;

	static constexpr int scalemap_size=256*256/6+1; // maximum number of beats in the song, for N/3 time signature
	std::array<unsigned short, scalemap_size> scalemap={};

	int color;

	NMSongPart()
	{
		Clear();
	}
	void Clear()
	{
		color=1;

		tempo=20;
		timesig1=4;
		timesig2=4;

		swing_amount=0;
		swing_time=4;
		_old_swing_amount=swing_amount;

		volume=10;

		mark_start=0;
		mark_end=timesig1*2;
		mark_loop=mark_start;

		for(int i=0;i<20;i++)
			if(st_store[i]!=NULL)
			{
				st_store[i]->Clear();
				st_store[i]->active=false;
			}
		for(int i=0;i<100;i++)
			if(ch_store[i]!=NULL)
			{
				ch_store[i]->Clear();
				ch_store[i]->active=false;
			}
		for(int i=0;i<scalemap_size;i++)
			scalemap[i]=0x0000;
	}

	constexpr int GetTicksPerBeat() const
	{
		return timesig2*2*timeline_substeps;
	}

	constexpr int GetStartPos() const
	{
		return mark_start*GetTicksPerBeat();
	}

	constexpr int GetLoopPos() const
	{
		return mark_loop*GetTicksPerBeat();
	}

	constexpr int GetEndPos() const
	{
		return mark_end*GetTicksPerBeat();
	}

	constexpr int GetEndFrame() const
	{
		const int loop=GetLoopPos();
		const int end=GetEndPos();
		int start=GetStartPos();
		if(loop<start)
			start=loop;

		return Pos2Frames(end-start);
	}

	constexpr int GetLoopFrame() const
	{
		const int loop=GetLoopPos();
		int start=GetStartPos();
		if(loop<start)
			start=loop;

		return Pos2Frames(loop-start);
	}

	constexpr int GetTempoRateRaw() const
	{
		int rate=35-tempo;
		// NOTE: tsone: workaround bug at tempo=32
		return rate>=timeline_substeps?rate:timeline_substeps;
	}

	constexpr int GetTempoRateSwing(int curpos) const
	{
		int tempo_rate=GetTempoRateRaw();
		const int amount=((tempo_rate-timeline_substeps)*swing_amount)/20;

		if ((curpos/((GetTicksPerBeat()<<2)>>swing_time))&1)
			return tempo_rate-amount;
		else
			return tempo_rate+amount;
	}

	constexpr int Pos2Frames(int pos) const
	{
		// round up
		return (pos*GetTempoRateRaw()+timeline_substeps-1)/timeline_substeps/4;
	}
};

extern void BuildGridCache();

struct NMSong
{
	// Version history, new features:
	// 0-6: Not publicly released.
	// 7: Channel color map change.
	// 8: Delta modulation channel, DPCM samples (DMC).
	// 9: Native mode.
	// 10: Native mode?
	// 11: Channel time shift, note source.
	// 12: Song parts.
	// 13: Channel alt wavemode (duty cycle, long/short noise).
	// 14: Song part swing.
	static const int NM_SONG_VERSION=14;

	unsigned worktimer_seconds;

	char title[256];
	char author[256];

	int gui_scroll_x;
	int gui_scroll_y;
	int gui_scroll_song;
	int gui_scroll_sounds;
	int gui_scroll_channels;
	bool gui_chenv_expanded;
	bool gui_st_expanded;
	bool gui_top_expanded;
	bool gui_show_worktimer;
	int gui_envtype;

	int settings_audio_driver;
	int settings_audio_buffer;
	int settings_audio_timing;
	int settings_lowpower;
	int settings_altbuttons;
	int settings_gamma;

	int selected_sound;
	int selected_st;
	int selected_ch;
	std::array<std::unique_ptr<NMSound>, SOUNDS_MAX> sound_store;

	std::array<std::unique_ptr<NMSongPart>, PARTS_MAX> song_parts;
	NMSongPart* part; // points to song_parts array
	int cur_part;

	std::unique_ptr<NMSound> tempsound;
	std::unique_ptr<NMChannel> tempchannel;

	bool SelectSongPart(int index)
	{
		if(index<0 || index>=PARTS_MAX || !song_parts[index] || cur_part==index)
			return false;

		cur_part=index;
		part=song_parts[index].get();

		ResetView();

		return true;
	}

	NMSong()
	{
		tempsound.reset(new NMSound());
		tempchannel.reset(new NMChannel(NMChannel::Type::ST));

		worktimer_seconds=0;

		gui_chenv_expanded=false;
		gui_st_expanded=false;
		gui_top_expanded=false;
		gui_show_worktimer=true;

		settings_audio_driver=0;
		settings_audio_buffer=15;
		settings_audio_timing=50;
		settings_lowpower=0;
		settings_altbuttons=0;
		settings_gamma=88;

		ReadSettings();

		Clear();
	}

	void WriteSettings()
	{
		FILE* settingsfile=fopen("startup.txt", "w");
		if(settingsfile)
		{
			fprintf(settingsfile, "audio_driver %i\n", settings_audio_driver);
			fprintf(settingsfile, "audio_buffer %i\n", settings_audio_buffer);
			fprintf(settingsfile, "audio_timing %i\n", settings_audio_timing);
			fprintf(settingsfile, "low_power %i\n", settings_lowpower);
			fprintf(settingsfile, "alt_buttons %i\n", settings_altbuttons);
			fprintf(settingsfile, "gamma %i\n", settings_gamma);
			fclose(settingsfile);
		}
	}

	void ReadSettings()
	{
		FILE* settingsfile=fopen("startup.txt", "r");
		if(settingsfile)
		{
			fscanf(settingsfile, "audio_driver %i\n", &settings_audio_driver);
#if !HEADLESS
			settings_audio_driver%=nmas_drivers_count;
#endif
			fscanf(settingsfile, "audio_buffer %i\n", &settings_audio_buffer);
			fscanf(settingsfile, "audio_timing %i\n", &settings_audio_timing);
			fscanf(settingsfile, "low_power %i\n", &settings_lowpower);
			fscanf(settingsfile, "alt_buttons %i\n", &settings_altbuttons);
			fscanf(settingsfile, "gamma %i\n", &settings_gamma);
			fclose(settingsfile);
		}
	}

	void Clear()
	{
		strcpy(title, "UNTITLED");
		strcpy(author, "UNKNOWN");

		ResetView();

		selected_sound=-1;
		selected_st=-1;
		selected_ch=-1;

		sound_store={};
		song_parts={};

		// must have at least one part
		cur_part=-1;
		song_parts[0].reset(new NMSongPart());
		SelectSongPart(0);
	}

	inline void ResetView()
	{
		gui_scroll_x=-32;
		gui_scroll_y=-53*15;//was: *timeline_gridstep_y;
		gui_scroll_song=-20;
		gui_scroll_sounds=0;
		gui_scroll_channels=0;
		gui_envtype=0;
		gui_show_worktimer=true;
	}

	void Write(unsigned char** buffer, long* size) const
	{
		NMByteBuffer bb;

		char version=NM_SONG_VERSION;
		bb.WriteBytes(&version, sizeof(char));

		song_file_version=version;

		char num_parts=0;
		for(int i=0;i<PARTS_MAX;i++)
			if(song_parts[i]!=NULL)
				num_parts++;
		bb.WriteBytes(&num_parts, 1);

		char byteval=0;
		unsigned short wordval=0;
		for(int pi=0;pi<PARTS_MAX;pi++)
		{
			const auto* p=song_parts[pi].get();
			if(!p) continue;

			byteval=pi; bb.WriteBytes(&byteval, 1);

			byteval=p->tempo; bb.WriteBytes(&byteval, 1);
			byteval=p->timesig1; bb.WriteBytes(&byteval, 1);
			byteval=p->timesig2; bb.WriteBytes(&byteval, 1);
			byteval=p->volume; bb.WriteBytes(&byteval, 1);

			wordval=p->mark_start; bb.WriteBytes(&wordval, 2);
			wordval=p->mark_end; bb.WriteBytes(&wordval, 2);
			wordval=p->mark_loop; bb.WriteBytes(&wordval, 2);

			byteval=p->color; bb.WriteBytes(&byteval, 1);

			byteval=p->swing_amount; bb.WriteBytes(&byteval, 1);
			byteval=p->swing_time; bb.WriteBytes(&byteval, 1);
		}

		byteval=native_mode; bb.WriteBytes(&byteval, 1);

		bb.WriteBytes(title, (int)strlen(title)+1);
		bb.WriteBytes(author, (int)strlen(author)+1);

		byteval=selected_sound; bb.WriteBytes(&byteval, 1);
		byteval=selected_st; bb.WriteBytes(&byteval, 1);
		byteval=selected_ch; bb.WriteBytes(&byteval, 1);

		bb.WriteBytes(&gui_scroll_x, sizeof(int));
		bb.WriteBytes(&gui_scroll_y, sizeof(int));
		bb.WriteBytes(&gui_scroll_song, sizeof(int));
		bb.WriteBytes(&gui_chenv_expanded, sizeof(bool));
		bb.WriteBytes(&gui_st_expanded, sizeof(bool));
		bb.WriteBytes(&gui_top_expanded, sizeof(bool));
		bb.WriteBytes(&gui_show_worktimer, sizeof(bool));
		byteval=gui_envtype; bb.WriteBytes(&byteval, 1);
		bb.WriteBytes(&worktimer_seconds, sizeof(int));

		char num_sounds=0;
		for(const auto& sound:sound_store)
			if(sound && sound->active)
				num_sounds++;
		bb.WriteBytes(&num_sounds, 1);
		for(size_t i=0;i<sound_store.size();i++)
			if(sound_store[i] && sound_store[i]->active)
			{
				byteval=i; bb.WriteBytes(&byteval, 1);
				sound_store[i]->Write(bb);
			}

		for(int pi=0;pi<PARTS_MAX;pi++)
		{
			const auto* p=song_parts[pi].get();
			if(!p) continue;

			char num_st=0;
			for(int i=0;i<20;i++)
			{
				if(p->st_store[i]!=NULL && p->st_store[i]->active)
					num_st++;
			}
			bb.WriteBytes(&num_st, 1);
			for(int i=0;i<20;i++)
			{
				if(p->st_store[i]!=NULL && p->st_store[i]->active)
				{
					byteval=i; bb.WriteBytes(&byteval, 1);
					p->st_store[i]->Write(bb);
				}
			}

			char num_ch=0;
			for(int i=0;i<100;i++)
			{
				if(p->ch_store[i]!=NULL && p->ch_store[i]->active)
					num_ch++;
			}
			bb.WriteBytes(&num_ch, sizeof(char));
			for(int i=0;i<100;i++)
			{
				if(p->ch_store[i]!=NULL && p->ch_store[i]->active)
				{
					byteval=i; bb.WriteBytes(&i, 1);
					p->ch_store[i]->Write(bb);
				}
			}

			unsigned short scalemap_maxlength=-1;
			for(int i=0;i<NMSongPart::scalemap_size;i++)
			{
				if(p->scalemap[i]!=0x0000)
					scalemap_maxlength=(unsigned short)i;
			}
			scalemap_maxlength+=1;
			bb.WriteBytes(&scalemap_maxlength, 2);
			int prev_i=0;
			for(int i=0;i<scalemap_maxlength;i++)
			{
				unsigned short s=p->scalemap[i];
				if(s!=0x0000)
				{
					unsigned char gaplength=i-prev_i;
					bb.WriteBytes(&gaplength, 1);
					bb.WriteBytes(&s, 2);
					prev_i=i+1;
				}
				else if(i-prev_i==255 || i==scalemap_maxlength)
				{
					unsigned char gaplength=i-prev_i;
					bb.WriteBytes(&gaplength, 1);
					unsigned short empty=0x0000;
					bb.WriteBytes(&empty, 2);
					prev_i=i+1;
				}
			}
		}

		byteval=cur_part;
		bb.WriteBytes(&byteval, 1);

		*size=bb.Size();
		*buffer=bb.Export();
	}

	bool Read(unsigned char* buffer, long size)
	{
		Clear();
		cur_part=-1;

		NMByteBuffer bb;
		bb.bytebuffer=buffer;
		bb.bytebuffer_alloc=size;
		bb.bytebuffer_i=0;

		char version=-1;
		bb.ReadBytes(&version, sizeof(char));
		if(version>NM_SONG_VERSION)
		{
			bb.Export();
			return false;
		}

		song_file_version=version;

		native_mode=0;

		char num_parts=1;
		if(version>=12)
			bb.ReadBytes(&num_parts, 1);

		char byteval=0;
		unsigned short wordval=0;
		for(char mi=0;mi<num_parts;mi++)
		{
			int pi=0;
			if(version>=12)
			{
				bb.ReadBytes(&byteval, 1); pi=byteval;
			}

			auto* p=new NMSongPart();
			song_parts[pi].reset(p);

			bb.ReadBytes(&byteval, 1); p->tempo=byteval;
			bb.ReadBytes(&byteval, 1); p->timesig1=byteval;
			p->timesig1=4; // NOTE: tsone: hard-coded 1/4 beat
			bb.ReadBytes(&byteval, 1); p->timesig2=byteval;
			bb.ReadBytes(&byteval, 1); p->volume=byteval;

			if(version>=10 && version<12)
			{
				bb.ReadBytes(&byteval, 1); native_mode=byteval;
			}

			bb.ReadBytes(&wordval, 2); p->mark_start=wordval;
			bb.ReadBytes(&wordval, 2); p->mark_end=wordval;
			bb.ReadBytes(&wordval, 2); p->mark_loop=wordval;
			if(version<2)
			{
				p->mark_start/=p->timesig2*2;
				p->mark_end/=p->timesig2*2;
				p->mark_loop/=p->timesig2*2;
			}

			if(version>=12)
			{
				bb.ReadBytes(&byteval, 1); p->color=byteval;
			}

			if(version>=14)
			{
				bb.ReadBytes(&byteval, 1); p->swing_amount=byteval;
				bb.ReadBytes(&byteval, 1); p->swing_time=byteval;
			}
		}

		if(version>=12)
		{
			bb.ReadBytes(&byteval, 1); native_mode=byteval;

			for(int i=0;i<sizeof(title);i++)
			{
				bb.ReadBytes(&title[i], 1);
				if(title[i]=='\0')
					break;
			}
			for(int i=0;i<sizeof(author);i++)
			{
				bb.ReadBytes(&author[i], 1);
				if(author[i]=='\0')
					break;
			}
		}

		bb.ReadBytes(&byteval, 1); selected_sound=byteval;
		bb.ReadBytes(&byteval, 1); selected_st=byteval;
		bb.ReadBytes(&byteval, 1); selected_ch=byteval;

		bb.ReadBytes(&gui_scroll_x, sizeof(int));
		bb.ReadBytes(&gui_scroll_y, sizeof(int));
		bb.ReadBytes(&gui_scroll_song, sizeof(int));
		bb.ReadBytes(&gui_chenv_expanded, sizeof(bool));
		if(version>=4)
		{
			bb.ReadBytes(&gui_st_expanded, sizeof(bool));
			bb.ReadBytes(&gui_top_expanded, sizeof(bool));
			if(version>=5)
				bb.ReadBytes(&gui_show_worktimer, sizeof(bool));
		}
		bb.ReadBytes(&byteval, 1); gui_envtype=byteval;
		if(version>=3)
			bb.ReadBytes(&worktimer_seconds, sizeof(int));
		else
			worktimer_seconds=0;

		char num_sounds=0;
		bb.ReadBytes(&num_sounds, 1);
		for(int ii=0;ii<num_sounds;ii++)
		{
			char i=0;
			bb.ReadBytes(&i, 1);
			assert(!sound_store[i]);
			if(sound_store[i]==NULL)
				sound_store[i].reset(new NMSound());
			sound_store[i]->Read(bb);
			if(version==0)
				sound_store[i]->name=i;
		}

		for(int pi=0;pi<PARTS_MAX;pi++)
		{
			auto* p=song_parts[pi].get();
			if(!p) continue;

			char num_st=0;
			bb.ReadBytes(&num_st, 1);
			for(int ii=0;ii<num_st;ii++)
			{
				char i=0;
				bb.ReadBytes(&i, 1);
				p->st_store[i].reset(new NMChannel(NMChannel::Type::ST));
				p->st_store[i]->Read(bb);
			}

			char num_ch=0;
			bb.ReadBytes(&num_ch, 1);
			for(int ii=0;ii<num_ch;ii++)
			{
				char i=0;
				bb.ReadBytes(&i, 1);
				p->ch_store[i].reset(new NMChannel(NMChannel::Type::CH));
				p->ch_store[i]->Read(bb);
			}

			unsigned short scalemap_maxlength=0;
			bb.ReadBytes(&scalemap_maxlength, 2);
			int i=0;
			while(i<scalemap_maxlength)
			{
				unsigned char gaplength=0;
				bb.ReadBytes(&gaplength, 1);
				i+=gaplength;
				bb.ReadBytes(&p->scalemap[i], 2);
				i++;
			}
		}

		byteval=0;
		if(version>=12)
			bb.ReadBytes(&byteval, 1);
		SelectSongPart(byteval);

		bb.Export();
		return true;
	}

	void WriteCompact(unsigned char** buffer, long* size);
	void ReadCompact(unsigned char* buffer, long size);

	NMSound* GetSound(int i)
	{
		if(i==-1)
			return NULL;
		if(sound_store[i]==NULL || !sound_store[i]->active)
			return NULL;
		return sound_store[i].get();
	}
	int FindSoundName()
	{
		int name=-1;
		for(int n=0;n<SOUNDS_MAX;n++)
		{
			bool nfree=true;
			for(int k=0;k<SOUNDS_MAX;k++)
				if(sound_store[k]!=NULL && sound_store[k]->active && sound_store[k]->name==n)
				{
					nfree=false;
					break;
				}
			if(nfree)
			{
				name=n;
				break;
			}
		}
		return name;
	}
	void SelectSound(int i)
	{
		bool fresh=false;
		int name=FindSoundName();
		if(sound_store[i]==NULL)
		{
			sound_store[i].reset(new NMSound());
			sound_store[i]->name=name;
			fresh=true;
		}
		if(!sound_store[i]->active)
		{
			sound_store[i]->Clear();
			sound_store[i]->name=name;
			fresh=true;
		}
		if(fresh && i!=0 && sound_store[0]!=NULL && sound_store[0]->active)
		{
			sound_store[i]->CopyFrom(sound_store[0].get()); // use sound 0 as default template
			sound_store[i]->name=name;
		}
		selected_sound=i;
		selected_st=-1;
		selected_ch=-1;
	}
	void RemoveSound(int i)
	{
		if(sound_store[i]!=NULL)
		{
			sound_store[i]->active=false;

			// clear any references to this sound
			for(int pi=0;pi<PARTS_MAX;pi++)
			{
				auto* sp=song_parts[pi].get();
				if(!sp) continue;

				for(int j=0;j<20;j++)
				{
					if(sp->st_store[j]!=NULL)
						sp->st_store[j]->sounds[i]=false;
				}
				for(int j=0;j<100;j++)
				{
					if(sp->ch_store[j]!=NULL)
						sp->ch_store[j]->sounds[i]=false;
				}
			}
		}
		if(i==selected_sound)
			selected_sound=-1;
	}
	void SwapSounds(int i0, int i1) // move from i0 to i1
	{
		if(sound_store[i0]==NULL || !sound_store[i0]->active)
			return;
		if(sound_store[i1]!=NULL)
		{
			tempsound->CopyFrom(sound_store[i1].get());
			sound_store[i1]->CopyFrom(sound_store[i0].get());
			sound_store[i0]->CopyFrom(tempsound.get());
		}
		else
		{
			SelectSound(i1);
			sound_store[i1]->CopyFrom(sound_store[i0].get());
			sound_store[i0]->active=false;
		}
		// remap any references to these sounds
		for(int pi=0;pi<PARTS_MAX;pi++)
		{
			auto* sp=song_parts[pi].get();
			if(!sp) continue;

			for(int j=0;j<20;j++)
			{
				if(sp->st_store[j]!=NULL)
				{
					bool a=sp->st_store[j]->sounds[i0];
					bool b=sp->st_store[j]->sounds[i1];
					sp->st_store[j]->sounds[i0]=b;
					sp->st_store[j]->sounds[i1]=a;
				}
			}
			for(int j=0;j<100;j++)
			{
				if(sp->ch_store[j]!=NULL)
				{
					bool a=sp->ch_store[j]->sounds[i0];
					bool b=sp->ch_store[j]->sounds[i1];
					sp->ch_store[j]->sounds[i0]=b;
					sp->ch_store[j]->sounds[i1]=a;
				}
			}
		}
	}

	constexpr NMChannel* GetCHST(int i)
	{
		if(IsChstCh(i))
			return GetCH(ChstCh(i));
		else if(IsChstSt(i))
			return GetST(ChstSt(i));
		else
			return nullptr;
	}

	constexpr NMChannel* GetCH(int i)
	{
		if(i==-1)
			return NULL;
		if(part->ch_store[i]==NULL || !part->ch_store[i]->active)
			return NULL;
		return part->ch_store[i].get();
	}
	void SelectCH(int i)
	{
		bool fresh=false;
		if(part->ch_store[i]==NULL)
		{
			part->ch_store[i].reset(new NMChannel(NMChannel::Type::CH));
			fresh=true;
		}
		if(!part->ch_store[i]->active)
		{
			part->ch_store[i]->Clear();
			fresh=true;
		}
		if(fresh)
		{
			part->ch_store[i]->notelength=part->timesig2; // half a beat
		}
		selected_ch=i;
		selected_sound=-1;
		selected_st=-1;
	}
	inline void RemoveCH(int i)
	{
		if(part->ch_store[i]!=NULL)
			part->ch_store[i]->active=false;
		if(i==selected_ch)
			selected_ch=-1;
	}
	void SwapCHs(int i0, int i1) // move from i0 to i1
	{
		if(part->ch_store[i0]==NULL || !part->ch_store[i0]->active)
			return;
		if(part->ch_store[i1]!=NULL)
		{
			tempchannel->CopyFrom(part->ch_store[i1].get());
			part->ch_store[i1]->CopyFrom(part->ch_store[i0].get());
			part->ch_store[i0]->CopyFrom(tempchannel.get());
		}
		else
		{
			SelectCH(i1);
			part->ch_store[i1]->CopyFrom(part->ch_store[i0].get());
			part->ch_store[i0]->active=false;
		}
	}

	constexpr NMChannel* GetST(int i)
	{
		if(i==-1)
			return NULL;
		if(part->st_store[i]==NULL || !part->st_store[i]->active)
			return NULL;
		return part->st_store[i].get();
	}
	void SelectST(int i)
	{
		if(part->st_store[i]==NULL)
		{
			part->st_store[i].reset(new NMChannel(NMChannel::Type::ST));
		}
		if(!part->st_store[i]->active)
			part->st_store[i]->Clear();
		selected_st=i;
		selected_sound=-1;
		selected_ch=-1;
	}
	inline void RemoveST(int i)
	{
		if(part->st_store[i]!=NULL)
			part->st_store[i]->active=false;
		if(i==selected_st)
			selected_st=-1;
	}
	void SwapSTs(int i0, int i1) // move from i0 to i1
	{
		if(part->st_store[i0]==NULL || !part->st_store[i0]->active)
			return;
		if(part->st_store[i1]!=NULL)
		{
			tempchannel->CopyFrom(part->st_store[i1].get());
			part->st_store[i1]->CopyFrom(part->st_store[i0].get());
			part->st_store[i0]->CopyFrom(tempchannel.get());
		}
		else
		{
			SelectST(i1);
			part->st_store[i1]->CopyFrom(part->st_store[i0].get());
			part->st_store[i0]->active=false;
		}
	}

	constexpr int ScalemapIndex(int t) const
	{
		int i=t/(part->timesig2*2*timeline_substeps);
		i=std::min(std::max(i, 0), NMSongPart::scalemap_size-1);
		return i;
	}
	inline bool ScalemapRead(int i, int note) const
	{
		return (part->scalemap[i]&(1<<note))!=0;
	}
	inline void ScalemapWrite(int i, int note, bool state)
	{
		if(state==true)
			part->scalemap[i]|=(1<<note);
		else
			part->scalemap[i]&=0x0FFF^(1<<note);
	}

	void GetArpeggioScale(char *scale, int scale_size, int pos, int basenote, int stepdir) const;

private:
	int ReadArpeggio(int pos, int basenote, int arpeggio_pos, unsigned short* prevscale, int* prevscalepos) const;
};

int NMSong::ReadArpeggio(int pos, int basenote, int arpeggio_pos, unsigned short* prevscale, int* prevscalepos) const
{
	if(arpeggio_pos==0)
		return 0;
	if(pos<0) pos=0;
	
	unsigned int smap=(unsigned int)*prevscale;
	int sp=*prevscalepos;
	int ep=pos/(part->timesig2*2*timeline_substeps);
	if(sp>ep)
		sp=ep-1;
	if(sp==-1) // just started, search back from current position
	{
		sp=ep+1;
		while(sp>0)
		{
			sp--;
			unsigned short sm=part->scalemap[sp];
			if(sm!=0x0000)
			{
				smap=(unsigned int)sm;
				*prevscale=sm;
				break;
			}
		}
	}
	else
	{
		while(sp<ep && sp<NMSongPart::scalemap_size)
		{
			sp++;
			if(sp<0)
				sp=0;
			unsigned short sm=part->scalemap[sp];
			if(sm!=0x0000)
			{
				smap=(unsigned int)sm;
				*prevscale=sm;
			}
		}
	}
	*prevscalepos=sp;
	
	basenote%=12;
	if(((smap>>basenote)&1)==0) // didn't start on scale
		return 0;
	unsigned int smap0=smap;
	smap=smap0|(smap0<<12);
	int i=0;
	int c=0;
	//float multiplier=1.0f;
	int skipsteps=basenote;
	int stepdir=1;
	if(arpeggio_pos<0)
	{
		stepdir=-1;
		arpeggio_pos=-arpeggio_pos;
		skipsteps=12-skipsteps;
		basenote=12-basenote;
	}
	bool any=false;
	while(arpeggio_pos>0 && i<12*5)
	{
		if(stepdir==1)
			smap>>=1;
		else
			smap<<=1;
		i++;
		c++;
		if(c==12)
		{
			c=0;
			smap=smap0|(smap0<<12);
		}
		if(((smap>>12)&1)!=0 && i>skipsteps)
		{
			arpeggio_pos--;
			any=true;
		}
	}
	if(!any)
		// NOTE: tsone: was originally 5 => log2(5)=~2.32 octaves... why?
		// assume it's supposed to be 5 octaves, based on while loop limit above
		return 12<<5;
	if(stepdir==1)
		return i-basenote;
	else
		return basenote-i;
}

void NMSong::GetArpeggioScale(char *scale, int num, int pos, int basenote, int stepdir) const
{
	if(!scale)
	{
		assert(0 && "GetArpeggioScale: !scale");
		return;
	}
	if(num<0)
	{
		assert(0 && "GetArpeggioScale: num<0");
		return;
	}
	if (num>8)
	{
		assert(0 && "GetArpeggioScale: num>8");
		num=8;
	}
	if(pos<0)
	{
		assert(0 && "GetArpeggioScale: pos<0");
		pos=0;
	}
	if(pos>=0xFFFF)
	{
		assert(0 && "GetArpeggioScale: pos>=0xFFFF");
		pos=0xFFFE;
	}
	if(basenote<0)
	{
		assert(0 && "GetArpeggioScale: basenote<0");
		basenote=0;
	}
	if (!stepdir || stepdir>1)
	{
		assert(0 && "GetArpeggioScale: !stepdir || stepdir>1");
		stepdir=1;
	}
	if (stepdir<-1)
	{
		assert(0 && "GetArpeggioScale: stepdir<-1");
		stepdir=-1;
	}

	unsigned short prevscale=0;
	int prevpos=-1;
	for(int i=0;i<num;i++)
		scale[i]=ReadArpeggio(pos, basenote, i*stepdir, &prevscale, &prevpos);
	scale[num]=0;
}

NMChannel* NMChannel::GetSourceChannel(NMSong* song)
{
	auto* source_ch=song->GetCH(note_source);
	return source_ch?source_ch:this;
}

int NMChannel::GetLengthFrames(const NMSongPart* part, const NMSound* snd) const
{
	const int zero=snd->GetVolZeroPos(volume);
	const int rate=GetEnvRate(snd->v_env_speed[0])<<1;
	const int env_length=(rate>0?zero*64/(rate*256):INT_MAX);

	const int chn_length=part->Pos2Frames(GetNoteLength());

	assert(chn_length>=0);
	assert(env_length>=0);
	if(zero>16*256)
	{
		// Env volume is 0 after/at sustain loop
		return chn_length;
	}
	else
	{
		// Env volume is 0 before sustain loop, take minimum
		return std::min(chn_length, env_length);
	}
}

#include "compactfile.h"

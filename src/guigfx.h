int guib_alpha=255;
void GUIbAlpha(int a)
{
	guib_alpha=a;
}

void DrawGUIb(int x0, int y0, int w, int h, int dx, int dy)
{
	Sprite s;
	s.source=guibits;
	s.region.SetXYWH(x0,y0,w,h);
	s.dx=dx;
	s.dy=dy;
	s.colorkey=COLORKEY;
	s.alpha=guib_alpha;
	blitter.DrawSprite(s);
}
/*
void DrawGUIbfast(int x0, int y0, int w, int h, int dx, int dy)
{
	Sprite s;
	s.source=guibits;
	s.region.SetXYWH(x0,y0,w,h);
	s.dx=dx;
	s.dy=dy;
	blitter.DrawSpriteFast(s);
}
*/
void DrawGUIr(int x0, int y0, int w, int h, int dx, int dy, int rx, int ry)
{
	int r=std::max(rx, ry);
	int rxd=rx!=0?1:0;
	int ryd=ry!=0?1:0;
	for(int i=0;i<r;i++)
	{
		DrawGUIb(x0, y0, w, h, dx, dy);
		dx+=rxd;
		dy+=ryd;
	}
}

void DrawPanel(bool selected, int ltype, int rtype, int x0, int y0, int x1, int y1)
{
	if(rtype==2 || rtype==3)
		x1-=13;
	if(ltype==0)
		x0+=13;

	if(rtype==3)
		selected=false;
	int srco=selected?0:27;
	if(ltype==0)
	{
		DrawGUIb(5,45,4,3,    x0,y0);
		DrawGUIb(5,60,4,3,    x0,y1-3);
	}
	if(ltype==1)
	{
		DrawGUIb(1,45,4,3,    x0,y0);
		DrawGUIb(1,60,4,3,    x0,y1-3);
	}
	DrawGUIr(4,45,1,3,    x0+4,y0, x1-(x0+4), 0);
	DrawGUIr(4,60,1,3,    x0+4,y1-3, x1-(x0+4), 0);

	if(rtype==0)
	{
		DrawGUIb(27,45,4,3,    x1-4,y0);
		DrawGUIb(27,60,4,3,    x1-4,y1-3);
	}
	if(rtype==1)
	{
		DrawGUIb(23,45,4,3,    x1-4,y0);
		DrawGUIb(23,60,4,3,    x1-4,y1-3);
	}
	if(rtype==2 || rtype==3)
	{
		DrawGUIb(1+srco,1,26,12,   x1,y0);
		DrawGUIr(1+srco,13,26,1,   x1,y0+12, 0, (y1-12)-(y0+12));
		DrawGUIb(1+srco,26,26,12,  x1,y1-12);
	}
}

void DrawPiece(int sx0, int sy0, int sw, int sh, int dx, int dy, int dw, int dh, int mx, int my)
{
	int cy=0;
	for(int y=0;y<dh;y++)
	{
		int cx=0;
		for(int x=0;x<dw;x++)
		{
			unsigned int c=guibits.GetPixel(sx0+cx, sy0+cy);
			if(c!=COLORKEY)
				blitter.DrawPixel(dx+x, dy+y, c);
			if(cx<mx || dw-x==sw-cx)
				cx++;
		}
		if(cy<my || dh-y==sh-cy)
			cy++;
	}
}

void BuildGridCache()
{
	Box gcbox;
	gcbox.SetXYWH(0, 0, timeline_gridstep_x*2, timeline_gridstep_y);
	int gcw=gcbox.Width();
	int gch=gcbox.Height();
	for(int i=0;i<4*4;i++)
		gridcache[i].FromBox(gcbox);
	for(int li=0;li<4;li++)
	{
		blitter.SetTarget(gridcache[li*4+0]);
		DrawPiece(75,28+li*15,12,12, 0, 0, gcw, gch, 9, 7);
		blitter.SetTarget(gridcache[li*4+1]);
		DrawPiece(87,28+li*15,12,12, 0, 0, gcw, gch, 9, 7);
		blitter.SetTarget(gridcache[li*4+2]);
		DrawPiece(111,28+li*15,12,12, 0, 0, gcw, gch, 9, 7);
		blitter.SetTarget(gridcache[li*4+3]);
		DrawPiece(99,28+li*15,12,12, 0, 0, gcw, gch, 6, 7);
	}

	gcbox.SetXYWH(0, 0, timeline_gridstep_x*2*timeline_substeps, timeline_gridstep_y);
	gcw=gcbox.Width();
	gch=gcbox.Height();
	for(int i=0;i<4*4;i++)
		gridcache_zoomed[i].FromBox(gcbox);
	for(int li=0;li<4;li++)
	{
		blitter.SetTarget(gridcache_zoomed[li*4+0]);
		DrawPiece(75,28+li*15,12,12, 0, 0, gcw, gch, 9, 7);
		blitter.SetTarget(gridcache_zoomed[li*4+1]);
		DrawPiece(87,28+li*15,12,12, 0, 0, gcw, gch, 9, 7);
		blitter.SetTarget(gridcache_zoomed[li*4+2]);
		DrawPiece(111,28+li*15,12,12, 0, 0, gcw, gch, 9, 7);
		blitter.SetTarget(gridcache_zoomed[li*4+3]);
		DrawPiece(99,28+li*15,12,12, 0, 0, gcw, gch, 6, 7);
	}

	int gridstep_x=timeline_gridstep_x;
	int doublestep_x=gridstep_x*2;
//	if(timeline_zoomed)
//		doublestep_x*=timeline_substeps;
	gcbox.SetXYWH(0, 0, doublestep_x, 7);
	for(int tt=0;tt<3;tt++)
		for(int lm=-1;lm<4;lm++)
		{
			int gci=16+tt*5+lm+1;
			gridcache[gci].FromBox(gcbox);
			blitter.SetTarget(gridcache[gci]);
			int rx=0;
			int ry=1;
//			blitter.DrawBar(0, 0, doublestep_x, 7, 0xFF0000);
			DrawGUIb(124+tt*13,47+lm*8,6,7, rx, ry-1);
			DrawGUIr(124+6+tt*13,47+lm*8,1,7, rx+6, ry-1, doublestep_x-6-5, 0);
			DrawGUIb(124+12-5+tt*13,47+lm*8,6,7, rx+doublestep_x-6, ry-1);
		}
	doublestep_x*=timeline_substeps;
	gcbox.SetXYWH(0, 0, doublestep_x, 7);
	for(int tt=0;tt<3;tt++)
		for(int lm=-1;lm<4;lm++)
		{
			int gci=16+tt*5+lm+1;
			gridcache_zoomed[gci].FromBox(gcbox);
			blitter.SetTarget(gridcache_zoomed[gci]);
			int rx=0;
			int ry=1;
//			blitter.DrawBar(0, 0, doublestep_x, 7, 0x00FF00);
			DrawGUIb(124+tt*13,47+lm*8,6,7, rx, ry-1);
			DrawGUIr(124+6+tt*13,47+lm*8,1,7, rx+6, ry-1, doublestep_x-6-5, 0);
			DrawGUIb(124+12-5+tt*13,47+lm*8,6,7, rx+doublestep_x-6, ry-1);
		}

	blitter.SetTarget(window);
}

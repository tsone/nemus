#ifndef BITSTREAM_H
#define BITSTREAM_H

#include "port.h"

class Bitstream
{
	long length;
	unsigned char* bytes;
	long numbytes;
	int readbyte;
	long allocbytes;
	char bitbuffer[8];
	int bitpos;
	int elapsedbits;
	FILE* source_file;

public:
	Bitstream()
	{
		length=0;
		bytes=NULL;
		numbytes=0;
		readbyte=0;
		allocbytes=0;
		bitpos=0;
		elapsedbits=0;
		source_file=NULL;
	}
	~Bitstream()
	{
		delete[] bytes;
	}
	int BitPos()
	{
		return elapsedbits;
	}
	void Clear()
	{
		length=0;
		numbytes=0;
		readbyte=0;
		bitpos=0;
		elapsedbits=0;
		source_file=NULL;
	}
	void FromBuffer(unsigned char* buffer, long size)
	{
		Clear();
		allocbytes=size;
		bytes=new unsigned char[size];
		for(int i=0;i<size;i++)
			bytes[i]=buffer[i];
		numbytes=size;
		length=numbytes*8;
	}
	void FromFile(FILE* file)
	{
		Clear();
		source_file=file;
		auto curpos=ftell(file);
		fseek(file, 0, SEEK_END);
		auto remaining_size=ftell(file)-curpos;
		fseek(file, curpos, SEEK_SET);
		numbytes=remaining_size;
		length=numbytes*8;
	}
	void Append(int bit)
	{
		bitbuffer[bitpos++]=bit;
		length++;
		if(bitpos==8)
		{
			if(numbytes==allocbytes)
			{
				if(allocbytes==0)
					allocbytes=1024;
				else
					allocbytes=allocbytes*3/2;
				unsigned char* newbytes=new unsigned char[allocbytes];
				for(int i=0;i<numbytes;i++)
					newbytes[i]=bytes[i];
				delete[] bytes;
				bytes=newbytes;
			}
			unsigned char byte=0x00;
			for(int i=0;i<8;i++)
			{
				byte<<=1;
				byte|=bitbuffer[7-i];
			}
			bytes[numbytes++]=byte;
			bitpos=0;
		}

		elapsedbits++;
	}
	void Append(Bitstream& stream)
	{
		stream.Restart(); // assume nobody else is using it
		auto l=stream.Length();
		for(int i=0;i<l;i++)
			Append(stream.ReadBit());
	}
	void AppendValue(unsigned int value, int numbits)
	{
		unsigned char bits[32];
		for(int i=0;i<numbits;i++)
			bits[i]=(value>>(numbits-1-i))&1;
		for(int i=0;i<numbits;i++)
			Append(bits[i]);
	}
	void AppendVLQ(unsigned int v)
	{
		while(v)
		{
			AppendValue(v&0x7F, 7);
			v>>=7;
			AppendValue(v?1:0, 1);
		}
	}
	int ReadBit()
	{
		if(bitpos==0)
		{
			if(readbyte>=numbytes)
				return 2;
			unsigned char byte=0;
			if(source_file!=NULL)
			{
				if(!feof(source_file))
					fread(&byte, 1, 1, source_file);
			}
			else
				byte=bytes[readbyte++];
			for(int i=0;i<8;i++)
				bitbuffer[i]=(byte>>(7-i))&1;
			bitpos=8;
		}
		elapsedbits++;

		return bitbuffer[--bitpos];
	}
	unsigned int ReadValue(int numbits)
	{
		unsigned char bits[32];
		for(int i=0;i<numbits;i++)
			bits[i]=ReadBit();
		unsigned int value=0;
		for(int i=0;i<numbits;i++)
		{
			value<<=1;
			value|=bits[i];
		}
		return value;
	}
	unsigned int ReadVLQ()
	{
		unsigned int result=0, shift=0;
		do
		{
			result|=ReadValue(7)<<shift;
			shift+=7;
		}
		while(ReadValue(1));
		return result;
	}
	long Length() const
	{
		return length;
	}
	long ByteLength() const
	{
		return (length+7)/8;
	}
	void Restart()
	{
		PadToBytes(); // can't write anymore

		readbyte=0;
		bitpos=0;
		elapsedbits=0;
	}
	long PadToBytes()
	{
		int padding=(8-bitpos)&7;
		for(int i=0;i<padding;i++)
			Append(0);
		return numbytes;
	}
	void WriteToFile(FILE* file)
	{
		// permanent zero bit padding to make even byte count (assume no further modifications to stream)
		PadToBytes();

		fwrite(bytes, numbytes, 1, file);
	}
	void WriteToBuffer(unsigned char* buffer)
	{
		// permanent zero bit padding to make even byte count (assume no further modifications to stream)
		PadToBytes();

		for(int i=0;i<numbytes;i++)
			buffer[i]=bytes[i];
	}

	int BitWidth(unsigned int value)
	{
		int bits=0;
		while(value!=0)
		{
			bits++;
			value>>=1;
		}
		return std::max(bits, 1);
	}
};

#endif

struct Sprite;

struct UColor
{
	unsigned char b, g, r, a;
	UColor()
	{
	}
	UColor(unsigned char _a, unsigned char _r, unsigned char _g, unsigned char _b)
	{
		a=_a;
		r=_r;
		g=_g;
		b=_b;
	}
	UColor(unsigned int c)
	{
		*this=*((UColor*)&c);
	}
	unsigned int ToUInt()
	{
		return *(unsigned int*)this;
	}
};

struct Image
{
	unsigned int* data;
	int width;
	int height;
	Image()
	{
		data=NULL;
		width=0;
		height=0;
	}
	~Image()
	{
	}
	unsigned int GetPixel(int x, int y)
	{
		if(x>=0 && y>=0 && x<width && y<height)
			return data[y*width+x];
		else
			return 0x000000;
	}
	Box Bounds()
	{
		Box bounds;
		bounds.x0=0;
		bounds.y0=0;
		bounds.x1=width;
		bounds.y1=height;
		return bounds;
	}
	void FromBox(Box& box)
	{
		if(box.Width()!=width || box.Height()!=height)
		{
			delete[] data;
			width=box.Width();
			height=box.Height();
			data=new unsigned int[width*height];
		}
	}
	void CopyFrom(Image& src)
	{
		FromBox(src.Bounds());
		for(int i=0;i<width*height;i++)
			data[i]=src.data[i];
	}
	void Fill(unsigned int color)
	{
		for(int i=0;i<width*height;i++)
			data[i]=color;
	}
	void FillBox(Box box, unsigned int color)
	{
		box.Clip(Bounds());
		for(int y=box.y0;y<box.y1;y++)
			for(int x=box.x0;x<box.x1;x++)
				data[y*width+x]=color;
	}
	Sprite GetSprite(int name, int x, int y);
	Sprite GetSprite(int name, float x, float y);
};

struct Sprite
{
	Image source;
	Box region;
	Point2D origin;
	int dx, dy;
	int layer;
	bool xflip;
	bool yflip;
	bool flash;
	unsigned int colorkey;
	unsigned int tintcolor;
	int alpha;

	Sprite()
	{
		dx=0;
		dy=0;
		origin=Point2D(0,0);
		layer=0;
		xflip=false;
		yflip=false;
		flash=false;
		colorkey=0x000040;
		tintcolor=0xFFFFFFFF;
		alpha=255;
	}
	~Sprite()
	{
	}
};

class Blitter
{
public:
	bool enable;

	Image target;

	int scroll_x;
	int scroll_y;
	Box clipbox;

	bool zoom;
	double zoom_x0;
	double zoom_y0;
	double zoom_x1;
	double zoom_y1;

	Blitter()
	{
		enable=true;

		target.data=NULL;
		target.width=0;
		target.height=0;

		scroll_x=0;
		scroll_y=0;
		clipbox.SetXYWH(0,0,0,0);

		zoom=false;
		zoom_x0=0.0;
		zoom_y0=0.0;
		zoom_x1=1.0;
		zoom_y1=1.0;
	}
	~Blitter()
	{
	}

	void SetTarget(Image& t)
	{
		target=t;
	}

	void DrawPixel(int x, int y, unsigned int color)
	{
		if(!enable)
			return;

		x-=scroll_x;
		y-=scroll_y;
		if(clipbox.Width()!=0 && (x<clipbox.x0 || y<clipbox.y0 || x>=clipbox.x1 || y>=clipbox.y1))
			return;
		if(x<0 || y<0 || x>=target.width || y>=target.height)
			return;
		target.data[y*target.width+x]=color;
	}

	void DrawBar(int x0, int y0, int w, int h, unsigned int color)
	{
		if(!enable)
			return;

		x0-=scroll_x;
		y0-=scroll_y;
		Box r;
		r.SetXYWH(x0, y0, w, h);
		r.Clip(target.Bounds());
		if(clipbox.Width()!=0)
			r.Clip(clipbox);

		x0=r.x0;
		y0=r.y0;
		w=r.Width();
		h=r.Height();
		for(int y=0;y<h;y++)
		{
			int di=(y0+y)*target.width+x0;
			for(int x=0;x<w;x++)
			{
				target.data[di]=color;
				di++;
			}
		}
	}

	void DrawBox(int x0, int y0, int w, int h, unsigned int color)
	{
		if(!enable)
			return;

		DrawBar(x0, y0, w, 1, color);
		DrawBar(x0, y0+h-1, w, 1, color);
		DrawBar(x0, y0, 1, h, color);
		DrawBar(x0+w-1, y0, 1, h, color);
	}

	void DrawBarAlpha(int x0, int y0, int w, int h, unsigned int color)
	{
		if(!enable)
			return;

		x0-=scroll_x;
		y0-=scroll_y;

		Box r;
		r.SetXYWH(x0, y0, w, h);
		r.Clip(target.Bounds());
		if(clipbox.Width()!=0)
			r.Clip(clipbox);
		x0=r.x0;
		y0=r.y0;
		w=r.Width();
		h=r.Height();

		int cr=(color>>16)&0xFF;
		int cg=(color>>8)&0xFF;
		int cb=color&0xFF;
		int alpha=(color>>24)&0xFF;
		alpha+=1;
		for(int y=0;y<h;y++)
		{
			int di=(y0+y)*target.width+x0;
			for(int x=0;x<w;x++)
			{
				unsigned int& c=target.data[di];
				int r=(c>>16)&0xFF;
				int g=(c>>8)&0xFF;
				int b=c&0xFF;
				r+=(cr-r)*alpha/256;
				g+=(cg-g)*alpha/256;
				b+=(cb-b)*alpha/256;
				c=(r<<16)|(g<<8)|b;
				di++;
			}
		}
	}

	void DrawBoxAlpha(int x0, int y0, int w, int h, unsigned int color)
	{
		if(!enable)
			return;

		DrawBarAlpha(x0, y0, w, 1, color);
		DrawBarAlpha(x0, y0+h-1, w, 1, color);
		DrawBarAlpha(x0, y0+1, 1, h-2, color);
		DrawBarAlpha(x0+w-1, y0+1, 1, h-2, color);
	}

	void DrawAntBox(int x0, int y0, int w, int h, unsigned int color)
	{
		if(!enable)
			return;

		x0-=scroll_x;
		y0-=scroll_y;

		static int t0=0;
		if(t0==0)
			t0=glrAppTimeMs();
		int phase=(glrAppTimeMs()-t0)/50%12;

		for(int x=-12;x<w+12;x+=12)
		{
			int a=x+phase;
			int b=a+6;
			if(a<0) a=0;
			if(b>w) b=w;
			DrawBar(x0+a, y0, b-a, 1, color);
			DrawBar(x0+a, y0+h-1, b-a, 1, color);
		}
		for(int y=-12;y<h+12;y+=12)
		{
			int a=y+phase;
			int b=a+6;
			if(a<0) a=0;
			if(b>h) b=h;
			DrawBar(x0, y0+a, 1, b-a, color);
			DrawBar(x0+w-1, y0+a, 1, b-a, color);
		}
	}


	void EnableZoom(double x0, double y0, double x1, double y1)
	{
		zoom=true;
		zoom_x0=x0;
		zoom_y0=y0;
		zoom_x1=x1;
		zoom_y1=y1;
	}

	void EnableZoom()
	{
		zoom=true;
	}
	void DisableZoom()
	{
		zoom=false;
	}

	void SetScroll(int x, int y)
	{
		scroll_x=x;
		scroll_y=y;
	}

	void SetClipping(Box box)
	{
		clipbox=box;
	}
	void DisableClipping()
	{
		clipbox.SetXYWH(0,0,0,0);
	}

	unsigned int FlashColor(unsigned int c)
	{
		int r=(c>>16)&0xFF;
		int g=(c>>8)&0xFF;
		int b=(c)&0xFF;
		int w=(r+g*2+b)/4;
		w=40+w*2;
		if(w>255) w=255;
		return (w<<16)|(w<<8)|w;
	}

	void DrawSpriteZoomed(Sprite& sprite)
	{
		if(!enable)
			return;

		int w=sprite.region.Width();
		int h=sprite.region.Height();
		int dx=sprite.dx-scroll_x;
		int dy=sprite.dy-scroll_y;
		dx-=sprite.origin.x-sprite.region.x0;
		dy-=sprite.origin.y-sprite.region.y0;
		// FIXME: tsone: not sure if || and && operators are fixed here as per intent
		if((dx>zoom_x1+1 || dx+w<zoom_x0)
		&& (dy>zoom_y1+1 || dy+h<zoom_y0))
			return;
		double fdx=(double)target.width/(zoom_x1-zoom_x0);
		double fdy=(double)target.height/(zoom_y1-zoom_y0);
		double fx0=-zoom_x0*fdx;
		double fy0=-zoom_y0*fdy;
		for(int y=0;y<h;y++)
		{
			int si=(sprite.region.y0+y)*sprite.source.width+sprite.region.x0;
			int y0=(int)(fy0+(dy+y)*fdy);
			int y1=(int)(fy0+(dy+y+1)*fdy);
			int sdi=1;
			if(sprite.xflip)
			{
				si+=w;
				sdi=-1;
			}
			for(int x=0;x<w;x++)
			{
				unsigned int c=sprite.source.data[si];
				si+=sdi;
				if(c!=sprite.colorkey)
				{
					int x0=(int)(fx0+(dx+x)*fdx);
					int x1=(int)(fx0+(dx+x+1)*fdx);
					if(sprite.flash)
						c=FlashColor(c);
					if(sprite.tintcolor!=0xFFFFFFFF)
						c=sprite.tintcolor;
					DrawBar(x0, y0, x1-x0, y1-y0, c);
				}
			}
		}
	}

	void DrawSprite(Sprite& sprite)
	{
		if(!enable)
			return;

		if(sprite.alpha==0)
			return;
		if(zoom)
		{
			DrawSpriteZoomed(sprite);
			return;
		}
		Point2D srcpos=Point2D(sprite.region.x0, sprite.region.y0);

		int w=sprite.region.Width();
		int h=sprite.region.Height();
		int x0=sprite.dx;
		int y0=sprite.dy;
		if(!sprite.xflip)
			x0-=sprite.origin.x;
		else
			x0-=w-sprite.origin.x;
		if(!sprite.yflip)
			y0-=sprite.origin.y;
		else
			y0-=h-sprite.origin.y;
		x0-=scroll_x;
		y0-=scroll_y;
		Box r;
		r.SetXYWH(x0, y0, w, h);
		r.Clip(target.Bounds());
		if(clipbox.Width()!=0)
			r.Clip(clipbox);
		srcpos.x+=r.x0-x0;
		srcpos.y+=r.y0-y0;
		w=r.Width();
		h=r.Height();

		for(int y=r.y0;y<r.y1;y++)
		{
			int si;
			if(sprite.yflip)
				si=(srcpos.y+h-1-(y-r.y0))*sprite.source.width+srcpos.x;
			else
				si=(srcpos.y+y-r.y0)*sprite.source.width+srcpos.x;
			int di=y*target.width+r.x0;
			int ss=1;
			if(sprite.xflip)
			{
				si+=sprite.region.Width()-1-(srcpos.x-sprite.region.x0)*2;
				ss=-1;
			}
			if(sprite.flash)
			{
				for(int x=0;x<w;x++)
				{
					unsigned int c=sprite.source.data[si];
					si+=ss;
					if(c!=sprite.colorkey)
						target.data[di]=FlashColor(c);
					di++;
				}
			}
			else if(sprite.alpha!=255)
			{
				int a=sprite.alpha+1;
				for(int x=0;x<w;x++)
				{
					unsigned int c=sprite.source.data[si];
					si+=ss;
					if(c!=sprite.colorkey)
					{
						unsigned int tc=target.data[di];
						UColor color0=UColor(tc);
						UColor color1=UColor(c);
						color0.r+=(color1.r-color0.r)*a/256;
						color0.g+=(color1.g-color0.g)*a/256;
						color0.b+=(color1.b-color0.b)*a/256;
						target.data[di]=color0.ToUInt();
					}
					di++;
				}
			}
			else if(sprite.tintcolor!=0xFFFFFFFF)
			{
				for(int x=0;x<w;x++)
				{
					unsigned int c=sprite.source.data[si];
					si+=ss;
					if(c!=sprite.colorkey)
						target.data[di]=sprite.tintcolor;
					di++;
				}
			}
			else
			{
				for(int x=0;x<w;x++)
				{
					unsigned int c=sprite.source.data[si];
					si+=ss;
					if(c!=sprite.colorkey)
						target.data[di]=c;
					di++;
				}
			}
		}
	}


	void DrawSpriteFast(Sprite& sprite)
	{
		if(!enable)
			return;

		Point2D srcpos=Point2D(sprite.region.x0, sprite.region.y0);

		int w=sprite.region.Width();
		int h=sprite.region.Height();
		int x0=sprite.dx;
		int y0=sprite.dy;
		x0-=sprite.origin.x;
		y0-=sprite.origin.y;
		x0-=scroll_x;
		y0-=scroll_y;
		Box r;
		r.SetXYWH(x0, y0, w, h);
		r.Clip(target.Bounds());
		if(clipbox.Width()!=0)
			r.Clip(clipbox);
		srcpos.x+=r.x0-x0;
		srcpos.y+=r.y0-y0;
		w=r.Width();
		h=r.Height();


		int si0=(srcpos.y+r.y0-r.y0)*sprite.source.width+srcpos.x;
		int di0=r.y0*target.width+r.x0;
		for(int y=r.y0;y<r.y1;y++)
		{
			int si=si0;
			int di=di0;
			for(int x=0;x<w;x++)
				target.data[di++]=sprite.source.data[si++];
			si0+=sprite.source.width;
			di0+=target.width;
		}
	}
};

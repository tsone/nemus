#ifndef DynamicArray_H
#define DynamicArray_H

template <typename TYPE> class DynamicArray
{
public:
	int min_alloc;
	TYPE* data;
	int alloc;
	int num;
	int* removed;
	int alloc_removed;
	int num_removed;

	void Init()
	{
		data=NULL;
		alloc=0;
		num=0;
		removed=NULL;
		alloc_removed=0;
		num_removed=0;
	}
	void Free()
	{
		delete[] data;
		delete[] removed;
		Init();
	}

	DynamicArray()
	{
//		min_alloc=16;
		min_alloc=0;
		Init();
	}
	DynamicArray(int ma)
	{
		min_alloc=ma;
		Init();
	}
	DynamicArray(const DynamicArray<TYPE>& da)
	{
		min_alloc=0;
		Init();
		for(int i=0;i<da.num;i++)
			Add(da.data[i]);
	}
	DynamicArray<TYPE>& operator=(const DynamicArray<TYPE> da)
	{
		min_alloc=da.min_alloc;
		data=da.data;
		alloc=da.alloc;
		num=da.num;
		removed=da.removed;
		alloc_removed=da.alloc_removed;
		num_removed=da.num_removed;
		return *this;
	}
	~DynamicArray()
	{
		Free();
	}
	void Reset()
	{
		num=0;
		num_removed=0;
	}
	TYPE* GetPtr()
	{
		return data;
	}

	TYPE& operator[](int index)
	{
		return data[index];
	}

	int Add(const TYPE& element)
	{
		if(num==alloc)
		{
			alloc*=2;
			if(alloc==0)
			{
				if(min_alloc==0)
					min_alloc=16;
				alloc=min_alloc;
			}
			TYPE* newdata=new TYPE[alloc];
			for(int i=0;i<num;i++)
				newdata[i]=data[i];
			delete[] data;
			data=newdata;
		}
		int index=num;
		if(num_removed>0)
			index=removed[--num_removed];
		else
			num++;
		data[index]=element;
		return index;
	}

	void Remove(int index)
	{
		if(num==0) return;
		if(num_removed==alloc_removed)
		{
			alloc_removed*=2;
			if(alloc_removed==0)
			{
				if(min_alloc==0)
					min_alloc=16;
				alloc_removed=min_alloc;
			}
			int* newremoved=new int[alloc];
			for(int i=0;i<num_removed;i++)
				newremoved[i]=removed[i];
			delete[] removed;
			removed=newremoved;
		}
		removed[num_removed++]=index;
	}

	void Delete(int index) // note: don't use together with Remove since indices will change
	{
		if(num==0) return;
		for(int i=index;i<num-1;i++)
			data[i]=data[i+1];
		num--;
	}
};

#endif

#import "glr_internal.h"
#import <Cocoa/Cocoa.h>
#import <mach/mach.h>
#import <mach/mach_time.h>

@interface GlrWindowDelegate : NSResponder <NSWindowDelegate>
{
	@public
	BOOL fullscreen_transitioning;
}
- (void)attach;
- (void)detach;

- (BOOL)windowShouldClose:(id)sender;
- (void)windowDidResize:(NSNotification *)notification;
- (void)windowDidMiniaturize:(NSNotification *)notification;
- (void)windowDidDeminiaturize:(NSNotification *)notification;
- (void)windowDidBecomeKey:(NSNotification *)notification;
- (void)windowDidResignKey:(NSNotification *)notification;
- (void)windowWillEnterFullScreen:(NSNotification *)notification;
- (void)windowDidFailToEnterFullScreen:(NSWindow *)window;
- (void)windowDidEnterFullScreen:(NSNotification *)notification;
- (void)windowWillExitFullScreen:(NSNotification *)notification;
- (void)windowDidFailToExitFullScreen:(NSWindow *)window;
- (void)windowDidExitFullScreen:(NSNotification *)notification;
- (void)flagsChanged:(NSEvent *)event;
- (void)keyDown:(NSEvent *)event;
- (void)keyUp:(NSEvent *)event;
@end

@interface GlrWindow : NSWindow
- (BOOL)canBecomeKeyWindow;
- (BOOL)canBecomeMainWindow;
- (void)doCommandBySelector:(SEL)selector;
@end

@interface GlrView : NSView
- (BOOL)isOpaque;
- (BOOL)wantsLayer;
- (BOOL)wantsUpdateLayer;
- (NSViewLayerContentsRedrawPolicy)layerContentsRedrawPolicy;
- (NSViewLayerContentsPlacement)layerContentsPlacement;
@end

@interface GlrApplication : NSApplication
- (void)terminate:(id)sender;
+ (void)registerDefaults;
@end

// Modified from SDL2 (scancodes_darwin.h)
// NOTE: Modifier keys (Shift, Ctrl, etc.) exluded as they're handled in HandleGenericEvent()
static const unsigned char _key_tab[]=
{
	GLR_KEY_A,
	GLR_KEY_S,
	GLR_KEY_D,
	GLR_KEY_F,
	GLR_KEY_H,
	GLR_KEY_G,
	GLR_KEY_Z,
	GLR_KEY_X,
	GLR_KEY_C,
	GLR_KEY_V,
	GLR_KEY_UNKNOWN,//NONUSBACKSLASH,
	GLR_KEY_B,
	GLR_KEY_Q,
	GLR_KEY_W,
	GLR_KEY_E,
	GLR_KEY_R,
	GLR_KEY_Y,
	GLR_KEY_T,
	GLR_KEY_1,
	GLR_KEY_2,
	GLR_KEY_3,
	GLR_KEY_4,
	GLR_KEY_6,
	GLR_KEY_5,
	GLR_KEY_UNKNOWN,//EQUALS,
	GLR_KEY_9,
	GLR_KEY_7,
	GLR_KEY_MINUS,
	GLR_KEY_8,
	GLR_KEY_0,
	GLR_KEY_UNKNOWN,//RIGHTBRACKET,
	GLR_KEY_O,
	GLR_KEY_U,
	GLR_KEY_UNKNOWN,//LEFTBRACKET,
	GLR_KEY_I,
	GLR_KEY_P,
	GLR_KEY_ENTER,//RETURN,
	GLR_KEY_L,
	GLR_KEY_J,
	GLR_KEY_UNKNOWN,//APOSTROPHE,
	GLR_KEY_K,
	GLR_KEY_SEMICOLON,
	GLR_KEY_UNKNOWN,//BACKSLASH,
	GLR_KEY_COMMA,
	GLR_KEY_UNKNOWN,//SLASH,
	GLR_KEY_N,
	GLR_KEY_M,
	GLR_KEY_PERIOD,
	GLR_KEY_TAB,
	GLR_KEY_SPACE,
	GLR_KEY_UNKNOWN,//GRAVE,
	GLR_KEY_BACKSPACE,
	GLR_KEY_ENTER,//KP_ENTER,
	GLR_KEY_ESCAPE,
	GLR_KEY_UNKNOWN,//RGUI,
	GLR_KEY_UNKNOWN,//LGUI,
	GLR_KEY_UNKNOWN,//LSHIFT,
	GLR_KEY_UNKNOWN,//CAPSLOCK,
	GLR_KEY_UNKNOWN,//LALT,
	GLR_KEY_UNKNOWN,//LCTRL,
	GLR_KEY_UNKNOWN,//RSHIFT,
	GLR_KEY_UNKNOWN,//RALT,
	GLR_KEY_UNKNOWN,//RCTRL,
	GLR_KEY_UNKNOWN,//RGUI,
	GLR_KEY_UNKNOWN,//F17,
	GLR_KEY_PERIOD,//KP_PERIOD,
	GLR_KEY_UNKNOWN, /* unknown (unused?) */
	GLR_KEY_UNKNOWN,//KP_MULTIPLY,
	GLR_KEY_UNKNOWN, /* unknown (unused?) */
	GLR_KEY_PLUS,//KP_PLUS,
	GLR_KEY_UNKNOWN, /* unknown (unused?) */
	GLR_KEY_UNKNOWN,//NUMLOCKCLEAR,
	GLR_KEY_UNKNOWN,//VOLUMEUP,
	GLR_KEY_UNKNOWN,//VOLUMEDOWN,
	GLR_KEY_UNKNOWN,//MUTE,
	GLR_KEY_UNKNOWN,//KP_DIVIDE,
	GLR_KEY_ENTER,//KP_ENTER,
	GLR_KEY_UNKNOWN, /* unknown (unused?) */
	GLR_KEY_MINUS,//KP_MINUS,
	GLR_KEY_UNKNOWN,//F18,
	GLR_KEY_UNKNOWN,//F19,
	GLR_KEY_UNKNOWN,//KP_EQUALS,
	GLR_KEY_0,//KP_0,
	GLR_KEY_1,//KP_1,
	GLR_KEY_2,//KP_2,
	GLR_KEY_3,//KP_3,
	GLR_KEY_4,//KP_4,
	GLR_KEY_5,//KP_5,
	GLR_KEY_6,//KP_6,
	GLR_KEY_7,//KP_7,
	GLR_KEY_UNKNOWN, /* unknown (unused?) */
	GLR_KEY_8,//KP_8,
	GLR_KEY_9,//KP_9,
	GLR_KEY_UNKNOWN,//INTERNATIONAL3,
	GLR_KEY_UNKNOWN,//INTERNATIONAL1,
	GLR_KEY_COMMA,//KP_COMMA, /* Cosmo_USB2ADB.c says ", JIS only" */
	GLR_KEY_F5,
	GLR_KEY_F6,
	GLR_KEY_F7,
	GLR_KEY_F3,
	GLR_KEY_F8,
	GLR_KEY_F9,
	GLR_KEY_UNKNOWN,//LANG2, /* Cosmo_USB2ADB.c says "Eisu" */
	GLR_KEY_F11,
	GLR_KEY_UNKNOWN,//LANG1, /* Cosmo_USB2ADB.c says "Kana" */
	GLR_KEY_UNKNOWN,//PRINTSCREEN,
	GLR_KEY_UNKNOWN,//F16,
	GLR_KEY_UNKNOWN,//SCROLLLOCK,
	GLR_KEY_UNKNOWN, /* unknown (unused?) */
	GLR_KEY_F10,
	GLR_KEY_UNKNOWN,//APPLICATION,
	GLR_KEY_F12,
	GLR_KEY_UNKNOWN, /* unknown (unused?) */
	GLR_KEY_UNKNOWN,//PAUSE,
	GLR_KEY_UNKNOWN,//INSERT,
	GLR_KEY_HOME,
	GLR_KEY_PAGEUP,
	GLR_KEY_DELETE,
	GLR_KEY_F4,
	GLR_KEY_END,
	GLR_KEY_F2,
	GLR_KEY_PAGEDOWN,
	GLR_KEY_F1,
	GLR_KEY_LEFT,
	GLR_KEY_RIGHT,
	GLR_KEY_DOWN,
	GLR_KEY_UP,
	GLR_KEY_UNKNOWN,//POWER
};

static mach_timebase_info_data_t _timebase;
static uint64_t _start_time;

static NSWindow* _nswindow;
static GlrWindowDelegate* _delegate;

static CGDataProviderRef _provider;
static CGColorSpaceRef _color_space;
static CGColorRef _black;

static unsigned char _keys[GLR_KEY_COUNT];

// TODO: tsone: could be in glr.cpp?
static void glrPixelBufferResize(glrPixelBuffer* p, int width, int height)
{
	if(!p->data || p->width!=width || p->height!=height)
	{
		if(p->data)
			free(p->data);
		p->width=width;
		p->height=height;
		p->pitch=4*width;
		p->data=calloc(1, height*p->pitch);
	}
}

static void HandleWindowResize(int width, int height)
{
	glrPixelBufferResize(&glr_buffer, width, height);
	CGDataProviderRelease(_provider);
	_provider=CGDataProviderCreateWithData(NULL, glr_buffer.data, glr_buffer.height*glr_buffer.pitch, NULL);
}

void glrSleep(int ms)
{
	usleep(ms*1000);
}

double glrAppTime(void)
{
	return (mach_absolute_time()-_start_time)*_timebase.numer/(1e9*_timebase.denom);
}

void glrSetCurrentDirectory(const char* dir)
{
	chdir(dir);
}

void glrGetCurrentDirectory(unsigned int num, char* dir)
{
	getcwd(dir, num);
}

void glrSetWindowTitle(const char* title)
{
	@autoreleasepool
	{
		[_nswindow setTitle:@(title)];
	}
}

void glrDisplayPixelBufferRectInternal(int x, int y, int w, int h)
{
	(void)x; (void)y; (void)w; (void)h;
	@autoreleasepool
	{
		[[_nswindow contentView] setNeedsDisplay:YES];

		NSView *view=[_nswindow contentView];
		CALayer *layer=[view layer];
		[layer setMinificationFilter:kCAFilterNearest];
		[layer setMagnificationFilter:kCAFilterNearest];
		[layer setOpaque:YES];
		[layer setContentsScale:1];
		[layer setBackgroundColor:_black];

		CGImageRef image=CGImageCreate(glr_buffer.width, glr_buffer.height, 8, 32, glr_buffer.pitch, _color_space, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipFirst, _provider, NULL, NO, kCGRenderingIntentDefault);
		[[view layer] setContents:(__bridge id _Nullable)(image)];
		CGImageRelease(image);
	}
}

static void ShowWindow(void)
{
	if(![_nswindow isMiniaturized])
		[_nswindow makeKeyAndOrderFront:nil];
}

static void CreateApplication(void);
static int CreateWindow(void);

glrHandle glrOpenWindowInternal(const char* title, int width, int height, glrMode mode)
{
	(void)title; (void)width; (void)height; (void)mode;
	@autoreleasepool
	{
		CreateApplication();
		CreateWindow();
		ShowWindow();
		return 0;
	}
}

int glrPerformResizeInternal(int fullscreen)
{
	(void)fullscreen;
	@autoreleasepool
	{
		if(!_delegate || _delegate->fullscreen_transitioning || !_nswindow)
			return 0;

		[_nswindow toggleFullScreen:nil];
		return 1;
	}
}

void glrResizeWindowInternal(int w, int h)
{
	(void)w; (void)h;
	// Not needed
}

void glrSwapBuffers(void)
{
	// Not needed
}

void glrCloseWindowInternal(void)
{
	@autoreleasepool
	{
		_nswindow=nil;

		[_delegate detach];
		_delegate=nil;

		CGColorSpaceRelease(_color_space);
		_color_space=nil;

		CGDataProviderRelease(_provider);
		_provider=nil;
		CGColorRelease(_black);
		_black=nil;

		free(glr_buffer.data);
		glr_buffer.data=0;
	}
}

static void HandleMouseEvent(const NSEvent* event)
{
	NSWindow* window=[event window];
	if(!window)
		return;

	NSRect rect=[[window contentView] frame];
	NSPoint point=[event locationInWindow];

	glr_mouse_x=(int)point.x;
	glr_mouse_y=(int)(glr_buffer.height-point.y);

	switch([event type])
	{
		case NSEventTypeLeftMouseDown:
		case NSEventTypeOtherMouseDown:
		case NSEventTypeRightMouseDown:
			if(!NSMouseInRect(point, rect, NO))
				return;
			switch([event buttonNumber])
			{
				case 0:
					glr_mouse_left=1;
					glr_mouse_left_click=1;
					break;
				case 1:
					glr_mouse_right=1;
					glr_mouse_right_click=1;
					break;
				default:
					break;
			}
			break;
		case NSEventTypeLeftMouseUp:
		case NSEventTypeOtherMouseUp:
		case NSEventTypeRightMouseUp:
			switch([event buttonNumber])
			{
				case 0:
					glr_mouse_left=0;
					break;
				case 1:
					glr_mouse_right=0;
					break;
				default:
					break;
			}
			break;
		case NSEventTypeScrollWheel:
			glr_mouse_scroll+=[event deltaY];
			break;
		default:
			break;
	}
}

static void HandleKeyEvent(const NSEvent* event)
{
	const unsigned short code=[event keyCode];
	if(code<sizeof(_key_tab)/sizeof(*_key_tab))
	{
		const glrKey key=_key_tab[code];
		if(key>=GLR_KEY_FIRST && key<=GLR_KEY_LAST)
		{
			switch([event type])
			{
				case NSEventTypeKeyDown:
					_keys[key]=1;
					break;
				case NSEventTypeKeyUp:
					_keys[key]=0;
					break;
				default:
					break;
			}
		}
	}
}

static void HandleGenericEvent(const NSEvent* ev)
{
	// Modifier keys
	const NSEventModifierFlags modifiers=[ev modifierFlags];
	_keys[GLR_KEY_SHIFT]=!!(modifiers&NSEventModifierFlagShift);
	_keys[GLR_KEY_CTRL]=!!(modifiers&NSEventModifierFlagControl);
	_keys[GLR_KEY_ALT]=!!(modifiers&NSEventModifierFlagOption);
	_keys[GLR_KEY_SUPER]=!!(modifiers&NSEventModifierFlagCommand);
	_keys[GLR_KEY_CAPSLOCK]=!!(modifiers&NSEventModifierFlagCapsLock);
}

int glrProcessEventsInternal(void)
{
	@autoreleasepool
	{
		NSEvent* event;

		while((event=[NSApp nextEventMatchingMask:NSEventMaskAny untilDate:[NSDate distantPast] inMode:NSDefaultRunLoopMode dequeue:YES]))
		{
			HandleGenericEvent(event);

			switch([event type])
			{
				case NSEventTypeLeftMouseDown:
				case NSEventTypeOtherMouseDown:
				case NSEventTypeRightMouseDown:
				case NSEventTypeLeftMouseUp:
				case NSEventTypeOtherMouseUp:
				case NSEventTypeRightMouseUp:
				case NSEventTypeLeftMouseDragged:
				case NSEventTypeRightMouseDragged:
				case NSEventTypeOtherMouseDragged:
				case NSEventTypeMouseMoved:
				case NSEventTypeScrollWheel:
					HandleMouseEvent(event);
					break;
				case NSEventTypeKeyDown:
				case NSEventTypeKeyUp:
				//case NSEventTypeFlagsChanged: // Handled by HandleGenericEvent()
					HandleKeyEvent(event);
					break;
				default:
					break;
			}

			[NSApp sendEvent:event];
		}
		return 1;
	}
}

int glrKeyPressedInternal(glrKey key)
{
	return _keys[key];
}

@implementation GlrWindow
- (BOOL)canBecomeKeyWindow
{
	return YES;
}
- (BOOL)canBecomeMainWindow
{
	return YES;
}
- (void)doCommandBySelector:(SEL)selector
{
	(void)selector;
	// No-op, prevents beep from Esc key etc.
}
@end

@implementation GlrView
- (BOOL)isOpaque
{
	return YES;
}
- (BOOL)wantsLayer
{
	return YES;
}
- (BOOL)wantsUpdateLayer
{
	return YES;
}
- (NSViewLayerContentsRedrawPolicy)layerContentsRedrawPolicy
{
	return NSViewLayerContentsRedrawNever;
}
- (NSViewLayerContentsPlacement)layerContentsPlacement
{
	return NSViewLayerContentsPlacementTopLeft;
}
@end

static void CreateApplication(void)
{
	assert(NSApp==nil);
	[GlrApplication registerDefaults];
	[GlrApplication sharedApplication];
	assert(NSApp!=nil);

	NSMenu* mainMenu=[[NSMenu alloc] init];
	[NSApp setMainMenu:mainMenu];

	NSMenu* appleMenu=[[NSMenu alloc] initWithTitle:@""];

	NSString* appName=@(glr_app_name);
	NSString* title=[@"About " stringByAppendingString:appName];
	[appleMenu addItemWithTitle:title action:@selector(orderFrontStandardAboutPanel:) keyEquivalent:@""];

	[appleMenu addItem:[NSMenuItem separatorItem]];

	title = [@"Quit " stringByAppendingString:appName];
	[appleMenu addItemWithTitle:title action:@selector(terminate:) keyEquivalent:@"q"];

	NSMenuItem* item=[[NSMenuItem alloc] initWithTitle:@"" action:nil keyEquivalent:@""];
	[item setSubmenu:appleMenu];
	[[NSApp mainMenu] addItem:item];

	// NOTE: tsone: View menu disabled
#if 0
	NSMenu* viewMenu=[[NSMenu alloc] initWithTitle:@"View"];
	item=[viewMenu addItemWithTitle:@"Toggle Full Screen" action:@selector(toggleFullScreen:) keyEquivalent:@"\r"];
	[item setKeyEquivalentModifierMask:NSEventModifierFlagOption];
	item=[[NSMenuItem alloc] initWithTitle:@"View" action:nil keyEquivalent:@""];
	[item setSubmenu:viewMenu];
	[[NSApp mainMenu] addItem:item];
#endif

	NSMenu* windowMenu=[[NSMenu alloc] initWithTitle:@"Window"];
	[windowMenu addItemWithTitle:@"Minimize" action:@selector(performMiniaturize:) keyEquivalent:@"m"];
	[windowMenu addItemWithTitle:@"Zoom" action:@selector(performZoom:) keyEquivalent:@""];
	item = [[NSMenuItem alloc] initWithTitle:@"Window" action:nil keyEquivalent:@""];
	[item setSubmenu:windowMenu];
	[[NSApp mainMenu] addItem:item];

	[NSApp finishLaunching];

	_black=CGColorCreateGenericRGB(0, 0, 0, 1);
}

static int CreateWindow(void)
{
	const NSUInteger style=NSWindowStyleMaskTitled|NSWindowStyleMaskClosable|NSWindowStyleMaskMiniaturizable|NSWindowStyleMaskResizable;

	NSScreen* screen=[NSScreen mainScreen];
	NSRect rect=[screen frame];
	const float scale=0.82f;
	rect.origin.x=rect.size.width*(0.5f-0.5f*scale);
	rect.origin.y=rect.size.height*(0.5f-0.5f*scale);
	rect.size.width=rect.size.width*scale;
	rect.size.height=rect.size.height*scale;

	NSWindow* window=[[GlrWindow alloc] initWithContentRect:rect styleMask:style backing:NSBackingStoreBuffered defer:NO screen:screen];

	[window setFrameAutosaveName:@(glr_app_name)];

	[window setCollectionBehavior:NSWindowCollectionBehaviorFullScreenPrimary];

	rect=[window contentRectForFrameRect:[window frame]];

	GlrView* view=[[GlrView alloc] initWithFrame:rect];
	[window setContentView:view];

	// Disable View menu automatic tabbing
	// Source: https://stackoverflow.com/a/39610898
	if(NSAppKitVersionNumber>1500)
		[window setValue:[NSNumber numberWithInt:2] forKey:@"tabbingMode"];

	_nswindow=window;
	_delegate=[[GlrWindowDelegate alloc] init];
	[_delegate attach];

	// Prevent window device from being destroyed when hidden
	// See: http://www.mikeash.com/pyblog/nsopenglcontext-and-one-shot.html
	[window setOneShot:NO];

	HandleWindowResize(rect.size.width, rect.size.height);

	_color_space=CGColorSpaceCreateDeviceRGB();
	return 0;
}

@implementation GlrWindowDelegate
- (void)attach
{
	fullscreen_transitioning=NO;

	NSWindow* window=_nswindow;
	NSView* view=[window contentView];

	[window setDelegate:self];
	[window setNextResponder:self];
	[window setAcceptsMouseMovedEvents:YES];
	[view setNextResponder:self];
}
- (void)detach
{
	NSWindow* window=_nswindow;
	[window setDelegate:nil];
	if([window nextResponder]==self)
		[window setNextResponder:nil];

	NSView* view=[window contentView];
	if([view nextResponder]==self)
		[view setNextResponder:nil];
}
- (BOOL)windowShouldClose:(id)sender
{
	(void)sender;
	glr_request_exit=1;
	return NO;
}
- (void)windowDidResize:(NSNotification *)notification
{
	(void)notification;
	const NSRect rect=[_nswindow contentRectForFrameRect:[_nswindow frame]];
	HandleWindowResize(rect.size.width, rect.size.height);
}
- (void)windowDidMiniaturize:(NSNotification *)notification
{
	(void)notification;
	glr_window_focus=0;
}
- (void)windowDidDeminiaturize:(NSNotification *)notification
{
	(void)notification;
	// TODO: tsone: bug: if deminiaturized when another app's at front, window's sent back
	glr_window_focus=1;
}
- (void)windowDidBecomeKey:(NSNotification *)notification
{
	(void)notification;
	glr_window_focus=1;
}
- (void)windowDidResignKey:(NSNotification *)notification
{
	(void)notification;
	glr_window_focus=0;
}
- (void)windowWillEnterFullScreen:(NSNotification *)notification
{
	(void)notification;
	fullscreen_transitioning=YES;
}
- (void)windowDidFailToEnterFullScreen:(NSWindow *)window
{
	(void)window;
	fullscreen_transitioning=NO;
}
- (void)windowDidEnterFullScreen:(NSNotification *)notification
{
	(void)notification;
	fullscreen_transitioning=NO;
}
- (void)windowWillExitFullScreen:(NSNotification *)notification
{
	(void)notification;
	fullscreen_transitioning=YES;
}
- (void)windowDidFailToExitFullScreen:(NSWindow *)window
{
	(void)window;
	fullscreen_transitioning=NO;
}
- (void)windowDidExitFullScreen:(NSNotification *)notification
{
	(void)notification;
	fullscreen_transitioning=NO;
}
- (void)flagsChanged:(NSEvent *)event
{
	(void)event;
	// No-op, prevents beep
}
- (void)keyDown:(NSEvent *)event
{
	(void)event;
	// No-op, prevents beep
}
- (void)keyUp:(NSEvent *)event
{
	(void)event;
	// No-op, prevents beep
}
@end

@implementation GlrApplication
- (void)terminate:(id)sender
{
	(void)sender;
	glr_request_exit=1;
}
+ (void)registerDefaults
{
	NSDictionary* defaults=[[NSDictionary alloc] initWithObjectsAndKeys:
							// TODO: tsone: Causes file browser to behave strangely, fix?
							[NSNumber numberWithBool:NO], @"AppleMomentumScrollSupported",
							[NSNumber numberWithBool:NO], @"ApplePressAndHoldEnabled",
							// NOTE: tsone: Causes a warning, disabled
							//[NSNumber numberWithBool:YES], @"ApplePersistenceIgnoreState",
							nil];
	[[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
}
@end

int main(int argc, const char **argv)
{
	(void)argc; (void)argv;
	mach_timebase_info(&_timebase);
	_start_time=mach_absolute_time();
	return glrMainLoopInternal();
}

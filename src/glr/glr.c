#include "glr_internal.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

glrPixelBuffer glr_buffer;

const char* glr_app_name;

char glr_window_focus=1;
char glr_request_exit;

int glr_mouse_x;
int glr_mouse_y;
int glr_mouse_scroll;
char glr_mouse_left;
char glr_mouse_right;
char glr_mouse_left_click;
char glr_mouse_right_click;
int glr_mousewrap_dx;
int glr_mousewrap_dy;
glrBool glr_mousewrap_x;
glrBool glr_mousewrap_y;

glrBool glr_key_held[GLR_KEY_COUNT];
glrBool glr_key_hit[GLR_KEY_COUNT];

static char glr_log_filename[256];
static glrBool glr_log_exists;
static glrBool glr_log_enabled;

static char resize_command;
#define GLR_TEXT_INPUT_MAX 128
static char glr_text_input[GLR_TEXT_INPUT_MAX];
static int glr_text_input_idx=-1;
static int glr_text_input_limit;

static glrBool glrKeyPressedBase(glrKey key)
{
	if(key!=GLR_KEY_ANY)
	{
		return glrKeyPressedInternal(key);
	}
	else
	{
		for(int i=GLR_KEY_FIRST;i<=GLR_KEY_LAST;i++)
			if(glr_key_held[i])
				return 1;
		return 0;
	}
}

static void glrUpdateKeyHits(void)
{
	for(int ii=GLR_KEY_FIRST;ii<=GLR_KEY_LAST+1;ii++)
	{
		int i=ii;
		if(i==GLR_KEY_LAST+1)
			i=0;
		glrBool pressed=glrKeyPressedBase((glrKey)i);
		glr_key_hit[i]=(pressed && !glr_key_held[i]);
		glr_key_held[i]=pressed;
	}
}

static glrBool glrGetMouseButton(int button)
{
	if(button==0)
		return glr_mouse_left || glr_mouse_left_click;
	else
		return glr_mouse_right || glr_mouse_right_click;
}

static inline void glrTextInputEnd(glrBool cancel)
{
	glr_text_input_idx=!cancel?-1:-2;
}

static inline void glrTextInputPop(void)
{
	if(glr_text_input_idx>0)
		glr_text_input_idx--;
	glr_text_input[glr_text_input_idx]=0;
}

static inline void glrTextInputPush(char c)
{
	if(glr_text_input_idx>glr_text_input_limit-1)
		glr_text_input_idx=glr_text_input_limit-1;
	glr_text_input[glr_text_input_idx]=c;
	glr_text_input[glr_text_input_idx+1]=0;
	glr_text_input_idx++;
}

static void glrTextInputTestAndPush(glrKey key)
{
	if(glr_key_hit[key])
		glrTextInputPush(glrKeyToAscii(key));
}

static void glrTextInputUpdate(void)
{
	if(glr_key_hit[GLR_KEY_ESCAPE]||glrGetMouseButton(1))
	{
		glrTextInputEnd(1);
		return;
	}

	if(glr_key_hit[GLR_KEY_ENTER]||glrGetMouseButton(0))
	{
		glrTextInputEnd(0);
		return;
	}

	for(int key=GLR_KEY_PERIOD;key<=GLR_KEY_MINUS;key++)
		glrTextInputTestAndPush((glrKey)key);

	for(int key=GLR_KEY_A;key<=GLR_KEY_9;key++)
		glrTextInputTestAndPush((glrKey)key);

	glrTextInputTestAndPush(GLR_KEY_SPACE);

	if(glr_key_hit[GLR_KEY_BACKSPACE])
		glrTextInputPop();
}

glrBool glrValidHandle(glrHandle handle)
{
	return handle!=-1;
}

void glrEnableLog(void)
{
	glr_log_enabled=1;
}

void glrLog(const char* msg, ...)
{
	if(!glr_log_enabled)
		return;
	va_list args;
	va_start(args, msg);
	char str[2048];
	vsprintf(str, msg, args);
	if(!glr_log_exists)
	{
		strcpy(glr_log_filename, "log.txt");
		FILE* file=fopen(glr_log_filename, "w");
		if(!file)
			return;
		fclose(file);
		glr_log_exists=1;
	}
	FILE* file=fopen(glr_log_filename, "a");
	if(!file)
		return;
	fprintf(file, "%s", str);
	fclose(file);
	va_end(args);
}

void glrDisplayPixelBuffer()
{
	glrDisplayPixelBufferRectInternal(0, 0, 0, 0);
}

void glrDisplayPixelBufferRect(int x, int y, int w, int h)
{
	if(x<0)
	{
		w+=x;
		x=0;
	}
	if(y<0)
	{
		h+=y;
		y=0;
	}
	if(x+w>glr_buffer.width)
		w=glr_buffer.width-x;
	if(y+h>glr_buffer.height)
		h=glr_buffer.height-y;
	glrDisplayPixelBufferRectInternal(x, y, w, h);
}

glrHandle glrOpenWindow(const char* title, int width, int height, glrMode mode)
{
	char* app_name=(char*)malloc(strlen(title)+1);
	strcpy(app_name, title);
	glr_app_name=app_name;

	return glrOpenWindowInternal(title, width, height, mode);
}

void glrCloseWindow()
{
	glrCloseWindowInternal();

	// NOTE: tsone: don't free glr_buffer.data, it may be owned by window system

	free((char*)glr_app_name);
	glr_app_name=0;
}

glrBool glrWindowFocused()
{
	return glr_window_focus;
}

void glrSetFullscreen()
{
	resize_command=1;
}

void glrSetWindowed()
{
	resize_command=2;
}

void glrPerformResize()
{
	if(resize_command==0)
		return;

	int success=0;

	if(resize_command==1)
		success=glrPerformResizeInternal(1);
	else if(resize_command==2)
		success=glrPerformResizeInternal(0);

	if(success)
		resize_command=0;
}

void glrGetMousePos(int* x, int* y)
{
	if(glrTextInputActive())
		return;
	*x=glr_mouse_x+glr_mousewrap_dx;
	*y=glr_mouse_y+glr_mousewrap_dy;
}

glrBool glrGetMouseLeft()
{
	if(glrTextInputActive())
		return 0;
	return glrGetMouseButton(0);
}

glrBool glrGetMouseRight()
{
	if(glrTextInputActive())
		return 0;
	return glrGetMouseButton(1);
}

int glrGetMouseScroll()
{
	if(glrTextInputActive())
		return 0;
	int i=glr_mouse_scroll;
	glr_mouse_scroll=0;
	return i;
}

void glrMouseWrap(glrBool x, glrBool y)
{
	if(glrTextInputActive())
		return;
	glr_mousewrap_x=x;
	glr_mousewrap_y=y;
	glr_mousewrap_dx=0;
	glr_mousewrap_dy=0;
}

glrBool glrKeyPressed(glrKey key)
{
	if(!glrWindowFocused() || glrTextInputActive())
		return 0;
	return glrKeyPressedBase(key);
}

glrBool glrKeyHit(glrKey key)
{
	if(!glrWindowFocused() || glrTextInputActive())
		return 0;

	if(key!=GLR_KEY_ANY)
	{
		return glr_key_hit[key];
	}
	else
	{
		for(int i=GLR_KEY_FIRST;i<=GLR_KEY_LAST;i++)
		{
			if(glr_key_hit[i])
				return 1;
		}
		return 0;
	}
}

int glrMainLoopInternal()
{
	for(int i=0;i<GLR_KEY_COUNT;i++)
		glr_key_held[i]=0;

	if(!glrAppInit())
		return 0;

	// run message loop
	for(;;)
	{
		glr_request_exit=0;
		glrPerformResize();
		if(!glrProcessEventsInternal())
			glr_request_exit=1;

		glrUpdateKeyHits();

		if(glrTextInputActive())
			glrTextInputUpdate();

		if(!glrAppUpdate())
			glr_request_exit=1;

		glr_mouse_left_click=0;
		glr_mouse_right_click=0;

		if(glr_request_exit)
		{
			// TODO: check if permitted
			break;
		}
	}

	// exit application
	glrAppExit();

	glrCloseWindow();
	return 0;
}

void glrTextInputStart(const char* text, int max_chars)
{
	if(max_chars<1)
		glr_text_input_limit=1;
	else if(max_chars>GLR_TEXT_INPUT_MAX-1)
		glr_text_input_limit=GLR_TEXT_INPUT_MAX-1;
	else
		glr_text_input_limit=max_chars;

	glr_text_input[0]=0;
	strncat(glr_text_input, text, glr_text_input_limit);
	glr_text_input_idx=(int)strlen(glr_text_input);
}

glrBool glrTextInputActive(void)
{
	return glr_text_input_idx>=0;
}

glrBool glrTextInputCancelled(void)
{
	return glr_text_input_idx==-2;
}

const char* glrTextInputGet(void)
{
	return glr_text_input;
}

char glrKeyToAscii(glrKey key)
{
	if(key>=GLR_KEY_A && key<=GLR_KEY_Z)
		return 'A'+key-GLR_KEY_A;
	if (key>=GLR_KEY_0 && key<=GLR_KEY_9)
		return '0'+key-GLR_KEY_0;
	switch(key)
	{
		case GLR_KEY_SPACE:     return ' ';
		case GLR_KEY_PERIOD:    return '.';
		case GLR_KEY_COMMA:     return ',';
		case GLR_KEY_SEMICOLON: return ';';
		case GLR_KEY_PLUS:      return '+';
		case GLR_KEY_MINUS:     return '-';
		default:                return '?'; // Unknown key
	}
}

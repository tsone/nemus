#ifndef GLR_H
#define GLR_H
#if defined(__cplusplus)
extern "C" {
#endif

#define GLR_PI     3.141592653589793f
#define GLR_2PI    6.283185307179586f
#define GLR_RADDEG 57.295779513082321f
#define GLR_DEGRAD 0.017453292519943f

typedef int glrHandle;
typedef char glrBool;

typedef struct
{
	int width, height, pitch;
	unsigned int* data;
} glrPixelBuffer;

typedef enum
{
	GLR_MODE_BITMAP, // Use software rendering
	GLR_MODE_GPU     // GPU rendering (if available)
} glrMode;

// NOTE: tsone: DO NOT REORDER THE FOLLOWING!
typedef enum
{
	GLR_KEY_ANY=0,
	GLR_KEY_FIRST,
	//
	GLR_KEY_ESCAPE=GLR_KEY_FIRST,
	GLR_KEY_SPACE,
	GLR_KEY_DELETE,
	GLR_KEY_BACKSPACE,
	GLR_KEY_ENTER,
	GLR_KEY_RIGHT,
	GLR_KEY_LEFT,
	GLR_KEY_DOWN,
	GLR_KEY_UP,
	GLR_KEY_CTRL,
	GLR_KEY_SHIFT,
	GLR_KEY_ALT,
	GLR_KEY_SUPER,
	GLR_KEY_TAB,
	GLR_KEY_PAGEUP,
	GLR_KEY_PAGEDOWN,
	GLR_KEY_HOME,
	GLR_KEY_END,
	GLR_KEY_PERIOD,
	GLR_KEY_COMMA,
	GLR_KEY_SEMICOLON,
	GLR_KEY_PLUS,
	GLR_KEY_MINUS,
	GLR_KEY_F1,
	GLR_KEY_F2,
	GLR_KEY_F3,
	GLR_KEY_F4,
	GLR_KEY_F5,
	GLR_KEY_F6,
	GLR_KEY_F7,
	GLR_KEY_F8,
	GLR_KEY_F9,
	GLR_KEY_F10,
	GLR_KEY_F11,
	GLR_KEY_F12,
	GLR_KEY_A,
	GLR_KEY_B,
	GLR_KEY_C,
	GLR_KEY_D,
	GLR_KEY_E,
	GLR_KEY_F,
	GLR_KEY_G,
	GLR_KEY_H,
	GLR_KEY_I,
	GLR_KEY_J,
	GLR_KEY_K,
	GLR_KEY_L,
	GLR_KEY_M,
	GLR_KEY_N,
	GLR_KEY_O,
	GLR_KEY_P,
	GLR_KEY_Q,
	GLR_KEY_R,
	GLR_KEY_S,
	GLR_KEY_T,
	GLR_KEY_U,
	GLR_KEY_V,
	GLR_KEY_W,
	GLR_KEY_X,
	GLR_KEY_Y,
	GLR_KEY_Z,
	GLR_KEY_0,
	GLR_KEY_1,
	GLR_KEY_2,
	GLR_KEY_3,
	GLR_KEY_4,
	GLR_KEY_5,
	GLR_KEY_6,
	GLR_KEY_7,
	GLR_KEY_8,
	GLR_KEY_9,
	GLR_KEY_CAPSLOCK,
	//
	GLR_KEY_COUNT,
	GLR_KEY_UNKNOWN=GLR_KEY_COUNT,
	GLR_KEY_LAST=GLR_KEY_COUNT-1
} glrKey;

extern glrPixelBuffer glr_buffer;

// --- application functions
glrBool glrAppInit(void);
glrBool glrAppUpdate(void);
void glrAppExit(void);
// ---

glrBool glrValidHandle(glrHandle handle);

void glrSleep(int ms);
void glrEnableLog(void);
void glrLog(const char* msg, ...);
double glrAppTime(void);
static inline int glrAppTimeMs(void)
{
	return (int)(glrAppTime()*1000);
}

void glrSetCurrentDirectory(const char* dir);
void glrGetCurrentDirectory(unsigned int num, char* dir);

glrHandle glrOpenWindow(const char* title, int width, int height, glrMode mode);
void glrSetWindowTitle(const char* title); // TODO: with window handle
void glrCloseWindow(void);
// NOTE: tsone: Currently for win32 only. Returns HWND (one must cast as HWND to use)
void* glrWindowNativeHandle(void);

// TODO: tsone: disabled for now
#if 0
enum glrAttributeType
{
	GLR_FLOAT,
	GLR_VEC2,
	GLR_VEC3,
	GLR_VEC4,
	GLR_UNDEFINED
};
glrHandle glrBuildShader(const char* src_vs, const char* src_fs);
void glrSelectShader(glrHandle handle);
void glrShaderAttributes(glrHandle shader, int num, ...);
void glrUniformMatrix4(const char* name, const float* data);
void glrUniformMatrix3(const char* name, const float* data);
void glrUniformVec4(const char* name, const float* data);
void glrUniformVec3(const char* name, const float* data);
void glrUniformVec2(const char* name, const float* data);
void glrUniformFloat(const char* name, float);

glrHandle glrCreateTexture(void);
void glrDeleteTexture(glrHandle handle);
void glrTextureData(glrHandle handle, int width, int height, unsigned int* data, glrBool filter);
void glrBindTexture(int unit, glrHandle handle);

void glrMatrixIdentity(float* matrix);
void glrMatrixOrtho(float* matrix, int width, int height);
void glrMatrixMultiply(float* m1, float* m2);

void glrClear(float red, float green, float blue);
#endif

void glrSwapBuffers(void);

void glrSetFullscreen(void);
void glrSetWindowed(void);

void glrPerformResize(void);

glrBool glrWindowFocused(void);

void glrGetMousePos(int* x, int* y);
glrBool glrGetMouseLeft(void);
glrBool glrGetMouseRight(void);
int glrGetMouseScroll(void);
void glrMouseWrap(glrBool x, glrBool y);

void glrDisplayPixelBuffer(void);
void glrDisplayPixelBufferRect(int x, int y, int w, int h);

glrBool glrKeyPressed(glrKey key);
glrBool glrKeyHit(glrKey key);

void glrTextInputStart(const char* text, int max_chars);
glrBool glrTextInputActive(void);
glrBool glrTextInputCancelled(void);
const char* glrTextInputGet(void);

char glrKeyToAscii(glrKey key);

#if defined(__cplusplus)
}
#endif
#endif // GLR_H

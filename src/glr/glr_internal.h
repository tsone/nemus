#ifndef GLR_INTERNAL_H
#define GLR_INTERNAL_H
#if defined(__cplusplus)
extern "C" {
#endif
#include "glr.h"

// TODO: tsone: move somewhere else? glr_config.h?
// Configuration defines
#define GLR_ALLOW_OPENGL 1

extern const char* glr_app_name;

extern char glr_window_focus;
extern char glr_request_exit;

extern int glr_mouse_x;
extern int glr_mouse_y;
extern int glr_mouse_scroll;
extern char glr_mouse_left;
extern char glr_mouse_right;
extern char glr_mouse_left_click;
extern char glr_mouse_right_click;
extern int glr_mousewrap_dx;
extern int glr_mousewrap_dy;
extern glrBool glr_mousewrap_x;
extern glrBool glr_mousewrap_y;

int glrPerformResizeInternal(int fullscreen);
void glrDisplayPixelBufferRectInternal(int x, int y, int w, int h);
glrHandle glrOpenWindowInternal(const char* title, int width, int height, glrMode mode);
void glrResizeWindowInternal(int w, int h);
void glrCloseWindowInternal(void);
int glrProcessEventsInternal(void);
int glrKeyPressedInternal(glrKey key);
int glrMainLoopInternal(void);

#if defined(__cplusplus)
}
#endif
#endif // GLR_INTERNAL_H

#include "glr_internal.h"
#include <SDL2/SDL.h>
#if GLR_ALLOW_OPENGL
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#endif

struct glrWindow
{
	SDL_Window* sdl_wnd;
	SDL_GLContext sdl_glctx;
	SDL_Surface* sdl_surface;
	bool using_opengl;
};
static glrWindow window;

static int prev_winw;
static int prev_winh;

static char glr_exe_path[512];

static double qpc_PCFreq;
static Uint64 qpc_CounterStart;

void glrSleep(int ms)
{
	usleep(ms*1000);
}

double glrAppTime(void)
{
	return (SDL_GetPerformanceCounter()-qpc_CounterStart)/qpc_PCFreq;
}

void glrSetCurrentDirectory(const char* dir)
{
	chdir(dir);
}

void glrGetCurrentDirectory(unsigned int num, char* dir)
{
	getcwd(dir, num);
}

int glrKeyPressedInternal(glrKey key)
{
	const Uint8* keys=SDL_GetKeyboardState(NULL);
	switch(key)
	{
		case GLR_KEY_ESCAPE: return keys[SDL_SCANCODE_ESCAPE];
		case GLR_KEY_SPACE: return keys[SDL_SCANCODE_SPACE];
		case GLR_KEY_DELETE: return keys[SDL_SCANCODE_DELETE];
		case GLR_KEY_BACKSPACE: return keys[SDL_SCANCODE_BACKSPACE];
		case GLR_KEY_ENTER: return keys[SDL_SCANCODE_RETURN];
		case GLR_KEY_RIGHT: return keys[SDL_SCANCODE_RIGHT];
		case GLR_KEY_LEFT: return keys[SDL_SCANCODE_LEFT];
		case GLR_KEY_DOWN: return keys[SDL_SCANCODE_DOWN];
		case GLR_KEY_UP: return keys[SDL_SCANCODE_UP];
		case GLR_KEY_CTRL: return keys[SDL_SCANCODE_LCTRL] || keys[SDL_SCANCODE_RCTRL];
		case GLR_KEY_SHIFT: return keys[SDL_SCANCODE_LSHIFT] || keys[SDL_SCANCODE_RSHIFT];
		case GLR_KEY_ALT: return keys[SDL_SCANCODE_LALT] || keys[SDL_SCANCODE_RALT];
		case GLR_KEY_SUPER: return keys[SDL_SCANCODE_LGUI] || keys[SDL_SCANCODE_RGUI];
		case GLR_KEY_TAB: return keys[SDL_SCANCODE_TAB];
		case GLR_KEY_PAGEUP: return keys[SDL_SCANCODE_PAGEUP];
		case GLR_KEY_PAGEDOWN: return keys[SDL_SCANCODE_PAGEDOWN];
		case GLR_KEY_HOME: return keys[SDL_SCANCODE_HOME];
		case GLR_KEY_END: return keys[SDL_SCANCODE_END];
		case GLR_KEY_PERIOD: return keys[SDL_SCANCODE_KP_PERIOD] | keys[SDL_SCANCODE_PERIOD];
		case GLR_KEY_COMMA: return keys[SDL_SCANCODE_KP_COMMA] | keys[SDL_SCANCODE_COMMA];
		case GLR_KEY_SEMICOLON: return keys[SDL_SCANCODE_KP_1];
		case GLR_KEY_PLUS: return keys[SDL_SCANCODE_KP_PLUS];
		case GLR_KEY_MINUS: return keys[SDL_SCANCODE_KP_MINUS] | keys[SDL_SCANCODE_MINUS];
		case GLR_KEY_F1: return keys[SDL_SCANCODE_F1];
		case GLR_KEY_F2: return keys[SDL_SCANCODE_F2];
		case GLR_KEY_F3: return keys[SDL_SCANCODE_F3];
		case GLR_KEY_F4: return keys[SDL_SCANCODE_F4];
		case GLR_KEY_F5: return keys[SDL_SCANCODE_F5];
		case GLR_KEY_F6: return keys[SDL_SCANCODE_F6];
		case GLR_KEY_F7: return keys[SDL_SCANCODE_F7];
		case GLR_KEY_F8: return keys[SDL_SCANCODE_F8];
		case GLR_KEY_F9: return keys[SDL_SCANCODE_F9];
		case GLR_KEY_F10: return keys[SDL_SCANCODE_F10];
		case GLR_KEY_F11: return keys[SDL_SCANCODE_F11];
		case GLR_KEY_F12: return keys[SDL_SCANCODE_F12];
		case GLR_KEY_A: return keys[SDL_SCANCODE_A];
		case GLR_KEY_B: return keys[SDL_SCANCODE_B];
		case GLR_KEY_C: return keys[SDL_SCANCODE_C];
		case GLR_KEY_D: return keys[SDL_SCANCODE_D];
		case GLR_KEY_E: return keys[SDL_SCANCODE_E];
		case GLR_KEY_F: return keys[SDL_SCANCODE_F];
		case GLR_KEY_G: return keys[SDL_SCANCODE_G];
		case GLR_KEY_H: return keys[SDL_SCANCODE_H];
		case GLR_KEY_I: return keys[SDL_SCANCODE_I];
		case GLR_KEY_J: return keys[SDL_SCANCODE_J];
		case GLR_KEY_K: return keys[SDL_SCANCODE_K];
		case GLR_KEY_L: return keys[SDL_SCANCODE_L];
		case GLR_KEY_M: return keys[SDL_SCANCODE_M];
		case GLR_KEY_N: return keys[SDL_SCANCODE_N];
		case GLR_KEY_O: return keys[SDL_SCANCODE_O];
		case GLR_KEY_P: return keys[SDL_SCANCODE_P];
		case GLR_KEY_Q: return keys[SDL_SCANCODE_Q];
		case GLR_KEY_R: return keys[SDL_SCANCODE_R];
		case GLR_KEY_S: return keys[SDL_SCANCODE_S];
		case GLR_KEY_T: return keys[SDL_SCANCODE_T];
		case GLR_KEY_U: return keys[SDL_SCANCODE_U];
		case GLR_KEY_V: return keys[SDL_SCANCODE_V];
		case GLR_KEY_W: return keys[SDL_SCANCODE_W];
		case GLR_KEY_X: return keys[SDL_SCANCODE_X];
		case GLR_KEY_Y: return keys[SDL_SCANCODE_Y];
		case GLR_KEY_Z: return keys[SDL_SCANCODE_Z];
		case GLR_KEY_0: return keys[SDL_SCANCODE_0];
		case GLR_KEY_1: return keys[SDL_SCANCODE_1];
		case GLR_KEY_2: return keys[SDL_SCANCODE_2];
		case GLR_KEY_3: return keys[SDL_SCANCODE_3];
		case GLR_KEY_4: return keys[SDL_SCANCODE_4];
		case GLR_KEY_5: return keys[SDL_SCANCODE_5];
		case GLR_KEY_6: return keys[SDL_SCANCODE_6];
		case GLR_KEY_7: return keys[SDL_SCANCODE_7];
		case GLR_KEY_8: return keys[SDL_SCANCODE_8];
		case GLR_KEY_9: return keys[SDL_SCANCODE_9];
		case GLR_KEY_CAPSLOCK: return keys[SDL_SCANCODE_CAPSLOCK];
	}
	return 0;
}

void glrSetWindowTitle(const char* title)
{
	SDL_SetWindowTitle(window.sdl_wnd, title);
}

void glrResizeWindowInternal(int w, int h)
{
	if(!window.using_opengl)
	{
		auto old_surface=window.sdl_surface;
		window.sdl_surface=SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);
		glr_buffer.data=(unsigned int *)window.sdl_surface->pixels;
		if(old_surface)
		{
			SDL_BlitSurface(old_surface, NULL, window.sdl_surface, NULL);
			SDL_FreeSurface(old_surface);
		}
		glr_buffer.width=w;
		glr_buffer.pitch=4*w;
		glr_buffer.height=h;
	}
	else
	{
#if GLR_ALLOW_OPENGL
		glr_buffer.width=w;
		glr_buffer.pitch=w;
		glr_buffer.height=h;
		glViewport(0, 0, w, h);
#endif
	}
}

void glrSwapBuffers(void)
{
	if(window.using_opengl)
	{
#if GLR_ALLOW_OPENGL
		glFlush();
		SDL_GL_SwapWindow(window.sdl_wnd);
#endif
	}
	else
	{
		SDL_UpdateWindowSurface(window.sdl_wnd);
	}
}

int glrPerformResizeInternal(int fullscreen)
{
	if(fullscreen)
	{
		prev_winw=glr_buffer.width;
		prev_winh=glr_buffer.height;
		SDL_SetWindowFullscreen(window.sdl_wnd, SDL_WINDOW_FULLSCREEN);
	}
	else
	{
		SDL_SetWindowFullscreen(window.sdl_wnd, 0);
		glrResizeWindowInternal(prev_winw, prev_winh);
	}
	return 1;
}

void glrDisplayPixelBufferRectInternal(int x, int y, int w, int h)
{
	auto screen=SDL_GetWindowSurface(window.sdl_wnd);
	if(w<=0 || h<=0)
	{
		SDL_BlitSurface(window.sdl_surface, NULL, screen, NULL);
	}
	else
	{
		SDL_Rect rect={ .x=x, .y=y, .w=w, .h=h };
		SDL_BlitSurface(window.sdl_surface, &rect, screen, &rect);
	}
	SDL_UpdateWindowSurface(window.sdl_wnd);
}

void glrCloseWindowInternal(void)
{
	if(window.using_opengl)
		SDL_GL_DeleteContext(window.sdl_glctx);
	else
		SDL_FreeSurface(window.sdl_surface);
	SDL_DestroyWindow(window.sdl_wnd);
}

static void glrHandleSDLEvent(SDL_Event *ev)
{
	switch(ev->type)
	{
		case SDL_WINDOWEVENT:
		{
			switch(ev->window.event)
			{
				case SDL_WINDOWEVENT_FOCUS_GAINED:
					glr_window_focus=1;
					break;
				case SDL_WINDOWEVENT_FOCUS_LOST:
					glr_window_focus=0;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
				case SDL_WINDOWEVENT_RESIZED:
					glrResizeWindowInternal((ev->window.data1>5)?ev->window.data1:5,
									(ev->window.data2>5)?ev->window.data2:5
									);
					break;
				case SDL_WINDOWEVENT_EXPOSED:
					// TODO: tsone: also for GL?
					glrDisplayPixelBuffer();
					break;
			}
			break;
		}
		case SDL_QUIT:
			glr_request_exit=true;
			break;
		case SDL_MOUSEWHEEL:
		{
			static char wheel_count=0;
			if(!wheel_count)
				glr_mouse_scroll=(ev->wheel.y>0)?1:(ev->wheel.y<0)?-1:0;
			wheel_count=(wheel_count+1)%16; // take every 16th event
			break;
		}
		case SDL_MOUSEMOTION:
			glr_mouse_x=ev->motion.x;
			glr_mouse_y=ev->motion.y;
			break;
		case SDL_MOUSEBUTTONDOWN:
			glr_mouse_x=ev->button.x;
			glr_mouse_y=ev->button.y;
			glr_mouse_left=(ev->button.button==SDL_BUTTON_LEFT);
			glr_mouse_left_click=glr_mouse_left;
			glr_mouse_right=(ev->button.button==SDL_BUTTON_RIGHT);
			glr_mouse_right_click=glr_mouse_right;
			break;
		case SDL_MOUSEBUTTONUP:
		{
			glr_mouse_left&=(ev->button.button!=SDL_BUTTON_LEFT);
			glr_mouse_right&=(ev->button.button!=SDL_BUTTON_RIGHT);
			break;
		}
	}
}

int glrProcessEventsInternal(void)
{
	SDL_Event ev;
	while(SDL_PollEvent(&ev))
		glrHandleSDLEvent(&ev);
	return 1;
}

glrHandle glrOpenWindowInternal(const char* title, int width, int height, glrMode mode)
{
	glr_buffer.width=width;
	glr_buffer.height=height;

	if(mode==GLR_MODE_BITMAP)
	{
		window.using_opengl=false;

		window.sdl_wnd=SDL_CreateWindow(title,
										SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
										width, height,
										SDL_WINDOW_RESIZABLE); // TODO: tsone: SDL_WINDOW_ALLOW_HIGHDPI anyone?
		// NOTE: tsone: will be 32-bit but alpha is ignored
		window.sdl_surface=SDL_CreateRGBSurface(0, width, height, 32, 0, 0, 0, 0);
		glr_buffer.pitch=4*width;
		glr_buffer.data=(unsigned int *)window.sdl_surface->pixels;
	}

#if GLR_ALLOW_OPENGL
	if(mode==GLR_MODE_GPU)
	{
		window.using_opengl=true;

		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8); // rgba 32-bit
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 0); // no depth buffer
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // double buffered

		window.sdl_wnd=SDL_CreateWindow(title,
										SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
										width, height,
										SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE); // TODO: tsone: SDL_WINDOW_ALLOW_HIGHDPI anyone?
		window.sdl_glctx=SDL_GL_CreateContext(window.sdl_wnd);

		glClearColor(0,0,0.2f,0);
		glClearDepth(1.0);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);

		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width, height, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		SDL_GL_SetSwapInterval(1);
	}
#endif

	SDL_ShowWindow(window.sdl_wnd);
	SDL_RaiseWindow(window.sdl_wnd);

	return 0;
}

int main(int argc, const char** argv)
{
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);

	char* path=SDL_GetBasePath();
	if(path)
	{
		strncpy(glr_exe_path, path, 512);
		SDL_free(path);
	}
	else
	{
		strncpy(glr_exe_path, "./", 2);
	}

	qpc_PCFreq=SDL_GetPerformanceFrequency()/1000.0;
	qpc_CounterStart=SDL_GetPerformanceCounter();

	return glrMainLoopInternal();
}

#include "glr_internal.h"
#include <windows.h>
#include <MMSystem.h>
#include <ShellAPI.h>
#include <gl/gl.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <algorithm>
#include "DynamicArray.h"

struct pBITMAPINFO
{
	BITMAPINFOHEADER bmiHeader;
	RGBQUAD bmiColors[256];
} BMInfo;

struct glrWindow
{
	HWND hwnd;
	HDC hdc;
	HBITMAP hbm;
	HGLRC hrc;
	glrBool using_opengl;
};
static glrWindow window;

static HINSTANCE glr_instance;

#if GLR_ALLOW_OPENGL
typedef BOOL	(APIENTRY * PFNWGLSWAPINTERVALEXT)(int interval);
typedef int		(APIENTRY * PFNWGLGETSWAPINTERVALEXT)();
PFNWGLSWAPINTERVALEXT		wglSwapIntervalEXT = NULL;
PFNWGLGETSWAPINTERVALEXT	wglGetSwapIntervalEXT = NULL;
#define LOAD_EXTENSION(name) *((void**)&name) = (void*)wglGetProcAddress(#name)
#endif

static char glr_exe_path[512];

static glrBool glr_opengl_initialized;

static int prev_winx;
static int prev_winy;
static int prev_winw;
static int prev_winh;

static bool qpc_available;
static double qpc_PCFreq;
static __int64 qpc_CounterStart;

void* glrWindowNativeHandle(void)
{
	return (void*)window.hwnd;
}

void glrSleep(int ms)
{
	Sleep(ms);
}

double glrAppTime(void)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart-qpc_CounterStart)/qpc_PCFreq;
}

void glrSetCurrentDirectory(const char* dir)
{
	SetCurrentDirectory(dir);
}

void glrGetCurrentDirectory(unsigned int num, char* dir)
{
	GetCurrentDirectory(num, dir);
}

int glrKeyPressedInternal(glrKey key)
{
	switch(key)
	{
		case GLR_KEY_ESCAPE: return ((GetKeyState(VK_ESCAPE)&8000)!=0);
		case GLR_KEY_SPACE: return ((GetKeyState(VK_SPACE)&8000)!=0);
		case GLR_KEY_DELETE: return ((GetKeyState(VK_DELETE)&8000)!=0);
		case GLR_KEY_BACKSPACE: return ((GetKeyState(VK_BACK)&8000)!=0);
		case GLR_KEY_ENTER: return ((GetKeyState(VK_RETURN)&8000)!=0);
		case GLR_KEY_RIGHT: return ((GetKeyState(VK_RIGHT)&8000)!=0);
		case GLR_KEY_LEFT: return ((GetKeyState(VK_LEFT)&8000)!=0);
		case GLR_KEY_DOWN: return ((GetKeyState(VK_DOWN)&8000)!=0);
		case GLR_KEY_UP: return ((GetKeyState(VK_UP)&8000)!=0);
		case GLR_KEY_CTRL: return ((GetKeyState(VK_LCONTROL)&8000)!=0 || (GetKeyState(VK_RCONTROL)&8000)!=0);
		case GLR_KEY_SHIFT: return ((GetKeyState(VK_LSHIFT)&8000)!=0 || (GetKeyState(VK_RSHIFT)&8000)!=0);
		case GLR_KEY_ALT: return ((GetKeyState(VK_MENU)&8000)!=0);
		case GLR_KEY_SUPER: return ((GetKeyState(VK_LWIN)&8000)!=0 || (GetKeyState(VK_RWIN)&8000)!=0);
		case GLR_KEY_TAB: return ((GetKeyState(VK_TAB)&8000)!=0);
		case GLR_KEY_PAGEUP: return ((GetKeyState(VK_PRIOR)&8000)!=0);
		case GLR_KEY_PAGEDOWN: return ((GetKeyState(VK_NEXT)&8000)!=0);
		case GLR_KEY_HOME: return ((GetKeyState(VK_HOME)&8000)!=0);
		case GLR_KEY_END: return ((GetKeyState(VK_END)&8000)!=0);
			//case GLR_KEY_PERIOD: return ((GetKeyState(VK_PERIOD)&8000)!=0);
			//case GLR_KEY_COMMA: return ((GetKeyState(VK_COMMA)&8000)!=0);
		case GLR_KEY_PERIOD: return ((GetKeyState(VK_OEM_PERIOD)&8000)!=0);
		case GLR_KEY_COMMA: return ((GetKeyState(VK_OEM_COMMA)&8000)!=0);
		case GLR_KEY_SEMICOLON: return ((GetKeyState(VK_OEM_1)&8000)!=0);
		case GLR_KEY_PLUS: return ((GetKeyState(VK_OEM_PLUS)&8000)!=0);
		case GLR_KEY_MINUS: return ((GetKeyState(VK_OEM_MINUS)&8000)!=0);
			//case GLR_KEY_PLUS: return ((GetKeyState(VK_ADD)&8000)!=0);
			//case GLR_KEY_MINUS: return ((GetKeyState(VK_SUBTRACT)&8000)!=0);
		case GLR_KEY_F1: return ((GetKeyState(VK_F1)&8000)!=0);
		case GLR_KEY_F2: return ((GetKeyState(VK_F2)&8000)!=0);
		case GLR_KEY_F3: return ((GetKeyState(VK_F3)&8000)!=0);
		case GLR_KEY_F4: return ((GetKeyState(VK_F4)&8000)!=0);
		case GLR_KEY_F5: return ((GetKeyState(VK_F5)&8000)!=0);
		case GLR_KEY_F6: return ((GetKeyState(VK_F6)&8000)!=0);
		case GLR_KEY_F7: return ((GetKeyState(VK_F7)&8000)!=0);
		case GLR_KEY_F8: return ((GetKeyState(VK_F8)&8000)!=0);
		case GLR_KEY_F9: return ((GetKeyState(VK_F9)&8000)!=0);
		case GLR_KEY_F10: return ((GetKeyState(VK_F10)&8000)!=0);
		case GLR_KEY_F11: return ((GetKeyState(VK_F11)&8000)!=0);
		case GLR_KEY_F12: return ((GetKeyState(VK_F12)&8000)!=0);
		case GLR_KEY_A: return ((GetKeyState('A')&8000)!=0);
		case GLR_KEY_B: return ((GetKeyState('B')&8000)!=0);
		case GLR_KEY_C: return ((GetKeyState('C')&8000)!=0);
		case GLR_KEY_D: return ((GetKeyState('D')&8000)!=0);
		case GLR_KEY_E: return ((GetKeyState('E')&8000)!=0);
		case GLR_KEY_F: return ((GetKeyState('F')&8000)!=0);
		case GLR_KEY_G: return ((GetKeyState('G')&8000)!=0);
		case GLR_KEY_H: return ((GetKeyState('H')&8000)!=0);
		case GLR_KEY_I: return ((GetKeyState('I')&8000)!=0);
		case GLR_KEY_J: return ((GetKeyState('J')&8000)!=0);
		case GLR_KEY_K: return ((GetKeyState('K')&8000)!=0);
		case GLR_KEY_L: return ((GetKeyState('L')&8000)!=0);
		case GLR_KEY_M: return ((GetKeyState('M')&8000)!=0);
		case GLR_KEY_N: return ((GetKeyState('N')&8000)!=0);
		case GLR_KEY_O: return ((GetKeyState('O')&8000)!=0);
		case GLR_KEY_P: return ((GetKeyState('P')&8000)!=0);
		case GLR_KEY_Q: return ((GetKeyState('Q')&8000)!=0);
		case GLR_KEY_R: return ((GetKeyState('R')&8000)!=0);
		case GLR_KEY_S: return ((GetKeyState('S')&8000)!=0);
		case GLR_KEY_T: return ((GetKeyState('T')&8000)!=0);
		case GLR_KEY_U: return ((GetKeyState('U')&8000)!=0);
		case GLR_KEY_V: return ((GetKeyState('V')&8000)!=0);
		case GLR_KEY_W: return ((GetKeyState('W')&8000)!=0);
		case GLR_KEY_X: return ((GetKeyState('X')&8000)!=0);
		case GLR_KEY_Y: return ((GetKeyState('Y')&8000)!=0);
		case GLR_KEY_Z: return ((GetKeyState('Z')&8000)!=0);
		case GLR_KEY_0: return ((GetKeyState('0')&8000)!=0);
		case GLR_KEY_1: return ((GetKeyState('1')&8000)!=0);
		case GLR_KEY_2: return ((GetKeyState('2')&8000)!=0);
		case GLR_KEY_3: return ((GetKeyState('3')&8000)!=0);
		case GLR_KEY_4: return ((GetKeyState('4')&8000)!=0);
		case GLR_KEY_5: return ((GetKeyState('5')&8000)!=0);
		case GLR_KEY_6: return ((GetKeyState('6')&8000)!=0);
		case GLR_KEY_7: return ((GetKeyState('7')&8000)!=0);
		case GLR_KEY_8: return ((GetKeyState('8')&8000)!=0);
		case GLR_KEY_9: return ((GetKeyState('9')&8000)!=0);
		case GLR_KEY_CAPSLOCK: return !!(GetKeyState(VK_CAPITAL)&1);
	}
	return 0;
}

void glrSetWindowTitle(const char* title)
{
	SetWindowText(window.hwnd, title);
}

void glrResizeWindowInternal(int w, int h)
{
	if(!window.using_opengl)
	{
		static unsigned int* tempbuffer=NULL;
		static int tempalloc=0;
		int tempsize=glr_buffer.width*glr_buffer.height;
		if(tempalloc<tempsize)
		{
			delete[] tempbuffer;
			tempalloc=tempsize*4/3;
			tempbuffer=new unsigned int[tempalloc];
		}
		for(int i=0;i<tempsize;i++)
			tempbuffer[i]=glr_buffer.data[i];
		int w0=glr_buffer.width;
		int h0=glr_buffer.height;

		DeleteObject(window.hbm);

		glr_buffer.width=w;
		glr_buffer.height=h;

		//window.hdc=CreateCompatibleDC(NULL);
		unsigned int* image_bitmap=NULL;
		HDC hdc=GetDC(window.hwnd);
		BMInfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
		BMInfo.bmiHeader.biWidth=glr_buffer.width;
		BMInfo.bmiHeader.biHeight=-abs(glr_buffer.height);
		BMInfo.bmiHeader.biPlanes=1;
		BMInfo.bmiHeader.biBitCount=32;
		BMInfo.bmiHeader.biCompression=BI_RGB;
		BMInfo.bmiHeader.biSizeImage=abs(glr_buffer.width*glr_buffer.height)*sizeof(DWORD);
		BMInfo.bmiHeader.biXPelsPerMeter=0;
		BMInfo.bmiHeader.biYPelsPerMeter=0;
		window.hbm=CreateDIBSection(hdc, (BITMAPINFO*)&BMInfo, DIB_RGB_COLORS, (void**)&image_bitmap, 0, 0);
		ReleaseDC(window.hwnd, hdc);
		glr_buffer.data=image_bitmap;

		int rw=std::min(w0, glr_buffer.width);
		int rh=std::min(h0, glr_buffer.height);
		for(int y=0;y<rh;y++)
		{
			int di=y*glr_buffer.width;
			int si=y*w0;
			for(int x=0;x<rw;x++)
				glr_buffer.data[di++]=tempbuffer[si++];
		}
	}
	else
	{
#if GLR_ALLOW_OPENGL
		glr_buffer.width=w;
		glr_buffer.height=h;
		glViewport(0, 0, w, h);
#endif
	}
}

void glrSwapBuffers(void)
{
	if(window.using_opengl)
	{
#if GLR_ALLOW_OPENGL
		glFlush();
		SwapBuffers(window.hdc);
#endif
	}
}

int glrPerformResizeInternal(int fullscreen)
{
	if(fullscreen)
	{
		RECT clientrect;
		GetClientRect(window.hwnd, &clientrect);
		POINT wr_topleft;
		wr_topleft.x=clientrect.left;
		wr_topleft.y=clientrect.top;
		POINT wr_bottomright;
		wr_bottomright.x=clientrect.right;
		wr_bottomright.y=clientrect.bottom;
		ClientToScreen(window.hwnd, &wr_topleft);
		ClientToScreen(window.hwnd, &wr_bottomright);
		prev_winx=wr_topleft.x;
		prev_winy=wr_topleft.y;
		prev_winw=wr_bottomright.x-wr_topleft.x;
		prev_winh=wr_bottomright.y-wr_topleft.y;

		HDC hDC=GetWindowDC(NULL);
		int full_width=GetDeviceCaps(hDC, HORZRES);
		int full_height=GetDeviceCaps(hDC, VERTRES);

		SetWindowLongPtr(window.hwnd, GWL_EXSTYLE, WS_EX_APPWINDOW|WS_EX_TOPMOST);
		SetWindowLongPtr(window.hwnd, GWL_STYLE, WS_POPUP|WS_VISIBLE);
		SetWindowPos(window.hwnd, HWND_TOPMOST, 0, 0, full_width, full_height, SWP_SHOWWINDOW);

		ShowWindow(window.hwnd, SW_MAXIMIZE);

		glrResizeWindowInternal(full_width, full_height);
	}
	else
	{
		RECT wr;
		int winx=prev_winx;
		int winy=prev_winy;
		wr.left=(long)winx; wr.right=(long)winx+(long)prev_winw;
		wr.top=(long)winy; wr.bottom=(long)winy+(long)prev_winh;
		unsigned long dwExStyle;
		unsigned long dwStyle;
		dwExStyle=WS_EX_APPWINDOW|WS_EX_WINDOWEDGE;
		dwStyle=WS_OVERLAPPEDWINDOW|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;
		AdjustWindowRectEx(&wr, dwStyle, FALSE, dwExStyle);

		SetWindowLongPtr(window.hwnd, GWL_EXSTYLE, WS_EX_APPWINDOW|WS_EX_WINDOWEDGE);
		SetWindowLongPtr(window.hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW|WS_VISIBLE);
		SetWindowPos(window.hwnd, HWND_NOTOPMOST, wr.left, wr.top, wr.right-wr.left, wr.bottom-wr.top, SWP_SHOWWINDOW);
		ShowWindow(window.hwnd, SW_RESTORE);
		glrResizeWindowInternal(prev_winw, prev_winh);
	}
	return 1;
}

void glrDisplayPixelBufferRectInternal(int x, int y, int w, int h)
{
	HDC hdc;
	HBITMAP default_bitmap;
	hdc=GetDC(window.hwnd);
	default_bitmap=(HBITMAP)SelectObject(window.hdc, window.hbm);
	if(w<=0 || h<=0)
		BitBlt(hdc, 0, 0, glr_buffer.width, glr_buffer.height, window.hdc, 0, 0, SRCCOPY);
	else
		BitBlt(hdc, x, y, w, h, window.hdc, x, y, SRCCOPY);
	SelectObject(window.hdc, default_bitmap);
	DeleteDC(hdc);
}

void glrCloseWindowInternal(void)
{
	timeEndPeriod(1);
	if(window.hdc)
		ReleaseDC(window.hwnd, window.hdc);
	if(window.hwnd)
		DestroyWindow(window.hwnd);
	UnregisterClass("GLR", glr_instance); // TODO: unique class names for each window... and find out exactly how that's supposed to work
}

static LRESULT CALLBACK glrWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
		case WM_ACTIVATE:
			if(LOWORD(wParam)==WA_ACTIVE || LOWORD(wParam)==WA_CLICKACTIVE)
				glr_window_focus=1;
			else
				glr_window_focus=0;
			return 0;
		case WM_CLOSE:
			glr_request_exit=true;
			return 0;
		case WM_PAINT:
			glrDisplayPixelBuffer();
			break;
		case WM_SIZE:
		{
			int w=5;
			int h=5;
			if((LOWORD(lParam))>5)
				w=LOWORD(lParam);
			if((HIWORD(lParam))>5)
				h=HIWORD(lParam);
			glrResizeWindowInternal(w, h);
			return 0;
		}

		case WM_MOUSEWHEEL:
		{
			int steps=GET_WHEEL_DELTA_WPARAM(wParam)/WHEEL_DELTA;
			glr_mouse_scroll+=steps;
			return 0;
		}
		case WM_MOUSEMOVE:
		{
			short x=LOWORD(lParam);
			short y=HIWORD(lParam);
			glr_mouse_x=(int)x;
			glr_mouse_y=(int)y;

			RECT windowrect;
			if(glr_mousewrap_x || glr_mousewrap_y)
			{
				RECT clientrect;
				GetClientRect(hWnd, &clientrect);
				POINT wr_topleft;
				wr_topleft.x=clientrect.left;
				wr_topleft.y=clientrect.top;
				POINT wr_bottomright;
				wr_bottomright.x=clientrect.right;
				wr_bottomright.y=clientrect.bottom;
				ClientToScreen(hWnd, &wr_topleft);
				ClientToScreen(hWnd, &wr_bottomright);
				windowrect.left=wr_topleft.x;
				windowrect.top=wr_topleft.y;
				windowrect.right=wr_bottomright.x;
				windowrect.bottom=wr_bottomright.y;
			}

			if(glr_mousewrap_x)
			{
				POINT pc;
				GetCursorPos(&pc);
				if(pc.x>windowrect.right-2)
				{
					SetCursorPos(windowrect.left+1, pc.y);
					glr_mousewrap_dx+=windowrect.right-windowrect.left-2;
				}
				else if(pc.x<windowrect.left+1)
				{
					SetCursorPos(windowrect.right-2, pc.y);
					glr_mousewrap_dx-=windowrect.right-windowrect.left-2;
				}
			}
			if(glr_mousewrap_y)
			{
				POINT pc;
				GetCursorPos(&pc);
				if(pc.y>windowrect.bottom-2)
				{
					SetCursorPos(pc.x, windowrect.top+1);
					glr_mousewrap_dy+=windowrect.bottom-windowrect.top-2;
				}
				else if(pc.y<windowrect.top+1)
				{
					SetCursorPos(pc.x, windowrect.bottom-2);
					glr_mousewrap_dy-=windowrect.bottom-windowrect.top-2;
				}
			}
			return 0;
		}
		case WM_LBUTTONDOWN:
		{
			short x=LOWORD(lParam);
			short y=HIWORD(lParam);
			glr_mouse_x=(int)x;
			glr_mouse_y=(int)y;
			glr_mouse_left=true;
			glr_mouse_left_click=true;
			SetCapture(hWnd);
			return 0;
		}
		case WM_LBUTTONUP:
			glr_mouse_left=false;
			ReleaseCapture();
			return 0;

		case WM_RBUTTONDOWN:
		{
			short x=LOWORD(lParam);
			short y=HIWORD(lParam);
			glr_mouse_x=(int)x;
			glr_mouse_y=(int)y;
			glr_mouse_right=true;
			glr_mouse_right_click=true;
			return 0;
		}
		case WM_RBUTTONUP:
			glr_mouse_right=false;
			return 0;
		case WM_SYSKEYDOWN:
			return 0;
		case WM_SYSCOMMAND:
			if(wParam==SC_KEYMENU)
				return 0;
			break;
	}
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

int glrProcessEventsInternal(void)
{
	MSG msg;
	glrBool done=false;
	while(PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
	{
		TranslateMessage(&msg);
		if(!GetMessage(&msg, NULL, 0, 0))
			return 0;
		DispatchMessage(&msg);
	}
	return 1;
}

int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow)
{
	glr_instance=hInstance;
	GetModuleFileName(NULL, glr_exe_path, 512);
	auto si=strlen(glr_exe_path)-1;
	while(si>0 && glr_exe_path[si]!='\\') { si--; }
	glr_exe_path[si+1]='\0';

	LARGE_INTEGER li;
	if(QueryPerformanceFrequency(&li))
	{
		qpc_available=true;
		qpc_PCFreq=double(li.QuadPart)/1000.0;
		QueryPerformanceCounter(&li);
		qpc_CounterStart=li.QuadPart;
	}

	timeBeginPeriod(1);

	return glrMainLoopInternal();
}

glrHandle glrOpenWindowInternal(const char* title, int width, int height, glrMode mode)
{
	int screenwidth=GetSystemMetrics(SM_CXSCREEN);
	int screenheight=GetSystemMetrics(SM_CYSCREEN);
	width=std::min(width, screenwidth-100);
	height=std::min(height, screenheight-100);

	glr_buffer.width=width;
	glr_buffer.height=height;

	RECT wr;
	int winx=GetSystemMetrics(SM_CXSCREEN)/2-width/2;
	int winy=GetSystemMetrics(SM_CYSCREEN)/2-height/2;
	wr.left=(long)winx; wr.right=(long)winx+(long)width;
	wr.top=(long)winy; wr.bottom=(long)winy+(long)height;

	WNDCLASSEX	wcx;
	ZeroMemory(&wcx, sizeof(WNDCLASSEX));
	wcx.cbSize=sizeof(WNDCLASSEX);
	wcx.lpfnWndProc=(WNDPROC)glrWindowProc;
	wcx.hInstance=glr_instance;
	//wcx.hIcon=LoadIcon(NULL, IDI_APPLICATION);
	//wcx.hIcon=LoadIcon(wcx.hInstance, "PROGRAMICON");
	//wcx.hIcon=(HICON)LoadImage(NULL, "icon.ico", IMAGE_ICON, 48, 48, LR_LOADFROMFILE);
	wcx.hIcon=LoadIcon(wcx.hInstance, "PROGRAMICON");
	wcx.hCursor=LoadCursor(NULL, IDC_ARROW);
	wcx.hbrBackground=NULL;
	wcx.lpszMenuName=NULL;
	wcx.lpszClassName="GLR";
	RegisterClassEx(&wcx);

	unsigned long dwExStyle;
	unsigned long dwStyle;
	dwExStyle=WS_EX_APPWINDOW|WS_EX_WINDOWEDGE;
	dwStyle=WS_OVERLAPPEDWINDOW|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;
	AdjustWindowRectEx(&wr, dwStyle, FALSE, dwExStyle);

	window.hwnd=CreateWindowEx(dwExStyle, "GLR", title, dwStyle,
							   winx, winy, wr.right-wr.left, wr.bottom-wr.top,
							   NULL, NULL, glr_instance, NULL);

	if(mode==GLR_MODE_BITMAP)
	{
		window.using_opengl=false;

		window.hdc=CreateCompatibleDC(NULL);
		unsigned int* image_bitmap=NULL;
		HDC hdc=GetDC(window.hwnd);
		BMInfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
		BMInfo.bmiHeader.biWidth=glr_buffer.width;
		BMInfo.bmiHeader.biHeight=-abs(glr_buffer.height);
		BMInfo.bmiHeader.biPlanes=1;
		BMInfo.bmiHeader.biBitCount=32;
		BMInfo.bmiHeader.biCompression=BI_RGB;
		BMInfo.bmiHeader.biSizeImage=abs(glr_buffer.width*glr_buffer.height)*sizeof(DWORD);
		BMInfo.bmiHeader.biXPelsPerMeter=0;
		BMInfo.bmiHeader.biYPelsPerMeter=0;
		window.hbm=CreateDIBSection(hdc, (BITMAPINFO*)&BMInfo, DIB_RGB_COLORS, (void**)&image_bitmap, 0, 0);
		ReleaseDC(window.hwnd, hdc);
		//	if(image_bitmap==NULL)
		//		LogPrint("image_bitmap==NULL, from CreateDIBSection");
		glr_buffer.data=image_bitmap;
	}

#if GLR_ALLOW_OPENGL
	if(mode==GLR_MODE_GPU)
	{
		window.using_opengl=true;

		static PIXELFORMATDESCRIPTOR pfd=
		{
			sizeof(PIXELFORMATDESCRIPTOR),
			1, PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
			32, // color bits
			0,0,0,0,0,0, 0, 0, 0, 0,0,0,0,
			//			24, // depth bits
			//			8, // 0 bits of stencil
			0, // depth bits
			0, // 0 bits of stencil
			0, PFD_MAIN_PLANE, 0, 0,0,0
		};
		window.hdc=GetDC(window.hwnd);
		GLuint pixelformat=ChoosePixelFormat(window.hdc, &pfd);
		SetPixelFormat(window.hdc, pixelformat, &pfd);
		window.hrc=wglCreateContext(window.hdc);
		wglMakeCurrent(window.hdc, window.hrc);

		// TODO: tsone: glew disabled for now
#if 0
		if(!glr_opengl_initialized)
		{
			glrLog("glewInit()");
			GLenum result=glewInit();
			if(result!=GLEW_OK)
			{
				glrLog(" failed\n");
				char* msg=(char*)glewGetErrorString(result);
				glrLog("GLEW error: %s\n", msg);
			}
			else
			{
				glrLog(" ok\n");
				glr_opengl_initialized=true;
			}
		}
#endif

		glClearColor(0,0,0.2f,0);
		glClearDepth(1.0);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);

		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width, height, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		// Enable vsync (if available)
		LOAD_EXTENSION(wglSwapIntervalEXT);
		LOAD_EXTENSION(wglGetSwapIntervalEXT);
		if(wglSwapIntervalEXT && wglGetSwapIntervalEXT)
			wglSwapIntervalEXT(1);
	}
#endif

	ShowWindow(window.hwnd, SW_SHOW);
	SetForegroundWindow(window.hwnd);
	SetFocus(window.hwnd);

	return 0;
}

#if GLR_ALLOW_OPENGL

void glrClear(float red, float green, float blue)
{
	glClearColor(red, green, blue, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}

// TODO: tsone: disabled for now
#if 0
struct glrAttribute
{
	glrAttributeType type;
	int size;
	int location;
};
struct glrShader
{
	GLuint program;
	glrAttribute attributes[32];
	int num_attributes;
	int attribute_stride;
};
DynamicArray<glrShader> glr_shaders;
glrHandle glr_current_shader;

void glrSelectShader(glrHandle handle)
{
	glr_current_shader=handle;
	glrShader shader=glr_shaders[glr_current_shader];
	glUseProgram(shader.program);
}
char* glrReadFile(const char *filename)
{
	int filesize;
	char *string;
	FILE *file;
	file=fopen(filename, "rb");
	if(!file)
	{
		glrLog("# file '%s' not found\n", filename);
		return NULL;
	}
	fseek(file, 0, SEEK_END);
	filesize=ftell(file);
	fseek(file, 0, 0);
	string=new char[filesize+1];
	fread(string, filesize, 1, file);
	string[filesize]='\0';
	fclose(file);
	return string;
}
glrHandle glrBuildShader(const char* src_vs, const char* src_fs)
{
	glrLog("building shader '%s'/'%s'\n", src_vs, src_fs);

	glrShader shader;

	GLuint shader_vs=glCreateShader(GL_VERTEX_SHADER);
	GLuint shader_fs=glCreateShader(GL_FRAGMENT_SHADER);

	int success=0;
	char logtext[1024];
	logtext[1023]='\0';
	int logsize=0;

	char* text=glrReadFile(src_vs);
	int length=strlen(text);
	glShaderSource(shader_vs, 1, &text, &length);
	glCompileShader(shader_vs);
	glGetShaderiv(shader_vs, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		glrLog("# failed to compile vertex shader '%s'\n", src_vs);
		glGetShaderInfoLog(shader_vs, 1023, &logsize, logtext);
		glrLog("%s\n", logtext);
	}

	delete[] text;
	text=glrReadFile(src_fs);
	length=strlen(text);
	glShaderSource(shader_fs, 1, &text, &length);
	glCompileShader(shader_fs);
	glGetShaderiv(shader_fs, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		glrLog("# failed to compile fragment shader '%s'\n", src_fs);
		glGetShaderInfoLog(shader_fs, 1023, &logsize, logtext);
		glrLog("%s\n", logtext);
	}
	delete[] text;

	shader.program=glCreateProgram();
	glAttachShader(shader.program, shader_vs);
	glAttachShader(shader.program, shader_fs);

	glLinkProgram(shader.program);
	glGetProgramiv(shader.program, GL_LINK_STATUS, &success);
	if(!success)
	{
		glrLog("# failed to link shader program '%s'/'%s'\n", src_vs, src_fs);
		glGetProgramInfoLog(shader.program, 1023, &logsize, logtext);
		glrLog("%s\n", logtext);
	}
	else
		glrLog("successfully linked shader\n");

	glDetachShader(shader.program, shader_vs);
	glDetachShader(shader.program, shader_fs);

	shader.num_attributes=0;
	shader.attribute_stride=0;

	glrHandle handle=glr_shaders.Add(shader);
	return handle;
}
void glrShaderAttribute(glrHandle handle, int index, glrAttributeType attrib)
{
	//	glrShader& shader=glr_shaders[glr_current_shader];
	glrShader& shader=glr_shaders[handle];
	shader.attributes[index].type=attrib;
	int datasize=0;
	switch(shader.attributes[index].type)
	{
		case GLR_FLOAT: datasize=1; break;
		case GLR_VEC2:  datasize=2; break;
		case GLR_VEC3:  datasize=3; break;
		case GLR_VEC4:  datasize=4; break;
	}
	shader.attributes[index].size=datasize;
	for(int i=0;i<index;i++)
		if(i>=shader.num_attributes)
		{
			shader.attributes[i].type=GLR_UNDEFINED;
			shader.attributes[i].size=0;
			shader.attributes[i].location=0;
		}
	shader.num_attributes=max(shader.num_attributes, index+1);
	int location=0;
	for(int i=0;i<shader.num_attributes;i++)
	{
		shader.attributes[i].location=location;
		location+=shader.attributes[i].size*sizeof(GLfloat);
	}
	shader.attribute_stride=location;
}
void glrShaderAttributes(glrHandle shader, int num, ...)
{
	va_list args;
	va_start(args, num);
	for(int i=0;i<num;i++)
	{
		glrAttributeType attrib=va_arg(args, glrAttributeType);
		glrShaderAttribute(shader, i, attrib);
	}
	va_end(args);
}
void glrSetShaderAttributes()
{
	//	static glrBool first=true;
	glrShader& shader=glr_shaders[glr_current_shader];
	for(int i=0;i<shader.num_attributes;i++)
	{
		//		if(first)
		//			glrLog("set shader attribute %i: size=%i, stride=%i, location=%i\n", i, shader.attributes[i].size, shader.attribute_stride, shader.attributes[i].location);
		glVertexAttribPointer(i, shader.attributes[i].size, GL_FLOAT, GL_FALSE, shader.attribute_stride, (void*)shader.attributes[i].location);
	}
	//	first=false;
}
void glrUniformMatrix4(const char* name, const float* data)
{
	glrShader& shader=glr_shaders[glr_current_shader];
	GLuint location=glGetUniformLocationARB(shader.program, name);
	glUniformMatrix4fv(location, 1, false, data);
}

const int GLR_TRIANGLES=0;
const int GLR_QUADS=1;
const int GLR_LINES=2;
DynamicArray<float> glr_immediate_geometry;
int glr_immediate_components;
int glr_immediate_type;
float glr_immediate_line_thickness;
float* glr_immediate_line_matrix;
int glr_immediate_line_width;
int glr_immediate_line_height;
GLuint glr_immediate_vao;
GLuint glr_immediate_vbo;
int glr_immediate_vbo_numverts=0;
void glrBeginTriangles()
{
	glr_immediate_geometry.Reset();
	glr_immediate_components=0;
	glr_immediate_type=GLR_TRIANGLES;
}
void glrBeginQuads()
{
	glr_immediate_geometry.Reset();
	glr_immediate_components=0;
	glr_immediate_type=GLR_QUADS;
}
void glrBeginLines(float thickness, float* matrix, int width, int height)
{
	glr_immediate_geometry.Reset();
	glr_immediate_components=0;
	glr_immediate_type=GLR_LINES;
	glr_immediate_line_thickness=thickness;
	glr_immediate_line_matrix=matrix;
	glr_immediate_line_width=width;
	glr_immediate_line_height=height;
}
#include <stdarg.h>
void glrVertex(int components, ...)
{
	glr_immediate_components=components;
	if(glr_immediate_type==GLR_QUADS && glr_immediate_geometry.num/glr_immediate_components%6==3) // adding the fourth vertex, need to copy data for the second triangle (TODO: indices?)
	{
		int start=glr_immediate_geometry.num-components*3;
		for(int i=0;i<components;i++)
			glr_immediate_geometry.Add(glr_immediate_geometry[start+i]);
		start+=components*2;
		for(int i=0;i<components;i++)
			glr_immediate_geometry.Add(glr_immediate_geometry[start+i]);
	}
	va_list args;
	va_start(args, components);
	for(int i=0;i<components;i++)
	{
		float x=(float)va_arg(args, double);
		glr_immediate_geometry.Add(x);
	}
	va_end(args);
}
void glrVertexV(int components, float* vdata)
{
	glr_immediate_components=components;
	if(glr_immediate_type==GLR_QUADS && glr_immediate_geometry.num/glr_immediate_components%6==3) // adding the fourth vertex, need to copy data for the second triangle (TODO: indices?)
	{
		int start=glr_immediate_geometry.num-components*3;
		for(int i=0;i<components;i++)
			glr_immediate_geometry.Add(glr_immediate_geometry[start+i]);
		start+=components*2;
		for(int i=0;i<components;i++)
			glr_immediate_geometry.Add(glr_immediate_geometry[start+i]);
	}
	for(int i=0;i<components;i++)
		glr_immediate_geometry.Add(vdata[i]);
}
void glrEnd()
{
	if(glr_immediate_geometry.num==0)
		return;

	float* data=glr_immediate_geometry.GetPtr();

	if(glr_immediate_vbo!=0 && glr_immediate_geometry.num>glr_immediate_vbo_numverts)
	{
		glDeleteBuffers(1, &glr_immediate_vbo);
		glr_immediate_vbo=0;
	}

	if(glr_immediate_vao==0)
		glGenVertexArrays(1, &glr_immediate_vao);
	glBindVertexArray(glr_immediate_vao);
	if(glr_immediate_vbo==0)
	{
		glGenBuffers(1, &glr_immediate_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, glr_immediate_vbo);
		glr_immediate_vbo_numverts=glr_immediate_geometry.num;
		//		glBufferData(GL_ARRAY_BUFFER, glr_immediate_geometry.num*sizeof(GLfloat), data, GL_STATIC_DRAW);
		glBufferData(GL_ARRAY_BUFFER, glr_immediate_vbo_numverts*sizeof(GLfloat), NULL, GL_STATIC_DRAW);
	}
	//	else
	//	{
	glBindBuffer(GL_ARRAY_BUFFER, glr_immediate_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, glr_immediate_geometry.num*sizeof(GLfloat), data);
	//	}

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glrSetShaderAttributes();

	//	if(glr_immediate_type==GLR_TRIANGLES)
	glDrawArrays(GL_TRIANGLES, 0, glr_immediate_geometry.num/glr_immediate_components);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glBindVertexArray(0);
}
#endif

struct glrTexture
{
	GLuint handle;
	int width;
	int height;
};
DynamicArray<glrTexture> glr_textures;
glrHandle glr_current_texture;

glrHandle glrCreateTexture()
{
	glrTexture texture;
	texture.width=0;
	texture.height=0;
	glGenTextures(1, &texture.handle);
	glrHandle handle=glr_textures.Add(texture);
	return handle;
}

void glrDeleteTexture(glrHandle handle)
{
	if(handle>=glr_textures.num)
		return;
	glrTexture& texture=glr_textures[handle];
	glDeleteTextures(1, &texture.handle);
	texture.handle=0;
}

void glrBindTexture(int unit, glrHandle handle)
{
	//	glActiveTexture(GL_TEXTURE0+unit);
	glBindTexture(GL_TEXTURE_2D, glr_textures[handle].handle);
}

void glrTextureData(glrHandle handle, int width, int height, unsigned int* data, glrBool filter)
{
	glrBindTexture(0, handle);
	glrTexture& texture=glr_textures[handle];
	texture.width=width;
	texture.height=height;
	if(filter)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
}
//void glrTextureFData(glrHandle handle, int width, int height, float* data);


void glrMatrixIdentity(float* matrix)
{
	for(int i=0;i<16;i++)
		matrix[i]=0.0f;
	matrix[0]=1.0f;
	matrix[5]=1.0f;
	matrix[10]=1.0f;
	matrix[15]=1.0f;
}

void glrMatrixOrtho(float* matrix, int width, int height)
{
	matrix[0]=2.0f/width;	matrix[1]=0.0f;			matrix[2]=0.0f;		matrix[3]=0.0f;
	matrix[4]=0.0f;			matrix[5]=-2.0f/height;	matrix[6]=0.0f;		matrix[7]=0.0f;
	matrix[8]=0.0f;			matrix[9]=0.0f;			matrix[10]=1.0f;	matrix[11]=0.0f;
	matrix[12]=-1.0f;		matrix[13]=1.0f;		matrix[14]=0.0f;	matrix[15]=1.0f;
}

void glrMatrixFrustum(float* matrix, float left, float right, float bottom, float top, float nearz, float farz)
{
	matrix[0]=2.0f*nearz/(right-left);		matrix[1]=0.0f;						matrix[2]=0.0f;								matrix[3]=0.0f;
	matrix[4]=0.0f;							matrix[5]=2.0f*nearz/(top-bottom);	matrix[6]=0.0f;								matrix[7]=0.0f;
	matrix[8]=(right+left)/(right-left);	matrix[9]=(top+bottom)/(top-bottom);matrix[10]=-(farz+nearz)/(farz-nearz);		matrix[11]=-1.0f;
	matrix[12]=0.0f;						matrix[13]=0.0f;					matrix[14]=-2.0f*farz*nearz/(farz-nearz);	matrix[15]=0.0f;
}

void glrMatrixTranslation(float* matrix, float x, float y, float z)
{
	matrix[0]=1.0f;	matrix[1]=0.0f;	matrix[2]=0.0f;	matrix[3]=0;
	matrix[4]=0.0f;	matrix[5]=1.0f;	matrix[6]=0.0f;	matrix[7]=0;
	matrix[8]=0.0f;	matrix[9]=0.0f;	matrix[10]=1.0f;matrix[11]=0;
	matrix[12]=x;	matrix[13]=y;	matrix[14]=z;	matrix[15]=1.0f;
}

void glrMatrixRotation(float* matrix, float a, float x, float y, float z)
{
	float l2=x*x+y*y+z*z;
	if(l2!=1.0f && l2!=0.0f)
	{
		float l=sqrt(l2);
		x/=l;
		y/=l;
		z/=l;
	}
	float cosa=cos(a);
	float icosa=1.0f-cosa;
	float sina=sin(a);

	matrix[0]=cosa+x*x*icosa; matrix[1]=x*y*icosa-z*sina; matrix[2]=x*z*icosa+y*sina; matrix[3]=0.0f;
	matrix[4]=y*x*icosa+z*sina; matrix[5]=cosa+y*y*icosa; matrix[6]=y*z*icosa-x*sina; matrix[7]=0.0f;
	matrix[8]=z*x*icosa-y*sina; matrix[9]=z*y*icosa+x*sina; matrix[10]=cosa+z*z*icosa; matrix[11]=0.0f;
	matrix[12]=0.0f; matrix[13]=0.0f; matrix[14]=0.0f; matrix[15]=1.0f;
}

void glrMatrixMultiply(float* m1, float* m2)
{
	float m3[16];
	for(int y=0;y<4;y++)
		for(int x=0;x<4;x++)
		{
			float& f=m3[y*4+x];
			f=0.0f;
			for(int i=0;i<4;i++)
				f+=m2[y*4+i]*m1[i*4+x];
		}
	for(int i=0;i<16;i++)
		m1[i]=m3[i];
}

#endif

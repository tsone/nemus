void DrawSnake(int x0, int y0, int x1, int y1, int sx, int sy, int sw, int sh)
{
	int cx=0;
	int hw=sw/2;
	int hh=sh/2;
	for(int x=x0;x<x1;x++)
	{
		int y=y0+(y1-y0)*(x-x0)/(x1-x0);
		DrawGUIb(sx+cx,sy,1,sh, x, y-hh);
		if(x<x0+hw || x1-x==sw-cx)
			cx++;
	}
}

void DrawEnvelopeSegment(int mode, int x0, int y0, int x1, int y1, int y2)
{
	if(mode==0) // volume envelope
	{
		unsigned int ca0=guibits.GetPixel(212,32-28);
		unsigned int ca1=guibits.GetPixel(215,32-28);
		unsigned int ca2=guibits.GetPixel(217,32-28);
		unsigned int cb0=guibits.GetPixel(212,37-28);
		unsigned int cb1=guibits.GetPixel(215,37-28);
		unsigned int cb2=guibits.GetPixel(217,37-28);
		for(int x=x0-1;x<x1;x++)
		{
			int y=y0+(y1-y0)*(x-x0)/(x1-x0);
			for(int i=0;i<y2-y;i++)
			{
				unsigned int color=window.GetPixel(x, y+i);
				if(color==ca0) color=cb0;
				if(color==ca1) color=cb1;
				if(color==ca2) color=cb2;
				blitter.DrawPixel(x, y+i, color);
			}
			DrawGUIb(47,45,1,7, x, y-2);
		}
	}
}

void DrawEnvelopeGraph(int mode, MapGraph* graph)
{
	int prev_i=-1;
	int prev_y=-1;
	bool prev_triggered=false;
	int ymax=15;
	if(mode==1)
		ymax=14;
	int i0=0;
	int i1=48;
	for(int i=i0;i<i1;i++)
	{
		GraphPoint point=graph->Read(i);
		if(i==i1-1 && point.Empty()) // implicitly extended to end
		{
			if(prev_i==-1)
			{
				prev_i=0;
				prev_y=0;
				if(mode==1)
					prev_y=7;
			}
			point.trigger=ymax-prev_y;
		}
		if(!point.Empty())
		{
			int y=prev_y;
			if(point.curve!=0xFF)
				y=ymax-point.curve;
			if(prev_i==-1 && i>0) // implicitly extended to start
			{
				prev_triggered=true;
				prev_i=0;
				prev_y=ymax;
				if(mode==1)
					prev_y=7;
				if(y==-1 && point.trigger!=0xFF)
					y=ymax-point.trigger;
				if(y!=-1)
					prev_y=y;
				if(y==-1)
					y=prev_y;
			}
			if(prev_i!=-1) // finish current segment
			{
				if(mode==0)
				{
					DrawEnvelopeSegment(mode, gui_main_x2+33+prev_i*envwidth, gui_main_y0+25+prev_y*envheight,
											  gui_main_x2+33+i*envwidth, gui_main_y0+25+y*envheight,
										gui_main_y0+28+ymax*envheight);
				}
				else
				{
					int ssw=5;
					int ssx=60;
					if(prev_triggered)
						ssx=58;
					if(i==47 || point.trigger!=0xFF)
					{
						if(ssx==58)
							ssw=8;
						else
							ssx=61;
					}
					DrawSnake(gui_main_x2+33+prev_i*envwidth, gui_main_y0+25+prev_y*envheight,
							  gui_main_x2+33+i*envwidth, gui_main_y0+25+y*envheight,
							  ssx, 35, ssw, 7);
				}
			}
			if(point.trigger!=0xFF)
			{
				y=ymax-point.trigger;
				prev_triggered=true;
			}
			else
				prev_triggered=false;
			prev_i=i;
			prev_y=y;
		}
	}
	for(int i=0;i<48;i++)
	{
		GraphPoint point=graph->Read(i);
		if(!point.Empty())
		{
			if(point.curve!=0xFF)
			{
				int y=ymax-point.curve;
				DrawGUIb(31,56,9,9, gui_main_x2+33+i*envwidth-4, gui_main_y0+25+y*envheight-4);
			}
			if(point.trigger!=0xFF)
			{
				int y=ymax-point.trigger;
				DrawGUIb(31,44,9,9, gui_main_x2+33+i*envwidth-4, gui_main_y0+25+y*envheight-4);
			}
		}
	}
}

void EditorPart_Envelope()
{
	for(int x=0;x<48;x++)
	{
		int rx0=gui_main_x2+30+x*envwidth;
		int cy=16;
		int my=-1;
		if(song->gui_envtype==1)
		{
			cy=15;
			my=7;
		}
		for(int y=0;y<cy;y++)
		{
			int ry=gui_main_y0+25+y*envheight;
			int sx=191;
			int rx=rx0;
			if(x==0)
				sx=162;
			else if(x==16 || x==32)
				sx=176;
			else if(x==47)
				sx=206;

			int w=envwidth;
			int h=envheight;

			if(y==0)
				DrawGUIb(sx,27,w,h, rx, ry-2);
			else if(y==my)
				DrawGUIb(sx,49,w,h, rx, ry-2);
			else if(y==cy-1)
				DrawGUIb(sx,60,w,h, rx, ry-2);
			else
				DrawGUIb(sx,37,w,h, rx, ry-2);
		}
	}

	if(song->gui_envtype==1)
	{
		const auto* sound=song->GetSound(song->selected_sound);
		if(sound)
		{
			GUIbAlpha(196);
			int x0=gui_main_x2+30+sound->wavemode_pos*envwidth+2;
			int y0=gui_main_y0+25+1;
			for(int k=0;k<4;k++)
				DrawGUIb(65,11,3,14, x0, y0+k*28);
			GUIbAlpha(255);
		}
	}

	if(song->gui_envtype==0)
		DrawEnvelopeGraph(0, venv_editor.graphs.graph_source[0]);
	else
		DrawEnvelopeGraph(1, penv_editor.graphs.graph_source[0]);

	if(song->gui_envtype==0)
		GraphEditor(EPM_VENV);
	else
		GraphEditor(EPM_PENV);
}

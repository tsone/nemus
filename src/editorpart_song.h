static bool show_settings=false;

void EditorPart_Song()
{
	const bool in_song_menu=(song->selected_sound==-1 && song->selected_st==-1 && song->selected_ch==-1) && !show_settings;
	const bool in_settings_menu=(song->selected_sound==-1 && song->selected_st==-1 && song->selected_ch==-1) && show_settings;

	if(in_song_menu)
	{
		PrintMenu(0, -1, -2, "(SONG)");
	}
	else
	{
		ButtonResult br;
		PrintMenu(0, -1, -2, "(    )");
		ButtonColor(1);
		br=MenuButton(100, -1+1, -2, "SONG");
		if(br.clicked)
		{
			song->selected_sound=-1;
			song->selected_st=-1;
			song->selected_ch=-1;

			song_modified=true;

			show_settings=false;
		}
	}

	if(in_settings_menu)
	{
		PrintMenu(0, 6, -2, "(SETTINGS)");
	}
	else
	{
		ButtonResult br;
		PrintMenu(0, 6, -2, "(        )");
		ButtonColor(1);
		br=MenuButton(101, 6+1, -2, "SETTINGS");
		if(br.clicked)
		{
			song->selected_sound=-1;
			song->selected_st=-1;
			song->selected_ch=-1;

			song_modified=true;

			show_settings=true;
		}
	}

	if(song->selected_sound!=-1)
		return;

	if(!mouse.left && !mouse.right && song_editor.region.Contains(mousepoint))
	{
		song_edit_scalemap=false;
		song_editor.graphs.num=0;
		if(song->selected_st!=-1)
		{
			song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetST(song->selected_st)->content;
			song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetST(song->selected_st)->envelope;
		}
		else if(song->selected_ch!=-1)
		{
			song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetCH(song->selected_ch)->content;
			song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetCH(song->selected_ch)->envelope;
			song_edit_scalemap=true;
		}
		else
		{
			// select all channels for editing
			for(int i=0;i<20;i++)
				if(song->GetST(i)!=NULL)
				{
					song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetST(i)->content;
					song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetST(i)->envelope;
				}
			for(int i=0;i<100;i++)
				if(song->GetCH(i)!=NULL)
				{
					song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetCH(i)->content;
					song_editor.graphs.graph_source[song_editor.graphs.num++]=song->GetCH(i)->envelope;
				}
			song_edit_scalemap=true;
		}
		SelectEditor(song_editor);
	}


	auto* part=song->part;
	int stepsperbeat=part->timesig2*2*timeline_substeps;
	int barsize=songeditor_barsize;
	int barheight0=20;
	int barheight1=80;

	if(songplay_mode>0)
	{
		int playp=nemus->songplayer.GetCurPos()/stepsperbeat*barsize;
		if(playp<song->gui_scroll_song)
			song->gui_scroll_song=playp-50;
		if(playp>song->gui_scroll_song+(gui_main_x3-gui_main_x2)-100)
			song->gui_scroll_song=playp-(gui_main_x3-gui_main_x2)*1/4;
	}

	const int song_scroll_x=song->gui_scroll_song;

	blitter.SetScroll(song_scroll_x, 0);
	blitter.SetClipping(song_editor.view);

	for(int j=0;j<100;j++)
	{
		NMChannel* ch=song->GetCH(j);
		if(ch==NULL)
			continue;
		if(ch->song_cache_dirty || all_song_cache_dirty)
			ch->ClearSongCache();
	}
	for(int j=0;j<20;j++)
	{
		NMChannel* st=song->GetST(j);
		if(st==NULL)
			continue;
		if(st->song_cache_dirty || all_song_cache_dirty)
			st->ClearSongCache();
	}

	const int i0=std::max(0, song_scroll_x/barsize-2);
	const int i1=std::min((song_scroll_x+(gui_main_x3-gui_main_x1))/barsize+5, 0x10000/stepsperbeat);
	for(int i=i0;i<i1;i++)
	{
		int y0=gui_main_y0+35;
		int y=y0;

		int x=gui_main_x2+i*barsize;
		bool b=(i%part->timesig1==0);

		if(b)
		{
			x+=1;
			barsize-=1;
		}
		int so=(i/part->timesig1%2)?11:0;

		if(songplay_mode>0 && i==nemus->songplayer.GetCurPos()/stepsperbeat)
			so=-11;

		int sdx=89;
		int sdy=116;

		int h=barheight0;
		DrawGUIb(sdx+109+so,sdy+1,4,4, x, y);
		DrawGUIr(sdx+114+so,sdy+1,1,4, x+4, y, barsize-8, 0);
		DrawGUIb(sdx+120-4+so,sdy+1,4,4, x+barsize-4, y);
		blitter.DrawBar(x, y+4, barsize-1, h-7, guibits.GetPixel(sdx+114+so,sdy+5));
		DrawGUIb(sdx+109+so,sdy+7,4,3, x, y+h-3);
		DrawGUIr(sdx+114+so,sdy+7,1,3, x+4, y+h-3, barsize-8, 0);
		DrawGUIb(sdx+120+so-4,sdy+7,4,3, x+barsize-4, y+h-3);

		y+=h+1;
		h=barheight1;
		DrawGUIb(sdx+109+so,sdy+1+11,4,4, x, y);
		DrawGUIr(sdx+114+so,sdy+1+11,1,4, x+4, y, barsize-8, 0);
		DrawGUIb(sdx+120+so-4,sdy+1+11,4,4, x+barsize-4, y);
		blitter.DrawBar(x, y+4, barsize-1, h-7, guibits.GetPixel(sdx+114+so,sdy+5+11));
		DrawGUIb(sdx+109+so,7+sdy+11,4,3, x, y+h-3);
		DrawGUIr(sdx+114+so,7+sdy+11,1,3, x+4, y+h-3, barsize-8, 0);
		DrawGUIb(sdx+120+so-4,sdy+7+11,4,3, x+barsize-4, y+h-3);

		if(b)
		{
			x-=1;
			barsize+=1;
		}

		bool all_selected=false;
		if(song->selected_sound==-1 && song->selected_st==-1 && song->selected_ch==-1)
			all_selected=true;
		for(int s=0;s<2;s++)
		{
			if(all_selected)
				s=1;
			else if(s==1 && song->selected_ch==-1)
				continue;
			for(int j=0;j<100;j++)
			{
				NMChannel* ch=song->GetCH(j);
				if(ch==NULL)
					continue;
				if(!all_selected)
				{
					if(s==0 && song->selected_ch==j)
						continue;
					if(s==1 && song->selected_ch!=j)
						continue;
				}

				int avgy=(int)ch->song_cache[i];
				if(avgy==0xFF || ch->song_cache_dirty || all_song_cache_dirty)
				{
					avgy=0;
					int avgc=0;
					int t0=i*stepsperbeat;
					int t1=(i+1)*stepsperbeat;
	/*				int rt=t0;
					while(rt<t1)
					{
						GraphPoint gp=ch->content->Read(rt);
						if(gp.trigger!=0xFF)
						{
							avgy+=gp.trigger;
							avgc++;
						}
						if(gp.curve!=0xFF)
						{
							avgy+=gp.curve;
							avgc++;
						}
						rt=ch->content->FindNext(rt);
						if(rt==-1)
							break;
					}*/
					for(int t=t0;t<t1;t++)
					{
						GraphPoint gp=ch->content->Read(t);
						if(gp.trigger!=0xFF)
						{
							avgy+=gp.trigger;
							avgc++;
						}
						if(gp.curve!=0xFF)
						{
							avgy+=gp.curve;
							avgc++;
						}
					}
					if(avgc==0)
					{
						ch->song_cache[i]=0xFE;
						continue;
					}

					avgy/=avgc;
					avgy-=5;
					avgy=avgy*barheight1/100;
					if(avgy<0) avgy=0;
					if(avgy>barheight1-8)
						avgy=barheight1-8;

					ch->song_cache[i]=(unsigned char)avgy;
				}
				if(avgy==0xFE)
					continue;

				int srx=0;
				int sry=0;
				if(s==1)
					sry+=4;
				//int colormap[]={2,0,1};
				int colormap[]={0,1,3,4,2};
				srx=colormap[ch->color]*10;
				DrawGUIb(73+srx,1+sry,9,3, x, y0+barheight0+barheight1-5-avgy);
			}
		}
		for(int s=0;s<2;s++)
		{
			if(all_selected)
				s=1;
			else if(s==1 && song->selected_st==-1)
				continue;
			for(int j=0;j<20;j++)
			{
				NMChannel* st=song->GetST(j);
				if(st==NULL)
					continue;
				if(!all_selected)
				{
					if(s==0 && song->selected_st==j)
						continue;
					if(s==1 && song->selected_st!=j)
						continue;
				}

				int avgy=(int)st->song_cache[i];
				if(avgy==0xFF || st->song_cache_dirty || all_song_cache_dirty)
				{
					avgy=0;
					int avgc=0;
					int t0=i*stepsperbeat;
					int t1=(i+1)*stepsperbeat;
	/*				int rt=t0;
					while(rt<t1)
					{
						GraphPoint gp=st->content->Read(rt);
						if(gp.trigger!=0xFF)
						{
							avgy+=j;
							avgc++;
						}
						rt=st->content->FindNext(rt);
						if(rt==-1)
							break;
					}*/
					for(int t=t0;t<t1;t++)
					{
						GraphPoint gp=st->content->Read(t);
						if(gp.trigger!=0xFF)
						{
							avgy+=j;
							avgc++;
						}
					}
					if(avgc==0)
					{
						st->song_cache[i]=0xFE;
						continue;
					}

					avgy=avgy*18/10/avgc;

					st->song_cache[i]=(unsigned char)avgy;
				}
				if(avgy==0xFE)
					continue;

				int srx=0;
				int sry=0;
				if(s==1)
					sry+=4;
				//int colormap[]={2,0,1};
				int colormap[]={0,1,3,4,2};
				srx=colormap[st->color]*10;
				DrawGUIb(73+srx,1+sry,9,3, x, y0+1+avgy);
			}
		}
	}

	for(int j=0;j<100;j++)
	{
		NMChannel* ch=song->GetCH(j);
		if(ch==NULL)
			continue;
		ch->song_cache_dirty=false;
	}
	for(int j=0;j<20;j++)
	{
		NMChannel* st=song->GetST(j);
		if(st==NULL)
			continue;
		st->song_cache_dirty=false;
	}
	all_song_cache_dirty=false;

	int viewstep=timeline_gridstep_x*stepsperbeat;
	if(!timeline_zoomed)
		viewstep/=timeline_substeps;
	int svx=song->gui_scroll_x*10/viewstep;
	blitter.DrawBox(gui_main_x2+svx, gui_main_y0+35, (gui_main_x3-gui_main_x1)*10/viewstep, 101, guibits.GetPixel(96, 14));
	blitter.DrawBox(gui_main_x2+svx, gui_main_y0+35+1, (gui_main_x3-gui_main_x1)*10/viewstep, 99, guibits.GetPixel(96, 14));



	// TODO: collaborate with selection/grapheditor to prevent doing both with the same click
	if(song_setscrollpos)
	{
		song->gui_scroll_x=(mouse.x+song_scroll_x-gui_main_x2)*viewstep/10-(gui_main_x3-gui_main_x1)/2;
		song_setscrollpos=false;
	}



	static int drag_item=-1;
	static int drag_m0=-1;
	static int drag_dir=0;
	if(mouse.dx>0) drag_dir=1;
	if(mouse.dx<0) drag_dir=-1;
	static bool clicked=false;
	if(song_editor.view.Contains(mousepoint) && !song_editor.region.Contains(mousepoint) && mousepoint.y<gui_main_y0+40 && !prevent_edit)
	{
		if(mouse.lclick)
			clicked=true;
	}
	//int timestep=stepsperbeat;
	const int mousemark=((drag_item!=-1?mousepoint.x:clickpoint.x)+song_scroll_x-gui_main_x2+5)/10;
	if(mouse.left && clicked && mousepoint.DistanceTo(clickpoint)>=5)
	{
		clicked=false;
		drag_item=3;
		drag_m0=mousemark;
		if(drag_m0<0) drag_m0=0;
		if(mousemark==part->mark_start)
			drag_item=0;
		if(mousemark==part->mark_end)
			drag_item=1;
		if(mousemark==part->mark_loop && (drag_item==-1 || clickpoint.y>gui_main_y0+20))
			drag_item=2;
	}
	if(!mouse.left)
	{
		clicked=false;
		drag_item=-1;
	}
	if(drag_item==0)
	{
		part->mark_start=mousemark;
		if(part->mark_start<0)
			part->mark_start=0;
		if(part->mark_start>=part->mark_end)
			part->mark_end=part->mark_start+1;

		song_modified=true;
	}
	if(drag_item==1)
	{
		part->mark_end=mousemark;
		if(part->mark_end<1)
			part->mark_end=1;
		if(part->mark_end<=part->mark_loop)
			part->mark_loop=part->mark_end-1;
		if(part->mark_start>=part->mark_end)
			part->mark_start=part->mark_end-1;

		song_modified=true;
	}
	if(drag_item==2)
	{
		part->mark_loop=mousemark;
		if(part->mark_loop<0)
			part->mark_loop=0;
		if(part->mark_loop>=part->mark_end)
			part->mark_end=part->mark_loop+1;

		song_modified=true;
	}

	if(drag_item==3)
	{
		part->mark_loop=drag_m0;
		part->mark_end=mousemark;
		if(part->mark_loop>part->mark_end)
		{
			int t=part->mark_loop;
			part->mark_loop=part->mark_end;
			part->mark_end=t;
		}
		if(part->mark_end==part->mark_loop)
		{
			if(drag_dir==1)
				part->mark_end+=1;
			else
				part->mark_loop-=1;
		}
		if(part->mark_loop<0)
		{
			part->mark_loop=0;
			if(part->mark_end==0)
				part->mark_end=1;
		}

		song_modified=true;
	}

	DrawGUIb(62,4,9,21, gui_main_x2+part->mark_start*barsize-4, gui_main_y0+35-21);
	if(part->mark_end>part->mark_loop)
	{
		blitter.DrawBar(gui_main_x2+part->mark_loop*barsize+5, gui_main_y0+35-10, (part->mark_end-part->mark_loop)*barsize-10, 1, guibits.GetPixel(82,15));
		blitter.DrawBar(gui_main_x2+part->mark_loop*barsize+5, gui_main_y0+35-10+1, (part->mark_end-part->mark_loop)*barsize-10, 1, guibits.GetPixel(82,16));
		blitter.DrawBar(gui_main_x2+part->mark_loop*barsize+5, gui_main_y0+35-10-1, (part->mark_end-part->mark_loop)*barsize-10, 1, guibits.GetPixel(82,16));
	}
	DrawGUIb(74,13,6,12, gui_main_x2+part->mark_loop*barsize-1, gui_main_y0+35-12);
	DrawGUIb(87,13,6,12, gui_main_x2+part->mark_end*barsize-5, gui_main_y0+35-12);

	if(songplay_mode!=0)
		DrawGUIb(95,22,7,4, gui_main_x2+nemus->songplayer.GetCurPos()*barsize/stepsperbeat-3, gui_main_y0+140);


	GraphEditor(EPM_SONG);


	blitter.DisableClipping();
	blitter.SetScroll(0, 0);


	if(!(song->selected_sound==-1 && song->selected_st==-1 && song->selected_ch==-1))
		return;


	selected_panel=-1;


	ButtonResult br;

	if(show_settings)
	{
		PrintMenu(0, 0, 0, "SETTINGS");

		ButtonColor(1);
		static unsigned int save_anim_t0=0;
		if(save_anim_t0!=0)
		{
			unsigned int t=glrAppTimeMs()-save_anim_t0;
			if(t/50%2==0)
				ButtonColor(1);
			else
				ButtonColor(0);
			if(t>=500)
				save_anim_t0=0;
		}
		br=MenuButton(1011, 27, 0, "SAVE");
		if(br.clicked)
		{
			song->WriteSettings();
			save_anim_t0=glrAppTimeMs();
		}

		ButtonColor(1);

		PrintMenu(0, 1, 5, "DRIVER");
		br=MenuButton(1012, 8, 5, "%s", nmas_drivers[song->settings_audio_driver]);
		if(nmas_drivers_count>1 && br.ClickTick(song->settings_audio_driver, nmas_drivers_count))
			UpdateAudioSettings();

		PrintMenu(0, 1, 6, "RATE");
		MenuButton(1017, 8, 6, "%i HZ", SAMPLE_RATE);

#ifdef _WIN32
		if(song->settings_audio_driver==0)
		{
			PrintMenu(0, 1, 7, "BUFFER      TIMING");
			br=MenuButton(1013, 8, 7, "%i", song->settings_audio_buffer*100);
			song->settings_audio_buffer+=br.delta;
			song->settings_audio_buffer=std::min(std::max(song->settings_audio_buffer, 5), 50);
			if(br.released)
				UpdateAudioSettings();

			br=MenuButton(1014, 20, 7, "%i", song->settings_audio_timing*10);
			song->settings_audio_timing+=br.delta;
			song->settings_audio_timing=std::min(std::max(song->settings_audio_timing, 0), 80);
			if(br.released)
				UpdateAudioSettings();
		}
		if(song->settings_audio_driver==1)
		{
			int asio_buffersizes[]={256, 512, 1024, 2048};
			PrintMenu(0, 1, 7, "BUFFER");
			song->settings_audio_buffer=std::min(std::max(song->settings_audio_buffer, 0), 3);
			br=MenuButton(1013, 8, 7, "%i", asio_buffersizes[song->settings_audio_buffer]);
			song->settings_audio_buffer+=br.delta;
			song->settings_audio_buffer=std::min(std::max(song->settings_audio_buffer, 0), 3);
			if(br.released)
				UpdateAudioSettings();
		}

#else

		song->settings_audio_driver=0;
		PrintMenu(0, 1, 7, "BUFFER");
		br=MenuButton(1013, 8, 7, "%i", song->settings_audio_buffer*100);
		song->settings_audio_buffer+=br.delta;
		song->settings_audio_buffer=std::min(std::max(song->settings_audio_buffer, 5), 50);
		if(br.released)
			UpdateAudioSettings();

#endif

		PrintMenu(0, 1, 2, "LOW POWER MODE");
		ButtonColor(1);
		br=MenuButton(1015, 1+15, 2, "%s", song->settings_lowpower?"ON":"OFF");
		br.ClickTick(song->settings_lowpower, 2);
		PrintMenu(0, 1, 3, "ALT MOUSE BUTTONS");
		ButtonColor(1);
		br=MenuButton(1016, 1+18, 3, "%s", song->settings_altbuttons?"ON":"OFF");
		br.ClickTick(song->settings_altbuttons, 2);

		PrintMenu(0, 1, 9, "GAMMA");
		br=MenuButton(1019, 8, 9, "%i.%02i", song->settings_gamma/100, song->settings_gamma%100);
		song->settings_gamma+=br.delta;
		song->settings_gamma=std::min(std::max(song->settings_gamma, 50), 150);
		if(br.delta)
			UpdateDisplaySettings();

		PrintMenu(0, 1, 11, "VERSION " VER_DISPLAY);
	}
	else
	{
		ButtonColor(1);
		br=MenuButton(1000, 22, 0, "SAVE");
		if(br.clicked)
			ExecuteFileCommand(FST_SONG_SAVE);

		br=MenuButton(1001, 27, 0, "OPEN");
		if(br.clicked)
			ExecuteFileCommand(FST_SONG_OPEN);

		ButtonColor((1+part->color)%5);

		int last_part=0;
		for(;last_part<PARTS_MAX-1;last_part++)
			if(!song->song_parts[last_part+1])
				break;
		PrintMenu(0, 0, 0, "SONG    / %02i ( / )", last_part);
		br=MenuButton(1002, 4, 0, " %02i", song->cur_part);
		if(br.delta)
		{
			if(song->SelectSongPart(std::max(0, std::min(last_part, song->cur_part+br.delta))))
			{
				part=song->part;
				song_modified=true;
			}
		}

		br=MenuButton(1003, 14, 0, "+");
		if(br.clicked && song->cur_part<PARTS_MAX-1)
		{
			for(int i=PARTS_MAX-2;i>song->cur_part;i--)
				song->song_parts[i+1].reset(song->song_parts[i].release());
			song->song_parts[song->cur_part+1].reset(new NMSongPart());
			song->SelectSongPart(song->cur_part+1);
			part=song->part;
			song_modified=true;
		}

		br=MenuButton(1004, 16, 0, "-");
		if(br.clicked && song->cur_part>0)
		{
			song->SelectSongPart(song->cur_part-1);
			part=song->part;
			for(int i=song->cur_part+1;i<PARTS_MAX-1;i++)
				song->song_parts[i].reset(song->song_parts[i+1].release());
			song_modified=true;
		}

		static const char* colornames[]={"\"A\"", "\"B\"", "\"C\"", "\"D\"", "\"E\"", "?"};
		PrintMenu(0, 1, 2, "COLOR");
		br=MenuButton(1005, 7, 2, colornames[part->color]);
		br.ClickTick(part->color, 5);

		PrintMenu(0, 20, 2, "VOLUME");
		br=MenuButton(1006, 26, 2, " %3i$", part->volume*10);
		part->volume+=br.delta; part->volume=std::min(std::max(part->volume, 0), 10);

		int tempo_rate=35-part->tempo;
		PrintMenu(0, 1, 3, "TEMPO    (%i BPM)", (int)((44100.0/184.0*60)/tempo_rate/(part->timesig2*2)+0.82));
		br=MenuButton(1007, 6, 3, " %02i", part->tempo);
		part->tempo+=br.delta; part->tempo=std::min(std::max(part->tempo, 1), 32);

		part->timesig1=4; // NOTE: tsone: hard-coded 1/4 beat
		PrintMenu(0, 1, 4, "TIME SIGNATURE   / 4");
		br=MenuButton(1008, 15, 4, " %i", part->timesig2);
		const auto prev_timesig2=part->timesig2;
		part->timesig2+=br.delta; part->timesig2=std::min(std::max(part->timesig2, 3), 6);
		if(prev_timesig2!=part->timesig2)
			all_song_cache_dirty=true;

		PrintMenu(0, 1, 5, "SWING");
		br=MenuButton(1020, 1+6, 5, "%3i$", (100*part->swing_amount)/20);
		if(!br.rclicked)
		{
			part->swing_amount+=br.delta;
			part->swing_amount=std::min(std::max(part->swing_amount, 0), 20);
		}
		else
		{
			if(part->swing_amount)
			{
				part->_old_swing_amount=part->swing_amount;
				part->swing_amount=0;
			}
			else
			{
				part->swing_amount=part->_old_swing_amount;
			}
		}

		PrintMenu(0, 1+6+5, 5, "1 /");
		br=MenuButton(1021, 1+6+9, 5, "%2d", (1 << part->swing_time));
		if(!br.rclicked)
		{
			part->swing_time+=br.delta;
			// Do not use <3 as it breaks native mode looping
			part->swing_time=std::min(std::max(part->swing_time, 3), 5);
		}
		else
		{
			part->swing_time=4;
		}

		ButtonColor(1);
		
		PrintMenu(0, 1, 7, "TITLE");
		MenuTextInput(1009, 8, 7, 23, song->title);

		PrintMenu(0, 1, 8, "AUTHOR");
		MenuTextInput(1010, 8, 8, 23, song->author);

		PrintMenu(0, 1, 9, "NATIVE MODE");
		br=MenuButton(1011, 13, 9, "%s", native_mode?"ON":"OFF");
		br.ClickTick(native_mode, 2);

		PrintMenu(0, 1, 11, "EXPORT");
		br=MenuButton(1012, 1+7, 11, "NSF");
		if(br.clicked)
			ExecuteFileCommand(FST_NSF);
		br=MenuButton(1013, 1+7+4, 11, "WAV");
		if(br.clicked)
			ExecuteFileCommand(FST_WAV);
		br=MenuButton(1014, 1+7+4+4, 11, "ASM");
		if(br.clicked)
			ExecuteFileCommand(FST_ASSEMBLY);
	}

#if DEBUG_SWEEP
	PrintMenu(0, 1+7+15, 12, "%.2f", nemus->apu.sweep_freq);
#endif
#if DEBUG_OUTPUT_TIMING
	PrintMenu(0, 1+7+15, 12, "%.2f", nemus->apu._output_time_eavg);
#endif
}

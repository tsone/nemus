#import <CoreMIDI/CoreMIDI.h>
#include "nmidi.h"

static MIDIClientRef client;
static MIDIPortRef input_port;
static MIDIEndpointRef connected_endpoint;

static void NMIDI_Reconnect(unsigned idx)
{
	MIDIEndpointRef endpoint=(idx<MIDIGetNumberOfSources()?MIDIGetSource(idx):0);
	if(endpoint==connected_endpoint)
		return;
	if(connected_endpoint)
		MIDIPortDisconnectSource(input_port, connected_endpoint);
	connected_endpoint=endpoint;
	if(endpoint)
		MIDIPortConnectSource(input_port, endpoint, NULL);
}

static void NMIDI_Notify(const MIDINotification* message, void* refcon)
{
	(void)message;
	NMIDI_Reconnect((unsigned)refcon);
}

static void NMIDI_Read(const MIDIPacketList* packets, void* read_refcon, void* proc_refcon)
{
	(void)proc_refcon;
	const MIDIPacket *packet=packets->packet;
	for(unsigned i=0;i<packets->numPackets;i++)
	{
		NMIDIPacketHandler handler=(NMIDIPacketHandler)read_refcon;
		handler(packet->data, packet->length);
		packet=MIDIPacketNext(packet);
	}
}

unsigned NMIDI_Init(unsigned idx, NMIDIPacketHandler handler)
{
	if(MIDIClientCreate((CFStringRef)@"nemus MIDI Client", NMIDI_Notify, (void*)(unsigned long)idx, &client)!=noErr)
		return 0;
	if(MIDIInputPortCreate(client, (CFStringRef)@"nemus MIDI Input Port", NMIDI_Read, handler, &input_port)!=noErr)
	{
		MIDIClientDispose(client);
		return 0;
	}
	NMIDI_Reconnect(idx);
	return 1;
}

void NMIDI_Exit(void)
{
	MIDIPortDisconnectSource(input_port, connected_endpoint);
	MIDIPortDispose(input_port);
	MIDIClientDispose(client);
}



#include "nmas.h"
#include <AudioToolbox/AudioToolbox.h>

#define BUFFERS_NUM 3

static AudioStreamBasicDescription _desc;
static AudioDeviceID _dev_id;
static AudioQueueRef _audio_queue;
static AudioQueueBufferRef _audio_buffers[BUFFERS_NUM];
static unsigned _buffer_samples;
static unsigned _buffer_bytes;

static void OutputCallback(void* user_data, AudioQueueRef in_audio_queue, AudioQueueBufferRef in_buffer)
{
	(void)user_data;
	(void)in_audio_queue;

	NMAS_Callback(in_buffer->mAudioData, _buffer_samples);
	in_buffer->mAudioDataByteSize=_buffer_bytes;
	AudioQueueEnqueueBuffer(_audio_queue, in_buffer, 0, 0);
}

static void CloseAudio(void)
{
	AudioQueueDispose(_audio_queue, 1);
	_audio_queue=nil;
}

static int ReopenAudio(int samplerate, int buffersize)
{
	AudioStreamBasicDescription* desc=&_desc;
	AudioDeviceID* dev_id=&_dev_id;
	OSStatus res;
	UInt32 size=0;
	UInt32 alive=0;
	pid_t pid=0;

	if(_audio_queue)
		CloseAudio();

	memset(desc, 0, sizeof(*desc));
	desc->mSampleRate=samplerate;
	desc->mFormatID=kAudioFormatLinearPCM;
	desc->mFormatFlags=kAudioFormatFlagIsPacked|kLinearPCMFormatFlagIsPacked;
	desc->mChannelsPerFrame=2;
	desc->mFramesPerPacket=1;
	desc->mBitsPerChannel=32;
	desc->mFormatFlags=kAudioFormatFlagIsFloat|kLinearPCMFormatFlagIsFloat;
	desc->mBytesPerFrame=desc->mBitsPerChannel*desc->mChannelsPerFrame/8;
	desc->mBytesPerPacket=desc->mBytesPerFrame*desc->mFramesPerPacket;

	_buffer_samples=buffersize;
	_buffer_bytes=desc->mBytesPerFrame*buffersize;

	AudioObjectPropertyAddress addr=
	{
		0,
		kAudioObjectPropertyScopeGlobal,
		kAudioObjectPropertyElementMaster
	};

	// Get default device id
	size=sizeof(AudioDeviceID);
	addr.mSelector=kAudioHardwarePropertyDefaultOutputDevice;
	res=AudioObjectGetPropertyData(kAudioObjectSystemObject, &addr, 0, NULL, &size, dev_id);
	if(res!=noErr)
		return res;

	// Verify device is alive
	addr.mSelector=kAudioDevicePropertyDeviceIsAlive;
	addr.mScope=kAudioDevicePropertyScopeOutput;
	size=sizeof(alive);
	res=AudioObjectGetPropertyData(*dev_id, &addr, 0, NULL, &size, &alive);
	if(res!=noErr || !alive)
		return res;

	// Get process id of exclusive owner (if axists)
	addr.mSelector=kAudioDevicePropertyHogMode;
	size=sizeof(pid);
	res=AudioObjectGetPropertyData(*dev_id, &addr, 0, NULL, &size, &pid);
	if((res==noErr) && (pid!=-1))
		return res;

	// Not sure why but this changes queue to curretnt output device
	addr.mSelector=kAudioHardwarePropertyRunLoop;
	addr.mScope=kAudioObjectPropertyScopeGlobal;
	CFRunLoopRef run_loop=NULL;
	size=sizeof(CFRunLoopRef);
	AudioObjectSetPropertyData(kAudioObjectSystemObject, &addr, 0, NULL, size, &run_loop);

	// Create queues
	res=AudioQueueNewOutput(desc, OutputCallback, NULL, NULL, NULL, 0, &_audio_queue);
	if(res!=noErr)
		return res;

	for(int i=0;i<BUFFERS_NUM;i++)
	{
		res=AudioQueueAllocateBuffer(_audio_queue, _buffer_bytes, &_audio_buffers[i]);
		if(res!=noErr)
			return res;

		memset(_audio_buffers[i]->mAudioData, 0, _audio_buffers[i]->mAudioDataBytesCapacity);
		_audio_buffers[i]->mAudioDataByteSize=_audio_buffers[i]->mAudioDataBytesCapacity;
		res=AudioQueueEnqueueBuffer(_audio_queue, _audio_buffers[i], 0, NULL);
		if(res!=noErr)
			return res;
	}

	res=AudioQueueStart(_audio_queue, NULL);
	if(res!=noErr)
		return res;

	return res;
}

int NMAS_OpenCoreAudio(int samplerate, int buffersize, int timingbias)
{
	(void)timingbias;
	return (ReopenAudio(samplerate, buffersize)==noErr);
}

void NMAS_CloseCoreAudio(void)
{
	CloseAudio();
}

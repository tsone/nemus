Image font;

int fontmap[256];
int fontbase;
int font_alpha=255;

void InitFont()
{
	fontbase=0;
	for(char c='A';c<='Z';c++)
		fontmap[c]=c-'A';
	for(char c='a';c<='z';c++)
		fontmap[c]=c-'a';
	for(char c='0';c<='9';c++)
		fontmap[c]=26+c-'0';
	const char* symbols=",.;:=+-()/'\"#$";
	for(unsigned int i=0;i<strlen(symbols);i++)
		fontmap[symbols[i]]=36+i;
}

void SelectFont(int i)
{
	fontbase=i;
}

void FontAlpha(int a)
{
	font_alpha=a;
}

void DrawStrChar(int x, int y, char c)
{
	if(c==' ')
		return;
	int ci=fontmap[c]+fontbase*26*3;
	Sprite s;
	s.colorkey=0x000000;
	s.source=font;
	s.region.SetXYWH(ci%26*9, ci/26*9, 9, 9);
	s.dx=x;
	s.dy=y;
	s.layer=-1;
	s.alpha=font_alpha;
	blitter.DrawSprite(s);
}

void DrawStringC(int x, int y, const char* str)
{
	auto l=strlen(str);
	for(int i=0;i<(int)l;i++)
	{
		DrawStrChar(x, y, str[i]);
		x+=9;
	}
}

#include <stdarg.h>
void DrawString(int x, int y, const char* str, ...)
{
	va_list args;
	va_start(args, str);
	char text[2048];
	vsprintf(text, str, args);
	DrawStringC(x, y, text);
	va_end(args);
}

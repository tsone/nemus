
/*

	BUGS

	- Crash when moving selected section in song editor (mm2 example, tricky to reproduce?)
	- Space right after startup crashed (but not now?? seemed reliable, try in old version maybe)
	x right-click to deselect in ST view, should work like CH and song
	x creating note on 8th beat (start of second bar) doesn't work on startup, block boundary?
	  similarly on save/open song
	x Delete not removing content in song editor (graphs missing?)
	x odd song selection pastes with wrong offset (due to half width, quantize to beats)

*/


#define ENABLE_MIDI_IO

#define COLORKEY 0x400000

struct Mouse
{
	int x;
	int y;
	int dx;
	int dy;
	bool left;
	bool right;
	bool lclick;
	bool rclick;
	int clickx;
	int clicky;
};
Mouse mouse;

bool alt_down=false;
bool ctrl_down=false;
bool shift_down=false;


#include "fileselector/fileselector.h"
void ResetPath();


bool prevent_edit=false;
bool prevent_navigation=false;
bool editor_helpscreen=false;

bool fullscreen=false;


int timeline_gridstep_x=12;
int timeline_gridstep_y=15;

unsigned int worktime_now=0; // Seconds
unsigned int worktime_end=0; // Seconds

static inline void WorktimeActive()
{
	worktime_end=worktime_now+5;
}

bool timeline_zoomed=false;


int envwidth=12;
int envheight=7;

int songeditor_barsize=10;
bool all_song_cache_dirty=true;

bool vis_noteplaying[120];
bool vis_stplaying[20];

#include "nmutil.h"
#include "nmsong.h"
#include "nmaudio.h"

struct KeyJazz
{
	struct Key
	{
		glrKey key;
		int note;
	};

	struct Channel
	{
		bool active=false;
		int note=0;
		NMAMultiSoundPlayer msp;
		glrKey key=GLR_KEY_ANY;
		int midikey=0;
		Channel(NMASoundStack& stack) : msp(stack)
		{
		}
	};

	std::vector<Channel> keyjazz;

	int polyphony=2;
	int curchannel=0;
	std::array<Key, 50> keymap={};
	int keymap_num=0;
	std::array<Key, 50> keymap_st={};
	int keymap_st_num=0;

	int external_keyjazz=-1;
	int external_keyjazz_triggered=-1;

	bool midi_activity=false;
	std::array<int, 120> midi_keys={};

	KeyJazz(NMASoundStack& stack)
	{
		constexpr unsigned size=8;
		keyjazz.reserve(size);
		for(unsigned i=0;i<size;i++)
			keyjazz.emplace_back(stack);
	}

	int GetOctave() const { return octave; }
	void SetOctave(int octave_)
	{
		octave=std::max(0, std::min(9, octave_));
	}

	void MIDI_Init()
	{
		std::fill(midi_keys.begin(), midi_keys.end(), 0);
	}
	void TriggerMIDI(int key)
	{
		midi_activity=true;
		midi_keys[key]=2;
	}
	void ReleaseMIDI(int key)
	{
		midi_activity=true;
		midi_keys[key]=0;
	}
	bool MIDI_Hit(int key)
	{
		if(midi_keys[key]==2)
		{
			midi_keys[key]=1;
			return true;
		}
		return false;
	}
	bool MIDI_Pressed(int key)
	{
		return midi_keys[key]!=0;
	}
	void Init();
	void Trigger(glrKey key, int midikey, int note, int numsounds, int* snd_idxs, int chst);
	void ExternalTrigger(int note);
	void ExternalRelease();
	void Update(int numsounds, int* snd_idxs, int chst);

private:
	int octave=0;
};

struct NemusState
{
	NMAPU apu;
	NMASoundStack stack;
	NMASongPlayer songplayer;
	KeyJazz keyjazz;
	NMAOutputGenerator output;

	NemusState() : songplayer(stack), keyjazz(stack)
	{
		keyjazz.Init();
	}
};

std::unique_ptr<NemusState> nemus;

std::unique_ptr<NMSong> song;

void NMAudioBuffer(float* output, unsigned buffersize)
{
	AcquireAudioLock();

	ClearAudioLights();

	for(unsigned i=0;i<recent_audio.size();i++)
	{
		recent_audio[i].level_min=1e12;
		recent_audio[i].level_max=-1e12;
	}

	if(nemus && !nemus->songplayer.IsRecording())
	{
		const auto output_func=[&output](float value)
		{
			recent_mix_energy.Update(value*value);

			recent_mix[recent_mix_i]=value;
			recent_mix_i=(recent_mix_i+1)%recent_mix.size();

			*output++=value;
			*output++=value;
		};
		nemus->output.SetVolume(master_volume);
		nemus->output.Output(output_func, buffersize, song.get(), nemus->songplayer, nemus->apu);
	}
	else
	{
		const auto l=std::min(buffersize, (unsigned)recent_mix.size());
		std::fill(output, output+2*l, 0.0f);
		std::fill(recent_mix.begin(), recent_mix.begin()+l, 0.0f);
	}

	ReleaseAudioLock();
}

MultiGraph* editgraphs;

enum EditorProfileMode : char
{
	EPM_CH,
	EPM_ST,
	EPM_CHENV,
	EPM_VENV,
	EPM_PENV,
	EPM_SONG,
	//
	EPM_COUNT
};

struct EditorProfile
{
	EditorProfileMode mode; // basically special path for various things (ST: disallow curve points, use y to pick channel, disallow any vertical move, use rectangular selection)
	MultiGraph graphs;
	Box region;
	Box view;
	int max_x;
	int max_y;
	int scroll_x;
	int scroll_y;
	int grid_x0;
	int grid_y0;
	int gridstep_x;
	int gridstep_y;
	int quantize_x;
};
EditorProfile ch_editor={};
EditorProfile st_editor={};
EditorProfile chenv_editor={};
EditorProfile venv_editor={};
EditorProfile penv_editor={};
EditorProfile song_editor={};

EditorProfile* current_editor;

EditorProfile* editorprofiles[6];

int channel_cursor_t=0;
int channel_cursor_note=0;

bool selection_active=false;
bool selection_deactivated=false;
int selection_editorid=-1;
int selection_x0=0;
int selection_x1=0;
int selection_y0=0;
int selection_y1=0;
bool selection_moving=false;
int selection_axis=0;
int selection_transpose_y=-1;
int selection_grabx=0;
MultiGraph selection_target;
unsigned short* selection_target_scalemap=NULL;
MultiGraph selection_prev;
unsigned short* selection_prev_scalemap=NULL;
int selection_px0=0;
int selection_px1=0;

MultiGraph clipboard_buffer;
unsigned short* clipboard_buffer_scalemap=NULL;
int clipboard_size=0;
int clipboard_y0=-1;
int clipboard_y1=-1;
bool clipboard_exists=false;
int clipboard_editorid=-1;
bool clipboard_scalemap=false;

int recent_sound=-1;
int recent_chst=-1;

NMChannel* preview_hover_channel=NULL;
int preview_hover_sound=-1;

void ClearAudioLights()
{
	for(int i=0;i<SOUNDS_MAX;i++)
	{
		NMSound* sound=song->GetSound(i);
		if(sound!=NULL)
			sound->audio_light=0;
	}
}

int songplay_mode=0;

bool song_saved=false;
char song_filename[512];

void UpdateTitle()
{
	char displayname[128];
	strcpy(displayname, "Untitled");
	if(song_filename[0]!='\0')
	{
		auto *p=get_basename(song_filename);
		if(p)
			strcpy(displayname, p);
	}
	char tstr[512];
	sprintf(tstr, "nemus - %s%s", displayname, song_saved?"":" *");
	glrSetWindowTitle(tstr);
}

void UpdateDisplaySettings()
{
	guibits.CopyFrom(guibits_orig);
	font.CopyFrom(font_orig);

	guibits.GammaTransform(song->settings_gamma/100.0, COLORKEY);
	font.GammaTransform(song->settings_gamma/100.0, COLORKEY);

	BuildGridCache();
}

void UpdateAudioSettings()
{
	NMAS_Close(false);
	if(song->settings_audio_driver==0)
	{
		// Also used by CoreAudio and SDL
		int bs=song->settings_audio_buffer;
		bs=std::min(std::max(bs, 5), 50);
		NMAS_Open(SAMPLE_RATE, bs*100, song->settings_audio_timing*10, false);
	}
	if(song->settings_audio_driver==1)
	{
		int asio_buffersizes[]={256, 512, 1024, 2048};
		int bsi=song->settings_audio_buffer;
		bsi=std::min(std::max(bsi, 0), 3);
		NMAS_Open(SAMPLE_RATE, asio_buffersizes[bsi], song->settings_audio_timing*10, true);
	}
}


void SaveSong()
{
	FILE* file=fopen(song_filename, "wb");
	if(!file)
		return;
	unsigned char* buffer=NULL;
	long size=0;

	auto fnl=strlen(song_filename); char fnext[4];
	strcpy(fnext, song_filename+fnl-3);
	for(int i=0;i<3;i++) if(fnext[i]>='a' && fnext[i]<='z') fnext[i]+='A'-'a';

	if(strcmp(fnext, "NMC")==0)
		song->WriteCompact(&buffer, &size);
	else
		song->Write(&buffer, &size);
	fwrite(buffer, size, 1, file);
	fclose(file);
	delete[] buffer;

	song_saved=true;
}

void EditorResetGraphs()
{
	for(int i=0;i<EPM_COUNT;i++)
		editorprofiles[i]->graphs.Reset();

	editgraphs->Reset();

	all_song_cache_dirty=true;

	preview_hover_channel=nullptr;
}

void ClearSong(NMSong *new_song)
{
	AcquireAudioLock();

	nemus.reset(new NemusState());
	songplay_mode=0;

	EditorResetGraphs();

	recent_chst=-1;

	song.reset(new_song?new_song:new NMSong());

	ReleaseAudioLock();
}

void OpenSong()
{
	auto buffer=ReadFile(song_filename);
	if(buffer.empty())
		return;

	auto fnl=strlen(song_filename); char fnext[4];
	strcpy(fnext, song_filename+fnl-3);
	for(int i=0;i<3;i++) if(fnext[i]>='a' && fnext[i]<='z') fnext[i]+='A'-'a';

	auto opened_song=new NMSong();
	if(strcmp(fnext, "NMC")==0)
		opened_song->ReadCompact(buffer.data(), (long)buffer.size());
	else
		if(!opened_song->Read(buffer.data(), (long)buffer.size()))
			return;

	ClearSong(opened_song);

	UpdateAudioSettings();
	UpdateDisplaySettings();

	song_saved=true;
}

const int MAX_UNDO=128;
unsigned char* undobuffer_data[MAX_UNDO];
long undobuffer_size[MAX_UNDO];
int undo_i;
int undo_n;
int redo_n;
int undo_latest;
bool song_modified;
void ResetUndo()
{
	for(int i=0;i<MAX_UNDO;i++)
	{
		delete[] undobuffer_data[i];
		undobuffer_data[i]=NULL;
	}
	undo_i=0;
	undo_n=-1;
	redo_n=0;
	undo_latest=-1;
	song_modified=true;
}
void StoreUndo()
{
	if(!song_modified)
		return;
	song_modified=false;
	if(undo_i==undo_latest)
		undo_i=(undo_i+1)%MAX_UNDO;
	delete[] undobuffer_data[undo_i];
	song->Write(&undobuffer_data[undo_i], &undobuffer_size[undo_i]);
	undo_latest=undo_i;
	undo_i=(undo_i+1)%MAX_UNDO;
	undo_n++;
	if(undo_n>MAX_UNDO-2) undo_n=MAX_UNDO-2;
	redo_n=0;
}
void Undo()
{
	if(undo_n==0)
		return;
	delete[] undobuffer_data[undo_i];
	song->Write(&undobuffer_data[undo_i], &undobuffer_size[undo_i]);
	undo_i=(undo_i-1+MAX_UNDO)%MAX_UNDO;
	if(undo_i==undo_latest)
		undo_i=(undo_i-1+MAX_UNDO)%MAX_UNDO;
	undo_n--;
	redo_n++;
	AcquireAudioLock();
	const auto worktimer_memo=song->worktimer_seconds;
	song->Read(undobuffer_data[undo_i], undobuffer_size[undo_i]);
	song->worktimer_seconds=worktimer_memo;
	ReleaseAudioLock();
	undo_latest=undo_i;
	EditorResetGraphs();
}
void Redo()
{
	if(redo_n==0)
		return;
	undo_i=(undo_i+1)%MAX_UNDO;
	undo_n++;
	redo_n--;
	AcquireAudioLock();
	const auto worktimer_memo=song->worktimer_seconds;
	song->Read(undobuffer_data[undo_i], undobuffer_size[undo_i]);
	song->worktimer_seconds=worktimer_memo;
	ReleaseAudioLock();
	undo_latest=undo_i;
	EditorResetGraphs();
}


void ScalemapRead(unsigned short* buffer, unsigned short* src, int i0, int i1)
{
	const auto* part=song->part;
	i0/=part->timesig2*2;
	i1/=part->timesig2*2;
	for(int i=i0;i<i1;i++)
		buffer[i-i0]=src[i];
}
void ScalemapWrite(unsigned short* buffer, unsigned short* src, int i0, int i1)
{
	const auto* part=song->part;
	i0/=part->timesig2*2;
	i1/=part->timesig2*2;
	for(int i=i0;i<i1;i++)
		src[i]=buffer[i-i0];
}


int graph_recent_x0=-1;
int graph_recent_x1=-1;
int graph_recent_type=-1;
int graph_recent_value0=-1;
int graph_recent_value1=-1;
MapGraph* graph_recent_graph=NULL;

void GraphRecentForget()
{
	graph_recent_x0=-1;
	graph_recent_x1=-1;
	graph_recent_type=-1;
	graph_recent_value0=-1;
	graph_recent_value1=-1;
	graph_recent_graph=NULL;
}

void SelectEditor(EditorProfile& ep)
{
	current_editor=&ep;

	editgraphs->Reset();

	for(int i=0;i<ep.graphs.num;i++)
	{
		current_editor->graphs.graph_source[i]=ep.graphs.graph_source[i];
		editgraphs->AddGraph(ep.graphs.graph_source[i]);
	}

	selection_target.SetSource(editgraphs);
	selection_prev.SetSource(editgraphs);
	clipboard_buffer.SetSource(editgraphs);
}

int notegrid_type[12];
int notegrid_key[12];
char keynames[12];
Image gridcache[4*4+3*5];
Image gridcache_zoomed[4*4+3*5];
void BuildGridCache();

#ifdef ENABLE_MIDI_IO
#include "nmidi.h"
#endif
void KeyJazz::Init()
{
	MIDI_Init();

	Key* km=keymap.data();
	int i=0;
	km[i].key=GLR_KEY_A; km[i].note=-1; i++;
	km[i].key=GLR_KEY_Z; km[i].note=0; i++;
	km[i].key=GLR_KEY_S; km[i].note=1; i++;
	km[i].key=GLR_KEY_X; km[i].note=2; i++;
	km[i].key=GLR_KEY_D; km[i].note=3; i++;
	km[i].key=GLR_KEY_C; km[i].note=4; i++;
	km[i].key=GLR_KEY_F; km[i].note=4; i++;
	km[i].key=GLR_KEY_V; km[i].note=5; i++;
	km[i].key=GLR_KEY_G; km[i].note=6; i++;
	km[i].key=GLR_KEY_B; km[i].note=7; i++;
	km[i].key=GLR_KEY_H; km[i].note=8; i++;
	km[i].key=GLR_KEY_N; km[i].note=9; i++;
	km[i].key=GLR_KEY_J; km[i].note=10; i++;
	km[i].key=GLR_KEY_M; km[i].note=11; i++;
	km[i].key=GLR_KEY_K; km[i].note=11; i++;
	km[i].key=GLR_KEY_COMMA; km[i].note=12; i++;
	km[i].key=GLR_KEY_L; km[i].note=13; i++;
	km[i].key=GLR_KEY_PERIOD; km[i].note=14; i++;
	km[i].key=GLR_KEY_SEMICOLON; km[i].note=15; i++;
	km[i].key=GLR_KEY_MINUS; km[i].note=16; i++;

	km[i].key=GLR_KEY_1; km[i].note=12-1; i++;
	km[i].key=GLR_KEY_Q; km[i].note=12+0; i++;
	km[i].key=GLR_KEY_2; km[i].note=12+1; i++;
	km[i].key=GLR_KEY_W; km[i].note=12+2; i++;
	km[i].key=GLR_KEY_3; km[i].note=12+3; i++;
	km[i].key=GLR_KEY_E; km[i].note=12+4; i++;
	km[i].key=GLR_KEY_4; km[i].note=12+4; i++;
	km[i].key=GLR_KEY_R; km[i].note=12+5; i++;
	km[i].key=GLR_KEY_5; km[i].note=12+6; i++;
	km[i].key=GLR_KEY_T; km[i].note=12+7; i++;
	km[i].key=GLR_KEY_6; km[i].note=12+8; i++;
	km[i].key=GLR_KEY_Y; km[i].note=12+9; i++;
	km[i].key=GLR_KEY_7; km[i].note=12+10; i++;
	km[i].key=GLR_KEY_U; km[i].note=12+11; i++;
	km[i].key=GLR_KEY_8; km[i].note=12+11; i++;
	km[i].key=GLR_KEY_I; km[i].note=12+12; i++;
	km[i].key=GLR_KEY_9; km[i].note=12+13; i++;
	km[i].key=GLR_KEY_O; km[i].note=12+14; i++;
	km[i].key=GLR_KEY_0; km[i].note=12+15; i++;
	km[i].key=GLR_KEY_P; km[i].note=12+16; i++;

	keymap_num=i;

	km=keymap_st.data();
	i=0;
	km[i].key=GLR_KEY_1; km[i].note=0; i++;
	km[i].key=GLR_KEY_2; km[i].note=1; i++;
	km[i].key=GLR_KEY_3; km[i].note=2; i++;
	km[i].key=GLR_KEY_4; km[i].note=3; i++;
	km[i].key=GLR_KEY_5; km[i].note=4; i++;
	km[i].key=GLR_KEY_6; km[i].note=5; i++;
	km[i].key=GLR_KEY_7; km[i].note=6; i++;
	km[i].key=GLR_KEY_8; km[i].note=7; i++;
	km[i].key=GLR_KEY_9; km[i].note=8; i++;

	km[i].key=GLR_KEY_Q; km[i].note=9; i++;
	km[i].key=GLR_KEY_W; km[i].note=10; i++;
	km[i].key=GLR_KEY_E; km[i].note=11; i++;
	km[i].key=GLR_KEY_R; km[i].note=12; i++;
	km[i].key=GLR_KEY_T; km[i].note=13; i++;
	km[i].key=GLR_KEY_Y; km[i].note=14; i++;
	km[i].key=GLR_KEY_U; km[i].note=15; i++;
	km[i].key=GLR_KEY_I; km[i].note=16; i++;
	km[i].key=GLR_KEY_O; km[i].note=17; i++;

	km[i].key=GLR_KEY_A; km[i].note=0; i++;
	km[i].key=GLR_KEY_S; km[i].note=1; i++;
	km[i].key=GLR_KEY_D; km[i].note=2; i++;
	km[i].key=GLR_KEY_F; km[i].note=3; i++;
	km[i].key=GLR_KEY_G; km[i].note=4; i++;
	km[i].key=GLR_KEY_H; km[i].note=5; i++;
	km[i].key=GLR_KEY_J; km[i].note=6; i++;
	km[i].key=GLR_KEY_K; km[i].note=7; i++;
	km[i].key=GLR_KEY_L; km[i].note=8; i++;

	km[i].key=GLR_KEY_Z; km[i].note=9; i++;
	km[i].key=GLR_KEY_X; km[i].note=10; i++;
	km[i].key=GLR_KEY_C; km[i].note=11; i++;
	km[i].key=GLR_KEY_V; km[i].note=12; i++;
	km[i].key=GLR_KEY_B; km[i].note=13; i++;
	km[i].key=GLR_KEY_N; km[i].note=14; i++;
	km[i].key=GLR_KEY_M; km[i].note=15; i++;
	km[i].key=GLR_KEY_COMMA; km[i].note=16; i++;
	km[i].key=GLR_KEY_PERIOD; km[i].note=17; i++;

	keymap_st_num=i;
}

void KeyJazz::Trigger(glrKey key, int midikey, int note, int numsounds, int* snd_idxs, int chst)
{
	// steal a channel (should perhaps pick the oldest, but who cares about keyjazz?)
	bool st_mode=false;
	if(polyphony==-1)
	{
		polyphony=1;
		st_mode=true;
	}
	if(polyphony==1)
		curchannel=0;
	AcquireAudioLock();
	Channel& kj=keyjazz[curchannel];
	kj.active=true;
	kj.key=key;
	kj.midikey=midikey;
	kj.msp.output_alternate_channel=(curchannel==1);

	if(st_mode)
	{
		kj.note=note;
		NMChannel* st=song->GetST(kj.note);
		if(st!=NULL)
			kj.msp.PlaySounds(song.get(), numsounds, snd_idxs, st->basepitch, chst, 15);
	}
	else
	{
		if(midikey!=-1)
			kj.note=note;
		else
			kj.note=octave*12+note;
		kj.msp.PlaySounds(song.get(), numsounds, snd_idxs, kj.note, chst, 15);
	}
	ReleaseAudioLock();

	curchannel=1-curchannel;
}

void KeyJazz::ExternalTrigger(int note)
{
	external_keyjazz=note;
}

void KeyJazz::ExternalRelease()
{
	if(external_keyjazz_triggered==-1)
		return;
	AcquireAudioLock();
	keyjazz[external_keyjazz_triggered].msp.ReleaseSounds();
	keyjazz[external_keyjazz_triggered].active=false;
	ReleaseAudioLock();
	external_keyjazz_triggered=-1;
}

void KeyJazz::Update(int numsounds, int* snd_idxs, int chst)
{
	for(int i=0;i<120;i++)
		vis_noteplaying[i]=false;
	for(int i=0;i<20;i++)
		vis_stplaying[i]=false;

	bool st_mode=false;
	if(current_editor!=NULL && current_editor->mode==EPM_ST)
		st_mode=true;

	for(int i=0;i<keyjazz.size();i++)
		if(keyjazz[i].active && (keyjazz[i].key!=GLR_KEY_ANY || keyjazz[i].midikey!=-1))
		{
			if(st_mode)
				vis_stplaying[keyjazz[i].note]=true;
			else
				vis_noteplaying[keyjazz[i].note]=true;

			AcquireAudioLock();
			bool released=false;
			if(keyjazz[i].midikey==-1 && !glrKeyPressed(keyjazz[i].key))
				released=true;
			if(keyjazz[i].midikey!=-1 && !MIDI_Pressed(keyjazz[i].midikey))
				released=true;
			if(released)
			{
				keyjazz[i].msp.ReleaseSounds();
				keyjazz[i].active=false;
			}
			ReleaseAudioLock();
		}
		else if(keyjazz[i].msp.active)
		{
			AcquireAudioLock();
			keyjazz[i].msp.CheckSounds();
			ReleaseAudioLock();
		}
	if(st_mode)
	{
		if((!ctrl_down && !shift_down && !alt_down) || external_keyjazz!=-1)
		{
			for(int i=0;i<keymap_st_num;i++)
			{
				if(glrKeyHit(keymap_st[i].key) || external_keyjazz==i)
				{
					int st_idx=keymap_st[i].note;
					NMChannel* st=song->GetST(st_idx);
					if(st!=NULL)
					{
						int ns=0;
						int s[SOUNDS_MAX];
						for(int j=0;j<SOUNDS_MAX;j++)
							if(st->sounds[j])
								s[ns++]=j;
						polyphony=-1;
						Trigger(keymap_st[i].key, -1, keymap_st[i].note, ns, s, StChst(st_idx));
					}
				}
			}
			for(int i=0;i<120;i++)
			{
				if(MIDI_Hit(i))
				{
					NMChannel* st=song->GetST(i);
					if(st!=NULL)
					{
						int ns=0;
						int s[SOUNDS_MAX];
						for(int j=0;j<SOUNDS_MAX;j++)
							if(st->sounds[j])
								s[ns++]=j;
						polyphony=-1;
						Trigger(GLR_KEY_ANY, i, i, ns, s, StChst(i));
					}
				}
			}
		}
		if(external_keyjazz!=-1)
		{
			external_keyjazz=-1;
			external_keyjazz_triggered=0;
		}
	}
	else
	{
		if(!ctrl_down && !shift_down && !alt_down)
		{
			for(int i=0;i<keymap_num;i++)
			{
				if(glrKeyHit(keymap[i].key))
					Trigger(keymap[i].key, -1, keymap[i].note, numsounds, snd_idxs, chst);
			}
			for(int i=0;i<120;i++)
			{
				if(MIDI_Hit(i))
					Trigger(GLR_KEY_ANY, i, i, numsounds, snd_idxs, chst);
			}
		}
		if(external_keyjazz!=-1)
		{
			Trigger(GLR_KEY_ANY, -1, external_keyjazz-octave*12, numsounds, snd_idxs, chst);
			external_keyjazz=-1;
			external_keyjazz_triggered=0;
		}
	}
}


void MakeDefaultSong()
{
	song->Clear();

	native_mode=1;

	// create default song configuration
	song->ScalemapWrite(0, 0, true); // default to marking C through the whole song
	song->SelectSound(0);
	song->SelectSound(1);

	song->SelectCH(0);
	song->GetCH(0)->sounds[0]=true;
	recent_chst=0;
	ch_editor.graphs.num=1;
	ch_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->content;
	chenv_editor.graphs.num=1;
	chenv_editor.graphs.graph_source[0]=song->GetCH(song->selected_ch)->envelope;

	SelectEditor(ch_editor);
	all_song_cache_dirty=true;

	song_filename[0]='\0';
	song_saved=true;

	UpdateTitle();
	UpdateAudioSettings();
	UpdateDisplaySettings();

	song->worktimer_seconds=0;
}

static void KeyJazzMIDIPacketHandler(const unsigned char* data, unsigned size)
{
	if(size>=2)
	{
		if((data[0]&0xF0)==0x90)
			nemus->keyjazz.TriggerMIDI(std::max(data[1]-12, 0));
		else if((data[0]&0xF0)==0x80)
			nemus->keyjazz.ReleaseMIDI(std::max(data[1]-12, 0));
	}
}

void InitEditor()
{
	for(int i=0;i<120;i++)
		vis_noteplaying[i]=false;

	nemus.reset(new NemusState());
#ifdef ENABLE_MIDI_IO
	NMIDI_Init(0, KeyJazzMIDIPacketHandler);
#endif

	InitAudio();


	int ni=0;
	notegrid_type[ni]=0; notegrid_key[ni]=2; ni++;
	notegrid_type[ni]=1; notegrid_key[ni]=2; ni++;
	notegrid_type[ni]=0; notegrid_key[ni]=3; ni++;
	notegrid_type[ni]=1; notegrid_key[ni]=3; ni++;
	notegrid_type[ni]=0; notegrid_key[ni]=4; ni++;
	notegrid_type[ni]=0; notegrid_key[ni]=5; ni++;
	notegrid_type[ni]=1; notegrid_key[ni]=5; ni++;
	notegrid_type[ni]=0; notegrid_key[ni]=6; ni++;
	notegrid_type[ni]=1; notegrid_key[ni]=6; ni++;
	notegrid_type[ni]=0; notegrid_key[ni]=0; ni++;
	notegrid_type[ni]=1; notegrid_key[ni]=0; ni++;
	notegrid_type[ni]=0; notegrid_key[ni]=1; ni++;

	editgraphs=new MultiGraph();

	editorprofiles[EPM_CH]=&ch_editor;
	editorprofiles[EPM_ST]=&st_editor;
	editorprofiles[EPM_CHENV]=&chenv_editor;
	editorprofiles[EPM_VENV]=&venv_editor;
	editorprofiles[EPM_PENV]=&penv_editor;
	editorprofiles[EPM_SONG]=&song_editor;



	AcquireAudioLock();
	song.reset(new NMSong());

	MakeDefaultSong();
	ReleaseAudioLock();

	selection_target_scalemap=new unsigned short[NMSongPart::scalemap_size];
	selection_prev_scalemap=new unsigned short[NMSongPart::scalemap_size];
	clipboard_buffer_scalemap=new unsigned short[NMSongPart::scalemap_size];
	for(int i=0;i<NMSongPart::scalemap_size;i++)
	{
		selection_target_scalemap[i]=0x0000;
		selection_prev_scalemap[i]=0x0000;
		clipboard_buffer_scalemap[i]=0x0000;
	}

	for(int i=0;i<MAX_UNDO;i++)
		undobuffer_data[i]=NULL;
	ResetUndo();

	FILE* startupfile=fopen("startup.nms", "rb");
	if(startupfile)
	{
		fclose(startupfile);
		strcpy(song_filename, "startup.nms");
		OpenSong();
	}

	song_filename[0]='\0';
	song_saved=true;

	UpdateTitle();

	BuildGridCache();
}

void SaveRecoveryFile();
void CloseEditor()
{
	SaveRecoveryFile();

#ifdef ENABLE_MIDI_IO
	NMIDI_Exit();
#endif
	CloseAudio();
}

static inline bool IsCtrlDown()
{
#if !defined(__APPLE__)
	return glrKeyPressed(GLR_KEY_CTRL);
#else
	// NOTE: tsone: macOS mirrors Ctrl in Command key
	return glrKeyPressed(GLR_KEY_CTRL) || glrKeyPressed(GLR_KEY_SUPER);
#endif
}

bool UpdateNavigation()
{
	bool navigating=false;

	static bool zoomview_init=true;
	if(zoomview_init)
	{
		zoomview_init=false;
		blitter.SetTarget(window);
	}

	blitter.SetTarget(window);

	int mx, my;
	static bool prev_lmb=false;
	static bool prev_rmb=false;
	bool lmb=glrGetMouseLeft();
	bool rmb=glrGetMouseRight();

	alt_down=glrKeyPressed(GLR_KEY_ALT);
	ctrl_down=IsCtrlDown();
	shift_down=glrKeyPressed(GLR_KEY_SHIFT);

	if(song->settings_altbuttons)
	{
		if(alt_down && !ctrl_down)
		{
			lmb=true;
			alt_down=false;
		}
		else if(alt_down && ctrl_down)
		{
			rmb=true;
			ctrl_down=false;
			alt_down=false;
		}
	}

	mouse.lclick=(lmb&&!prev_lmb);
	mouse.rclick=(rmb&&!prev_rmb);
	prev_lmb=lmb;
	prev_rmb=rmb;
	mouse.left=lmb;
	mouse.right=rmb;
	glrGetMousePos(&mx, &my);
	mouse.dx=mx-mouse.x;
	mouse.dy=my-mouse.y;
	mouse.x=mx;
	mouse.y=my;
	if(mouse.lclick || mouse.rclick)
	{
		mouse.clickx=mouse.x;
		mouse.clicky=mouse.y;
	}

	return navigating;
}

void DrawAntBox(Box& box, unsigned int color)
{
	int sb_x0=box.x0;
	int sb_y0=box.y0;
	int sb_x1=box.x1;
	int sb_y1=box.y1;
	blitter.DrawAntBox(std::min(sb_x0,sb_x1), std::min(sb_y0,sb_y1), abs(sb_x1-sb_x0), abs(sb_y1-sb_y0), color);
}


int gui_main_x0=10;
int gui_main_x1a=140;
int gui_main_x1=70;
int gui_main_x2=450;
int gui_main_x3=1000;
int gui_main_y0=25;
int gui_main_y1=gui_main_y0+155;
int gui_main_y2=gui_main_y1+4;
int gui_main_y3=gui_main_y2+35;
int gui_main_y4=gui_main_y3+17;
int gui_main_y5=900;

Point2D mousepoint;
Point2D clickpoint;

int st_expand=0;
int cv_expand=0;
int top_expand=0;

bool picking_sounds=false;
int picking_sounds_target=0;

int selected_panel=2;

#include "guigfx.h"


int menu_scrollx=0;
int menu_scrolly=0;
void MenuScroll(int x, int y)
{
	menu_scrollx=x;
	menu_scrolly=y;
}

void PrintMenu(int ft, int x, int y, const char* str, ...)
{
	va_list args;va_start(args,str);char text[2048];vsprintf(text,str,args);va_end(args);

	SelectFont(ft);
	x=x*9+menu_scrollx;
	y=y*11+menu_scrolly;
	DrawString(gui_main_x1a+15+x, gui_main_y0+8+y, text);
}

int button_hover=-1;
int button_click=-1;
int button_rclick=-1;
int button_color=1;

int mouseprevq=0;
int mousedelta=0;

struct ButtonResult
{
	bool clicked;
	bool rclicked;
	int delta;
	bool released;

	template <typename T, typename S>
	bool ClickTick(T& target, S modulo) const
	{
		const int d=clicked?+1:(rclicked?-1:0);
		if(d)
			target=static_cast<T>(((int)target+d+(int)modulo)%(int)modulo);
		return d;
	}
};

void ButtonColor(int c)
{
	button_color=c;
}

ButtonResult MenuButtonBase(int id, int x, int y, const char* text, int size, bool highlight, bool modify)
{
	int rx=x*9+menu_scrollx;
	int ry=y*11+menu_scrolly;
	Box bb;
	bb.SetXYWH(gui_main_x1a+15+rx-2, gui_main_y0+8+ry-1, size*9+4, 11);
	if(bb.Contains(mousepoint))
	{
		button_hover=id;
		if(mouse.lclick)
			button_click=id;
		if(mouse.rclick)
			button_rclick=id;
	}
	ButtonResult r;
	r.clicked=false;
	r.rclicked=false;
	r.delta=0;
	r.released=false;
	if(button_click==id)
	{
		if(!mouse.left && button_hover==id)
		{
			r.clicked=true;
			song_modified=modify;
		}
		if(!mouse.left)
			r.released=true;
		r.delta=mousedelta;
		if(r.delta!=0)
			song_modified=modify;
	}
	if(button_rclick==id && !mouse.right && button_hover==id)
	{
		r.rclicked=true;
		song_modified=modify;
	}

	if(button_click==id || (button_click==-1 && button_hover==id))
		highlight=true;
	SelectFont(button_color);
	DrawString(gui_main_x1a+15+rx, gui_main_y0+8+ry, text);
	if(highlight)
	{
		FontAlpha(170);
		SelectFont(0);
		DrawString(gui_main_x1a+15+rx, gui_main_y0+8+ry, text);
		FontAlpha(255);
	}

	return r;
}

ButtonResult MenuButton(int id, int x, int y, const char* str, ...)
{
	char text[2048];
	va_list args;
	va_start(args, str);
	vsprintf(text, str, args);
	va_end(args);

	return MenuButtonBase(id, x, y, text, (int)strlen(text), false, true);
}

void MenuTextInput(int id, int x, int y, int size, char* text)
{
	static int text_input_click=-1;

	const bool active=(glrTextInputActive() && text_input_click==id);

	if(!glrTextInputActive() && text_input_click==id)
	{
		text_input_click=-1;
		if(strcmp(text, glrTextInputGet()) && !glrTextInputCancelled())
		{
			text[0]=0;
			strncat(text, glrTextInputGet(), size);
			song_modified=true;
		}
	}

	std::vector<char> buf(size+1);
	buf[0]=0;
	strncat(buf.data(), !active?text:glrTextInputGet(), size);
	for(int i=(int)strlen(buf.data());i<size;i++)
		buf[i]='.';
	buf[size]=0;

	if(!active)
	{
		const auto br=MenuButtonBase(id, x, y, buf.data(), size, false, false);
		if(br.clicked)
		{
			text_input_click=id;
			glrTextInputStart(text, size);
		}
	}
	else
	{
		MenuButtonBase(id, x, y, buf.data(), size, true, false);
		if(glrAppTimeMs()&512)
		{
			const int cur_x=std::min(size-1, (int)strlen(glrTextInputGet()));
			PrintMenu(0, x+cur_x, y, "#");
		}
	}
}

Box st_viewbox;
Box cv_viewbox;
Box ch_viewbox;

int scroll_x=0;
int scroll_y=0;

void AnimateEditor();
void ExecuteFileCommand(FileSelectorType file_command);

#include "grapheditor.h"

#include "editorpart_envelope.h"
#include "editorpart_sound.h"
#include "editorpart_st.h"
#include "editorpart_ch.h"
#include "editorpart_chenv.h"
#include "editorpart_song.h"

#include "audiovis.h"

bool animation_running=false;

void AnimateEditor()
{
	animation_running=true;
}

void SaveRecoveryFile()
{
	glrLog("SaveRecoveryFile...");
	char filename[512];
	char sfn[512];
	if(song_filename[0]=='\0')
		strcpy(sfn, "Untitled");
	else
	{
		auto fn0=get_basename(song_filename);
		if(!fn0)
			fn0=song_filename;
		strcpy(sfn, fn0);
		auto l=strlen(sfn);
		for(long i=l-1;i>=0;i--)
			if(sfn[i]=='.')
			{
				sfn[i]='\0';
				break;
			}
	}
	sprintf(filename, "vault/%s-%04X%04X.nms", sfn, ((unsigned int)time(NULL))&0xFFFF, glrAppTimeMs()&0xFFFF);
	FILE* file=fopen(filename, "wb");
	if(file)
	{
		fclose(file);
		char old_filename[512];
		strcpy(old_filename, song_filename);
		strcpy(song_filename, filename);
		glrLog(" '%s'\n", song_filename);
		SaveSong();
		strcpy(song_filename, old_filename);
	}
	else
		glrLog(" vault not found\n");
}

static int savedisk_t0=0;
static bool savedisk_show=false;
static bool listen_for_changes=false;
static unsigned int last_autosave=0xFFFFFFFF;

void ExecuteFileCommand(FileSelectorType file_command)
{
	char filename[512];
	filename[0]=0;
	switch(file_command)
	{
		case FST_SONG_SAVE:
			if(FileSelectorSave(filename, FST_SONG_SAVE, "Save Song"))
			{
				strcpy(song_filename, filename);
				SaveSong();
				listen_for_changes=false;

				UpdateTitle();

				savedisk_t0=glrAppTimeMs();
				savedisk_show=true;

				last_autosave=glrAppTimeMs();
			}
			ResetPath();
			break;
		case FST_SONG_OPEN:
			if(FileSelectorLoad(filename, FST_SONG_OPEN, "Open Song"))
			{
				ResetPath();
				SaveRecoveryFile();

				strcpy(song_filename, filename);
				OpenSong();
				listen_for_changes=false;
				ResetUndo();

				UpdateTitle();

				last_autosave=glrAppTimeMs();
			}
			ResetPath();
			break;
		case FST_NSF:
			if(FileSelectorSave(filename, FST_NSF, "Export NSF"))
			{
				AcquireAudioLock();
				nemus->songplayer.Stop();
				ReleaseAudioLock();
				songplay_mode=0;
				nemus->songplayer.ExportNSF(song.get(), filename);
			}
			ResetPath();
			break;
		case FST_ASSEMBLY:
			if(FileSelectorSave(filename, FST_ASSEMBLY, "Export Assembly"))
			{
				AcquireAudioLock();
				nemus->songplayer.Stop();
				ReleaseAudioLock();
				songplay_mode=0;
				nemus->songplayer.ExportASM(song.get(), filename);
			}
			ResetPath();
			break;
		case FST_WAV:
			if(FileSelectorSave(filename, FST_WAV, "Export WAV"))
			{
				AcquireAudioLock();
				nemus->songplayer.Stop();
				ReleaseAudioLock();
				songplay_mode=0;
				nemus->songplayer.ExportWAV(song.get(), filename);
			}
			ResetPath();
			break;
		default:
			break;
	}
}

void Editor()
{
	static bool was_focused=true;
	bool is_focused=glrWindowFocused();
	was_focused=is_focused;


	redraw_window=true;
	blitter.enable=true;

	prevent_edit=UpdateNavigation();

	static int frameskip=0;
	static unsigned int last_activity=0;
	if(mouse.dx!=0 || mouse.dy!=0 || mouse.left || mouse.right || glrKeyPressed(GLR_KEY_ANY)) // maybe use a secondary shorter sleep period for passive audio/display monitoring? like Sleep(5)
		last_activity=glrAppTimeMs();
	bool user_active=true;
	if(glrAppTimeMs()>last_activity+100)
		user_active=false;
	static int starting_up=10;
	if(starting_up>0)
	{
		starting_up--;
		user_active=true;
		last_activity=glrAppTimeMs();
		frameskip=99;
	}
	if(animation_running)
	{
		user_active=true;
		last_activity=glrAppTimeMs();
		frameskip=99;
	}
	if(nemus->keyjazz.midi_activity)
	{
		nemus->keyjazz.midi_activity=false;
		user_active=true;
		last_activity=glrAppTimeMs();
	}
	static int idle_refresh=0;
	if(glrAppTimeMs()>last_activity+15000 && songplay_mode==0) // really inactive, probably afk
	{
		glrSleep(200);
		idle_refresh++;
		if(idle_refresh<10)
		{
			redraw_window=false;
			blitter.enable=false;
		}
		else
			idle_refresh=0;
	}
	else
		idle_refresh=0;
/*	if(!user_active)
	{
		if(songplay_mode>0)
			Sleep(1);
		else
			Sleep(10);
	}*/
	if(!glrWindowFocused())
	{
		if(user_active)
			glrSleep(15);
		else
		{
			if(songplay_mode!=0)
				glrSleep(20);
			else
				glrSleep(100);
		}
		frameskip=99;
	}
/*
	if(song->settings_lowpower)
	{
		if(!(glrKeyPressed(GLR_KEY_SPACE) && mouse.left))
		{
			// extra sleep while not scrolling, since refresh smoothness isn't as noticeable then
			if(songplay_mode!=0)
				Sleep(10);
			else
				Sleep(20);
		}
		else
			Sleep(10);
		if(frameskip<15)
			frameskip=15;
	}
*/
	if(song->settings_lowpower==0 && songplay_mode!=0)
	{
		if(frameskip<17)
			frameskip=17;
	}

	static int active_framecount=0;
	static bool mouse_left_prev=false;
	static bool mouse_right_prev=false;
	active_framecount++;
	if(mouse.left!=mouse_left_prev || mouse.right!=mouse_right_prev || glrKeyHit(GLR_KEY_ANY))
		active_framecount=0;
	if(mouse.dx==0 && mouse.dy==0 && mouse.left==mouse_left_prev && mouse.right==mouse_right_prev && !glrKeyHit(GLR_KEY_ANY) && active_framecount>3) // TODO: also check for altered integer songplay position?
	{
		// Inactive
		if(songplay_mode!=0)
			WorktimeActive();

		if(song->settings_lowpower)
		{
			glrSleep(5);
			if(frameskip<10)
				frameskip=10;
		}
		else
			glrSleep(1);
		
		frameskip++;
		if(songplay_mode>0 && frameskip<15)
			frameskip=15;
		if(frameskip<20)
		{
			redraw_window=false;
			blitter.enable=false;
		}
		else
			frameskip=0;
	}
	else
	{
		// Active
		WorktimeActive();

		frameskip=0;
		if(song->settings_lowpower)
		{
			if(!(glrKeyPressed(GLR_KEY_SPACE) && mouse.left))
			{
				// extra sleep while not scrolling, since refresh smoothness isn't as noticeable then
				if(songplay_mode!=0 || mouse.left || mouse.right)
					glrSleep(10);
				else
					glrSleep(15);
			}
			else
				glrSleep(5);
		}
	}
	mouse_left_prev=mouse.left;
	mouse_right_prev=mouse.right;

	animation_running=false;

/*	if(glrAppTimeMs()>last_activity+500)
	{
		Sleep(100);
	}*/
	// TODO: use logical hotzones to crop drawing around mouse activity? mainly exclude ch view while working on upper panels (could also just skip their render paths)
	//       might need to explicitly copy in content from archived frame though, since it's been cleared


//	blitter.DrawBar(0, 0, window.width, window.height, 0x000000);
	blitter.DrawBar(0, 0, window.width, gui_main_y4, 0x000000);
	blitter.DrawBar(0, gui_main_y4, gui_main_x1, gui_main_y5, 0x000000);

	const auto now=(unsigned int)glrAppTime(); // Seconds
	if(now>worktime_now && now<=worktime_end)
		song->worktimer_seconds+=now-worktime_now;
	worktime_now=now;

	mousepoint=Point2D(mouse.x, mouse.y);
	clickpoint=Point2D(mouse.clickx, mouse.clicky);
	//int mm=mouse.dx+mouse.dy;
	int mq=((mouse.x-mouse.clickx)-(mouse.y-mouse.clicky)+20010)/20;
	mousedelta=mq-mouseprevq;
	mouseprevq=mq;
	if(mouse.lclick)
	{
		mouseprevq=1000;
		mousedelta=0;
	}
	button_hover=-1;

//	if(glrKeyHit(GLR_KEY_F1))
//		editor_helpscreen=!editor_helpscreen;

	gui_main_x3=window.width;
	gui_main_y5=window.height;



	const int diskanim_frames[17]={0,1,2,3,4,3,2,1,0,8,7,6,5,6,7,8,0};
	if(savedisk_show)
	{
		int dfi=(glrAppTimeMs()-savedisk_t0)/30;
		if(dfi>25)
			savedisk_show=false;
		else
		{
			AnimateEditor();

			GUIbAlpha(255);
			DrawGUIb(76+diskanim_frames[dfi%17]*14,165,13,13, gui_main_x0+108, gui_main_y0-16);
		}
	}


	SelectFont(0);
	if(song->gui_show_worktimer)
		DrawString(gui_main_x3-120, 8, "%02i:%02i:%02i", song->worktimer_seconds/3600, song->worktimer_seconds/60%60, song->worktimer_seconds%60);
	else
		DrawString(gui_main_x3-120, 8, "--:--:--");
	Box wtb;
	wtb.SetXYWH(gui_main_x3-120, 6, 8*9, 13);
	if(wtb.Contains(mousepoint) && mouse.lclick)
		song->gui_show_worktimer=!song->gui_show_worktimer;

	SelectFont(0);
	if(top_expand==0 || top_expand==3)
		DrawString(gui_main_x3-24, gui_main_y0/2-4, "+");
	else
		DrawString(gui_main_x3-24, gui_main_y0/2-4, "-");
	static int top_expanding_t=0;
	Box cvx_b;
	cvx_b.SetXYWH(gui_main_x3-35, 0, 22, gui_main_y0);
	bool toggle_expand=false;
	if(cvx_b.Contains(mousepoint) && mouse.lclick && (top_expand==0 || top_expand==2))
		song->gui_top_expanded=!song->gui_top_expanded;
	if(top_expand==0 && song->gui_top_expanded)
		toggle_expand=true;
	if(top_expand==2 && !song->gui_top_expanded)
		toggle_expand=true;
	if(toggle_expand)
	{
		top_expand=(top_expand+1)%4;
		top_expanding_t=glrAppTimeMs();
	}
	if(top_expand==1 || top_expand==3)
	{
		AnimateEditor();

		int d=150-25;
		int t=(glrAppTimeMs()-top_expanding_t);
		if(t>d)
			t=d;
		if(top_expand==1)
			gui_main_y0=25+t;
		else
			gui_main_y0=25+d-t;
		if(t==d)
			top_expand=(top_expand+1)%4;
	}
	if(top_expand==2)
		DrawAudioVis();

	gui_main_y1=gui_main_y0+155;
	gui_main_y2=gui_main_y1+4;


	if(glrKeyPressed(GLR_KEY_ESCAPE))
	{
		song->selected_sound=-1;
		song->selected_st=-1;
		song->selected_ch=-1;
		show_settings=false;

		selection_active=false;
		selection_deactivated=true;

		song_modified=true;
	}

	if(glrKeyHit(GLR_KEY_HOME))
	{
		song->ResetView();
		song_modified=true;
	}

	static bool was_modified=true;
	if(song_modified)
		was_modified=true;
	else if(was_modified)
	{
		if(!mouse.left && !mouse.right)
		{
			song_modified=true;
			StoreUndo();
			was_modified=false;

			if(listen_for_changes)
			{
				song_saved=false;
				UpdateTitle();
			}
			listen_for_changes=true;
		}
	}
	song_modified=false;

	if(ctrl_down && glrKeyHit(GLR_KEY_Z))
	{
		int prev_scroll_x=song->gui_scroll_x;
		int prev_scroll_y=song->gui_scroll_y;
		int prev_scroll_song=song->gui_scroll_song;

		if(shift_down)
			Redo();
		else
			Undo();

		song->gui_scroll_x=prev_scroll_x;
		song->gui_scroll_y=prev_scroll_y;
		song->gui_scroll_song=prev_scroll_song;
	}

	static int auto_index=7;
	if(last_autosave==0xFFFFFFFF)
	{
		FILE* file=fopen("vault/__autoinfo.dat", "rb");
		if(file)
		{
			fread(&auto_index, 1, sizeof(int), file);
			fclose(file);
		}
		auto_index=(auto_index+1)%8;
		file=fopen("vault/__autoinfo.dat", "wb");
		if(file)
		{
			fwrite(&auto_index, 1, sizeof(int), file);
			fclose(file);
		}
		last_autosave=glrAppTimeMs();
	}
	if(glrAppTimeMs()>last_autosave+30000 && !mouse.left && !mouse.right)
	{
		last_autosave=glrAppTimeMs();
		char filename[128];
		sprintf(filename, "vault/__autosave%i.nms", auto_index+1);
		FILE* file=fopen(filename, "wb");
		if(file)
		{
			fclose(file);
			char old_filename[512];
			strcpy(old_filename, song_filename);
			strcpy(song_filename, filename);
			SaveSong();
			strcpy(song_filename, old_filename);
		}
	}

	static FileSelectorType file_command=FST_NONE;
	if(ctrl_down && glrKeyPressed(GLR_KEY_S))
	{
		if(shift_down)
		{
			file_command=FST_SONG_SAVE;
		}
		else if(file_command==FST_NONE)
		{
			if(song_filename[0]!='\0')
			{
				SaveSong();
				listen_for_changes=false;

				UpdateTitle();

				file_command=FST_COUNT;

				savedisk_t0=glrAppTimeMs();
				savedisk_show=true;

				last_autosave=glrAppTimeMs();
			}
			else
			{
				file_command=FST_SONG_SAVE;
			}
		}
	}
	if(ctrl_down && glrKeyPressed(GLR_KEY_O))
	{
		file_command=FST_SONG_OPEN;
	}
	if(ctrl_down && glrKeyPressed(GLR_KEY_E))
	{
		if(shift_down)
			file_command=alt_down?FST_ASSEMBLY:FST_WAV;
		else
			file_command=FST_NSF;
	}

	// DEBUG: tsone: quick export
#if !NDEBUG
	if(glrKeyHit(GLR_KEY_F6))
	{
		nemus->songplayer.ExportASM(song.get(), "/Users/tsone/Documents/code/nemus/nmn/neskit/examples/audio/song.s");
	}

	if(glrKeyHit(GLR_KEY_F7))
	{
		nemus->songplayer.ExportNSF(song.get(), "/Users/tsone/Documents/code/nemus/nmn/song.nsf");
	}
#endif

	if(!ctrl_down && !shift_down && !glrKeyPressed(GLR_KEY_S) && !glrKeyPressed(GLR_KEY_O) && !glrKeyPressed(GLR_KEY_E))
	{
		ExecuteFileCommand(file_command);
		file_command=FST_NONE;
	}

	if((alt_down || ctrl_down) && glrKeyHit(GLR_KEY_ENTER))
	{
		fullscreen=!fullscreen;
		if(fullscreen)
			glrSetFullscreen();
		else
			glrSetWindowed();
	}

	if(ctrl_down && glrKeyHit(GLR_KEY_N))
	{
		SaveRecoveryFile();

		ClearSong(nullptr);

		MakeDefaultSong();
		song_modified=true;

		last_autosave=glrAppTimeMs();
	}


	scroll_x=song->gui_scroll_x;
	scroll_y=song->gui_scroll_y;

	if(glrKeyHit(GLR_KEY_F2))
	{
		timeline_zoomed=!timeline_zoomed;
		scroll_x+=(mouse.x-gui_main_x1);
		if(timeline_zoomed)
			scroll_x*=timeline_substeps;
		else
			scroll_x/=timeline_substeps;
		scroll_x-=(mouse.x-gui_main_x1);
	}

	static int play_request=0;
	if(glrKeyHit(GLR_KEY_SPACE))
	{
		if(ctrl_down)
			play_request=2;
		else
			play_request=1;
	}

	const auto* part=song->part;
	int editplaypos=(song->gui_scroll_x+timeline_gridstep_x*5)/timeline_gridstep_x;
	if(!timeline_zoomed)
	{
		editplaypos=editplaypos/(part->timesig2*2)*(part->timesig2*2);
		editplaypos*=timeline_substeps;
	}
	if(editplaypos<0)
		editplaypos=0;

	if(glrKeyHit(GLR_KEY_F5))
	{
		if(songplay_mode>0)
		{
			nemus->songplayer.Stop();
			songplay_mode=0;
		}
		else
		{
			if(ctrl_down)
			{
				int channels[20];
				int num=0;
				if(song->selected_ch!=-1)
					channels[num++]=ChChst(song->selected_ch);
				else if(song->selected_st!=-1)
				{
					for(int i=0;i<20;i++)
						if(song->GetST(i)!=NULL)
							channels[num++]=StChst(i);
				}
				if(nemus->songplayer.Play(song.get(), channels, num, -1))
					songplay_mode=2;
			}
			else
			{
				if(nemus->songplayer.Play(song.get(), NULL, 0, -1))
					songplay_mode=2;
			}
		}
	}

	static int scrolling=0;
	if(glrKeyPressed(GLR_KEY_SPACE))
	{
		prevent_edit=true;
	}
	else
	{
		if(play_request>0)
		{
			if(songplay_mode>0)
			{
				nemus->songplayer.Stop();
				songplay_mode=0;
			}
			else
			{
				if(play_request==2)
				{
					int channels[20];
					int num=0;
					if(song->selected_ch!=-1)
						channels[num++]=ChChst(song->selected_ch);
					else if(song->selected_st!=-1)
					{
						for(int i=0;i<20;i++)
							if(song->GetST(i)!=NULL)
								channels[num++]=StChst(i);
					}
					if(nemus->songplayer.Play(song.get(), channels, num, editplaypos))
						songplay_mode=1;
				}
				else
				{
					if(nemus->songplayer.Play(song.get(), NULL, 0, editplaypos))
						songplay_mode=1;
				}
			}
			play_request=0;
		}
	}

	int smooth_playpos=0;
	static bool following=false;
	bool prevent_horizontal_scroll=false;
	if(songplay_mode>0)
	{
		const double curpos_float=nemus->songplayer.GetCurPosFloat();
		int x=(int)(curpos_float*timeline_gridstep_x);
		if(!timeline_zoomed)
			x/=timeline_substeps;
		following=true;
		if(songplay_mode==2)
		{
			scroll_x=x-(gui_main_x3-gui_main_x1)*1/3;
			prevent_horizontal_scroll=true;
		}
		smooth_playpos=x;
	}
	else
		following=false;

	if(glrKeyPressed(GLR_KEY_SPACE) && mouse.lclick && (st_editor.region.Contains(mousepoint) || chenv_editor.region.Contains(mousepoint) || ch_editor.region.Contains(mousepoint) || song_editor.view.Contains(mousepoint)))
	{
		play_request=0;

		if(ch_editor.region.Contains(mousepoint))
			glrMouseWrap(true, true);
		else
			glrMouseWrap(true, false);
		scrolling=1;
		if(ch_editor.region.Contains(mousepoint))
			scrolling=2;
		if(song_editor.view.Contains(mousepoint))
			scrolling=3;
	}
	if(scrolling>0)
	{
		if(scrolling==3)
		{
			song->gui_scroll_song-=mouse.dx;
		}
		else
		{
			if(!prevent_horizontal_scroll)
				scroll_x-=mouse.dx;
			if(scrolling==2)
				scroll_y-=mouse.dy;
		}
	}
	if(!mouse.left)
	{
		scrolling=0;
		glrMouseWrap(false, false);
	}
	static unsigned int mwscroll_t0=0;
	static int mwscroll_count=0;
	static int mwscroll_maxrate=0;
	static bool mw_alternate=false;
	int scrollwheel=glrGetMouseScroll();
	if(mwscroll_count==0)
	{
		mwscroll_t0=glrAppTimeMs();
		mwscroll_maxrate=0;
	}
	if(scrollwheel!=0)
	{
		active_framecount=0;
		if(mwscroll_count==0)
		{
			if(shift_down)
				mw_alternate=true;
			else
				mw_alternate=false;
		}
		mwscroll_count-=scrollwheel*150;
	}
	bool listscroll=false;
	if(mousepoint.x<gui_main_x1 && mousepoint.y>gui_main_y4) // scroll channels
	{
		listscroll=true;
		if(mw_alternate)
			mwscroll_count=-mwscroll_count;
		song->gui_scroll_channels+=mwscroll_count/150*3;
		mwscroll_count=0;
		song->gui_scroll_channels=std::min(std::max(0, song->gui_scroll_channels), 90);
	}
	else if(mousepoint.x<gui_main_x0+130 && mousepoint.y<gui_main_y2) // scroll sounds
	{
		listscroll=true;
		if(mw_alternate)
			mwscroll_count=-mwscroll_count;
		song->gui_scroll_sounds+=mwscroll_count/150;
		mwscroll_count=0;
		song->gui_scroll_sounds=std::min(std::max(0, song->gui_scroll_sounds), 8);
	}
	if(!listscroll && mwscroll_count!=0 && glrAppTimeMs()>mwscroll_t0+10)
	{
		mwscroll_t0=glrAppTimeMs();
		last_activity=glrAppTimeMs();
		int n=2+abs(mwscroll_count/17);
		if(n>mwscroll_maxrate)
			mwscroll_maxrate=n;
		if(n<mwscroll_maxrate)
			n=mwscroll_maxrate;
		n=std::min(n, abs(mwscroll_count));
		if(mwscroll_count>0)
		{
			mwscroll_count-=n;
			if(mw_alternate)
			{
				if(!prevent_horizontal_scroll)
					scroll_x+=n*3/2;
			}
			else
				scroll_y+=n;
		}
		else if(mwscroll_count<0)
		{
			mwscroll_count+=n;
			if(mw_alternate)
			{
				if(!prevent_horizontal_scroll)
					scroll_x-=n*3/2;
			}
			else
				scroll_y-=n;
		}
	}

	song->gui_scroll_x=scroll_x;
	song->gui_scroll_y=scroll_y;



	blitter.SetTarget(window);
	blitter.DisableZoom();

	ch_editor.mode=EPM_CH;
	ch_editor.region.SetXYWH(gui_main_x1+9, gui_main_y4+2, window.width-(gui_main_x1+9), window.height-(gui_main_y4+2));
	ch_editor.view.SetXYWH(ch_editor.region.x0+3, ch_editor.region.y0+2, ch_editor.region.Width()-5, ch_editor.region.Height()-4);
	ch_viewbox=ch_editor.view;
	ch_editor.max_x=0xFFFF;
	ch_editor.max_y=119;
	ch_editor.scroll_x=scroll_x;
	ch_editor.scroll_y=scroll_y;
	ch_editor.grid_x0=gui_main_x1+6;
	ch_editor.grid_y0=gui_main_y4+5+timeline_gridstep_y;
	ch_editor.gridstep_x=timeline_gridstep_x;
	ch_editor.gridstep_y=timeline_gridstep_y;
	ch_editor.quantize_x=1;
	if(!timeline_zoomed)
		ch_editor.quantize_x=timeline_substeps;

	st_editor.mode=EPM_ST;
	st_editor.region.SetXYWH(gui_main_x1+9, gui_main_y2+2, window.width-(gui_main_x1+9), gui_main_y3-2-(gui_main_y2+2));
	st_editor.view.SetXYWH(st_editor.region.x0+2, st_editor.region.y0+2, st_editor.region.Width()-4, st_editor.region.Height()-4);
	st_viewbox=st_editor.view;
	st_editor.max_x=0xFFFF;
	st_editor.max_y=1;
	st_editor.scroll_x=scroll_x;
	st_editor.scroll_y=0;
	st_editor.grid_x0=gui_main_x1+6;
	st_editor.grid_y0=gui_main_y2+18*12+4;
	st_editor.gridstep_x=timeline_gridstep_x;
	st_editor.gridstep_y=12;
	st_editor.quantize_x=1;
	if(!timeline_zoomed)
		st_editor.quantize_x=timeline_substeps;

	chenv_editor.mode=EPM_CHENV;
	chenv_editor.region.SetXYWH(gui_main_x1+9, gui_main_y3, window.width-(gui_main_x1+9), gui_main_y4-gui_main_y3);
	if(cv_expand==2)
		chenv_editor.region.SetXYWH(gui_main_x1+9, gui_main_y3-2, window.width-(gui_main_x1+9), gui_main_y4+2-(gui_main_y3-2));
	chenv_editor.view.SetXYWH(chenv_editor.region.x0+2, chenv_editor.region.y0+2, chenv_editor.region.Width()-4, chenv_editor.region.Height()-4);
	cv_viewbox=chenv_editor.view;
	chenv_editor.max_x=0xFFFF;
	chenv_editor.max_y=15;
	chenv_editor.scroll_x=scroll_x;
	chenv_editor.scroll_y=0;
	chenv_editor.grid_x0=gui_main_x1+6;
	chenv_editor.grid_y0=gui_main_y4-4;
	chenv_editor.gridstep_x=timeline_gridstep_x;
	chenv_editor.gridstep_y=5;
	chenv_editor.quantize_x=1;
	if(!timeline_zoomed)
		chenv_editor.quantize_x=timeline_substeps;

	venv_editor.mode=EPM_VENV;
	venv_editor.region.SetXYWH(0,0,0,0);
	if(song->gui_envtype==0)
		venv_editor.region.SetXYWH(gui_main_x2+30, gui_main_y0+22, 48*envwidth-5, 16*envheight+1);
	venv_editor.view=venv_editor.region;
	venv_editor.max_x=47;
	venv_editor.max_y=15;
	venv_editor.scroll_x=0;
	venv_editor.scroll_y=0;
	venv_editor.grid_x0=gui_main_x2+27;
	venv_editor.grid_y0=gui_main_y0+22+16*envheight;
	venv_editor.gridstep_x=envwidth;
	venv_editor.gridstep_y=envheight;
	venv_editor.quantize_x=1;

	penv_editor.mode=EPM_PENV;
	penv_editor.region.SetXYWH(0,0,0,0);
	if(song->gui_envtype==1)
		penv_editor.region.SetXYWH(gui_main_x2+30, gui_main_y0+22, 48*envwidth-5, 15*envheight+1);
	penv_editor.view=penv_editor.region;
	penv_editor.max_x=47;
	penv_editor.max_y=14;
	penv_editor.scroll_x=0;
	penv_editor.scroll_y=0;
	penv_editor.grid_x0=gui_main_x2+27;
	penv_editor.grid_y0=gui_main_y0+22+15*envheight;
	penv_editor.gridstep_x=envwidth;
	penv_editor.gridstep_y=envheight;
	penv_editor.quantize_x=1;

	song_editor.mode=EPM_SONG;
	song_editor.region.SetXYWH(gui_main_x2+10, gui_main_y0+35, gui_main_x3-12-(gui_main_x2+10), 100);
	song_editor.view.SetXYWH(song_editor.region.x0, gui_main_y0+4, song_editor.region.x1-song_editor.region.x0, gui_main_y1-4-(gui_main_y0+4));
	song_editor.max_x=0xFFFF;
	song_editor.max_y=1;
	song_editor.scroll_x=song->gui_scroll_song;
	song_editor.scroll_y=0;
	song_editor.grid_x0=gui_main_x2;
	song_editor.grid_y0=gui_main_y0+135;
	song_editor.gridstep_x=songeditor_barsize;
	song_editor.gridstep_y=100;
	song_editor.quantize_x=part->timesig2*2*timeline_substeps;



	DrawGUIb(75,147,99,16, gui_main_x0+3, gui_main_y0-18);

	DrawPanel(selected_panel==0, 1, 2, gui_main_x0, gui_main_y0, gui_main_x1a, gui_main_y1);
	DrawPanel(true, 0, 2, gui_main_x1a, gui_main_y0, gui_main_x2, gui_main_y1);
	DrawPanel(true, 0, 1, gui_main_x2, gui_main_y0, gui_main_x3-10, gui_main_y1);

	DrawPanel(selected_panel==1, 1, st_expand==2?2:3, gui_main_x0, gui_main_y2, gui_main_x1, gui_main_y3);
	DrawPanel(true, 0, 0, gui_main_x1, gui_main_y2, gui_main_x3+4, gui_main_y3);

	DrawPanel(selected_panel==2, 1, 2, gui_main_x0, gui_main_y4, gui_main_x1, gui_main_y5+12);
	DrawPanel(true, 0, 0, gui_main_x1, gui_main_y4, gui_main_x3+4, gui_main_y5+12);

	if(current_editor!=NULL)
	{
		unsigned int sgec=guibits.GetPixel(58, 27);
		if(current_editor->mode==EPM_SONG || current_editor->mode==EPM_VENV || current_editor->mode==EPM_PENV)
		{
			blitter.DrawBar(gui_main_x2+9, gui_main_y0+2, gui_main_x3-gui_main_x2-21, 1, sgec);
			blitter.DrawBar(gui_main_x2+9, gui_main_y0+2, 1, gui_main_y1-gui_main_y0-8, sgec);
			blitter.DrawBar(gui_main_x2+14, gui_main_y1-3, gui_main_x3-gui_main_x2-26, 1, sgec);
			DrawGUIb(58,27,5,5, gui_main_x2+9, gui_main_y1-7);
		}
		if(current_editor->mode==EPM_ST)
		{
			blitter.DrawBar(gui_main_x1+9, gui_main_y2+2, gui_main_x3-gui_main_x1+5, 1, sgec);
			blitter.DrawBar(gui_main_x1+9, gui_main_y2+2, 1, gui_main_y3-gui_main_y2-8, sgec);
			blitter.DrawBar(gui_main_x1+14, gui_main_y3-3, gui_main_x3-gui_main_x1+5, 1, sgec);
			DrawGUIb(58,27,5,5, gui_main_x1+9, gui_main_y3-7);
		}
		if(current_editor->mode==EPM_CHENV)
		{
			blitter.DrawBar(gui_main_x1+13, gui_main_y3+1, gui_main_x3-gui_main_x1+5, 1, sgec);
			blitter.DrawBar(gui_main_x1+7, gui_main_y4-2, gui_main_x3-gui_main_x1+5, 1, sgec);
		}
		if(current_editor->mode==EPM_CH)
		{
			blitter.DrawBar(gui_main_x1+9, gui_main_y4+2, gui_main_x3-gui_main_x1+5, 1, sgec);
			blitter.DrawBar(gui_main_x1+9, gui_main_y4+2, 1, gui_main_y5-gui_main_y4+5, sgec);
		}
	}


	selection_deactivated=false;
	if(selection_active && (mouse.lclick || mouse.rclick) && current_editor!=NULL && !current_editor->region.Contains(mousepoint))
	{
		selection_active=false;
		selection_deactivated=true;
	}



	static unsigned int repeat_t0=0;
	static bool repeat_phase=0;
	bool repeat_command=false;
	if(ctrl_down && glrKeyPressed(GLR_KEY_R))
	{
		if(repeat_phase==1 && glrAppTimeMs()>repeat_t0+300)
		{
			repeat_command=true;
			repeat_t0=glrAppTimeMs()-250;
		}
	}
	else
		repeat_phase=0;
	if(ctrl_down && glrKeyHit(GLR_KEY_R) && repeat_phase==0)
	{
		repeat_command=true;

		repeat_t0=glrAppTimeMs();
		repeat_phase=1;
	}
	if(repeat_command)
	{
		if(graph_recent_x0!=-1 && graph_recent_x1!=-1 && graph_recent_graph!=NULL)
		{
			int delta=graph_recent_x1-graph_recent_x0;
			int x=graph_recent_x1+delta;
			graph_recent_x0+=delta;
			graph_recent_x1+=delta;
			int vdelta=graph_recent_value1-graph_recent_value0;
			int v=graph_recent_value1+vdelta;
			graph_recent_value0+=vdelta;
			graph_recent_value1+=vdelta;
			GraphPoint gp=graph_recent_graph->Read(x);
			if(graph_recent_type==0)
				gp.curve=v;
			else
				gp.trigger=v;
			graph_recent_graph->SetPoint(x, gp);
		}
		song_modified=true;
	}




	preview_hover_sound=-1;
	EditorPart_Sound();


	preview_hover_channel=NULL;
	EditorPart_ST();


	EditorPart_CHEnv();


	EditorPart_CH();


	EditorPart_Song();
	part=song->part;

	if(glrKeyPressed(GLR_KEY_CAPSLOCK))
		nemus->keyjazz.polyphony=2;
	else
		nemus->keyjazz.polyphony=1;

	nemus->keyjazz.SetOctave((-(gui_main_y5-gui_main_y4)/2-90-song->gui_scroll_y)/ch_editor.gridstep_y/12);
	
	if(current_editor && current_editor->mode==EPM_ST)
		nemus->keyjazz.Update(0, nullptr, -1);
	else if(song->selected_st!=-1)
	{
		NMChannel* st=song->GetST(song->selected_st);
		int sounds[SOUNDS_MAX];
		int num=0;
		for(int i=0;i<SOUNDS_MAX;i++)
			if(st->sounds[i])
				sounds[num++]=i;
		nemus->keyjazz.Update(num, sounds, StChst(song->selected_st));
	}
	else if(song->selected_ch!=-1)
	{
		NMChannel* ch=song->GetCH(song->selected_ch);
		int sounds[SOUNDS_MAX];
		int num=0;
		for(int i=0;i<SOUNDS_MAX;i++)
			if(ch->sounds[i])
				sounds[num++]=i;
		if(num==0)
		{
			if(song->selected_sound!=-1)
			{
				nemus->keyjazz.Update(1, &song->selected_sound, ChChst(song->selected_ch));
			}
		}
		else
			nemus->keyjazz.Update(num, sounds, ChChst(song->selected_ch));
	}
	else if(song->selected_sound!=-1)
	{
		nemus->keyjazz.Update(1, &song->selected_sound, -1);
	}
	else if(recent_chst!=-1)
	{
		auto ch=song->GetCHST(recent_chst);
		if(ch)
		{
			int sounds[SOUNDS_MAX];
			int num=0;
			for(int i=0;i<SOUNDS_MAX;i++)
				if(ch->sounds[i])
					sounds[num++]=i;
			nemus->keyjazz.Update(num, sounds, recent_chst);
		}
	}

	if(!mouse.left)
		nemus->keyjazz.ExternalRelease();

	blitter.SetScroll(0, song->gui_scroll_y);
	blitter.SetClipping(ch_viewbox);

	blitter.DrawBar(gui_main_x1+12, gui_main_y4+11-nemus->keyjazz.GetOctave()*12*ch_editor.gridstep_y+ch_editor.gridstep_y/2, 8, 3, 0xFFFFFF);
	blitter.DrawBar(gui_main_x1+12, gui_main_y4+11-(nemus->keyjazz.GetOctave()*12+28)*ch_editor.gridstep_y-ch_editor.gridstep_y/2, 8, 3, 0xFFFFFF);
	blitter.DrawBar(gui_main_x1+12, gui_main_y4+11-(nemus->keyjazz.GetOctave()*12+28)*ch_editor.gridstep_y-ch_editor.gridstep_y/2, 2, 29*ch_editor.gridstep_y, 0xFFFFFF);

	if(songplay_mode>0)
	{
		blitter.SetScroll(song->gui_scroll_x, 0);
		int rx=gui_main_x1+9+smooth_playpos;

		// latency compensation (depends on audio buffer)
//#if PA_USE_ASIO
//		rx-=2;
//#else
//		rx-=20;
//#endif
		blitter.SetClipping(st_viewbox);
		blitter.DrawBarAlpha(rx, gui_main_y0, 8, gui_main_y5-gui_main_y0, 0x3377BBFF);
		blitter.SetClipping(cv_viewbox);
		blitter.DrawBarAlpha(rx, gui_main_y0, 8, gui_main_y5-gui_main_y0, 0x3377BBFF);
		blitter.SetClipping(ch_viewbox);
		blitter.DrawBarAlpha(rx, gui_main_y0, 8, gui_main_y5-gui_main_y0, 0x3377BBFF);
	}

	blitter.SetScroll(0, 0);
	blitter.DisableClipping();


	if(song->selected_sound!=-1)
	{
		recent_sound=song->selected_sound;
		if(recent_chst!=-1)
		{
			const auto recent_channel=song->GetCHST(recent_chst);
			if(!recent_channel || !recent_channel->sounds[recent_sound])
				recent_chst=-1;
		}
	}
	if(song->selected_ch!=-1)
	{
		recent_chst=ChChst(song->selected_ch);
		recent_sound=-1;
	}
	if(song->selected_st!=-1)
	{
		recent_chst=StChst(song->selected_st);
		recent_sound=-1;
	}

	master_volume=(float)part->volume/10;

	if(!mouse.left)
		button_click=-1;
	if(!mouse.right)
		button_rclick=-1;
}

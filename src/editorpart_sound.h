void EditorPart_Sound()
{
	static int clicked_sound=-1;
	static int dragged_sound=-1;
	static bool dragged_copy=false;
	static bool clicked_right=false;
	int hovered_sound=-1;

	for(int x=0;x<5;x++)
		for(int cy=0;cy<12;cy++)
		{
			int y=cy+song->gui_scroll_sounds;
			if(y<0) continue;
			if(y>99) break;
			int rx=gui_main_x0+3+x*24;
			int ry=gui_main_y0+7+cy*12;
			int i=y*5+x;

			char str[4];
			sprintf(str, "--");

			bool selected=false;

			NMSound* sound=song->GetSound(i);

			bool active=false;
			if(sound!=NULL)
				active=true;

			if(active)
				sprintf(str, "%02i", sound->name);

			if(active && sound->audio_light>0)
			{
				FontAlpha(std::min(sound->audio_light*400/15, 255));
				SelectFont(2);
				DrawString(rx-1, ry, str);
				DrawString(rx+1, ry, str);
				DrawString(rx, ry-1, str);
				DrawString(rx, ry+1, str);
				FontAlpha(std::min(std::max(sound->audio_light*600/15-250, 0), 255));
				SelectFont(0);
				DrawString(rx-1, ry, str);
				DrawString(rx+1, ry, str);
				DrawString(rx, ry-1, str);
				DrawString(rx, ry+1, str);
				FontAlpha(255);
			}

			if(!active)
			{
				FontAlpha(60);
				SelectFont(2);
			}
			else
				SelectFont((1+sound->color)%5);

			if(song->selected_st!=-1)
			{
				if(song->GetST(song->selected_st)->sounds[i])
					selected=true;
			}
			else if(song->selected_ch!=-1)
			{
				if(song->GetCH(song->selected_ch)->sounds[i])
					selected=true;
			}
			else if(recent_chst!=-1)
			{
				const auto recent_channel=song->GetCHST(recent_chst);
				if(!recent_channel)
					recent_chst=-1;
				else if(recent_channel->sounds[i])
					selected=true;
			}

			if(selected)
			{
				SelectFont(0);
				FontAlpha(255);
			}

			DrawString(rx, ry, str);

			bool hoverlight=false;
			if(preview_hover_channel!=NULL && preview_hover_channel->sounds[i]==true)
				hoverlight=true;

			Box bb;
			bb.SetXYWH(rx-5, ry+1, 24, 12);
			if(bb.Contains(mousepoint) || hoverlight)
			{
				if(active || !picking_sounds)
				{
					if(!selected)
					{
						FontAlpha(90);
						if(active)
						{
							SelectFont(0);
							FontAlpha(170);
						}
						else
							SelectFont(2);
						DrawString(rx, ry, str);
					}

					if(!hoverlight)
						hovered_sound=i;
				}
			}

			FontAlpha(255);
		}

	preview_hover_sound=hovered_sound;

	if(mouse.lclick && hovered_sound!=-1)
		clicked_sound=hovered_sound;
	if(mouse.rclick && hovered_sound!=-1)
	{
		clicked_sound=hovered_sound;
		clicked_right=true;
	}
	if(mouse.left && clicked_sound!=-1 && hovered_sound!=clicked_sound)
	{
		if(song->GetSound(clicked_sound)!=NULL)
		{
			dragged_sound=clicked_sound;
			if(shift_down)
				dragged_copy=true;
			else
				dragged_copy=false;
			clicked_sound=-1;
		}
	}
	if(!mouse.left && !mouse.right)
	{
		if(dragged_sound!=-1 && hovered_sound!=dragged_sound && hovered_sound!=-1)
		{
			if(dragged_copy || shift_down)
			{
				song->SelectSound(hovered_sound);
				song->GetSound(hovered_sound)->CopyFrom(song->GetSound(dragged_sound));
				song->GetSound(hovered_sound)->name=song->FindSoundName();
			}
			else
			{
				song->SwapSounds(dragged_sound, hovered_sound);
				song->SelectSound(hovered_sound);
			}
			venv_editor.graphs.num=1;
			venv_editor.graphs.graph_source[0]=song->GetSound(song->selected_sound)->v_env_graph;
			penv_editor.graphs.num=1;
			penv_editor.graphs.graph_source[0]=song->GetSound(song->selected_sound)->p_env_graph;
			if(song->gui_envtype==0)
				SelectEditor(venv_editor);
			if(song->gui_envtype==1)
				SelectEditor(venv_editor);

			song_modified=true;
		}
		if(clicked_sound!=-1 && clicked_sound==hovered_sound)
		{
			if(picking_sounds)
			{
				if(song->selected_st!=-1)
				{
					bool& flag=song->GetST(song->selected_st)->sounds[clicked_sound];
					flag=!flag;

					song_modified=true;
				}
				if(song->selected_ch!=-1)
				{
					bool& flag=song->GetCH(song->selected_ch)->sounds[clicked_sound];
					flag=!flag;

					song_modified=true;
				}
			}
			else
			{
				if(clicked_right)
				{
					if(song->GetSound(clicked_sound)!=NULL)
					{
						song->RemoveSound(clicked_sound);
						song_modified=true;
					}
				}
				else
				{
					if(song->selected_sound==clicked_sound) // explicitly clicked selected sound again, forget anything else
						recent_chst=-1;
					song->SelectSound(clicked_sound);
					venv_editor.graphs.num=1;
					venv_editor.graphs.graph_source[0]=song->GetSound(song->selected_sound)->v_env_graph;
					penv_editor.graphs.num=1;
					penv_editor.graphs.graph_source[0]=song->GetSound(song->selected_sound)->p_env_graph;
					if(song->gui_envtype==0)
						SelectEditor(venv_editor);
					if(song->gui_envtype==1)
						SelectEditor(venv_editor);

					song_modified=true;
				}
			}
		}
		clicked_sound=-1;
		clicked_right=false;
		dragged_sound=-1;
	}

	if(dragged_sound!=-1)
	{
		SelectFont(0);
		DrawString(mouse.x-9, mouse.y-4, "%02i", song->GetSound(dragged_sound)->name);
	}

	if(song->selected_sound==-1)
		return;
		


	if(venv_editor.graphs.num==0 || penv_editor.graphs.num==0) // TODO: if it's in any way not equal to what it should be
	{
		venv_editor.graphs.num=1;
		venv_editor.graphs.graph_source[0]=song->GetSound(song->selected_sound)->v_env_graph;
		penv_editor.graphs.num=1;
		penv_editor.graphs.graph_source[0]=song->GetSound(song->selected_sound)->p_env_graph;
		if(song->gui_envtype==0)
			SelectEditor(venv_editor);
		if(song->gui_envtype==1)
			SelectEditor(venv_editor);
	}



	selected_panel=0;



	NMSound* sound=song->GetSound(song->selected_sound);


	int sound_selpos_x=song->selected_sound%5;
	int sound_selpos_y=song->selected_sound/5-song->gui_scroll_sounds;
	if(sound_selpos_y>=0 && sound_selpos_y<12)
		DrawGUIb(131,1,24,12, gui_main_x0+3+sound_selpos_x*24-4, gui_main_y0+7+sound_selpos_y*12-2);



	EditorPart_Envelope();

	if(song->gui_envtype==0 && sound->audio_v_envpos!=NONE_POS && sound->audio_v_envpos<47)
		blitter.DrawBarAlpha(gui_main_x2+35+sound->audio_v_envpos*envwidth, gui_main_y0+26, 6, 15*envheight, 0x3377BBFF);
	if(song->gui_envtype==1 && sound->audio_p_envpos!=NONE_POS && sound->audio_p_envpos<47)
		blitter.DrawBarAlpha(gui_main_x2+35+sound->audio_p_envpos*envwidth, gui_main_y0+26, 6, 14*envheight, 0x3377BBFF);



	ButtonResult br;

	ButtonColor(2);
	PrintMenu(0, 0, 0, "SOUND");
	PrintMenu(2, 6, 0, "%02i", song->selected_sound);

	PrintMenu(0, 1, 2, "COLOR");
	ButtonColor((1+sound->color)%5);
	static const char* colornames[]={"\"A\"", "\"B\"", "\"C\"", "\"D\"", "\"E\"", "?"};
	br=MenuButton(1008, 1+6, 2, colornames[sound->color]);
	br.ClickTick(sound->color, 5);
	ButtonColor(1);

	PrintMenu(0, 1, 4, "WAVEFORM");
	const char* wavenames[5]={"SQUARE-1", "SQUARE-2", "TRIANGLE", "NOISE   ", "DPCM    "};
	br=MenuButton(1000, 1+9, 4, "%s", wavenames[(int)sound->waveform]);
	if(br.ClickTick(sound->waveform, NMSound::Type::COUNT))
	{
		sound->wavemode=sound->recent_wavemode[(int)sound->waveform];
		sound->wavemode2=sound->recent_wavemode2[(int)sound->waveform];
	}

	if(sound->waveform<=NMSound::Type::SQUARE2)
	{
		PrintMenu(0, 1, 3, "DUTY CYCLE");
		const char* dutynames[4]={"1:8", "1:4", "1:2", "3:4"};
		br=MenuButton(1001, 1+11, 3, "%s", dutynames[sound->wavemode]);
		br.ClickTick(sound->wavemode, 4);

		PrintMenu(0, 20, 3, "ALT");
		br=MenuButton(1009, 20+4, 3, "%s", dutynames[sound->wavemode2]);
		br.ClickTick(sound->wavemode2, 4);
	}
	if(sound->waveform==NMSound::Type::NOISE)
	{
		PrintMenu(0, 1, 3, "INTERVAL");
		const char* modenames[]={"SHORT", "LONG "};
		br=MenuButton(1001, 1+9, 3, "%s", modenames[sound->wavemode&1]);
		br.ClickTick(sound->wavemode, 2);

		PrintMenu(0, 20, 3, "ALT");
		br=MenuButton(1009, 20+4, 3, "%s", modenames[sound->wavemode2&1]);
		br.ClickTick(sound->wavemode2, 2);
	}
	if(sound->waveform==NMSound::Type::DMC)
	{
		PrintMenu(0, 1, 3, "NAME");
		const char *name=sound->dpcm_sample.name;
		if(!name || !name[0])
			name="(OPEN)";
		br=MenuButton(1001, 1+5, 3, "%-13s", name);
		if(br.rclicked)
		{
			AcquireAudioLock();
			sound->dpcm_sample.Clear();
			ReleaseAudioLock();
		}
		else if(br.clicked)
		{
			char filename[512];
			if(FileSelectorLoad(filename, FST_SAMPLE, "Open DMC Sample"))
			{
				AcquireAudioLock();
				sound->dpcm_sample.LoadDMC(filename);
				ReleaseAudioLock();
			}
		}

		PrintMenu(0, 20, 3, "LOOP");
		const char* modenames[]={"ON ", "OFF"};
		br=MenuButton(1011, 20+5, 3, "%s", modenames[sound->wavemode&1]);
		br.ClickTick(sound->wavemode, 2);
	}

	sound->recent_wavemode[(int)sound->waveform]=sound->wavemode;
	sound->recent_wavemode2[(int)sound->waveform]=sound->wavemode2;
	if(sound->waveform<=NMSound::Type::SQUARE2) // keep squares at same duty, so they're immediately interchangeable
	{
		sound->recent_wavemode[1-(int)sound->waveform]=sound->recent_wavemode[(int)sound->waveform];
		sound->recent_wavemode2[1-(int)sound->waveform]=sound->recent_wavemode2[(int)sound->waveform];
	}

	if(sound->HasVolumeControl())
	{
		PrintMenu(0, 20, 4, "VOLUME");
		br=MenuButton(1002, 20+7, 4, "%i", sound->Volume());
		const int new_volume=sound->Volume()+br.delta;
		sound->Volume(std::min(std::max(new_volume, 0), 15));
		if(br.rclicked)
			sound->Volume(sound->Volume()?0:15);
	}

	ButtonColor(1);
	br=MenuButton(1020, 22, 0, "SAVE");
	if(br.clicked)
	{
		char filename[512];
		if(FileSelectorSave(filename, FST_SOUND, "Save Sound"))
			sound->SaveFile(filename);
		ResetPath();
	}
	br=MenuButton(1021, 27, 0, "OPEN");
	if(br.clicked)
	{
		char filename[512];
		if(FileSelectorLoad(filename, FST_SOUND, "Open Sound"))
			sound->OpenFile(filename);
		ResetPath();
	}

	PrintMenu(0, 1, 6, "VIBRATO STRENGTH");
	br=MenuButton(1003, 1+17, 6, "%i", sound->vibrato_strength);
	sound->vibrato_strength+=br.delta; sound->vibrato_strength=std::min(std::max(sound->vibrato_strength, 0), 15);
	if(br.rclicked) sound->vibrato_strength=0;

	PrintMenu(0, 1, 7, "VIBRATO SPEED");
	br=MenuButton(1004, 1+17, 7, "%i", sound->vibrato_speed);
	sound->vibrato_speed+=br.delta; sound->vibrato_speed=std::min(std::max(sound->vibrato_speed, 1), 16);

	PrintMenu(0, 1, 8, "VIBRATO DELAY");
	br=MenuButton(1005, 1+17, 8, "%i", sound->vibrato_delay);
	sound->vibrato_delay+=br.delta; sound->vibrato_delay=std::min(std::max(sound->vibrato_delay, 0), 15);
	if(br.rclicked) sound->vibrato_delay=0;

	PrintMenu(0, 1, 10, "DETUNE");
	br=MenuButton(1006, 1+7, 10, "%c%i.%i", sound->detune-128<0?'-':'+', abs(sound->detune-128)/10, abs(sound->detune-128)%10);
	sound->detune+=br.delta; sound->detune=std::min(std::max(sound->detune, 128-120), 128+120);
	if(br.rclicked) sound->detune=128;


	PrintMenu(0, 40, 0, "ENVELOPE");

	if(song->gui_envtype==1)
		ButtonColor(1);
	else
		ButtonColor(2);
	br=MenuButton(2000, 49, 0, "%s", song->gui_envtype==0?"VOLUME":"PITCH ");
	br.ClickTick(song->gui_envtype, 2);

	if(song->gui_envtype==1)
	{
		PrintMenu(0, 56, 0, "RANGE");
		br=MenuButton(2006, 56+6, 0, "%i", sound->pitchrange);
		sound->pitchrange+=br.delta; sound->pitchrange=std::min(std::max(sound->pitchrange, 1), 16);

		PrintMenu(0, 65, 0, "LOCKED");
		br=MenuButton(2004, 65+7, 0, "%s", sound->fixedpitch?"YES":"NO ");
		br.ClickTick(sound->fixedpitch, 2);
		if(sound->fixedpitch)
		{
			int ni=sound->basepitch;
			ButtonColor(2);
			PrintMenu(0, 65+7+4, 0, "(   )");
			br=MenuButton(2005, 65+7+5, 0, "%c%c%i", 'A'+notegrid_key[ni%12], notegrid_type[ni%12]==1?'#':'-', ni/12);
			sound->basepitch+=br.delta; sound->basepitch=std::min(std::max(sound->basepitch, 0), 119);
		}
	}

	int* envspeeds=sound->v_env_speed;
	ButtonColor(2);
	if(song->gui_envtype==1)
	{
		envspeeds=sound->p_env_speed;
		ButtonColor(1);
	}

	ButtonColor(2);
	br=MenuButton(2008, 94, 0, "CLEAR");
	if(br.clicked)
	{
		if(song->gui_envtype==0)
			sound->v_env_graph->Clear();
		else
			sound->p_env_graph->Clear();
		envspeeds[0]=0;
		envspeeds[1]=0;
		envspeeds[2]=0;
	}


	PrintMenu(0, 42+22*0, 12, "SPEED-A");
	br=MenuButton(2001, 42+22*0+8, 12, "%i", envspeeds[0]);
	envspeeds[0]+=br.delta;
	envspeeds[0]=std::min(std::max(envspeeds[0], 0), 15);
	if(br.rclicked) envspeeds[0]=0;

	PrintMenu(0, 42+22*1, 12, "SPEED-B");
	br=MenuButton(2002, 42+22*1+8, 12, "%i", envspeeds[1]);
	envspeeds[1]+=br.delta;
	envspeeds[1]=std::min(std::max(envspeeds[1], 0), 15);
	if(br.rclicked) envspeeds[1]=0;

	PrintMenu(0, 42+22*2, 12, "SPEED-C");
	br=MenuButton(2003, 42+22*2+8, 12, "%i", envspeeds[2]);
	envspeeds[2]+=br.delta;
	envspeeds[2]=std::min(std::max(envspeeds[2], 0), 15);
	if(br.rclicked) envspeeds[2]=0;
}

#include "nmas.h"

#include "port.h"

#include "glr/glr.h"

enum NMASMode
{
	NMAS_MODE_CLOSED=0,
	NMAS_MODE_DIRECTSOUND=1,
	NMAS_MODE_ASIO=2,
	NMAS_MODE_SDL=3,
	NMAS_MODE_CORE_AUDIO=4,
};

bool nmas_initialized=false;
NMASMode nmas_mode=NMAS_MODE_CLOSED;

#if !defined(NMAS_ENABLE_SDL) && !defined(NMAS_ENABLE_CORE_AUDIO)
int nmas_buffersize=0;
float* nmas_audiobuffer=NULL;
#endif

const char* nmas_drivers[]=
{
#ifdef NMAS_ENABLE_DIRECTSOUND
	"DIRECTSOUND",
#endif
#ifdef NMAS_ENABLE_ASIO
	"ASIO",
#endif
#ifdef NMAS_ENABLE_CORE_AUDIO
	"CORE AUDIO",
#endif
#ifdef NMAS_ENABLE_SDL
	"SDL",
#endif
	0 // terminator
};
const unsigned nmas_drivers_count=sizeof(nmas_drivers)/sizeof(*nmas_drivers)-1;

#ifdef NMAS_ENABLE_DIRECTSOUND
#include <windows.h>
#include <mmsystem.h>
#include <dsound.h>

IDirectSound8* m_DirectSound=NULL;
IDirectSoundBuffer8* m_primaryBuffer=NULL;
IDirectSoundBuffer8* m_secondaryBuffer=NULL;

HANDLE dsound_thread=0;

int dsound_timingbias=0;
bool dsound_playing=false;
int dsound_bufferid=0;
void dsound_stream()
{
	unsigned int playpos=0;
//	unsigned int writepos=0;
//	m_primaryBuffer->GetCurrentPosition(&playpos, &writepos);
//	glrLog("0x%08X->GetCurrentPosition()\n", m_secondaryBuffer);
	m_secondaryBuffer->GetCurrentPosition((LPDWORD)&playpos, NULL);
	playpos/=2*sizeof(short);
//	writepos/=2*sizeof(short);

//	glrLog("stream: %i\n", playpos);

//	int bfshift=nmas_buffersize/2;
//	int bfshift=max(512, nmas_buffersize*14/16); // stability trick to enable lower latency, perhaps unreliable? set to zero for "normal" behavior
	int bfshift=std::min(dsound_timingbias, nmas_buffersize*14/16); // stability trick to enable lower latency, perhaps unreliable? set to zero for "normal" behavior

	int writepos=-1;
	if(dsound_bufferid==0 && playpos>=(unsigned int)nmas_buffersize-bfshift && playpos<(unsigned int)nmas_buffersize*2-bfshift)
	{
		writepos=0;
		dsound_bufferid=1;
	}
	else if(dsound_bufferid==1 && (playpos<(unsigned int)nmas_buffersize-bfshift || playpos>=(unsigned int)nmas_buffersize*2-bfshift))
	{
		writepos=nmas_buffersize;
		dsound_bufferid=0;
	}
	if(writepos!=-1)
	{
//		glrLog("stream: writepos=%i\n", writepos);

		NMAS_Callback(nmas_audiobuffer, nmas_buffersize);

//		for(int i=0;i<2;i++)
//		{
			void* channelbuffer=NULL;
			int size=0;
			void* buf2=NULL;
			int size2=0;
			if(!FAILED(m_secondaryBuffer->Lock(writepos*2*sizeof(short), nmas_buffersize*2*sizeof(short), (LPVOID*)&channelbuffer, (LPDWORD)&size, (LPVOID*)&buf2, (LPDWORD)&size2, 0)))
//				glrLog("lock failed\n");
//			else
			{
//				glrLog("locked 0x%08X\n", channelbuffer);
				short* channelbuffer_s=(short*)channelbuffer;
				for(int j=0;j<nmas_buffersize*2;j++)
					channelbuffer_s[j]=(short)(nmas_audiobuffer[j]*0x7FFF);
				m_secondaryBuffer->Unlock(channelbuffer, size, buf2, size2);
			}
//		}
	}
}

DWORD WINAPI DirectSoundAudioThread(LPVOID lpParam)
{
	glrLog("dsound audio thread started\n");
	while(dsound_playing)
	{
		dsound_stream();
		Sleep(1);
	}
	glrLog("dsound audio thread closed\n");
	return 0;
}

bool NMAS_OpenDirectSound(int samplerate, int buffersize, int timingbias)
{
	HRESULT result;

	dsound_timingbias=timingbias;

//	buffersize=2048;

	glrLog("NMAS_OpenDirectSound(%i, %i)\n", samplerate, buffersize);
 
	result=DirectSoundCreate8(NULL, &m_DirectSound, NULL);
	if(FAILED(result))
	{
		glrLog("failed to create dsound8\n");
		return false;
	}
 
	result=m_DirectSound->SetCooperativeLevel((HWND)glrWindowNativeHandle(), DSSCL_PRIORITY);
	if(FAILED(result))
	{
		glrLog("failed to set cooperative level\n");
		return false;
	}

	WAVEFORMATEX waveFormat;
	memset(&waveFormat, 0, sizeof(WAVEFORMATEX));
	waveFormat.wFormatTag=WAVE_FORMAT_PCM;
	waveFormat.nSamplesPerSec=samplerate;
	waveFormat.wBitsPerSample=16;
	waveFormat.nChannels=2;
	waveFormat.nBlockAlign=(waveFormat.wBitsPerSample/8)*waveFormat.nChannels;
	waveFormat.nAvgBytesPerSec=waveFormat.nSamplesPerSec*waveFormat.nBlockAlign;
	waveFormat.cbSize=0;

	DSBUFFERDESC bufferDesc;
	memset(&bufferDesc, 0, sizeof(DSBUFFERDESC));
	bufferDesc.dwSize=sizeof(DSBUFFERDESC);
//	bufferDesc.dwFlags=DSBCAPS_PRIMARYBUFFER|DSBCAPS_CTRLVOLUME;
	bufferDesc.dwFlags=DSBCAPS_PRIMARYBUFFER;
	bufferDesc.dwBufferBytes=0;
	bufferDesc.dwReserved=0;
	bufferDesc.lpwfxFormat=NULL;
	bufferDesc.guid3DAlgorithm=GUID_NULL;
	result=m_DirectSound->CreateSoundBuffer(&bufferDesc, (LPDIRECTSOUNDBUFFER*)&m_primaryBuffer, NULL);
	if(FAILED(result))
	{
		glrLog("failed to create primary buffer\n");
		return false;
	}

//	int wsize=0;
//	waveFormat.wBitsPerSample=99;
//	m_primaryBuffer->GetFormat(&waveFormat, sizeof(WAVEFORMATEX), (LPDWORD)&wsize);
//	glrLog("bits per sample: %i\n", waveFormat.wBitsPerSample);
//	return false;

	result=m_primaryBuffer->SetFormat(&waveFormat);
	if(FAILED(result))
	{
		glrLog("failed to set primary buffer format\n");
		return false;
	}

	memset(&bufferDesc, 0, sizeof(DSBUFFERDESC));
	bufferDesc.dwSize=sizeof(DSBUFFERDESC);
	bufferDesc.dwFlags=DSBCAPS_GLOBALFOCUS;
	bufferDesc.dwBufferBytes=buffersize*2*2*sizeof(short);
	bufferDesc.dwReserved=0;
	bufferDesc.lpwfxFormat=&waveFormat;
	bufferDesc.guid3DAlgorithm=GUID_NULL;
	result=m_DirectSound->CreateSoundBuffer(&bufferDesc, (LPDIRECTSOUNDBUFFER*)&m_secondaryBuffer, NULL);
	if(FAILED(result))
	{
		glrLog("failed to create secondary buffer\n");
		return false;
	}

	if(FAILED(m_secondaryBuffer->SetCurrentPosition(0)))
	{
		glrLog("failed to set position\n");
		return false;
	}
//	if(FAILED(m_secondaryBuffer->SetVolume(DSBVOLUME_MAX)))
//		return false;

	nmas_buffersize=buffersize;
	nmas_audiobuffer=new float[nmas_buffersize*2];

	dsound_playing=true;
	dsound_thread=CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DirectSoundAudioThread, (LPVOID)0, 0, NULL);

	if(FAILED(m_secondaryBuffer->Play(0, 0, DSBPLAY_LOOPING)))
	{
		glrLog("failed to play loop\n");
		return false;
	}

	return true;
}

void NMAS_CloseDirectSound()
{
	glrLog("closing directsound...\n");

	m_secondaryBuffer->Stop();

	dsound_playing=false;
	WaitForMultipleObjects(1, &dsound_thread, TRUE, INFINITE);

	if(m_primaryBuffer)
	{
		m_primaryBuffer->Release();
		m_primaryBuffer=NULL;
	}
	if(m_secondaryBuffer)
	{
		m_secondaryBuffer->Release();
		m_secondaryBuffer=NULL;
	}
	if(m_DirectSound)
	{
		m_DirectSound->Release();
		m_DirectSound=NULL;
	}

	glrLog("directsound closed\n");
}

#endif


#ifdef NMAS_ENABLE_ASIO
#include "asiosys.h"
#include "asio.h"
#include "asiodrivers.h"

bool asio_request_stop=false;

ASIODriverInfo driverInfo;
ASIOSampleRate sampleRate;
long outputLatency;
bool postOutput=false;

long asio_buffersize;
ASIOBufferInfo bufferInfos[2];
ASIOChannelInfo channelInfos[2];

ASIOCallbacks asioCallbacks;

//AsioDrivers* asio_drivers=NULL;
extern AsioDrivers* asioDrivers;
bool loadAsioDriver(char *name);
long getAsioDriverNames(char **names, long maxDrivers);

//void bufferSwitch(long index, ASIOBool processNow);
//ASIOTime *bufferSwitchTimeInfo(ASIOTime *timeInfo, long index, ASIOBool processNow);
//void sampleRateChanged(ASIOSampleRate sRate);
//long asioMessages(long selector, long value, void* message, double* opt);

ASIOTime *bufferSwitchTimeInfo(ASIOTime *timeInfo, long index, ASIOBool processNow)
{
//	glrLog("bufferswitchtimeinfo\n");

//	static long processedSamples = 0;

	// store the timeInfo for later use
//	asioDriverInfo.tInfo = *timeInfo;

	// get the time stamp of the buffer, not necessary if no
	// synchronization to other media is required
/*	if (timeInfo->timeInfo.flags & kSystemTimeValid)
		asioDriverInfo.nanoSeconds = ASIO64toDouble(timeInfo->timeInfo.systemTime);
	else
		asioDriverInfo.nanoSeconds = 0;

	if (timeInfo->timeInfo.flags & kSamplePositionValid)
		asioDriverInfo.samples = ASIO64toDouble(timeInfo->timeInfo.samplePosition);
	else
		asioDriverInfo.samples = 0;

	if (timeInfo->timeCode.flags & kTcValid)
		asioDriverInfo.tcSamples = ASIO64toDouble(timeInfo->timeCode.timeCodeSamples);
	else
		asioDriverInfo.tcSamples = 0;

	// get the system reference time
	asioDriverInfo.sysRefTime = get_sys_reference_time();
*/
/*
#if WINDOWS && _DEBUG
	// a few debug messages for the Windows device driver developer
	// tells you the time when driver got its interrupt and the delay until the app receives
	// the event notification.
	static double last_samples = 0;
	char tmp[128];
	sprintf (tmp, "diff: %d / %d ms / %d ms / %d samples                 \n", asioDriverInfo.sysRefTime - (long)(asioDriverInfo.nanoSeconds / 1000000.0), asioDriverInfo.sysRefTime, (long)(asioDriverInfo.nanoSeconds / 1000000.0), (long)(asioDriverInfo.samples - last_samples));
	OutputDebugString (tmp);
	last_samples = asioDriverInfo.samples;
#endif
*/
	// buffer size in samples
	long buffSize=asio_buffersize;

	NMAS_Callback(nmas_audiobuffer, (int)buffSize);

	// perform the processing
	for(int i=0;i<2;i++)
	{
		// OK do processing for the outputs only

		switch (channelInfos[i].type)
		{
		case ASIOSTFloat32LSB:
		{
	//			fprintf(nmas_debugfile, "correct channel %i type, fill %i samples\n", i, buffSize);
	//			glrLog("correct channel %i type, fill %i samples\n", i, buffSize);
	//					case ASIOSTFloat32LSB:		// IEEE 754 32 bit float, as found on Intel x86 architecture
	//			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 4);
			float* buffer=(float*)bufferInfos[i].buffers[index];
			for(int j=0;j<buffSize;j++)
				buffer[j]=nmas_audiobuffer[j*2+i];
			break;
		}
		case ASIOSTInt32LSB:
		{
			int* buffer=(int*)bufferInfos[i].buffers[index];
			for(int j=0;j<buffSize;j++)
				buffer[j]=(int)(nmas_audiobuffer[j*2+i]*0x7FFFFFFF);
			break;
		}
		case ASIOSTInt16LSB:
		{
			short* buffer=(short*)bufferInfos[i].buffers[index];
			for(int j=0;j<buffSize;j++)
				buffer[j]=(short)(nmas_audiobuffer[j*2+i]*0x7FFF);
			break;
		}
		default:
//			glrLog("wrong channel %i type (%i)\n", i, channelInfos[i].type);
			break;
		}

/*
	ASIOSTInt16LSB   = 16,
	ASIOSTInt24LSB   = 17,		// used for 20 bits as well
	ASIOSTInt32LSB   = 18,
	ASIOSTFloat32LSB = 19,		// IEEE 754 32 bit float, as found on Intel x86 architecture
	ASIOSTFloat64LSB = 20, 		// IEEE 754 64 bit double float, as found on Intel x86 architecture
*/


/*		switch (asioDriverInfo.channelInfos[i].type)
		{
		case ASIOSTInt16LSB:
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 2);
			break;
		case ASIOSTInt24LSB:		// used for 20 bits as well
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 3);
			break;
		case ASIOSTInt32LSB:
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 4);
			break;
		case ASIOSTFloat32LSB:		// IEEE 754 32 bit float, as found on Intel x86 architecture
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 4);
			break;
		case ASIOSTFloat64LSB: 		// IEEE 754 64 bit double float, as found on Intel x86 architecture
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 8);
			break;

			// these are used for 32 bit data buffer, with different alignment of the data inside
			// 32 bit PCI bus systems can more easily used with these
		case ASIOSTInt32LSB16:		// 32 bit data with 18 bit alignment
		case ASIOSTInt32LSB18:		// 32 bit data with 18 bit alignment
		case ASIOSTInt32LSB20:		// 32 bit data with 20 bit alignment
		case ASIOSTInt32LSB24:		// 32 bit data with 24 bit alignment
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 4);
			break;

		case ASIOSTInt16MSB:
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 2);
			break;
		case ASIOSTInt24MSB:		// used for 20 bits as well
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 3);
			break;
		case ASIOSTInt32MSB:
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 4);
			break;
		case ASIOSTFloat32MSB:		// IEEE 754 32 bit float, as found on Intel x86 architecture
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 4);
			break;
		case ASIOSTFloat64MSB: 		// IEEE 754 64 bit double float, as found on Intel x86 architecture
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 8);
			break;

			// these are used for 32 bit data buffer, with different alignment of the data inside
			// 32 bit PCI bus systems can more easily used with these
		case ASIOSTInt32MSB16:		// 32 bit data with 18 bit alignment
		case ASIOSTInt32MSB18:		// 32 bit data with 18 bit alignment
		case ASIOSTInt32MSB20:		// 32 bit data with 20 bit alignment
		case ASIOSTInt32MSB24:		// 32 bit data with 24 bit alignment
			memset (asioDriverInfo.bufferInfos[i].buffers[index], 0, buffSize * 4);
			break;
		}*/
	}

	// finally if the driver supports the ASIOOutputReady() optimization, do it here, all data are in place
	if(postOutput)
		ASIOOutputReady();
/*
	if (processedSamples >= asioDriverInfo.sampleRate * TEST_RUN_TIME)	// roughly measured
		asioDriverInfo.stopped = true;
	else
		processedSamples += buffSize;
*/
	return 0L;
}

void bufferSwitch(long index, ASIOBool processNow)
{
//	glrLog("bufferswitch\n");

	ASIOTime timeInfo;
	memset(&timeInfo, 0, sizeof(timeInfo));
	// get the time stamp of the buffer, not necessary if no
	// synchronization to other media is required
//	if(ASIOGetSamplePosition(&timeInfo.timeInfo.samplePosition, &timeInfo.timeInfo.systemTime) == ASE_OK)
//		timeInfo.timeInfo.flags = kSystemTimeValid | kSamplePositionValid;
	bufferSwitchTimeInfo(&timeInfo, index, processNow);
}

void sampleRateChanged(ASIOSampleRate sRate)
{
}

long asioMessages(long selector, long value, void* message, double* opt)
{
	long ret=0;
	switch(selector)
	{
		case kAsioSelectorSupported:
			if(value==kAsioResetRequest
			|| value==kAsioEngineVersion
			|| value==kAsioResyncRequest
			|| value==kAsioLatenciesChanged
			// the following three were added for ASIO 2.0, you don't necessarily have to support them
			|| value==kAsioSupportsTimeInfo)
//			|| value == kAsioSupportsTimeCode
//			|| value == kAsioSupportsInputMonitor)
				ret=1L;
			break;
		case kAsioResetRequest:
			// defer the task and perform the reset of the driver during the next "safe" situation
			// You cannot reset the driver right now, as this code is called from the driver.
			// Reset the driver is done by completely destruct is. I.e. ASIOStop(), ASIODisposeBuffers(), Destruction
			// Afterwards you initialize the driver again.
//			asioDriverInfo.stopped;  // In this sample the processing will just stop
			asio_request_stop=true;
			ret=1L; break;
		case kAsioResyncRequest:
			ret=1L; break;
		case kAsioLatenciesChanged:
			ret=1L; break;
		case kAsioEngineVersion:
			ret=2L; break;
		case kAsioSupportsTimeInfo:
			ret=1; break;
		case kAsioSupportsTimeCode:
			ret=0; break;
	}
	return ret;
}

bool create_asio_buffers(long buffersize)
{
	// create buffers for all inputs and outputs of the card with the 
	// preferredSize from ASIOGetBufferSize() as buffer size

	glrLog("create_asio_buffers\n");

	long i;
	ASIOError result;
	// fill the bufferInfos from the start without a gap
	ASIOBufferInfo *info=bufferInfos;
	// prepare inputs (Though this is not necessaily required, no opened inputs will work, too
//	if (asioDriverInfo->inputChannels > kMaxInputChannels)
//		asioDriverInfo->inputBuffers = kMaxInputChannels;
//	else
//		asioDriverInfo->inputBuffers = asioDriverInfo->inputChannels;
//	for(i=0;i<asioDriverInfo->inputBuffers;i++,info++)
//	{
//		info->isInput = ASIOTrue;
//		info->channelNum = i;
//		info->buffers[0] = info->buffers[1] = 0;
//	}

	// prepare outputs
/*	if(asioDriverInfo->outputChannels>kMaxOutputChannels)
		asioDriverInfo->outputBuffers=kMaxOutputChannels;
	else
		asioDriverInfo->outputBuffers=asioDriverInfo->outputChannels;*/
	for(i=0;i<2;i++,info++)
	{
		info->isInput=ASIOFalse;
		info->channelNum=i;
		info->buffers[0]=0;
		info->buffers[1]=0;

		// set preferred sample format? (float32)
//		info->type=ASIOSTFloat32LSB;
	}

	// create and activate buffers
	result=ASIOCreateBuffers(bufferInfos, 2, buffersize, &asioCallbacks);
	if(result!=ASE_OK)
	{
		glrLog("failed to create buffers\n", i);
		return false;
	}
	// now get all the buffer details, sample word length, name, word clock group and activation
	for(i=0;i<2;i++)
	{
		channelInfos[i].channel=bufferInfos[i].channelNum;
		channelInfos[i].isInput=bufferInfos[i].isInput;
//		channelInfos[i].type=ASIOSTFloat32LSB;
		result=ASIOGetChannelInfo(&channelInfos[i]);
		if(result!=ASE_OK)
		{
			glrLog("failed to get channel info %i\n", i);
			break;
		}
		long type=channelInfos[i].type;
		if(!(type==ASIOSTFloat32LSB || type==ASIOSTInt32LSB || ASIOSTInt16LSB))
		{
			glrLog("unsupported sample format\n", i);
			return false;
		}
	}

	if(result!=ASE_OK)
		return false;

	// get the input and output latencies
	// Latencies often are only valid after ASIOCreateBuffers()
	// (input latency is the age of the first sample in the currently returned audio block)
	// (output latency is the time the first sample in the currently returned audio block requires to get to the output)
	long inputLatency=0;
	result=ASIOGetLatencies(&inputLatency, &outputLatency);
	if(result!=ASE_OK)
		outputLatency=0;
	else
		glrLog("got latency %i\n", (int)outputLatency);

//				printf ("ASIOGetLatencies (input: %d, output: %d);\n", asioDriverInfo->inputLatency, asioDriverInfo->outputLatency);

	return true;
}


bool NMAS_OpenASIO(int samplerate, int buffersize)
{
	asio_request_stop=false;

	glrLog("NMAS_OpenASIO(%i, %i)\n", samplerate, buffersize);

/*	if(asioDrivers==NULL)
	{
		glrLog("asio_drivers=new AsioDrivers()\n");
		asioDrivers=new AsioDrivers();
		if(!asioDrivers)
			glrLog("failed!!\n");
	}
*/
	char* drivernames[8];
	for(int i=0;i<8;i++)
	{
		drivernames[i]=new char[32];
		drivernames[i][0]='?';
		drivernames[i][1]='\0';
	}

//	glrLog("skip\n");
//	return false;

	glrLog("getDriverNames()\n");
	int numdrivers=(int)getAsioDriverNames((char**)drivernames, 8);
	glrLog("%i drivers:\n", numdrivers);
	for(int i=0;i<numdrivers;i++)
	{
		drivernames[i][31]='\0';
		glrLog("%i: '%s'\n", i, drivernames[i]);
	}
	if(numdrivers==0)
	{
		for(int i=0;i<8;i++)
			delete[] drivernames[i];

		delete asioDrivers;
		asioDrivers=NULL;
		return false;
	}

	// load the driver, this will setup all the necessary internal data structures
	if(loadAsioDriver(drivernames[0]))
	{
		glrLog("driver loaded\n");

		// initialize the driver
		if(ASIOInit(&driverInfo)==ASE_OK)
		{
			glrLog("asioinit ok\n");

/*			printf ("asioVersion:   %d\n"
					"driverVersion: %d\n"
					"Name:          %s\n"
					"ErrorMessage:  %s\n",
					asioDriverInfo.driverInfo.asioVersion, asioDriverInfo.driverInfo.driverVersion,
					asioDriverInfo.driverInfo.name, asioDriverInfo.driverInfo.errorMessage);*/
//			if(init_asio_static_data(&asioDriverInfo)==0)
//			{
			// ASIOControlPanel(); you might want to check wether the ASIOControlPanel() can open

			if(ASIOOutputReady()==ASE_OK)
			{
				glrLog("outputready yes\n");
				postOutput=true;
			}
			else
			{
				glrLog("outputready no\n");
				postOutput=false;
			}

			ASIOSampleRate sr=(double)samplerate;
			if(ASIOCanSampleRate(sr)==ASE_OK)
			{
				ASIOSetSampleRate(sr);

				// set up the asioCallback structure and create the ASIO data buffer
				asioCallbacks.bufferSwitch=&bufferSwitch;
				asioCallbacks.sampleRateDidChange=&sampleRateChanged;
				asioCallbacks.asioMessage=&asioMessages;
				asioCallbacks.bufferSwitchTimeInfo=&bufferSwitchTimeInfo;

				long bs_min=0;
				long bs_max=0;
				long bs_preferred=0;
				long bs_granularity=0;
				ASIOGetBufferSize(&bs_min, &bs_max, &bs_preferred, &bs_granularity);

				// try to use given buffersize if allowed (or nearest value, power of two if granularity is -1)
				if(buffersize==0)
				{
					buffersize=bs_preferred;
				}
				else if(bs_granularity==-1)
				{
					int bs=bs_min;
					int nearest=bs;
					int nearest_diff=100000;
					while(bs<=bs_max && bs<44100/5)
					{
						int d=abs(bs-buffersize);
						if(d<nearest_diff)
						{
							nearest_diff=d;
							nearest=bs;
						}
						bs*=2;
					}
					buffersize=nearest;
				}
				else
				{
					int bs=bs_min;
					int nearest=bs;
					int nearest_diff=100000;
					while(bs<=bs_max && bs<44100/5)
					{
						int d=abs(bs-buffersize);
						if(d<nearest_diff)
						{
							nearest_diff=d;
							nearest=bs;
						}
						bs+=bs_granularity;
					}
					buffersize=nearest;
				}

				if(create_asio_buffers(buffersize))
				{
					glrLog("createbuffers ok\n");

					asio_buffersize=buffersize;
					nmas_buffersize=(int)asio_buffersize;
					nmas_audiobuffer=new float[nmas_buffersize*2];
					glrLog("using buffersize %i\n", nmas_buffersize);

					if(ASIOStart()==ASE_OK)
					{
						glrLog("asiostart ok\n");
						for(int i=0;i<8;i++)
							delete[] drivernames[i];
						return true;
					}
					else
						glrLog("asiostart failed\n");
				}
			}
			else
				glrLog("can't samplerate %i\n", samplerate);
//			}
		}
		else
			glrLog("asioinit failed\n");

	}
	else
		glrLog("driver not loaded\n");

	for(int i=0;i<8;i++)
		delete[] drivernames[i];
	return false;
}

void NMAS_CloseASIO()
{
	ASIOStop();
	ASIODisposeBuffers();
	ASIOExit();
	asioDrivers->removeCurrentDriver();
//	delete asioDrivers;
//	asioDrivers=NULL;
}

#endif


#ifdef NMAS_ENABLE_SDL

void NMAS_CallbackSDL(void *userdata, unsigned char *stream, int len)
{
	len /= sizeof(float) * 2; // 32-bit float, stereo
	if(len <= 0)
	{
		glrLog("sdl audio callback requested zero samples!\n");
	}
	NMAS_Callback((float*) stream, len);
}

static SDL_AudioDeviceID nmas_sdl_device_id;

// Origin: http://stackoverflow.com/a/10143264/6327867
/**
 * return the smallest power of two value
 * greater than x
 *
 * Input range:  [2..2147483648]
 * Output range: [2..2147483648]
 *
 */
static inline uint32_t next_pow2(uint32_t x)
{
#if 0
	assert(x > 1);
	assert(x <= ((UINT32_MAX/2) + 1));
#endif
	return 1 << (32 - __builtin_clz(x - 1));
}

bool NMAS_OpenSDL(int samplerate, int buffersize, int timingbias)
{
	glrLog("NMAS_OpenSDL(%i, %i, %i)\n", samplerate, buffersize, timingbias);
	
	if(buffersize < 512)
	{
		glrLog("request audio buffer size %d is below 512 - using this size instead\n", buffersize);
		buffersize = 512;
	}

	if(SDL_InitSubSystem(SDL_INIT_AUDIO))
	{
		glrLog("failed to init SDL audio subsystem\n");
		return false;
	}

	int numAudioDrivers = SDL_GetNumAudioDrivers();
	
	glrLog("%d audio backends compiled into SDL:", numAudioDrivers);
	for(int i = 0; i < numAudioDrivers; i++)
	{
		glrLog(" \'%s\'", SDL_GetAudioDriver(i));
	}
	glrLog("\n");
	
	for(int i = 0; i < numAudioDrivers; i++)
	{
		if(!SDL_AudioInit(SDL_GetAudioDriver(i)))
		{
			break;
		}
		else if(i == numAudioDrivers-1)
		{
			glrLog("failed to init audio: %s\n", SDL_GetError());
			return false;
		}
	}
	
	SDL_AudioSpec want, have;
	SDL_zero(want);
	want.freq = samplerate;
	want.format = AUDIO_F32;
	want.channels = 2;
	want.samples = next_pow2(buffersize);
	want.callback = NMAS_CallbackSDL;
	want.userdata = nullptr;

	nmas_sdl_device_id = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE);
	if(!nmas_sdl_device_id)
	{
		glrLog("failed to open audio device: %s\n", SDL_GetError());
		return false;
	}
	
	if(want.freq != have.freq)
	{
		glrLog("requested samplerate: %d, obtained: %d\n", want.freq, have.freq);
	}
	
	SDL_PauseAudioDevice(nmas_sdl_device_id, 0);
	
	return true;
}

void NMAS_CloseSDL()
{
	glrLog("closing SDL audio...\n");
	
	SDL_CloseAudioDevice(nmas_sdl_device_id);
	
	glrLog("SDL audio closed\n");
}

#endif

#if defined(NMAS_ENABLE_CORE_AUDIO)
extern "C"
{
	int NMAS_OpenCoreAudio(int samplerate, int buffersize, int timingbias);
	void NMAS_CloseCoreAudio(void);
}
#endif

bool NMAS_Init()
{
	glrLog("NMAS_Init()\n");
	nmas_initialized=true;
	return true;
}

void NMAS_Exit()
{
	glrLog("NMAS_Exit()\n");

#ifdef NMAS_ENABLE_SDL
	SDL_Quit();
#endif
}

int NMAS_Open(int samplerate, int buffersize, int timingbias, int prefer_asio)
{
	if(!nmas_initialized && !NMAS_Init())
		return false;

	glrLog("NMAS_Open(%i, %i)\n", samplerate, buffersize);

//	nmas_audiobuffer=new float[buffersize*2];

#ifdef NMAS_ENABLE_ASIO
	if(prefer_asio && NMAS_OpenASIO(samplerate, buffersize))
	{
		nmas_mode=NMAS_MODE_ASIO;
		glrLog("nmas_mode=NMAS_MODE_ASIO\n");
		return true;
	}
#else
	(void)prefer_asio;
#endif

#ifdef NMAS_ENABLE_DIRECTSOUND
	if(NMAS_OpenDirectSound(samplerate, buffersize, timingbias))
	{
		nmas_mode=NMAS_MODE_DIRECTSOUND;
		glrLog("nmas_mode=NMAS_MODE_DIRECTSOUND\n");
		return true;
	}
#endif

#ifdef NMAS_ENABLE_SDL
	if(NMAS_OpenSDL(samplerate, buffersize, timingbias))
	{
		nmas_mode=NMAS_MODE_SDL;
		glrLog("nmas_mode=NMAS_MODE_SDL\n");
		return true;
	}
#endif

#ifdef NMAS_ENABLE_CORE_AUDIO
	if(NMAS_OpenCoreAudio(samplerate, buffersize, timingbias))
	{
		nmas_mode=NMAS_MODE_CORE_AUDIO;
		return true;
	}
#endif

	glrLog("NMAS_Init failed\n");

//	delete[] nmas_audiobuffer;
//	nmas_audiobuffer=NULL;
	return false;
}

void NMAS_Close(int exit)
{
	if(nmas_mode==0)
		return;

	glrLog("NMAS_Close()\n");

#ifdef NMAS_ENABLE_DIRECTSOUND
	if(nmas_mode==NMAS_MODE_DIRECTSOUND)
		NMAS_CloseDirectSound();
#endif

#ifdef NMAS_ENABLE_ASIO
	if(nmas_mode==NMAS_MODE_ASIO)
		NMAS_CloseASIO();
#endif

#ifdef NMAS_ENABLE_SDL
	if(nmas_mode==NMAS_MODE_SDL)
		NMAS_CloseSDL();
#endif

#ifdef NMAS_ENABLE_CORE_AUDIO
	if(nmas_mode==NMAS_MODE_CORE_AUDIO)
		NMAS_CloseCoreAudio();
#endif

#if !defined(NMAS_ENABLE_SDL) && !defined(NMAS_ENABLE_CORE_AUDIO)
	delete[] nmas_audiobuffer;
	nmas_audiobuffer=NULL;
#endif
	nmas_mode=NMAS_MODE_CLOSED;

	if(exit)
	{
		NMAS_Exit();
		nmas_initialized=false;
	}
}


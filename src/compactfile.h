//	void WriteGap(Bitstream& bs, gaplength)
//	{
//	}
void MapGraph::WriteCompact(Bitstream& bs)
{
//		glrLog("[@%05i] BlockGraph.WriteCompact (content_type=%i)\n", bs.BitPos(), content_type);

//		bs.AppendValue(content_type, 2);

	if(Empty())
	{
		bs.AppendValue(0, 1);
		return;
	}
	else
		bs.AppendValue(1, 1);

	int range=48;
	int i0=0;
	int i1=48;
	int bitsize=4;

	if(content_type!=0)
	{
		range=0;
		i0=FindNext(-1);
		i1=FindPrevious(0x10000);
		if(i0!=NONE_POS && i1!=NONE_POS)
		{
			i0/=8;
			i1/=8;

			i1+=1;
			range=i1-i0;
		}
		if(range>0xFFFF/8)
		{
			range=0xFFFF/8;
			i0=0;
			i1=0xFFFF/8;
		}
		bitsize=6;
		if(content_type!=0)
		{
			bitsize=bs.BitWidth(i1);
			bs.AppendValue(bitsize-1, 4);
		}
		unsigned short wordval=(unsigned short)range;
		bs.AppendValue(wordval, bitsize);
		if(range==0)
			return;

//		glrLog("write graph: range=%i (%i-%i)\n", range, i0, i1);

		wordval=(unsigned short)i0;
//		bb.WriteBytes(&wordval, 2);
		bs.AppendValue(i0, bitsize);

		i0*=8;
		i1*=8;
		range*=8;
	}

	bitsize=4;
	int point_floor=0;
	if(content_type==2) // only CH content graphs have wide range of values that can be compressed
	{
		point_floor=1000;
//		glrLog("graph points: ");
		for(int i=i0;i<i1;i++)
		{
			GraphPoint gp=Read(i);
			if(!gp.Empty())
			{
				if(gp.trigger!=0xFF && gp.trigger<point_floor)
					point_floor=gp.trigger;
				if(gp.curve!=0xFF && gp.curve<point_floor)
					point_floor=gp.curve;
			}
		}

		if(point_floor==1000)
			point_floor=0;
		bs.AppendValue(point_floor, 7);

		bitsize=1;
		for(int i=i0;i<i1;i++)
		{
			GraphPoint gp=Read(i);
			if(!gp.Empty())
			{
				if(gp.trigger!=0xFF)
				{
					bitsize=std::max(bitsize, bs.BitWidth(gp.trigger-point_floor));
//					glrLog("T%i ", gp.trigger);
				}
				if(gp.curve!=0xFF)
				{
					bitsize=std::max(bitsize, bs.BitWidth(gp.curve-point_floor));
//					glrLog("C%i ", gp.curve);
				}
			}
		}
		bs.AppendValue(bitsize-1, 3);
	}


//		glrLog("[@%05i] write graph: range=%i (%i-%i) bitsize %i\n", bs.BitPos(), range, i0, i1, bitsize);

//		glrLog("[@%05i] write: ---------- content_type %i\n", bs.BitPos(), content_type);

	int predict_phase=-1;
	int predict_gap=-1;
	//int predict_point=-1;

	int prev_point_value=point_floor;
	int prev_i=i0;
//		unsigned char gapcode=0xFE;
	for(int i=i0;i<i1;i++)
	{
//			while(i-prev_i>=256)
/*			if(content_type==0)
		{
			while(i-prev_i>=64)
			{
//					bs.AppendValue(0, 1); // don't predict
				bs.AppendValue(0, 2); // code 0: long gap
				bs.AppendValue(64-1, 6);
				prev_i+=64;

//					predict_phase=1;
//					predict_gap=64;

//					glrLog("[@%05i] write: gap %i\n", bs.BitPos(), 64);
			}
		}
		else
		{
			while(i-prev_i>=128)
			{
				bs.AppendValue(0, 1); // don't predict
				bs.AppendValue(0, 2); // code 0: long gap
				bs.AppendValue(128-1, 7);
				prev_i+=128;

				predict_phase=1;
				predict_gap=128;

//					glrLog("[@%05i] write: gap %i\n", bs.BitPos(), 128);
			}
		}
		*/
//			glrLog("read(%i)\n", i);
		GraphPoint gp=Read(i);
		if(!gp.Empty())
		{
//					bb.WriteBytes(&gapcode, 1);
			int gaplength=i-prev_i;
//				bb.WriteBytes(&gaplength, 1);
/*				while(content_type!=0 && gaplength>127)
			{
				bs.AppendValue(0, 1); // don't predict
				bs.AppendValue(0, 2); // code 0: long gap
				bs.AppendValue(127, 7); // escape code for superlong gap
				int gap=min(gaplength, 0x1000);
				glrLog("write: superlong gap C1 %i, %i->%i\n", gap, prev_i, prev_i+gap);
				bs.AppendValue(gap-1, 12); // superlong gap
				gaplength-=gap;
				prev_i+=gap;
				predict_phase=1;
				predict_gap=gaplength;
			}*/
			while(content_type!=0 && gaplength>127)
			{
				bs.AppendValue(0, 1); // don't predict
				bs.AppendValue(0, 2); // code 0: long gap
/*					if(gaplength<=255) // better to just put in a long gap and let the remainder be caught normally
				{
					int gap=min(gaplength, 127);
					bs.AppendValue(gap-1, 12);
					gaplength-=gap;
					prev_i+=gap;
					predict_phase=1;
					predict_gap=gap;
				}
				else*/
				{
					bs.AppendValue(127, 7); // escape code for superlong gap
					int gap=std::min(gaplength, 0x1000);
//						glrLog("write: superlong gap C1 %i, %i->%i\n", gap, prev_i, prev_i+gap);
					bs.AppendValue(gap-1, 12); // superlong gap
					gaplength-=gap;
					prev_i+=gap;
					predict_phase=1;
					predict_gap=gap;
				}
			}
			if(gaplength>0)
			{
				if(content_type==0)
				{
					// gaplength can't be over 48

//						if(predict_phase==0 && predict_gap==gaplength)
//							bs.AppendValue(1, 1); // predict
//						else
//						{
//							bs.AppendValue(0, 1); // don't predict
						if(gaplength<=8)
						{
							bs.AppendValue(1, 2); // code 1: short gap
							bs.AppendValue(gaplength-1, 3);
//							glrLog("i=%03i  code 1: short gap %i\n", i+gaplength, gaplength);
						}
						else
						{
							bs.AppendValue(0, 2); // code 0: long gap
							bs.AppendValue(gaplength-1, 6);
//							glrLog("i=%03i  code 0: long gap %i\n", i+gaplength, gaplength);
						}
//						}

//						glrLog("[@%05i] write: gap %i\n", bs.BitPos(), gaplength);
				}
				else
				{
					if(predict_phase==0 && predict_gap==gaplength)
					{
						bs.AppendValue(1, 1); // predict
//							glrLog("[@%05i] write: predict gap %i\n", bs.BitPos(), gaplength);
					}
					else
					{
						bs.AppendValue(0, 1); // don't predict
						if((gaplength+1)/4<=8 && ((gaplength+1)&3)==0)
						{
							bs.AppendValue(1, 2); // code 1: short gap
							bs.AppendValue((gaplength+1)/4-1, 3);
//							glrLog("i=%03i  code 1: short gap %i\n", i+gaplength, gaplength);
						}
						else
						{
							bs.AppendValue(0, 2); // code 0: long gap
							bs.AppendValue(gaplength-1, 7);
							if(gaplength-1==127) // accidentally equal to escape code
							{
								bs.AppendValue(gaplength-1, 12); // need to put it in as superlong gap too
//									glrLog("write: superlong gap C2 %i, %i->%i\n", gaplength, prev_i, prev_i+gaplength);
							}
//							glrLog("i=%03i  code 0: long gap %i\n", i+gaplength, gaplength);
						}

//							glrLog("[@%05i] write: gap %i\n", bs.BitPos(), gaplength);
					}
					predict_phase=1;
					predict_gap=gaplength;
				}
				prev_i+=gaplength;
			}
/*				if(gaplength!=0)
			{
				gaplength=0; // gap of zero indicates point value follows
//					bb.WriteBytes(&gaplength, 1);
				bs.AppendValue(gaplength, 8);
			}*/

			bool predict_ok=false;
			if(content_type!=0)
			{
//					if(predict_phase==1 && ((predict_point==0 && gp.trigger!=0xFF && gp.curve==0xFF) || (predict_point==1 && gp.trigger==0xFF && gp.curve!=0xFF)))
				if(predict_phase==1)
				{
					bs.AppendValue(1, 1); // predict
					predict_ok=true;

//					predict_point=-1;
					if(gp.trigger!=0xFF)
						bs.AppendValue(1, 1);
					else
						bs.AppendValue(0, 1);
				}
				else
					bs.AppendValue(0, 1); // don't predict

				predict_phase=0;
//					if(predict_ok)
//						glrLog("[@%05i] write: predict point %i\n", bs.BitPos(), predict_point);
			}

			int code=2;
			if(gp.trigger!=0xFF)
				code=3;
			if(!predict_ok)
				bs.AppendValue(code, 2); // code 2: point, second bit is trigger mask

//				glrLog("[@%05i] write: point i=%i\n", bs.BitPos(), i);

//				glrLog("i=%03i  code %i: ", i, code);
			if(content_type!=1) // ST has no relevant point info, they're all 1-valued triggers
			{
//					if(!predict_ok)
					bs.AppendValue(gp.curve!=0xFF?1:0, 1); // curve mask
				if(gp.trigger!=0xFF)
				{
					int value=gp.trigger;
					if(content_type==2)
					{
						if(value==prev_point_value)
							bs.AppendValue(0, 1);
						else
						{
							bs.AppendValue(1, 1);
							bs.AppendValue(value-point_floor, bitsize);
							prev_point_value=value;
						}
					}
					else
						bs.AppendValue(value-point_floor, bitsize);

/*						if(gp.curve!=0xFF) // this point also has curve value, append another code block for it
					{
						code=2;
						bs.AppendValue(code, 2); // code 2: point, second bit is trigger mask
					}*/
				}
				if(gp.curve!=0xFF)
				{
					int value=gp.curve;
					if(content_type==2)
					{
						if(value==prev_point_value)
							bs.AppendValue(0, 1);
						else
						{
							bs.AppendValue(1, 1);
							bs.AppendValue(value-point_floor, bitsize);
							prev_point_value=value;
						}
					}
					else
						bs.AppendValue(value-point_floor, bitsize);
				}
			}
//				glrLog("\n");
//				glrLog("point[%i]=[%i,%i]\n", i, gp.trigger, gp.curve);
			prev_i=i+1;
//				prev_i=i;
		}
		else if(i==i1-1)
		{
			i++;
/*				if(content_type==0)
			{
				while(i-prev_i>=64)
				{
//						bs.AppendValue(0, 1); // don't predict
					bs.AppendValue(0, 2); // code 0: long gap
					bs.AppendValue(64-1, 6);
					prev_i+=64;

//						glrLog("[@%05i] write: gap %i\n", bs.BitPos(), 64);
				}
			}
			else
			{
				while(i-prev_i>=128)
				{
					bs.AppendValue(0, 1); // don't predict
					bs.AppendValue(0, 2); // code 0: long gap
					bs.AppendValue(128-1, 7);
					prev_i+=128;

//						glrLog("[@%05i] write: gap %i\n", bs.BitPos(), 128);
				}
			}*/
			int gaplength=i-prev_i;
			while(content_type!=0 && gaplength>127)
			{
				bs.AppendValue(0, 1); // don't predict
				bs.AppendValue(0, 2); // code 0: long gap
/*					if(gaplength<=255) // better to just put in a long gap and let the remainder be caught normally
				{
					int gap=min(gaplength, 127);
					bs.AppendValue(gap-1, 12);
					gaplength-=gap;
					prev_i+=gap;
				}
				else*/
				{
					bs.AppendValue(127, 7); // escape code for superlong gap
					int gap=std::min(gaplength, 0x1000);
//						glrLog("write: superlong gap E1 %i, %i->%i\n", gap, prev_i, prev_i+gap);
					bs.AppendValue(gap-1, 12); // superlong gap
					gaplength-=gap;
					prev_i+=gap;
				}
			}
			if(gaplength>0)
			{
				if(content_type==0)
				{
//						bs.AppendValue(0, 1); // don't predict
					if(gaplength<=8)
					{
						bs.AppendValue(1, 2); // code 1: short gap
						bs.AppendValue(gaplength-1, 3);
					}
					else
					{
						bs.AppendValue(0, 2); // code 0: long gap
						bs.AppendValue(gaplength-1, 6);
					}

//						glrLog("[@%05i] write: gap %i\n", bs.BitPos(), gaplength);
				}
				else
				{
					bs.AppendValue(0, 1); // don't predict
					if((gaplength+1)/4<=8 && ((gaplength+1)&3)==0)
					{
						bs.AppendValue(1, 2); // code 1: short gap
						bs.AppendValue((gaplength+1)/4-1, 3);
					}
					else
					{
						bs.AppendValue(0, 2); // code 0: long gap
						bs.AppendValue(gaplength-1, 7);
						if(gaplength-1==127) // accidentally equal to escape code
						{
							bs.AppendValue(gaplength-1, 12); // need to put it in as superlong gap too
//								glrLog("write: superlong gap E2 %i, %i->%i\n", gaplength, prev_i, prev_i+gaplength);
						}
					}

//						glrLog("[@%05i] write: gap %i\n", bs.BitPos(), gaplength);
				}
				prev_i+=gaplength;
			}
		}
	}
//		glrLog("[@%05i] write graph done\n", bs.BitPos());
}
void MapGraph::ReadCompact(Bitstream& bs)
{
	Clear();

//		glrLog("[@%05i] BlockGraph.ReadCompact (content_type=%i)\n", bs.BitPos(), content_type);

//		int content_type=bs.ReadValue(2); // 0=sound envelope, 1=ST, 2=CH, 3=chenv

	if(bs.ReadValue(1)==0) // empty
		return;

	int bitsize=4;
	int i=0;
	int i1=48;
	int range=48;
	if(content_type!=0)
	{
		bitsize=bs.ReadValue(4)+1;
		range=bs.ReadValue(bitsize)*8;
		if(range==0)
			return;
		i=bs.ReadValue(bitsize)*8;
		i1=i+range;
	}

	bitsize=4;
	int point_floor=0;
	if(content_type==2)
	{
		point_floor=bs.ReadValue(7);
		bitsize=bs.ReadValue(3)+1;
	}

	int predict_phase=-1;
	int predict_gap=-1;
	int predict_point=-1;

//		glrLog("[@%05i] read: ---------- content_type %i\n", bs.BitPos(), content_type);

	int prev_point_value=point_floor;
//		glrLog("[@%05i] read graph: range=%i (%i-%i) bitsize %i\n", bs.BitPos(), range, i, i1, bitsize);
//		int bail=0;
	while(i<i1)
	{
/*			bail++;
		if(bail>500)
		{
			glrLog("BAIL\n");
		}*/

		bool createpoint=false;
		int code=0;

		bool predict_ok=false;
		if(content_type!=0 && bs.ReadValue(1)) // predict
			predict_ok=true;
		if(!predict_ok)
		{
			if(content_type!=0)
				debug_file_stats_predictwaste++;

			code=bs.ReadValue(2);
//				glrLog("[@%05i] read: code %i\n", bs.BitPos(), code);
			debug_file_stats_graphcode+=2;
//			glrLog("i=%03i  code %i: ", i, code);
			if(code==0)
			{
				int b=7;
				if(content_type==0)
					b=6;
				int gaplength=bs.ReadValue(b)+1;
				if(gaplength-1==127)
				{
					gaplength=bs.ReadValue(12)+1; // superlong gap
//						glrLog("read: superlong gap %i, %i->%i\n", gaplength, i, i+gaplength);
					debug_file_stats_graphgaplong+=12;
				}
				debug_file_stats_graphgaplong+=b;
				i+=gaplength;

//					glrLog("[@%05i] read: gap %i\n", bs.BitPos(), gaplength);

//				glrLog("long gap %i\n", gaplength);

				predict_gap=gaplength;
				predict_phase=1;
			}
			if(code==1)
			{
				int gaplength=0;
				if(content_type==0)
					gaplength=bs.ReadValue(3)+1;
				else
					gaplength=(bs.ReadValue(3)+1)*4-1;
				debug_file_stats_graphgapshort+=3;
				i+=gaplength;
//				glrLog("short gap %i\n", gaplength);

//					glrLog("[@%05i] read: gap %i\n", bs.BitPos(), gaplength);

				predict_gap=gaplength;
				predict_phase=1;
			}
			if(code&2)
				createpoint=true;
		}

		if(predict_ok)
		{
			if(predict_phase==0)
			{
//					glrLog("[@%05i] read: predict gap %i\n", bs.BitPos(), predict_gap);
				i+=predict_gap;
				predict_phase=1;
			}
			else
			{
//					glrLog("[@%05i] read: predict point %i\n", bs.BitPos(), predict_point);
				createpoint=true;
				if(bs.ReadValue(1))
					code=1;
				else
					code=0;
			}
		}

		if(createpoint)
		{
//				glrLog("[@%05i] read: point i=%i\n", bs.BitPos(), i);

//				GraphPoint gp=Read(i);
			GraphPoint gp;
			gp.Clear();
			if(content_type==1) // ST only has value-less triggers
			{
				gp.trigger=1;

				predict_point=0;
			}
			else
			{
				bool trigger=(code&1)!=0;
				bool curve=false;
//					if(!predict_ok)
					curve=bs.ReadValue(1)!=0;
//					else if(predict_point==1)
//						curve=true;
				debug_file_stats_graphpoint+=1;
				if(trigger)
				{
					int value=prev_point_value;
					if(content_type==2)
					{
						debug_file_stats_graphpoint+=1;
						if(bs.ReadValue(1))
						{
							value=bs.ReadValue(bitsize)+point_floor;
							debug_file_stats_graphpoint+=bitsize;
							prev_point_value=value;
						}
					}
					else
					{
						value=bs.ReadValue(bitsize)+point_floor;
						debug_file_stats_graphpoint+=bitsize;
					}
					gp.trigger=value;

					predict_point=0;
				}
				if(curve)
				{
					int value=prev_point_value;
					if(content_type==2)
					{
						debug_file_stats_graphpoint+=1;
						if(bs.ReadValue(1))
						{
							value=bs.ReadValue(bitsize)+point_floor;
							debug_file_stats_graphpoint+=bitsize;
							prev_point_value=value;
						}
					}
					else
					{
						value=bs.ReadValue(bitsize)+point_floor;
						debug_file_stats_graphpoint+=bitsize;
					}
					gp.curve=value;

					predict_point=1;
				}
			}
			SetPoint(i, gp);
//				glrLog("point[%i]=[%i,%i]\n", i, gp.trigger, gp.curve);
			i++;

			predict_phase=0;
		}
//			glrLog("\n");
	}
//		glrLog("[@%05i] read graph done\n", bs.BitPos());
}


void NMSound::WriteCompact(Bitstream& bs)
{
//		glrLog("[@%05i] NMSound.WriteCompact\n", bs.BitPos());

//		bs.AppendValue(color, 2);
//		bs.AppendValue(name, 6);
	bs.AppendValue((unsigned)waveform, 3);
	bs.AppendValue(wavemode, 2);
	bs.AppendValue(wavemode2, 2);
	bs.AppendValue(wavemode_pos, 6);
	if(volume==15)
		bs.AppendValue(0, 1);
	else
	{
		bs.AppendValue(1, 1);
		bs.AppendValue(volume, 4);
	}
	if(vibrato_strength==0)
		bs.AppendValue(0, 1);
	else
	{
		bs.AppendValue(1, 1);
		bs.AppendValue(vibrato_strength, 4);
		bs.AppendValue(vibrato_delay, 4);
		bs.AppendValue(vibrato_speed-1, 4);
	}
//		glrLog("[@%05i] NMSound A\n", bs.BitPos());

	if(detune==0)
		bs.AppendValue(0, 1);
	else
	{
		bs.AppendValue(1, 1);
		bs.AppendValue(detune, 8);
	}

//		glrLog("[@%05i] NMSound B\n", bs.BitPos());

	bs.AppendValue(fixedpitch?1:0, 1);
	if(fixedpitch)
		bs.AppendValue(basepitch, 7);

//		glrLog("[@%05i] NMSound C\n", bs.BitPos());

	if(p_env_graph->Empty())
		bs.AppendValue(0, 1);
	else
	{
		bs.AppendValue(1, 1);
		bs.AppendValue(pitchrange-1, 4);
		bs.AppendValue(p_env_speed[0], 4);
		bs.AppendValue(p_env_speed[1], 4);
		bs.AppendValue(p_env_speed[2], 4);
		p_env_graph->WriteCompact(bs);
	}

//		glrLog("[@%05i] NMSound D\n", bs.BitPos());

	if(v_env_graph->Empty() && v_env_speed[0]==0 && v_env_speed[1]==0 && v_env_speed[2]==0)
		bs.AppendValue(0, 1);
	else
	{
		bs.AppendValue(1, 1);
		bs.AppendValue(v_env_speed[0], 4);
		bs.AppendValue(v_env_speed[1], 4);
		bs.AppendValue(v_env_speed[2], 4);
		v_env_graph->WriteCompact(bs);
	}
//		glrLog("[@%05i] NMSound E\n", bs.BitPos());

	if(waveform==Type::DMC)
		dpcm_sample.WriteCompact(bs);
}
void NMSound::ReadCompact(Bitstream& bs)
{
	Clear();

//		glrLog("[@%05i] NMSound.ReadCompact\n", bs.BitPos());

	if(song_file_version>=1 && song_file_version<4)
	{
		color=bs.ReadValue(2);
		name=bs.ReadValue(6);
	}
	else
	{
		color=1;
		name=0;
	}
	waveform=(Type)bs.ReadValue((song_file_version>=8)?3:2);
	wavemode=bs.ReadValue(2);
	if(song_file_version>=13)
	{
		wavemode2=bs.ReadValue(2);
		wavemode_pos=bs.ReadValue(6);
	}
	else
	{
		if(waveform==Type::SQUARE1 || waveform==Type::SQUARE2)
			wavemode=2-wavemode;
		wavemode2=wavemode;
		wavemode_pos=wavemode_pos_default;
	}

	if(bs.ReadValue(1))
		volume=bs.ReadValue(4);
	else
		volume=15;

	// triangle constraints
	if(waveform==Type::TRIANGLE)
	{
		wavemode=wavemode2=1; // extra triangle has been removed
		volume=15; // volume forced to 15
	}

	if(bs.ReadValue(1))
	{
		vibrato_strength=bs.ReadValue(4);
		vibrato_delay=bs.ReadValue(4);
		vibrato_speed=bs.ReadValue(4)+1;
	}
	else
		vibrato_strength=0;

//		glrLog("[@%05i] NMSound A\n", bs.BitPos());

	if(bs.ReadValue(1))
		detune=bs.ReadValue(8);

//		glrLog("[@%05i] NMSound B\n", bs.BitPos());

	fixedpitch=bs.ReadValue(1)!=0;
	if(fixedpitch)
		basepitch=bs.ReadValue(7);

//		glrLog("[@%05i] NMSound C\n", bs.BitPos());

	p_env_graph->Clear();
	if(bs.ReadValue(1))
	{
		pitchrange=bs.ReadValue(4)+1;
		p_env_speed[0]=bs.ReadValue(4);
		p_env_speed[1]=bs.ReadValue(4);
		p_env_speed[2]=bs.ReadValue(4);
		p_env_graph->ReadCompact(bs);
	}
	else
	{
		p_env_speed[0]=0;
		p_env_speed[1]=0;
		p_env_speed[2]=0;
	}

//		glrLog("[@%05i] NMSound D\n", bs.BitPos());

	v_env_graph->Clear();
	if(bs.ReadValue(1))
	{
		v_env_speed[0]=bs.ReadValue(4);
		v_env_speed[1]=bs.ReadValue(4);
		v_env_speed[2]=bs.ReadValue(4);
		v_env_graph->ReadCompact(bs);
	}
	else
	{
		v_env_speed[0]=0;
		v_env_speed[1]=0;
		v_env_speed[2]=0;
	}

//		glrLog("[@%05i] NMSound E\n", bs.BitPos());

	if(song_file_version>=8 && waveform==Type::DMC)
		dpcm_sample.ReadCompact(bs);

	active=true;
}


void NMChannel::WriteCompact(Bitstream& bs) const
{
	bs.AppendValue(volume, 4);
	if(type==Type::CH)
	{
		if(notelength>8)
		{
			bs.AppendValue(1, 1);
			bs.AppendValue(notelength-1, 7);
		}
		else
		{
			bs.AppendValue(0, 1);
			bs.AppendValue(notelength-1, 3);
		}
		if(notelength_adjust!=0)
		{
			bs.AppendValue(1, 1);
			if(notelength_adjust>0)
				bs.AppendValue(notelength_adjust-1, 3);
			else
				bs.AppendValue(3-notelength_adjust, 3);
		}
		else
			bs.AppendValue(0, 1);
		if(arp_size==1)
			bs.AppendValue(0, 1);
		else
		{
			bs.AppendValue(1, 1);
			bs.AppendValue(arp_size-1, 3);
			bs.AppendValue(arp_speed-1, 4);
			bs.AppendValue(arp_dir, 1);
		}
		bs.AppendValue(time_shift+128, 8);
		bs.AppendValue(note_source+1, 7);
	}
	if(type==Type::ST)
	{
		if(basepitch==3*12)
			bs.AppendValue(0, 1);
		else
		{
			bs.AppendValue(1, 1);
			bs.AppendValue(basepitch, 7);
		}
	}

	char num_sounds=0;
	for(int i=0;i<SOUNDS_MAX;i++)
		if(sounds[i])
			num_sounds++;
	bs.AppendValue(num_sounds, 8);
	for(char i=0;i<SOUNDS_MAX;i++)
		if(sounds[i])
			bs.AppendValue(i, 8);

	if(content->Empty())
		bs.AppendValue(0, 1);
	else
	{
		bs.AppendValue(1, 1);
		content->WriteCompact(bs);
	}
	if(envelope->Empty())
		bs.AppendValue(0, 1);
	else
	{
		bs.AppendValue(1, 1);
		envelope->WriteCompact(bs);
	}
}

void NMChannel::ReadCompact(Bitstream& bs)
{
	if(song_file_version<4)
	{
		type=(Type)bs.ReadValue(1);
		color=bs.ReadValue(2);
	}
	volume=bs.ReadValue(4);
	if(type==Type::CH)
	{
		if(bs.ReadValue(1))
			notelength=bs.ReadValue(7)+1;
		else
			notelength=bs.ReadValue(3)+1;
		if(bs.ReadValue(1))
		{
			notelength_adjust=bs.ReadValue(3)+1;
			if(notelength_adjust>4)
				notelength_adjust=-(notelength_adjust-4);
		}
		if(bs.ReadValue(1))
		{
			arp_size=bs.ReadValue(3)+1;
			arp_speed=bs.ReadValue(4)+1;
			arp_dir=bs.ReadValue(1);
		}
		if(song_file_version>=10)
		{
			time_shift=bs.ReadValue(8)-128;
			note_source=bs.ReadValue(7)-1;
		}
	}
	if(type==Type::ST)
	{
		basepitch=3*12;
		if(bs.ReadValue(1))
			basepitch=bs.ReadValue(7);
	}

	char num_sounds=0;
	num_sounds=bs.ReadValue(8);
	for(char ii=0;ii<num_sounds;ii++)
	{
		char i=0;
		i=bs.ReadValue(8);
		sounds[i]=true;
	}

	if(bs.ReadValue(1))
		content->ReadCompact(bs);
	if(bs.ReadValue(1))
		envelope->ReadCompact(bs);

	active=true;
}


void NMSong::WriteCompact(unsigned char** buffer, long* size)
{
	Bitstream bs;
	int bitsize;

	char version=NM_SONG_VERSION;
	bs.AppendValue(version, 4);

	song_file_version=version;

	char num_parts=0;
	for(int i=0;i<PARTS_MAX;i++)
	{
		if(song_parts[i]!=NULL)
			num_parts++;
	}
	bs.AppendValue(num_parts, 7);

	for(int pi=0;pi<PARTS_MAX;pi++)
	{
		const auto* p=song_parts[pi].get();
		if(!p) continue;

		bs.AppendValue(p->tempo, 9);
		bs.AppendValue(p->timesig2, 3);
		bs.AppendValue(p->volume, 4);

		bitsize=bs.BitWidth(std::max(p->mark_start, std::max(p->mark_end, p->mark_loop)));
		bs.AppendValue(bitsize-1, 4);
		bs.AppendValue(p->mark_start, bitsize);
		bs.AppendValue(p->mark_end, bitsize);
		bs.AppendValue(p->mark_loop, bitsize);

		bs.AppendValue(p->swing_amount, 5);
		bs.AppendValue(p->swing_time, 3);
	}

	bs.AppendValue(native_mode, 1);

	int num_sounds=0;
	bitsize=0;
	for(unsigned i=0;i<sound_store.size();i++)
	{
		if(sound_store[i]!=NULL && sound_store[i]->active)
		{
			num_sounds++;
			bitsize=bs.BitWidth(i);
		}
	}
	bs.AppendValue(num_sounds, 8);
	if(num_sounds>0)
		bs.AppendValue(bitsize-1, 3);
	for(unsigned i=0;i<sound_store.size();i++)
	{
		if(sound_store[i]!=NULL && sound_store[i]->active)
		{
			bs.AppendValue(i, bitsize);
			sound_store[i]->WriteCompact(bs);
		}
	}

	for(int pi=0;pi<PARTS_MAX;pi++)
	{
		const auto* p=song_parts[pi].get();
		if(!p) continue;

		int num_st=0;
		bitsize=0;
		for(int i=0;i<20;i++)
		{
			if(p->st_store[i]!=NULL && p->st_store[i]->active)
			{
				num_st++;
				bitsize=bs.BitWidth(i);
			}
		}
		bs.AppendValue(num_st, 8);
		if(num_st>0)
			bs.AppendValue(bitsize-1, 3);
		for(int i=0;i<20;i++)
		{
			if(p->st_store[i]!=NULL && p->st_store[i]->active)
			{
				bs.AppendValue(i, bitsize);
				p->st_store[i]->WriteCompact(bs);
			}
		}

		int num_ch=0;
		bitsize=0;
		for(int i=0;i<100;i++)
		{
			if(p->ch_store[i]!=NULL && p->ch_store[i]->active)
			{
				num_ch++;
				bitsize=bs.BitWidth(i);
			}
		}
		bs.AppendValue(num_ch, 8);
		if(num_ch>0)
			bs.AppendValue(bitsize-1, 3);
		for(int i=0;i<100;i++)
		{
			if(p->ch_store[i]!=NULL && p->ch_store[i]->active)
			{
				bs.AppendValue(i, bitsize);
				p->ch_store[i]->WriteCompact(bs);
			}
		}

		unsigned short scalemap_maxlength=-1;
		for(int i=0;i<NMSongPart::scalemap_size;i++)
		if(p->scalemap[i]!=0x0000)
			scalemap_maxlength=(unsigned short)i;
		scalemap_maxlength+=1;
		bs.AppendValue(scalemap_maxlength, 16);
		int prev_i=0;
		for(int i=0;i<scalemap_maxlength;i++)
		{
			unsigned short s=p->scalemap[i];
			if(s!=0x0000)
			{
				unsigned char gaplength=i-prev_i;
				bs.AppendValue(gaplength, 6);
				bs.AppendValue(s, 12);
				prev_i=i+1;
			}
			else if(i-prev_i==63 || i==scalemap_maxlength)
			{
				unsigned char gaplength=i-prev_i;
				bs.AppendValue(gaplength, 6);
				bs.AppendValue(s, 12);
				prev_i=i+1;
			}
		}
	}

	Bitstream cbs;
	BitCrunch(bs, cbs);

	cbs.PadToBytes();
	*size=cbs.ByteLength();

	*buffer=new unsigned char[*size+16];

	cbs.WriteToBuffer(*buffer);
}

void NMSong::ReadCompact(unsigned char* buffer, long size)
{
	debug_file_stats_graphcode=0;
	debug_file_stats_graphgaplong=0;
	debug_file_stats_graphgapshort=0;
	debug_file_stats_graphpoint=0;

	int bitsize;

	Clear();
	cur_part=-1;

	Bitstream cbs;
	cbs.FromBuffer(buffer, size);

	Bitstream bs;
	BitDecrunch(cbs, bs);

	char version=-1;
	version=bs.ReadValue(4);
	if(version>NM_SONG_VERSION)
		return;

	song_file_version=version;

	native_mode=0;

	int num_parts=1;
	if(version>=10)
		num_parts=bs.ReadValue(7);

	for(int pi=0;pi<num_parts;pi++)
	{
		auto* p=new NMSongPart();
		song_parts[pi].reset(p);

		p->tempo=bs.ReadValue(9);
		if(version<10)
			bs.ReadValue(3); // was timesig1
		p->timesig1=4; // NOTE: tsone: hard-coded 1/4 beat
		p->timesig2=bs.ReadValue(3);
		p->volume=bs.ReadValue(4);

		if(version==9)
			native_mode=bs.ReadValue(1);

		bitsize=bs.ReadValue(4)+1;
		p->mark_start=bs.ReadValue(bitsize);
		p->mark_end=bs.ReadValue(bitsize);
		p->mark_loop=bs.ReadValue(bitsize);
		if(version<2)
		{
			p->mark_start/=p->timesig2*2;
			p->mark_end/=p->timesig2*2;
			p->mark_loop/=p->timesig2*2;
		}
		if(version>=14)
		{
			p->swing_amount=bs.ReadValue(5);
			p->swing_time=bs.ReadValue(3);
		}
	}

	if(version>=10)
		native_mode=bs.ReadValue(1);

	bitsize=0;
	char num_sounds=0;
	num_sounds=bs.ReadValue(8);
	if(num_sounds>0)
		bitsize=bs.ReadValue(3)+1;
	for(int ii=0;ii<num_sounds;ii++)
	{
		char i=0;
		i=bs.ReadValue(bitsize);
		sound_store[i].reset(new NMSound());
		sound_store[i]->ReadCompact(bs);
		if(version==0)
			sound_store[i]->name=i;
	}

	for(int pi=0;pi<num_parts;pi++)
	{
		auto* p=song_parts[pi].get();

		char num_st=0;
		num_st=bs.ReadValue(8);
		if(num_st>0)
			bitsize=bs.ReadValue(3)+1;
		for(int ii=0;ii<num_st;ii++)
		{
			char i=0;
			i=bs.ReadValue(bitsize);
			p->st_store[i].reset(new NMChannel(NMChannel::Type::ST));
			p->st_store[i]->ReadCompact(bs);
		}

		char num_ch=0;
		num_ch=bs.ReadValue(8);
		if(num_ch>0)
			bitsize=bs.ReadValue(3)+1;
		for(int ii=0;ii<num_ch;ii++)
		{
			char i=0;
			i=bs.ReadValue(bitsize);
			p->ch_store[i].reset(new NMChannel(NMChannel::Type::CH));
			p->ch_store[i]->ReadCompact(bs);
		}

		unsigned short scalemap_maxlength=0;
		scalemap_maxlength=bs.ReadValue(16);
		int i=0;
		while(i<scalemap_maxlength)
		{
			unsigned char gaplength=0;
			gaplength=bs.ReadValue(6);
			i+=gaplength;
			p->scalemap[i]=bs.ReadValue(12);
			i++;
		}
	}

	SelectSongPart(0);
}

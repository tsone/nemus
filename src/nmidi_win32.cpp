#include "nmidi.h"
#include <windows.h>
#include <MMSystem.h>

static unsigned char midi_buffer[256];
static MIDIHDR midi_hdr;
static HMIDIIN handle_in;
static NMIDIPacketHandler data_handler;

static void CALLBACK NMIDI_Callback(HMIDIIN handle, UINT uMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2)
{
	if(uMsg==MIM_DATA)
	{
		const unsigned char data[4]={dwParam1&255, (dwParam1>>8)&255, (dwParam1>>16)&255, (dwParam1>>24)&255};
		data_handler(data, sizeof(data));
	}
}

extern "C" unsigned NMIDI_Init(unsigned idx, NMIDIPacketHandler handler)
{
	if(midiInOpen(&handle_in, idx, (DWORD_PTR)NMIDI_Callback, 0, CALLBACK_FUNCTION)!=MMSYSERR_NOERROR)
		return 0;

	midi_hdr.lpData=(char*)midi_buffer;
	midi_hdr.dwBufferLength=sizeof(midi_buffer);
	midi_hdr.dwFlags=0;
	if(midiInPrepareHeader(handle_in, &midi_hdr, sizeof(MIDIHDR))!=MMSYSERR_NOERROR)
	{
		midiInClose(handle_in);
		return 0;
	}
	data_handler=handler;
	midiInAddBuffer(handle_in, &midi_hdr, sizeof(MIDIHDR));
	midiInStart(handle_in);
	return 1;
}

extern "C" void NMIDI_Exit(void)
{
	midiInReset(handle_in);
	midiInUnprepareHeader(handle_in, &midi_hdr, sizeof(MIDIHDR));
	midiInClose(handle_in);
}

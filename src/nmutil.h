
#define DEBUG_SWEEP 0
#define DEBUG_OUTPUT_TIMING 0

static constexpr int SEMITONE_STEPS=16;
static constexpr int OCTAVE_STEPS=12*SEMITONE_STEPS; // ptab size
static constexpr int FULL_STEPS=9*OCTAVE_STEPS;
static constexpr int timeline_substeps=4;
static constexpr unsigned SAMPLE_RATE=48000; // Hz

static constexpr double clockrate=1789773.0; // Hz, NTSC
//static const double clockrate=1662607.0; // Hz, PAL
// Minimum oversampling is ceil(clockrate/samplerate)
static constexpr unsigned oversampling_shift=SAMPLE_RATE>=96000?1:2;
static constexpr unsigned oversampling=3*3*2*oversampling_shift;
static constexpr double samplesperclock=SAMPLE_RATE*oversampling/clockrate;
// Integer period near 240 Hz
static constexpr unsigned audioblocksize=(unsigned)(SAMPLE_RATE/240.+0.5);

static const int VIB_LEVELS=16;
static const int VIB_SAMPLES=16;
static uint16_t period_tab[OCTAVE_STEPS];

const int VIB_SCALE_PREC=8; // # of fractional bits
static uint16_t vib_scale_tab[VIB_LEVELS];

static uint8_t vol_mul_tab[16][16];

static uint8_t blookup16_noise_rate[15];
static uint8_t blookup16_dmc_rate[15];

static const uint16_t dmc_periods[]= // in cpu cycles
{
	428, 380, 340, 320, 286, 254, 226, 214, 190, 160, 142, 128, 106, 84, 72, 54
};

static int native_mode=1;

static int GetBLookup16(int v, const unsigned char *lookup)
{
	int x=1;
	while(x<16)
		x=(x<<1)+(lookup[x-1]>=v);
	return x^31;
}

const int PENV_SCALE_PREC=5; // # of fractional bits
static unsigned short penv_scale_tab[16];

inline int GetPeriod(int pi)
{
	return (int)(clockrate/(440.0*std::exp2((3.0/12.0)+pi/(double)OCTAVE_STEPS))+0.5);
}

inline int GetNoiseRate(int pi)
{
	double freq=clockrate/(16.0*GetPeriod(pi));
	return std::min(std::max(int(std::pow(std::max(1.0-freq/2000.0, 0.0), 1.5)*15), 0), 15);
}

static void GeneratePeriodTable()
{
	static bool generated=false;
	if(generated) return;
	generated=true;

	for(int i=0;i<OCTAVE_STEPS;i++)
		period_tab[i]=GetPeriod(i);
}

static void GenerateVolMulTable()
{
	for(int y=0;y<16;y++)
		for(int x=0;x<16;x++)
			vol_mul_tab[y][x]=(char)std::round(x*y/15.0);
}

static void MakeBLookup16(unsigned char *dst, const unsigned char *src)
{
	int j=14;
	for(int k=2;k<=16;k*=2)
		for(int i=k/2;i<16;i+=k)
			dst[j--]=src[i]-1;
}

static void GenerateNoiseTable()
{
	static bool generated=false;
	if(generated) return;
	generated=true;

	unsigned char tmp[16]={};
	for(int pi=0;pi<FULL_STEPS;pi++)
	{
		auto v=15-GetNoiseRate(pi);
		if (!tmp[v])
			tmp[v]=(unsigned char)(2.0*pi/SEMITONE_STEPS+0.5);
	}

	MakeBLookup16(blookup16_noise_rate, tmp);
}

inline int CalcDetune(int detune)
{
	return (int)std::round((SEMITONE_STEPS/10.0)*(detune-128));
}

inline int CalcVibratoDelay(int vibrato_delay)
{
	if(vibrato_delay!=1 || !native_mode)
		return (int)(std::pow(vibrato_delay/15.0, 1.8)*400);
	else
		return 4; // Special case, in 60Hz this results in 1 frame delay.
}

inline int CalcVibratoRate(int vibrato_speed)
{
	return (int)(16*(std::pow(vibrato_speed/16.0, 3.0)*70+7));
}

static void GenerateVibratoTable()
{
	static bool generated=false;
	if(generated) return;
	generated=true;

	for(int lev=0;lev<VIB_LEVELS;lev++)
	{
		// NOTE: tsone: matches old vib_scale calculation
		double vib_scl=std::pow(lev/(double)(VIB_LEVELS-1), 4.5)*1.2;
		if(lev>0) vib_scl+=0.01;
		vib_scl=std::log2(1+vib_scl);
		vib_scl*=OCTAVE_STEPS/(double)(VIB_SAMPLES-1);
		vib_scale_tab[lev]=(unsigned short)std::round((1<<VIB_SCALE_PREC)*vib_scl);
	}
}

static void GenerateDMCTable()
{
	unsigned char tmp[16];
	int j=0;
	for(int pi=0;pi<FULL_STEPS;pi++)
	{
		if(GetPeriod(pi)<=dmc_periods[j])
		{
			tmp[j]=(unsigned char)(pi/8.0+0.5);
			j++;
			if(j==16) break;
		}
	}

	MakeBLookup16(blookup16_dmc_rate, tmp);
}

static void GeneratePitchEnvelopeTable()
{
	// Find maximum scale values for native
	for(int pitchrange=1;pitchrange<=16;pitchrange++)
	{
		double r=std::pow((double)pitchrange, 2.0)/100.0;
		double v=std::log2(1+r)*OCTAVE_STEPS/7*(1<<PENV_SCALE_PREC);
		penv_scale_tab[pitchrange-1]=(unsigned short)std::round(v);
	}
}

constexpr int VolumeMul(int v1, int v2)
{
	assert(v1>=0 && v1<16 && v2>=0 && v2<16);
	return vol_mul_tab[v1][v2];
}

static void PrintNMNTables()
{
	printf("c_nk_audio_period_table_lo:");
	for(int i=0;i<OCTAVE_STEPS;i++)
	{
		if(!(i%12))
			printf("\n    .BYTE   ");
		auto c=((i+1)%12)?", ":"";
		printf("$%02X%s", period_tab[i]&0xFF, c);
	}
	printf("\nc_nk_audio_period_table_hi_4bit:");
	for(int i=0;i<OCTAVE_STEPS;i+=2)
	{
		auto c=(!(i%24))?"\n    .BYTE   ":", ";
		int v=(period_tab[i]>>8)+((period_tab[i+1]>>8)<<4);
		printf("%s$%02X", c, v);
	}

	puts("\n\nc_nk_audio_vol_mul_tab:");
	for(int y=1;y<16;y++)
	{
		printf("    .BYTE   ");
		for(int x=0;x<8;x++)
		{
			auto c=(x<7?", ":"\n");
			auto v0=vol_mul_tab[y][2*x];
			auto v1=vol_mul_tab[y][2*x+1];
			assert(v0>=0 && v0<16 && v1>=0 && v1<16);
			printf("$%02X%s", v0|(v1<<4), c);
		}
	}

	printf("\nc_nk_audio_vibrato_rate_hi:\n    .BYTE   ");
	for(int i=1;i<=16;i++)
	{
		printf("%3d%s", (4*CalcVibratoRate(i))>>8, (i<16?", ":"\n"));
	}
	printf("c_nk_audio_vibrato_rate_lo:\n    .BYTE   ");
	for(int i=1;i<=16;i++)
	{
		printf("%3d%s", (4*CalcVibratoRate(i))&0xFF, (i<16?", ":"\n"));
	}

	printf("c_nk_audio_vibrato_scale_hi:\n    .BYTE   ");
	for(int i=0;i<16;i++)
	{
		printf("%3d%s", vib_scale_tab[i]>>8, (i<15?", ":"\n"));
	}
	printf("c_nk_audio_vibrato_scale_lo:\n    .BYTE   ");
	for(int i=0;i<16;i++)
	{
		printf("%3d%s", vib_scale_tab[i]&0xFF, (i<15?", ":"\n"));
	}
	puts("");

#if 0
	puts("penv_scale_tab errors:");
	for(int pitchrange=1;pitchrange<=16;pitchrange++)
	{
		for(int i=0;i<16;i++)
		{
			int v=((i-7)*penv_scale_tab[pitchrange-1])/(1<<PENV_SHIFT);
			v-=ReferencePitchEnvelope(pitchrange, i);
			if(v)
				printf("(%d,%d): error:%d\n", pitchrange, i, v);
		}
	}
#endif

	printf("c_nk_audio_penv_scale_hi:\n    .BYTE   ");
	for(int i=0;i<16;i++)
	{
		printf("%3d%s", penv_scale_tab[i]>>8, (i<15?", ":"\n"));
	}
	printf("c_nk_audio_penv_scale_lo:\n    .BYTE   ");
	for(int i=0;i<16;i++)
	{
		printf("%3d%s", penv_scale_tab[i]&0xFF, (i<15?", ":"\n"));
	}
	puts("");

	printf("c_nk_audio_blookup_noise_rate_tab:\n    .BYTE   ");
	for(int i=0;i<15;i++)
		printf("%3d%s", blookup16_noise_rate[i], i==14?"\n":", ");

	printf("c_nk_audio_blookup_dmc_rate_tab:\n    .BYTE   ");
	for(int i=0;i<15;i++)
		printf("%3d%s", blookup16_dmc_rate[i], i==14?"\n":", ");
}

static int GetEnvRate(int spd)
{
	assert(spd>=0 && spd<16);
	static const unsigned char rate_tab[16]=
	{
		//=(i+2)*(i+2)*2/3 for i=1..15, 0 for i=0
		0,6,10,16,24,32,42,54,66,80,96,112,130,150,170,192
	};
	// NOTE: tsone: must be <128 for nmn, divide by 2
	const int result=rate_tab[spd]>>1;
	assert(result>=0);
	return result;
}

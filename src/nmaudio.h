
struct RecentAudio
{
	static constexpr unsigned SIZE=2048;
	std::array<float, SIZE> buffer;
	float level_min=1e6, level_max=-1e6;
};
std::array<RecentAudio, 5> recent_audio;
unsigned recent_audio_i=0;

// One-pole low-pass IIR.
// References:
// - Redmon, N., "A one-pole filter"
//   http://www.earlevel.com/main/2012/12/15/a-one-pole-filter/
// - Smith, S.W., "The scientist and engineer's guide to digital signal
//   processing: chapter 19: recursive filters"
//   http://www.dspguide.com/ch19/2.htm
template <typename P>
struct LowPass
{
	inline LowPass(P fc) : a0(1-std::exp(-2*M_PI*fc)) {}
	inline void Update(P x) { y+=a0*(x-y); }
	inline P Get() const { return y; }
protected:
	P y=0;
	const P a0;
};

// Multi-stage decimator, factors 2^N.
// References:
// - Lyons, R. "Optimizing the Half-band Filters in Multistage Decimation
//   and Interpolation"
//   https://www.dsprelated.com/showarticle/903.php
// - Turkowski, K. "Filters for Common Resampling Tasks"
//   http://www.realitypixels.com/turk/computergraphics/ResamplingFilters.pdf
template <typename P, unsigned N>
struct MSD2
{
	// Kernel peak scaled to 1 (avoid one multiplication).
#if 1
	// Turkowski (modified Lanczos2)
	static constexpr P c0=16/32.;
	static constexpr P c1=9/32./c0;
	static constexpr P c2=-1/32./c0;
#elif 0
	// Tailored Remez/Parks-McClellan (see genfilter.py script)
	static constexpr P c0=0.500034749753;
	static constexpr P c1=0.28260793179/c0;
	static constexpr P c2=-0.0326253066665/c0;
#elif 0
	// Lanczos2 (see genfilter.py script)
	static constexpr P c0=0.495307056664;
	static constexpr P c1=0.283889780626/c0;
	static constexpr P c2=-0.0315433089585/c0;
#else
	// Lanczos2 (rounded, from Turkowski)
	static constexpr P c0=.496;
	static constexpr P c1=.284/c0;
	static constexpr P c2=-.032/c0;
#endif

	inline MSD2() : scale(std::pow(c0, N)) {}

	inline void Update(P x)
	{
		unsigned k=count++;

		auto* p=z;
		for(unsigned j=N;j!=0;--j)
		{
			const P pp=p[0];
			for(unsigned i=0;i<5;i++)
				p[i]=p[i+1];
			p[5]=x;

			if(k%2)
				return;
			k/=2;

			// Exploiting symmetry for multiplications
			x=c2*(pp+x)
			+c1*(p[1]+p[3])
			+p[2];

			p+=6;
		}
		y=x*scale;
	}

	inline P Get() const { return y; }

private:
	unsigned count=0;
	P z[N*6]={};
	const P scale;
	P y=0;
};

// Multi-stage decimator, factors 3^N.
// For references see MSD2 class above.
template <typename P, unsigned N>
struct MSD3
{
	// Kernel peak scaled to 1 (avoid one multiplication).
#if 1
	// Tailored Remez/Parks-McClellan (see genfilter.py script)
	static constexpr P c4=0.3355265604;
	static constexpr P c0=-0.0169351061899/c4;
	static constexpr P c1=-0.0334871388819/c4;
	static constexpr P c2=0.11657606414/c4;
	static constexpr P c3=0.266082900732/c4;
#elif 0
	// Lanczos2 (see genfilter.py script)
	static constexpr P c4=0.330119968003;
	static constexpr P c0=-0.0104280998618/c4;
	static constexpr P c1=-0.0282218731048/c4;
	static constexpr P c2=0.112887492419/c4;
	static constexpr P c3=0.260702496546/c4;
#else
	// Lanczos2 (rounded, from Turkowski)
	static constexpr float c4=.330;
	static constexpr float c0=-.010/c4;
	static constexpr float c1=-.028/c4;
	static constexpr float c2= .113/c4;
	static constexpr float c3= .261/c4;
#endif

	// Lanczos2 (from Turkowski, rounded)
	inline MSD3() : scale(std::pow(c4, N)) {}

	inline bool Update(P x)
	{
		unsigned k=count++;

		auto* p=z;
		for(unsigned j=N;j!=0;--j)
		{
			const P pp=p[0];
			for(unsigned i=0;i<9;i++)
				p[i]=p[i+1];
			p[9]=x;

			if(k%3)
				return false;
			k/=3;

			// Exploiting symmetry for multiplications
			x=c0*(pp+x)
			+c1*(p[0]+p[8])
			+c2*(p[2]+p[6])
			+c3*(p[3]+p[5])
			+p[4];

			p+=10;
		}
		y=x*scale;
		count=1;
		return true;
	}

	inline P Get() const { return y; }

private:
	unsigned count=0;
	P z[N*10]={};
	const P scale;
	P y=0;
};

struct NMAPU
{
	static constexpr int numchannels=(int)NMSound::Type::COUNT;

	struct Channel
	{
		enum class Type : char
		{
			SQUARE,
			TRIANGLE,
			NOISE,
			DPCM
		};

		std::array<uint8_t, audioblocksize*oversampling> output_buffer={};

		float pulse_length=0;
		uint8_t pulse_value=0;

		Type type=Type::SQUARE;
		int state=0; // internal counter/data, updated once per period
		int mode=0; // waveform settings
		int period=0; // in clock cycles
		int volume=0; // where applicable

		bool active=false;
		bool reset=true;

		struct DPCMPlayback
		{
			const DPCMSample *sample=nullptr;
			int pos=0;
			int bits=0;
			bool play=true;
		};

		DPCMPlayback dpcm;

		void Reset()
		{
			if(type==Type::SQUARE)
			{
				state=0;
			}
			else if(type==Type::DPCM)
			{
				state=0;
				if(dpcm.sample && !dpcm.sample->data.empty())
				{
					dpcm.play=true;
					dpcm.bits=dpcm.sample->data[0]|0x100;
					dpcm.pos=1;
				}
				else
					dpcm.play=false;
			}
			mode&=3;
			reset=false;
		}

		void Update()
		{
			if(reset)
				Reset();

			if(!active)
				return;
			if(type==Type::SQUARE)
			{
				static constexpr uint8_t square_pattern[]=
				{
					0,1,0,0,0,0,0,0,
					0,1,1,0,0,0,0,0,
					0,1,1,1,1,0,0,0,
					1,0,0,1,1,1,1,1
				};

				if(period<8)
				{
					active=false;
					return;
				}
				state=(state+1)&7;
				pulse_length=2*samplesperclock*period;
				pulse_value=square_pattern[state+mode*8]*volume;
			}
			else if(type==Type::TRIANGLE)
			{
				static constexpr uint8_t triangle_pattern[]=
				{
					15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0,
					0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
				};

				if(period<2 || volume==0)
				{
					active=false;
					return;
				}
				state=(state+1)&31;
				pulse_length=samplesperclock*period;
				pulse_value=triangle_pattern[state];
			}
			else if(type==Type::NOISE)
			{
				static constexpr uint16_t noise_periods[]=
				{
					4,8,16,32,64,96,128,160,202,254,380,508,762,1016,2034,4068
				};

				if(mode==1)
					state|=((state^(state>>6))&1)<<15;
				else
					state|=((state^(state>>1))&1)<<15;
				state>>=1;
				pulse_length=samplesperclock*noise_periods[period];
				pulse_value=((state&1)^1)*volume;
			}
			else if(type==Type::DPCM)
			{
				if(dpcm.play && dpcm.sample && !dpcm.sample->data.empty())
				{
					if(dpcm.bits&1)
						state=std::min(state+1, 31);
					else
						state=std::max(state-1,-32);

					dpcm.bits>>=1;
					if(dpcm.bits==1)
					{
						// satefy checks... sample may be changed any time
						if(dpcm.pos<(int)dpcm.sample->data.size())
						{
							dpcm.bits=dpcm.sample->data[dpcm.pos]|0x100;
							dpcm.pos++;
						}
						if(dpcm.pos>=(int)dpcm.sample->data.size()){
							if(dpcm.pos==(int)dpcm.sample->data.size())
							{
								state=0;
								dpcm.pos=0;
							}
							else
								Reset(); // sample changed
							dpcm.play=!(mode&1);
						}
					}
				}

				pulse_length=period*samplesperclock;
				pulse_value=2*state*volume/15+64;
			}
		}

		void BufferSamples(unsigned numsamples)
		{
			assert(numsamples>0);
			auto* output=output_buffer.data();
			do
			{
				unsigned writespan;
				if(active)
				{
					writespan=(unsigned)pulse_length;
					if(writespan>numsamples)
						writespan=numsamples;
					if(writespan==0)
						Update();
					pulse_length-=writespan; // fract()
				}
				else
				{
					writespan=numsamples;
				}

				const uint8_t v=pulse_value;
				numsamples-=writespan;
				for(;writespan!=0;writespan--)
					*output++=v;
			}
			while(numsamples);
		}
	};
	std::array<Channel, numchannels> channels;

	static void InitTables()
	{
		static bool generated=false;
		if(generated) return;
		generated=true;

		GeneratePeriodTable();
		GenerateVolMulTable();
		GenerateNoiseTable();
		GenerateVibratoTable();
		GeneratePitchEnvelopeTable();
		GenerateDMCTable();
		//PrintNMNTables();
	}

	NMAPU()
	{
		InitTables();

		channels[0].type=Channel::Type::SQUARE;
		channels[1].type=Channel::Type::SQUARE;
		channels[2].type=Channel::Type::TRIANGLE;
		channels[3].type=Channel::Type::NOISE;
		channels[3].state=1;
		channels[4].type=Channel::Type::DPCM;
	}

	// Filters
	MSD3<float, 2> msd3n;
	LowPass<float> dc={23.0f/(SAMPLE_RATE*2*oversampling_shift)};
	MSD2<float, oversampling_shift> msd2n;

#if DEBUG_OUTPUT_TIMING
	double _output_time_eavg=0;
#endif
#if DEBUG_SWEEP
	double sweep_freq=0;
	size_t sweep_i=0;
#endif

	void Output(float* buffer, unsigned numsamples)
	{
#if DEBUG_OUTPUT_TIMING
		const double start_time=glrTimestamp();
#endif

		{
			const unsigned count=numsamples*oversampling;
			for(unsigned chi=0;chi<numchannels;chi++)
				channels[chi].BufferSamples(count);
		}

#if DEBUG_SWEEP
		sweep_freq=SAMPLE_RATE*sweep_i/(10.*clockrate);
#endif

		unsigned i=0;
		for(unsigned si=0;si<numsamples;si++)
		{
			for(unsigned oi=0;oi<oversampling;oi++)
			{
				const float squaresum=(float)(channels[0].output_buffer[i]+channels[1].output_buffer[i]);
				const float triangle=channels[2].output_buffer[i];
				const float noise=channels[3].output_buffer[i];
				const float dpcm=channels[4].output_buffer[i];

				float mix=0;
				if(squaresum!=0)
				{
					mix=95.88f/(8128.f/squaresum+100.f);
				}
				if(triangle!=0 || noise!=0 || dpcm!=0)
				{
					mix+=159.79f/(1.f/(triangle*(1.f/8227.f)+noise*(1.f/12241.f)+dpcm*(1.f/22638.f))+100.f);
				}

#if DEBUG_SWEEP
				const double freq=SAMPLE_RATE*sweep_i/(10.*clockrate);
				mix=(.25f)*sin(sweep_i*(2*M_PI*freq/(SAMPLE_RATE*oversampling)));
				sweep_i++;
#endif

				// Downsample by 3^N
				if(msd3n.Update(mix))
				{
					// Then remove DC and downsample by 2^M
					dc.Update(msd3n.Get());
					msd2n.Update(dc.Get()-msd3n.Get());
				}

				i++;
			}

			*buffer++=msd2n.Get();

			for(unsigned chi=0;chi<recent_audio.size();chi++)
			{
				auto& ra=recent_audio[chi];
				const float v=channels[chi].output_buffer[i-1]*(1.f/15.f);
				ra.buffer[recent_audio_i]=v;
				if(v<ra.level_min) ra.level_min=v;
				if(v>ra.level_max) ra.level_max=v;
			}
			recent_audio_i=recent_audio_i<RecentAudio::SIZE-1?recent_audio_i+1:0;
		}

#if DEBUG_OUTPUT_TIMING
		_output_time_eavg=0.998*_output_time_eavg+0.002*1e3*(glrTimestamp()-start_time);
#endif
	}

	void MacroCommand(NMSound::Type type, bool reset, int period, int volume, int mode, const DPCMSample *dpcm_sample)
	{
		Channel& ch=channels[(int)type];

		bool was_active=ch.active;
		ch.active=true;
		ch.reset=reset;
		ch.volume=volume;
		ch.period=std::max(0, std::min(2047, period));
		ch.mode=mode;
		ch.dpcm.sample=dpcm_sample;
		if(!native_mode && !was_active)
			ch.Update();
	}
	inline void Silence()
	{
		for(int i=0;i<numchannels;i++)
			channels[i].volume=0;
	}
};

#if !HEADLESS
#include <mutex>

struct Mutex
{
	unsigned int waittime=0;
	unsigned int longestwait=0;
	int waitcount=0;

	void Acquire()
	{
		unsigned int t0=glrAppTimeMs();

		_mutex.lock();

		unsigned wait=glrAppTimeMs()-t0;
		waittime+=wait;
		if(wait>longestwait)
			longestwait=wait;
		waitcount++;
	}
	inline void Release()
	{
		_mutex.unlock();
	}
private:
	std::recursive_mutex _mutex;
};

Mutex audio_mutex;
inline void AcquireAudioLock()
{
	audio_mutex.Acquire();
}
inline void ReleaseAudioLock()
{
	audio_mutex.Release();
}

#endif // !HEADLESS

struct NMAEnvelopePlayer
{
	enum Type
	{
		VOLUME,
		PITCH
	};
	int pos;

	void SetSource(Type type, const NMSound* sound)
	{
		if(type==VOLUME)
			graph.SetSource(sound->v_env_graph, 15);
		else
			graph.SetSource(sound->p_env_graph, 7);
	}

	void Init(Type type, const NMSound* sound)
	{
		SetSource(type, sound);

		const int* speed=(type==VOLUME?sound->v_env_speed:sound->p_env_speed);

		pos=(speed[0]==0?16*256:0);
	}

	bool UpdateNative(const int* speed, bool released)
	{
		int phase=released?2:pos/16/256;
		int spd=speed[phase];
		if(spd==0)
		{
			if(!released)
				pos=16*256;
			else
				goto stop_envelope;
		}
		else
		{
			pos+=GetEnvRate(spd)<<1;

			if(!released && pos>=32*256)
				pos-=16*256;
		}

		if(pos<47*256)
			return true;

	stop_envelope:
		pos=47*256; // exactly at the last point
		return false;
	}

	bool UpdateNormal(const int* speed, bool released)
	{
		int phase;
		if(!released)
		{
			if(pos>=32*256)
			{
				pos-=16*256;
			}
			phase=pos/16/256;
		}
		else
		{
			phase=2;
		}

		int spd=speed[phase];
		if(spd==0)
		{
			if(!released)
			{
				pos=16*256;
			}
			else
			{
				goto stop_envelope;
			}
		}

		pos+=GetEnvRate(spd)<<1;

		if(pos<47*256){
			return true;
		}

	stop_envelope:
		pos=47*256; // exactly at the last point
		return false;
	}

	bool Update(const int* speed, bool released)
	{
		if(native_mode)
			return UpdateNative(speed, released);
		else
			return UpdateNormal(speed, released);
	}

	// result is 4-bit fixed-point
	inline int Read() const
	{
		return graph.Read(pos, 256, 16);
	}

private:
	GraphReader graph;
};

using NMAArpScale = std::array<char, 1+8+1>;
struct NMAArpState
{
	NMAArpScale scale={};
	int pos=0;
	int delay=-1;
};

struct NMASoundPlayer
{
	bool owned;
	bool triggered;
	bool reset;
	int period_tab_idx;

	int chst=-1;
	int snd_idx=-1;

	int output_period;
	int output_volume;

	int v_envelope_zero=48*256;

	bool output_alternate_channel;

	NMAEnvelopePlayer v_envelope;
	NMAEnvelopePlayer p_envelope;
	float p_envrange;

	int vib_delay;
	int vib_pos;
	int vib_rate;

	int detune;
	bool fixedpitch;

	NMAArpState arp={};

	NMASoundPlayer()
	{
		active=false;
		owned=false;
		output_alternate_channel=false;
	}

	inline void SetPeriodTabIdx(int idx)
	{
		if(fixedpitch)
			return;
		period_tab_idx=idx+detune;
	}
	inline void SetChannelEnvelopeVolume(int chn_env_vol)
	{
		channel_envelope_volume=chn_env_vol;
	}
	constexpr int GetChannelEnvelopeVolume() const
	{
		return channel_envelope_volume;
	}
	inline void SetArpScale(const char *scale)
	{
		int i=1;
		for(;scale[i];i++)
			arp.scale[i]=scale[i];
		arp.scale[i]=0;
	}
	inline void SetArpPosDelay(int pos, int delay)
	{
		arp.pos=pos;
		arp.delay=delay;
	}

	void Trigger(NMSong* song, int snd_i)
	{
		active=true;
		triggered=true;
		reset=true;
		released=false;
		snd_idx=snd_i;

		auto sound=song->GetSound(snd_idx);
		assert(sound);

		vib_pos=0;
		vib_delay=CalcVibratoDelay(sound->vibrato_delay);

		vib_rate=CalcVibratoRate(sound->vibrato_speed);

		detune=CalcDetune(sound->detune);

		arp = NMAArpState{};

		fixedpitch=sound->fixedpitch;
		if(fixedpitch)
			period_tab_idx=SEMITONE_STEPS*sound->basepitch;

		v_envelope.Init(NMAEnvelopePlayer::VOLUME, sound);
		p_envelope.Init(NMAEnvelopePlayer::PITCH, sound);
		p_envrange=pow((float)sound->pitchrange, 2.0f)/100;

		sound->audio_v_envpos=v_envelope.pos/256;
		sound->audio_p_envpos=p_envelope.pos/256;
	}

	inline void Release()
	{
		released=true;
	}
	constexpr bool GetReleased() const
	{
		return released;
	}

	inline void Kill()
	{
		active=false;
	}
	constexpr bool GetActive() const
	{
		return active;
	}

	void Step(NMSong* song)
	{
		auto sound=song->GetSound(snd_idx);

		if(!active || sound==NULL || !sound->active)
		{
			active=false;
			return;
		}

		v_envelope.SetSource(NMAEnvelopePlayer::VOLUME, sound);
		p_envelope.SetSource(NMAEnvelopePlayer::PITCH, sound);

		if(!v_envelope.Update(sound->v_env_speed, released))
		{
			active=false;
			sound->audio_v_envpos=NONE_POS;
			sound->audio_p_envpos=NONE_POS;
			return;
		}
		else
			sound->audio_v_envpos=v_envelope.pos/256;
		if(!p_envelope.Update(sound->p_env_speed, released))
		{
			sound->audio_p_envpos=NONE_POS;
		}
		else
			sound->audio_p_envpos=p_envelope.pos/256;

		if(vib_delay>0)
			vib_delay--;
		else
		{
			vib_pos+=vib_rate;
			vib_pos&=(4096*4)-1;
		}
	}

	void PrepareOutput(NMSong* song)
	{
		auto sound=song->GetSound(snd_idx);

		if(active && sound && sound->active)
		{
			v_envelope.SetSource(NMAEnvelopePlayer::VOLUME, sound);
			p_envelope.SetSource(NMAEnvelopePlayer::PITCH, sound);

			UpdateVolumeEnvelopeEndPos(song);
			output_volume=CalcOutputVolume(song);
			output_period=CalcOutputPeriod(song);
		}
		else
		{
			active=false;
		}
	}

	// if zero, leave channel alone for use further down in the stack (ultimate default is always silence)
	bool HasOutput(NMSong* song) const
	{
		if(!active)
			return false;
		if(!native_mode)
		{
			if(!output_volume)
				return false;
		}
		else
		{
			// Native driver can't continue playing sound that was once stopped,
			// i.e. normal mode volume==0 condition doesn't work as the volume
			// may become >0 later... Instead, we must detect when sound is not
			// playing AND cannot be playing later.
			// Note, channel volume envelope is not take account as that would
			// require costly ahead-of-time volume computations.
			auto sound=song->GetSound(snd_idx);
			if(!sound)
				return false;

			int chn_vol=GetChstVolume(song);
			if(sound->GetInstrumentVolume(chn_vol)<=0)
				return false;

			if(released || v_envelope_zero<=16*256)
			{
				if(v_envelope.pos>=v_envelope_zero)
					return false;
			}
		}
		return true;
	}

	void Output(NMSong* song, NMAPU& apu)
	{
		triggered=false;

		if(!HasOutput(song))
			return;

		auto sound=song->GetSound(snd_idx);
		assert(sound);

		sound->audio_light=output_volume;

		auto wf=sound->waveform;
		int wm=sound->wavemode;
		if(sound->waveform!=NMSound::Type::DMC && p_envelope.pos/256>=sound->wavemode_pos)
			wm=sound->wavemode2;
		switch(sound->waveform)
		{
			case NMSound::Type::TRIANGLE:  wm=1; break;
			case NMSound::Type::NOISE:  wm^=1; break;
			case NMSound::Type::DMC:  wm&=1; break;
			default: break; // sq1,sq2
		}
		if(output_alternate_channel && !native_mode)
		{
			// pick the other square channel if possible, to allow limited polyphony
			if(wf==NMSound::Type::SQUARE1)
				wf=NMSound::Type::SQUARE2;
			else if(wf==NMSound::Type::SQUARE2)
				wf=NMSound::Type::SQUARE1;
		}

		const DPCMSample* dpcm_sample=&sound->dpcm_sample;
		if(dpcm_sample->data.empty())
			dpcm_sample=nullptr;

		apu.MacroCommand(wf, reset, output_period, output_volume, wm, dpcm_sample);
		reset=false;
	}

private:
	int channel_envelope_volume=15;

	bool active;
	bool released;

	void UpdateVolumeEnvelopeEndPos(NMSong* song)
	{
		// Ues default in case update fails
		v_envelope_zero=48*256;

		if(native_mode)
		{
			const auto* sound=song->GetSound(snd_idx);
			if(sound)
			{
				int chn_vol=GetChstVolume(song);
				v_envelope_zero=sound->GetVolZeroPos(chn_vol);
			}
		}
	}

	constexpr int GetChstVolume(NMSong* song) const
	{
		if(const auto* channel=song->GetCHST(chst))
			return channel->volume;
		else if(chst==-1)
			return 15; // Undefined channel (keyjazz)
		else
			return 0; // Channel removed
	}

	int CalcOutputVolumeNative(NMSong* song, int snd_env_vol_raw) const
	{
		auto sound=song->GetSound(snd_idx);
		if(!sound)
			return 0;

		const int inst_vol=sound->GetInstrumentVolume(GetChstVolume(song));
		if(inst_vol<=0) // mimics note removal at export
			return 0;

		const int env_vol=VolumeMul(snd_env_vol_raw/16, channel_envelope_volume);

		if(sound->waveform==NMSound::Type::TRIANGLE || sound->waveform==NMSound::Type::DMC)
			return env_vol>0?15:0; // inst_vol=15 always
		else
			return VolumeMul(inst_vol, env_vol);
	}

	int CalcOutputVolumeNormal(NMSong* song, int snd_env_vol_raw) const
	{
		auto sound=song->GetSound(snd_idx);
		assert(sound);

		auto result=VolumeMul(channel_envelope_volume, GetChstVolume(song));
		result=result*((snd_env_vol_raw+15)/16+1)/16;
		result=result*(sound->Volume()+1)/16;
		return result;
	}

	int CalcOutputVolume(NMSong* song)
	{
		const int snd_env_vol_raw=v_envelope.Read();
		if(native_mode)
			return CalcOutputVolumeNative(song, snd_env_vol_raw);
		else
			return CalcOutputVolumeNormal(song, snd_env_vol_raw);
	}

	int CalcOutputPeriod(NMSong* song)
	{
		auto sound=song->GetSound(snd_idx);
		assert(sound);

		int vibrato_mod=0;
		if(vib_delay<=0)
		{
			int vib_x=vib_pos>>8;
			int vib_sample=vib_x&31;
			if(vib_sample>=16)
				vib_sample=31-vib_sample;

			int vib_level=std::max(0, std::min(VIB_LEVELS-1, sound->vibrato_strength));
			int vib=vib_scale_tab[vib_level]*vib_sample;
			vib/=(1<<VIB_SCALE_PREC);
			vibrato_mod=(vib_x<32)?vib:-vib;
		}

		const int pitch_env=p_envelope.Read();
		int pidx_mod;
		if(native_mode)
		{
			pidx_mod=(pitch_env/16-7)*penv_scale_tab[sound->pitchrange-1];
			pidx_mod/=(1<<PENV_SCALE_PREC);
		}
		else
		{
			const float range=powf((float)sound->pitchrange, 2)/100.0f;
			const float v=(pitch_env/16.0f-7)/7;
			pidx_mod=(int)(OCTAVE_STEPS*std::log2(1+std::fabs(v)*range)+0.5f);
			if(v<0)
				pidx_mod=-pidx_mod;
		}

		pidx_mod+=SEMITONE_STEPS*arp.scale[arp.pos];

		int pidx=sound->CalcFinalPeriodTabIdx(period_tab_idx);
		pidx=std::max(0, std::min(FULL_STEPS-1, pidx+vibrato_mod+pidx_mod));

		if(sound->waveform==NMSound::Type::NOISE)
			return 15^GetBLookup16(pidx>>3, blookup16_noise_rate);
		else if(sound->waveform==NMSound::Type::DMC && native_mode)
			return dmc_periods[GetBLookup16(pidx>>3, blookup16_dmc_rate)];
		else
			return period_tab[pidx%OCTAVE_STEPS]>>(pidx/OCTAVE_STEPS);
	}
};

struct NMASoundStack
{
	static constexpr unsigned SOUNDSTACK_SIZE=128;
	std::array<NMASoundPlayer, SOUNDSTACK_SIZE> soundplayers={};
	std::array<int, SOUNDSTACK_SIZE> soundstack={};
	int soundstacksize=0;

	NMASoundPlayer* GetSoundPlayer(int chst)
	{
		for(int i=0;i<soundplayers.size();i++)
		{
			if(!soundplayers[i].owned)
			{
				soundplayers[i].chst=chst;
				soundplayers[i].owned=true; // caller responsible for releasing it
				soundstack[soundstacksize++]=i; // TODO: respect priority
				return &soundplayers[i];
			}
		}
		return nullptr;
	}
	void CleanStack()
	{
		for(int si=0;si<soundstacksize;si++)
		{
			if(!soundplayers[soundstack[si]].owned)
			{
				soundstacksize--;
				for(int j=si;j<soundstacksize;j++)
					soundstack[j]=soundstack[j+1];
				si--;
			}
		}
	}
};


struct NMAMultiSoundPlayer
{
	bool active;
	NMASoundPlayer* sounds[SOUNDS_MAX];
	int numsounds;
	bool output_alternate_channel;

	NMAMultiSoundPlayer(NMASoundStack& stack) : _stack(stack)
	{
		active=false;
		numsounds=0;
		for(int i=0;i<SOUNDS_MAX;i++)
			sounds[i]=NULL;
		output_alternate_channel=false;
	}

	void SetPeriodTabIdx(int idx)
	{
		for(int i=0;i<numsounds;i++)
			sounds[i]->SetPeriodTabIdx(idx);
	}

	void SetChannelEnvelopeVolume(int chn_env_vol)
	{
		for(int i=0;i<numsounds;i++)
			sounds[i]->SetChannelEnvelopeVolume(chn_env_vol);
	}

	void SetArpPosDelay(int pos, int delay)
	{
		for(int i=0;i<numsounds;i++)
			sounds[i]->SetArpPosDelay(pos, delay);
	}

	void PlaySounds(NMSong* song, int num, const int* playsnd_idxs, int note, int chst, int chn_env_vol, const char* arp_scale = nullptr)
	{
		KillSounds();
		numsounds=0;
		active=true;
		for(int i=0;i<num;i++)
		{
			NMASoundPlayer* sp=_stack.GetSoundPlayer(chst);
			if(sp==NULL)
				break;
			sp->output_alternate_channel=output_alternate_channel;
			sp->Trigger(song, playsnd_idxs[i]);
			sp->SetPeriodTabIdx(SEMITONE_STEPS*note);
			sp->SetChannelEnvelopeVolume(chn_env_vol);
			if(arp_scale)
				sp->SetArpScale(arp_scale);
			sounds[numsounds++]=sp;
		}
	}

	void ReleaseSounds() // just trip the release phase, still own the sound and need to disown it later
	{
		for(int i=0;i<numsounds;i++)
			sounds[i]->Release();
	}

	void KillSounds()
	{
		for(int i=0;i<numsounds;i++)
		{
			sounds[i]->Kill();
			sounds[i]->owned=false;
		}
		_stack.CleanStack();
		numsounds=0;
		active=false;
	}

	void CheckSounds()
	{
		for(int i=0;i<numsounds;i++)
			if(!sounds[i]->GetActive())
			{
				sounds[i]->owned=false;
				numsounds--;
				for(int j=i;j<numsounds;j++)
					sounds[j]=sounds[j+1];
				i--;
			}
		_stack.CleanStack();
		if(numsounds==0)
			active=false;
	}

private:
	NMASoundStack& _stack;
};

struct NMAChannelPlayer
{
	NMAMultiSoundPlayer msp;
	GraphReader content_reader;
	GraphReader envelope_reader;

	int chst=-1;

	bool coldstart;

	int curpos;
	int subpos;

	int basenote;
	int triggered_basenote;
	int arpeggio_pos;
	int arpeggio_delay;
	int arpeggio_rate;
	unsigned short arpeggio_prevscale;
	int arpeggio_prevpos;

	bool activenote;
	bool holdnote;
	int notelength;

	NMAChannelPlayer(NMASoundStack& stack) : msp(stack)
	{
		release=false;
	}
	~NMAChannelPlayer()
	{
		msp.KillSounds();
	}

	void UpdateReaderSources(NMSong *song)
	{
		auto* channel=song->GetCHST(chst);
		if(!channel)
			return; // Channel removed
		auto* source_ch=channel->GetSourceChannel(song);
		assert(source_ch);
		content_reader.SetSource(source_ch->content, 0);
		envelope_reader.SetSource(source_ch->envelope, 15);
	}

	void Play(int chst_, int start)
	{
		Stop();

		chst=chst_;

		curpos=start;
		subpos=0;
		basenote=0;
		triggered_basenote=0;
		arpeggio_pos=0;
		arpeggio_delay=0;
		arpeggio_rate=0;
		arpeggio_prevscale=0x0000;
		arpeggio_prevpos=-1;

		notelength=0;
		activenote=false;
		release=false;
	}
	void Stop()
	{
		msp.KillSounds();
	}

	void Goto(int pos)
	{
		curpos=pos;
		subpos=0;

		HandleRelease(true);
	}

	void Update(NMSong *song, int subrate)
	{
		auto channel=song->GetCHST(chst);
		if(!channel)
			return; // Channel removed

		assert(subpos<subrate);

		msp.CheckSounds();

		int readpos=(curpos-1)*subrate+subpos;

		if(msp.active)
		{
			int idx;
			if(channel->type==NMChannel::Type::ST)
				idx=SEMITONE_STEPS*channel->basepitch;
			else if(activenote)
				idx=SEMITONE_STEPS*content_reader.Read(readpos, subrate, 256)/256;
			else
				idx=SEMITONE_STEPS*basenote;

			msp.SetPeriodTabIdx(idx);
			msp.SetArpPosDelay(arpeggio_pos, arpeggio_delay);

			const int chn_env_vol=envelope_reader.Read(readpos, subrate, 1);
			msp.SetChannelEnvelopeVolume(chn_env_vol);
		}
		subpos++;
		if(arpeggio_rate!=0)
		{
			arpeggio_delay++;
			if(arpeggio_delay>=arpeggio_rate)
			{
				arpeggio_pos++;
				if(arpeggio_pos>=channel->arp_size)
					arpeggio_pos=0;
				arpeggio_delay=0;
				arpeggio_rate=channel->GetArpRate();
			}
		}
	}

	void Step(NMSong *song)
	{
		auto channel=song->GetCHST(chst);
		if(!channel)
			return; // Channel removed

		content_reader.pos_offset=channel->time_shift;
		envelope_reader.pos_offset=channel->time_shift;

		auto it=content_reader.GetIterator(curpos);
		it.Next(&content_reader);
		const auto next_gp=it.Read(&content_reader);

		bool trigger=false;
		const auto gp=content_reader.Read(curpos);
		if(gp.trigger!=0xFF) // start a note
			trigger=true;
		if(activenote) // playing sound
		{
			if(!holdnote)
			{
				notelength++;
				if(notelength>=channel->GetNoteLength())
					release=true;
			}
			if(gp.curve!=0xFF) // continue a note
			{
				notelength=0;
				holdnote=true;
				if(next_gp.Empty() || next_gp.curve==0xFF) // end of curve sequence
				{
					basenote=gp.curve;
					release=true;
				}
			}
		}

		if(trigger)
		{
			basenote=content_reader.Read(curpos, 1, 1);
			triggered_basenote=basenote;
			const int chn_env_vol=envelope_reader.Read(curpos, 1, 1);
			int numsounds=0;
			int sounds[SOUNDS_MAX];
			for(int i=0;i<SOUNDS_MAX;i++)
				if(channel->sounds[i])
					sounds[numsounds++]=i;

			char arp_scale[sizeof(NMAArpScale)];
			song->GetArpeggioScale(arp_scale, channel->arp_size, curpos, basenote, channel->arp_dir?-1:1);

			msp.PlaySounds(song, numsounds, sounds, basenote, chst, chn_env_vol, arp_scale);
			release=false;

			arpeggio_pos=0;
			arpeggio_delay=0;
			arpeggio_rate=channel->GetArpRate();

			notelength=0;
			holdnote=false;
			if(!next_gp.Empty() && next_gp.curve!=0xFF)
				holdnote=true;
			else if(channel->GetNoteLength()==0) // immediately release
				release=true;
			activenote=true;
		}

		curpos++;
		subpos=0;
	}

	void HandleRelease(bool force)
	{
		if(release || force)
		{
			msp.ReleaseSounds();
			activenote=false;
			release=false;
		}
	}

private:

	bool release;
};

#include "nmn_export.h"

struct NMASongPlayer
{
	int GetCurPos() const { return curpos; }
	double GetCurPosFloat() const { return curpos_float; }

	NMASongPlayer(NMASoundStack& stack) : _stack(stack)
	{
		constexpr unsigned size=128;
		players.reserve(size);
		for(unsigned i=0;i<size;i++)
			players.emplace_back(stack);
	}

	constexpr bool IsRecording() const
	{
		return _loop_stop>0;
	}

	void Stop()
	{
		for(int i=0;i<num_players;i++)
			players[i].Stop();
		num_players=0;

		playing=false;
		_loop_stop=0;
	}

	bool Play(NMSong* song, int* chsts, int numchannels, int start, int record_loops=0) // start==-1 means use song settings
	{
#if !HEADLESS
		AcquireAudioLock();
#endif

		Stop();

		_loop_count=0;
		_loop_stop=record_loops;
		_end_tick=0;

		tick=0;

//		glrLog("\n----song play---\n\n");
		const auto* part=song->part;

		if(start==-1)
			start=part->GetStartPos();
		curpos=start;
		if(curpos>=part->GetEndPos())
			curpos=part->GetLoopPos();

		// force UpdateStep() immediately at first Update()
		substep_counter=part->GetTempoRateSwing(curpos)-timeline_substeps;

		if(numchannels>0)
		{
			assert(chsts);
			num_players=0;
			for(int i=0;i<numchannels;i++)
				players[num_players++].Play(chsts[i], curpos);
		}
		else
		{
			num_players=0;
			// Enable all channels
			for(int i=0;i<100;i++)
				players[num_players++].Play(ChChst(i), curpos);
			for(int i=0;i<20;i++)
				players[num_players++].Play(StChst(i), curpos);
		}

#if !HEADLESS
		ReleaseAudioLock();
#endif

		if(num_players==0)
			return false;

		playing=true;
		return true;
	}

	void Tick(NMSong* song, NMAPU& apu, NMNSongCapture *cap)
	{
		apu.Silence();

		for(int i=0;i<num_players;i++)
			players[i].UpdateReaderSources(song);

		if(native_mode)
			TickNative(song, apu, cap);
		else
			TickNormal(song, apu);

		tick++;
	}

	void ExportNSF(NMSong* song, const char* filename)
	{
		int native_mode_memo=native_mode;
		native_mode=1;
		NMAPU apu;
		NMNSongData song_data(GetCapture(song, apu), *song);
		song_data.WriteNSF(filename);
		native_mode=native_mode_memo;
	}

	void ExportASM(NMSong* song, const char* filename)
	{
		int native_mode_memo=native_mode;
		native_mode=1;
		NMAPU apu;
		NMNSongData song_data(GetCapture(song, apu), *song);
		song_data.WriteASM(filename);
		native_mode=native_mode_memo;
	}

	void ExportWAV(NMSong* song, const char* filename);

private:
	std::vector<NMAChannelPlayer> players;
	int num_players=0;

	bool playing=false;

	int curpos=0;
	double curpos_float=0;
	int tick=0;
	int substep_counter=0;

	NMASoundStack& _stack;
	int _loop_count=0;
	int _loop_stop=0;
	int _end_tick=0;

	void Step(NMSong* song)
	{
		const auto* part=song->part;
		const int markstart=part->GetStartPos();
		const int markend=part->GetEndPos();
		const int markloop=part->GetLoopPos();
		const int end_pos=markloop>markstart?markloop:markstart;
		if(curpos==end_pos && IsRecording() && _loop_count>=_loop_stop)
		{
			_end_tick=tick;
			Stop();
		}

		for(int i=0;i<num_players;i++)
		{
			if(players[i].curpos!=curpos)
				players[i].Goto(curpos);
			players[i].Step(song);
		}

		curpos++;
		if(curpos>=markend)
		{
			_loop_count++;
			curpos=markloop;
		}
	}

	void TickStep(NMSong* song)
	{
		if(num_players==0)
			return;

		const int tempo_rate=song->part->GetTempoRateSwing(curpos);
		substep_counter+=timeline_substeps;
		while(substep_counter>=tempo_rate)
		{
			Step(song);
			substep_counter-=tempo_rate;
		}

		curpos_float=curpos+(double)substep_counter/tempo_rate;
	}

	void TickPlayers(NMSong* song)
	{
		if(num_players==0)
			return;

		const int subrate=(song->part->GetTempoRateSwing(curpos)+timeline_substeps-1)/timeline_substeps;
		for(int i=0;i<num_players;i++)
		{
			players[i].Update(song, subrate);
		}
	}

	inline void UpdateRelease()
	{
		if(num_players==0)
			return;

		for(int i=0;i<num_players;i++)
			players[i].HandleRelease(false);
	}

	void TickNative(NMSong* song, NMAPU& apu, NMNSongCapture *cap)
	{
		assert(native_mode);

		const int startpos=curpos;
		const int frame=tick/4;
		const int steps=((tick%4)==0)?4:0;
		const bool record=cap&&steps;

		if(steps)
			UpdateRelease();

		for(int i=0;i<steps;i++)
		{
			for(int si=0;si<_stack.soundstacksize;si++)
				_stack.soundplayers[_stack.soundstack[si]].Step(song);
		}

		for(int i=0;i<steps;i++)
		{
			TickStep(song);
			TickPlayers(song);
		}

		_stack.CleanStack();
		const int endpos=curpos;

		if(record) cap->BeginFrame();
		for(int si=0;si<_stack.soundstacksize;si++)
		{
			auto& sp=_stack.soundplayers[_stack.soundstack[si]];
			sp.PrepareOutput(song);
			if(record) cap->Capture(song, frame, curpos, sp);
		}
		if(record) cap->EndFrame(frame, startpos, endpos, *song->part);

		for(int si=0;si<_stack.soundstacksize;si++)
			_stack.soundplayers[_stack.soundstack[si]].Output(song, apu);
	}

	void TickNormal(NMSong* song, NMAPU& apu)
	{
		assert(!native_mode);

		TickStep(song);
		UpdateRelease();
		TickPlayers(song);

		_stack.CleanStack();

		for(int si=0;si<_stack.soundstacksize;si++)
		{
			auto& sp=_stack.soundplayers[_stack.soundstack[si]];
			sp.Step(song);
			sp.PrepareOutput(song);
			sp.Output(song, apu);
		}
	}

	std::vector<NMNSongCapture> GetCapture(NMSong* song, NMAPU& apu)
	{
		assert(native_mode);
		int song_part_memo=song->cur_part;

		std::vector<NMNSongCapture> caps;
		for(int i=0;i<PARTS_MAX;i++)
		{
			if(song->song_parts[i])
			{
				song->SelectSongPart(i);
				caps.emplace_back(*song->song_parts[i]);
				Play(song, NULL, 0, -1, 1);
				while(playing)
					Tick(song, apu, &caps.back());
			}
		}

		song->SelectSongPart(song_part_memo);
		return caps;
	}
};

void ClearAudioLights();

float master_volume=1.0f;

std::array<float, 4096> recent_mix;
unsigned recent_mix_i=0;

int write_wav(const char* filename, const short* samples, unsigned num_samples, unsigned rate)
{
#define W4(i_, v_) \
	buf[(i_)]=(v_)&255;\
	buf[(i_)+1]=((v_)>>8)&255;\
	buf[(i_)+2]=((v_)>>16)&255;\
	buf[(i_)+3]=((v_)>>24)&255

	char buf[]=
	"RIFF----WAVEfmt \x10\x00\x00\x00" // chunk size: set below
	"\x01\x00" // format: 1 (PCM)
	"\x01\x00" /// channels: 1 (mono)
	"----" // sample rate: set below
	"----" // = rate * channels * bits/8: set below
	"\x02\x00" // = channels * bits/8
	"\x10\x00" // bits: 16-bit samples
	"data----" // data size: set below
	;
	FILE *f;
	unsigned n=sizeof(short)*num_samples;

	n+=36; W4(4, n); // chunk size
	W4(24, rate); // sample rate
	rate*=2; W4(28, rate); // = rate * channels * bits/8
	n-=36; W4(40, n); // data size

	f=fopen(filename, "wb");
	if(f)
	{
		fwrite(buf, 44, 1, f);
		fwrite(samples, n, 1, f);
		fclose(f);
	}
	return !!f;
#undef W4
}

auto recent_mix_energy=LowPass<float>(7.5f/SAMPLE_RATE);

struct NMAOutputGenerator
{
	using OutputFunc=std::function<void(float)>;

	NMAOutputGenerator()
	{
		Reset();
	}

	void Reset()
	{
		_audioblock_i=_audioblock.size();
		SetVolume(1.0f);
	}

	void SetVolume(float volume)
	{
		_volume=1.7f*volume;
	}

	void Output(const OutputFunc& output_func, size_t num_samples, NMSong* song, NMASongPlayer& player, NMAPU& apu)
	{
		for(size_t i=0;i<num_samples;i++)
		{
			if(_audioblock_i==_audioblock.size())
			{
				player.Tick(song, apu, nullptr);
				apu.Output(_audioblock.data(), (int)_audioblock.size());
				_audioblock_i=0;
			}
			float s=_audioblock[_audioblock_i++]*_volume;
			s=std::max(-1.0f, std::min(1.0f, s));
			output_func(s);
		}
	}

private:
	std::array<float, audioblocksize> _audioblock={};
	size_t _audioblock_i;
	float _volume;
};

void NMASongPlayer::ExportWAV(NMSong* song, const char* filename)
{
	NMAPU apu;
	NMAOutputGenerator generator;
	std::vector<short> output;
	output.reserve(5*SAMPLE_RATE); // 5 s initial reserve

	const auto output_func=[&output](float value)
	{
		output.emplace_back((short)std::round(32767.0f*value));
	};

	Play(song, NULL, 0, -1, 1);
	while(_end_tick==0)
	{
		generator.Output(output_func, audioblocksize, song, *this, apu);
	}

	output.resize(_end_tick*audioblocksize);

	write_wav(filename, output.data(), (unsigned)output.size(), SAMPLE_RATE);
}

#if !HEADLESS

#define USE_NMAS

#ifdef USE_NMAS
#include "nmas.h"

void NMAudioBuffer(float* output, unsigned buffersize);
int NMAS_Callback(float* buffer, int buffersize)
{
	NMAudioBuffer(buffer, buffersize);
	return true;
}
#endif



#ifdef USE_PORTAUDIO

#include "pa/portaudio.h"
PortAudioStream *pastream;

static int paCallback(void *inputBuffer, void *outputBuffer,
			unsigned long framesPerBuffer, PaTimestamp outTime, void *userData)
{
	float *output=(float*)outputBuffer;
	float *input=(float*)inputBuffer;
	(void)outTime;

	NMAudioBuffer(output, (int)framesPerBuffer);

	return 0;
}

#endif

void InitAudio()
{
#ifdef USE_PORTAUDIO
	Pa_Initialize();
	Pa_OpenDefaultStream(
			&pastream,
			0,				// input channels
			2,				// output channels
			paFloat32,		// 32 bit floating point output
			SAMPLE_RATE,
			1024,			// frames per buffer
			0,				// number of buffers, if zero then use default minimum
			paCallback,
			NULL);
	Pa_StartStream(pastream);
#endif

#ifdef USE_NMAS
//	NMAS_Open(44100, 512, 0, true);
//	NMAS_Open(44100, 1500, 500, false);

	/*

	user settings:

	two rows for audio:
	driver: DSOUND, ASIO
	DSOUND buffer: 500...3000 (steps of 100)
	DSOUND timing: -100...+100 (x8, maybe not bidirectional since it would likely not make sense and require more code...)
	ASIO buffer: 256, 512, 1024, 2048 (use oversized value range and quantize to steps of 3, for broader mouse movement?)

	AUDIO DRIVER: DSOUND
	AUDIO BUFFER: 900  TIMING BIAS: 24

	AUDIO DRIVER: ASIO
	AUDIO BUFFER: 1024

	immediate refresh on mouse up, show error message if unable to init (or flash "ok" if accepted)

	explicit save to startup.txt with a button
	audio_driver 0
	audio_buffer 9
	audio_timing 24

	*/

#endif
}

void CloseAudio()
{
#ifdef USE_PORTAUDIO
	Pa_StopStream(pastream);
	Pa_CloseStream(pastream);
	Pa_Terminate();
#endif

#ifdef USE_NMAS
	NMAS_Close(true);
#endif
}

#endif // !HEADLESS

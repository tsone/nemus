#ifndef NMIDI_H
#define NMIDI_H
#ifdef __cplusplus
extern "C" {
#endif

typedef void (*NMIDIPacketHandler)(const unsigned char* data, unsigned size);

extern unsigned NMIDI_Init(unsigned idx, NMIDIPacketHandler handler);
extern void NMIDI_Exit(void);

#ifdef __cplusplus
}
#endif
#endif
